package com.vidhyasagar.fitcentive.parse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.vidhyasagar.fitcentive.activities.MainActivity;

/**
 * Created by vharihar on 2016-10-23.
 */
public class FitcentiveBroadcastReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushOpen(Context context, Intent intent) {
        Intent i = new Intent(context, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.PUSH_START);
        i.putExtras(intent.getExtras());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }
}
