package com.vidhyasagar.fitcentive.parse;

/**
 * Created by vharihar on 2016-10-31.
 */
public class Constants {

    //--------------------------
    /* APP CONFIG */
    //--------------------------

    public static String appId = "fitcentive1210140029240";
    public static String serverUrl = "http://ec2-52-55-241-177.compute-1.amazonaws.com/parse/";

    //---------------------------
    /* COMMON CONSTANTS */
    //---------------------------
    public static String objectId = "objectId";
    public static String createdAt = "createdAt";

    //----------------------------
    /* CONSTANTS FOR USER CLASS*/
    //----------------------------

    public static String User = "User";
    public static String username = "username";
    public static String password = "password";
    public static String email = "email";
    public static String isMale = "isMale";
    public static String firstname = "firstname";
    public static String lastname = "lastname";
    public static String birthdayDay = "birthdayDay";
    public static String birthdayMonth = "birthdayMonth";
    public static String birthdayYear = "birthdayYear";
    public static String profilePicture = "profilePicture";
    public static String coverPicture = "coverPicture";
    public static String profileTagline = "profileTagline";
    public static String city = "city";
    public static String country = "country";
    public static String isPublic = "isPublic";

    //----------------------------
    /* CONSTANTS FOR COMMENT CLASS*/
    //----------------------------

    public static String Comment = "Comment";
    public static String postId = "postId";
    public static String comment = "comment";

    //----------------------------
    /* CONSTANTS FOR FOLLOW CLASS*/
    //----------------------------


    public static String Follow = "Follow";
    public static String following = "following";
    public static String followers = "followers";
    public static String numFollowing = "numFollowing";
    public static String numFollowers = "numFollowers";

    //----------------------------
    /* CONSTANTS FOR NOTIFICATION CLASS*/
    //----------------------------

    public static String Notification = "Notification";
    public static String image = "image";
    public static String type = "type";
    public static String receiverUsername = "receiverUsername";
    public static String providerUsername = "providerUsername";
    public static String isActive = "isActive";
    public static String changedCaption = "changedCaption";

    //----------------------------
    /* CONSTANTS FOR POST CLASS*/
    //----------------------------

    public static String Post = "Post";
    public static String caption = "caption";
    public static String numberComments = "numberComments";
    public static String numberLikes = "numberLikes";
    public static String likers = "likers";

    //----------------------------
    /* CONSTANTS FOR CREATEDFOOD CLASS*/
    //----------------------------

    public static String CreatedFood = "CreatedFood";
    public static String foodNameField = "foodName";
    public static String foodBrand = "foodBrand";
    public static String foodDescriptionField = "foodDescription";
    public static String servingSizeField = "servingSize";
    public static String servingSizeUnit = "servingSizeUnit";
    public static String caloriesField = "calories";
    public static String proteinsField = "proteins";
    public static String fatsField = "fats";
    public static String carbsField = "carbs";
    public static String cholesterolField = "cholesterol";
    public static String polyFats = "polyFats";
    public static String transFats = "transFats";
    public static String satFats = "satFats";
    public static String monoFats = "monoFats";
    public static String vitaminA = "vitaminA";
    public static String vitaminC = "vitaminC";

    //----------------------------
    /* CONSTANTS FOR FOODDIARY CLASS*/
    //----------------------------

    public static String FoodDiary = "FoodDiary";
    public static String mealType = "mealType";
    public static String foodIdNumber = "foodId";
    public static String isPermanent = "isPermanent";
    public static String numberServings = "numberServings";
    public static String servingIdField = "servingId";
    public static String isFavorite = "isFavorite";
    public static String entryType = "entryType";
    public static String mealObjectId = "mealObjectId";
    public static String daysSinceEpoch = "daysSinceEpoch";
    public static String foodEntryIdField = "foodEntryId";
    public static String createdFoodObjectId = "createdFoodObjectId";


    //----------------------------
    /* CONSTANTS FOR EXERCISEDIARY CLASS*/
    //----------------------------

    public static String ExerciseDiary = "ExerciseDiary";
    public static String exerciseName = "exerciseName";
    public static String exerciseType = "exerciseType";
    public static String exerciseId = "exerciseId";
    public static String numberOfSets = "numberOfSets";
    public static String repsPerSet = "repsPerSet";
    public static String weightPerSet = "weightPerSet";
    public static String weightUnit = "weightUnit";
    public static String minutesPerformed = "minutesPerformed";
    public static String caloriesBurned = "caloriesBurned";

    //----------------------------
    /* CONSTANTS FOR CREATEDEXERCISE CLASS*/
    //----------------------------

    public static String CreatedExercise = "CreatedExercise";
    public static String exerciseDescription = "exerciseDescription";
    public static String isCardio = "isCardio";
    public static String metValue = "metValue";
    public static String equipmentList = "equipmentList";
    public static String primaryMusclesList = "primaryMusclesList";
    public static String secondaryMusclesList = "secondaryMusclesList";
    public static String isCreated = "isCreated";
    public static String createdExerciseObjectId = "createdExerciseObjectId";

    //----------------------------
    /* CONSTANTS FOR CREATEDWORKOUT CLASS*/
    //----------------------------

    public static String CreatedWorkout = "CreatedWorkout";
    public static String workoutName = "workoutName";
    public static String workoutDays = "workoutDays";

    //----------------------------
    /* CONSTANTS FOR CREATED_WORKOUT_CARDIO_COMPONENT CLASS*/
    //----------------------------
    public static String CreatedWorkoutCardioComponent = "CreatedWorkoutCardioComponent";

    //----------------------------
    /* CONSTANTS FOR CREATED_WORKOUT_STRENGTH_COMPONENT CLASS*/
    //----------------------------

    public static String CreatedWorkoutStrengthComponent = "CreatedWorkoutStrengthComponent";
    public static String workoutId = "workoutId";


    //----------------------------
    /* CONSTANTS FOR DIARY CLASS*/
    //----------------------------

    public static String Fatsecret = "FatSecret";
    public static String userToken = "userToken";
    public static String userSecret = "userSecret";

    //----------------------------
    /* CONSTANTS FOR CREATEDMEAL CLASS*/
    //----------------------------

    public static String CreatedMeal = "CreatedMeal";
    public static String mealName = "mealName";
    public static String foodIds = "foodIds";
    public static String foodServingIds = "foodServingIds";
    public static String foodNumberOfServings = "foodNumberOfServings";
    public static String recipeIds = "recipeIds";
    public static String recipeServingOptions = "recipeServingOptions";
    public static String recipeNumberOfServings = "recipeNumberOfServings";

    //----------------------------
    /* CONSTANTS FOR WATERDIARY CLASS*/
    //----------------------------

    public static String WaterDiary = "WaterDiary";
    public static String cupsOfWater = "cupsOfWater";

    //----------------------------
    /* FATSECRET API KEYS */
    //----------------------------

    public static String RestApiConsumerKey = "7e24d25842ce4acfa5fa5877319200d7";
    public static String RestApiSharedSecret = "d69cd14676b84a8bbfe4e36818207e34";
    public static String userOauthToken = null;
    public static String userOauthSecret = null;

    //-------------------------------
    /* MAIN ACTIVITY START METHODS */
    //-------------------------------

    public static String START_METHOD = "START_METHOD";
    public static String PUSH_START = "PUSH_START";
    public static String NORMAL_START = "NORMAL_START";
    public static String RETURN_FROM_SEARCH_FOOD = "RETURN_FROM_SEARCH_FOOD";

    //-------------------------------
    /* MISCELLANEOUS EXTRAS KEYS  */
    //-------------------------------

    public static String  DAY_OFFSET = "DAY_OFFSET";
    public static String TOTAL_CALORIES_AMOUNT = "TOTAL_CALORIES_AMOUNT";
    public static String returnDate = "returnDate";
    public static String FOOD_OBJECT = "FOOD_OBJECT";
    public static int defaultTargetCalories = 1500;

    //-------------------------------
    /* FATSECRET OBJECT RETRIEVAL KEYS */
    //-------------------------------

    public static String unknown = "Unknown";
    public static String noUrlFound = "No Url Found";
    public static String success = "success";
    public static String food = "food";
    public static String profile = "profile";
    public static String recipe = "recipe";
    public static String recipes = "recipes";
    public static String foods = "foods";
    public static String brandName = "brand_name";
    public static String foodDescription = "food_description";
    public static String foodId = "food_id";
    public static String foodName = "food_name";
    public static String foodType = "food_type";
    public static String foodUrl = "food_url";
    public static String foodEntryId = "food_entry_id";
    public static String value = "value";
    public static String placeholderName = "food_place_holder_name";
    public static String other = "other";
    public static String suggestions = "suggestions";
    public static String suggestion = "suggestion";
    public static String auth_secret = "auth_secret";
    public static String auth_token = "auth_token";


    // Serving/nutritional info retrieval
    public static String servings = "servings";
    public static String serving = "serving";
    public static String servingId = "serving_id";
    public static String servingUrl = "serving_url";
    public static String servingDescription = "serving_description";
    public static String metricServingAmount = "metric_serving_amount";
    public static String metricServingUnit = "metric_serving_unit";
    public static String measurementDescription = "measurement_description";
    public static String numberOfUnits = "number_of_units";
    public static String calories = "calories";
    public static String carbs = "carbohydrate";
    public static String protein = "protein";
    public static String fat = "fat";
    public static String saturatedFat = "saturated_fat";
    public static String polyUnsaturatedFat = "polyunsaturated_fat";
    public static String monoUnsaturatedFat = "monounsaturated_fat";
    public static String transFat = "trans_fat";
    public static String cholestrol = "cholestrol";
    public static String sodium = "sodium";
    public static String potassium = "potassium";
    public static String fiber = "fiber";
    public static String sugar = "sugar";
    public static String vitamin_a = "vitamin_a";
    public static String vitamin_c = "vitamin_c";
    public static String calcium = "calcium";
    public static String iron = "iron";

    // Recipe info retrieval
    public static String recipeId = "recipe_id";
    public static String recipeIdField = "recipeId";
    public static String recipeServingOption = "recipeServingOption";
    public static String recipeName = "recipe_name";
    public static String recipeUrl = "recipe_url";
    public static String recipeDescription = "recipe_description";
    public static String recipeImage = "recipe_image";
    public static String cookingTime = "cooking_time_min";
    public static String prepTime = "preparation_time_min";
    public static String numberOfServings = "number_of_servings";
    public static String rating = "rating";
    public static String recipeType = "recipe_type";
    public static String recipeCategoryName = "recipe_category_name";
    public static String recipeCategoryUrl = "recipe_category_url";
    public static String servingSize = "serving_size";
    public static String directions = "directions";
    public static String direction = "direction";
    public static String directionNumber = "direction_number";
    public static String directionDescription = "direction_description";
    public static String ingredients = "ingredients";
    public static String ingredient = "ingredient";
    public static String ingredientDescription = "ingredient_description";
    public static String ingredientUrl = "ingredient";
    public static String servingSizes = "serving_sizes";


    //-------------------------------
    /* WGER ENDPOINTS */
    //-------------------------------

    public static String wgerBaseUrl = "https://wger.de/api/v2/";
    public static String results = "results";
    public static String count = "count";
    public static String name = "name";
    public static String description = "description";
    public static String id = "id";
    public static String originalName = "name_original";
    public static String category = "category";
    public static String muscles = "muscles";
    public static String secondaryMuscles = "muscles_secondary";
    public static String equipment = "equipment";
    public static String next = "next";
    public static String nullString = "null";

    //-------------------------------
    /* BARCODE FORMATS */
    //-------------------------------

    public static String upc_a = "UPC_A";
    public static String upc_e = "UPC_E";
    public static String ean_13 = "EAN_13";
    public static String ean_8 = "EAN_8";

}
