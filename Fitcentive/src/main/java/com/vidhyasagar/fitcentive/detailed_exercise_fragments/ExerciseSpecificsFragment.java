package com.vidhyasagar.fitcentive.detailed_exercise_fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseObject;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseSpecificsFragment extends Fragment {


    public static ExerciseSpecificsFragment newInstance(int dayOffset, ExerciseInfo info, boolean isEditMode) {
        ExerciseSpecificsFragment fragment = new ExerciseSpecificsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putParcelable(ExpandedExerciseActivity.INFO, info);
        args.putBoolean(ExpandedExerciseActivity.IS_EDITING, isEditMode);
        fragment.setArguments(args);
        return fragment;
    }

    public ExerciseSpecificsFragment() {
        // Required empty public constructor
    }

    int dayOffset;
    ExerciseInfo info;
    boolean isEditMode;

    TextView exerciseName;
    EditText numberOfSets, repsPerSet, weightPerSet, weightUnit;
    Button viewDetails;
    RelativeLayout mainLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_specifics, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpOnScreenData();
        setUpMainLayoutKeyboardHideListener();
        setUpViewDetailsButton();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        info = getArguments().getParcelable(ExpandedExerciseActivity.INFO);
        isEditMode = getArguments().getBoolean(ExpandedExerciseActivity.IS_EDITING);
    }

    private void bindViews(View v) {
        numberOfSets = (EditText) v.findViewById(R.id.number_of_sets_edittext);
        repsPerSet = (EditText) v.findViewById(R.id.reps_per_set_edittext);
        weightPerSet = (EditText) v.findViewById(R.id.weight_edittext);
        weightUnit = (EditText) v.findViewById(R.id.weight_unit_edittext);
        viewDetails = (Button) v.findViewById(R.id.view_details_button);
        exerciseName = (TextView) v.findViewById(R.id.exercise_name);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
    }

    private void setUpOnScreenData() {
        exerciseName.setText(info.getName());

        if(isEditMode) {
            numberOfSets.setText(String.valueOf(info.getNumberOfSets()));
            numberOfSets.setSelection(numberOfSets.getText().length());
            repsPerSet.setText(String.valueOf(info.getRepsPerSet()));

            if(!info.getWeightUnit().equals(Constants.unknown)) {
                weightUnit.setText(info.getWeightUnit());
            }

            if(info.getWeightPerSet() != -1) {
                weightPerSet.setText(String.valueOf(info.getWeightPerSet()));
            }
        }
    }

    private void setUpViewDetailsButton() {
        viewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExpandedExerciseActivity) getActivity()).goToNextFragment();
            }
        });
    }

    private void setUpMainLayoutKeyboardHideListener() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void handleErrors() {
        if(numberOfSets.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.number_of_sets_required));
            numberOfSets.requestFocus();
        }
        else if(repsPerSet.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.reps_per_set_required));
            repsPerSet.requestFocus();
        }
    }

    private void handleOptionalErrors() {
        if(weightUnit.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.unit_required_if_weight_set));
            weightUnit.requestFocus();
        }
        else if(weightPerSet.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.weight_required_if_unit_set));
            weightPerSet.requestFocus();
        }
    }

    public boolean isDataValidForSave() {
        if(!numberOfSets.getText().toString().isEmpty() && !repsPerSet.getText().toString().isEmpty()) {
            // If both weight and unit are empty, OR BOTH weight and unit are filled, data is valid
            if((weightPerSet.getText().toString().isEmpty() && weightUnit.getText().toString().isEmpty())
                    || (!weightUnit.getText().toString().isEmpty() && !weightPerSet.getText().toString().isEmpty())) {
                return true;
            }
            else { //If either one is filled and the other isn't, data is not valid then
                handleOptionalErrors();
                return false;
            }
        }
        else {
            handleErrors();
            return false;
        }
    }

    public void sendDataToParent() {
        ((ExpandedExerciseActivity) getActivity()).
                setRequiredFields(Integer.valueOf(numberOfSets.getText().toString()), Double.valueOf(repsPerSet.getText().toString()));

        if(!weightUnit.getText().toString().isEmpty() && !weightPerSet.getText().toString().isEmpty()) {
            ((ExpandedExerciseActivity) getActivity()).
                    setOptionalFields(Double.valueOf(weightPerSet.getText().toString()), weightUnit.getText().toString());
        }
    }

    private void showSnackBar(String message) {
        Snackbar snackbar =  Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }
}
