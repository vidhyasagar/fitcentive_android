package com.vidhyasagar.fitcentive.detailed_exercise_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.adapters.ExerciseImageAdapter;
import com.vidhyasagar.fitcentive.api_requests.wger.ExerciseRequest;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseDetailsFragment extends Fragment {


    public static ExerciseDetailsFragment newInstance(int dayOffset, ExerciseInfo info) {
        ExerciseDetailsFragment fragment = new ExerciseDetailsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putParcelable(ExpandedExerciseActivity.INFO, info);
        fragment.setArguments(args);
        return fragment;
    }

    public ExerciseDetailsFragment() {
        // Required empty public constructor
    }

    int dayOffset;
    ExerciseInfo info;
    ArrayList<Drawable> exerciseImageDrawables;
    ExerciseRequest exerciseRequest;
    String exCategory;
    ArrayList<String> primaryMusclesList;
    ArrayList<String> secondaryMusclesList;
    ArrayList<String> equipmentList;

    ExerciseImageAdapter imageAdapter;
    ArrayAdapter<String> equipmentAdapter, primaryMusclesAdapter, secondaryMusclesAdapter;
    ExpandableHeightListView exerciseImageList, equipment, primaryMuscles, secondaryMuscles;
    TextView exerciseName, exerciseCategory;
    WebView webView;
    RelativeLayout mainLayout;
    ScrollView scrollView;
    ProgressBar progressBar;
    View lineBreak;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_details, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpOnScreenData();
        setUpRelativeLayoutKeyboardBehavior();
        setUpListView();

        new FetchExerciseMetaData().execute();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        info = getArguments().getParcelable(ExpandedExerciseActivity.INFO);
    }

    private void bindViews(View v) {
        exerciseName = (TextView) v.findViewById(R.id.exercise_name);
        webView = (WebView) v.findViewById(R.id.web_view);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        scrollView = (ScrollView) v.findViewById(R.id.scrollview);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        lineBreak = v.findViewById(R.id.line_break_2);
        equipment = (ExpandableHeightListView) v.findViewById(R.id.equipment_listview);
        primaryMuscles = (ExpandableHeightListView) v.findViewById(R.id.primary_muscles_listview);
        secondaryMuscles = (ExpandableHeightListView) v.findViewById(R.id.secondary_muscles_listview);
        exerciseImageList = (ExpandableHeightListView) v.findViewById(R.id.exercise_images_list);
        exerciseCategory = (TextView) v.findViewById(R.id.category);
        exerciseRequest = new ExerciseRequest();
    }

    private void setUpListView() {
        exerciseImageDrawables = new ArrayList<>();
        primaryMusclesList = new ArrayList<>();
        secondaryMusclesList = new ArrayList<>();
        equipmentList = new ArrayList<>();

        imageAdapter = new ExerciseImageAdapter(getContext(), -1, exerciseImageDrawables);
        exerciseImageList.setAdapter(imageAdapter);
        exerciseImageList.setExpanded(true);

        equipmentAdapter = new ArrayAdapter<>(getContext(), R.layout.layout_exercise_listview_textview, equipmentList);
        equipment.setAdapter(equipmentAdapter);
        equipment.setExpanded(true);

        primaryMusclesAdapter = new ArrayAdapter<>(getContext(), R.layout.layout_exercise_listview_textview, primaryMusclesList);
        primaryMuscles.setAdapter(primaryMusclesAdapter);
        primaryMuscles.setExpanded(true);

        secondaryMusclesAdapter = new ArrayAdapter<>(getContext(), R.layout.layout_exercise_listview_textview, secondaryMusclesList);
        secondaryMuscles.setAdapter(secondaryMusclesAdapter);
        secondaryMuscles.setExpanded(true);
    }

    private void setUpOnScreenData() {
        exerciseName.setText(info.getName());
        webView.loadData(info.getDescription(), "text/html; charset=utf-8", "UTF-8");
        webView.setBackgroundColor(Color.TRANSPARENT);
    }

    private void setUpRelativeLayoutKeyboardBehavior() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private void modifyOnScreenData() {
        if(exerciseImageDrawables != null && !exerciseImageDrawables.isEmpty()) {
            imageAdapter.notifyDataSetChanged();
        }
        else {
            exerciseImageList.setVisibility(View.GONE);
            lineBreak.setVisibility(View.GONE);
        }

        if(exCategory == null) {
            exerciseCategory.setText(Constants.unknown);
        }
        else {
            exerciseCategory.setText(exCategory);
        }

        equipmentAdapter.notifyDataSetChanged();
        primaryMusclesAdapter.notifyDataSetChanged();
        secondaryMusclesAdapter.notifyDataSetChanged();

    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void loadImageFromWebOperations() throws Exception{
        ArrayList<String> urls = exerciseRequest.wgerGetExerciseImageUrls(info.getId());
        if(urls != null && !urls.isEmpty()) {
            for(String url : urls) {
                if (!url.equals(Constants.unknown)) {
                    InputStream is = (InputStream) new URL(url).getContent();
                    exerciseImageDrawables.add(Drawable.createFromStream(is, "src name"));
                }
            }
        }
    }

    //--------------------------------------------------------------------------------------------
    // WGER SPECIFIC: Keep on the lookout for API changes and incorporate
    //--------------------------------------------------------------------------------------------
    private void getExerciseCategory() {
        if(info.getCategory() == 8) {
            exCategory = getString(R.string.arms);
        }
        else if(info.getCategory() == 9) {
            exCategory = getString(R.string.legs);
        }
        else if(info.getCategory() == 10) {
            exCategory = getString(R.string.abs);
        }
        else if(info.getCategory() == 11) {
            exCategory = getString(R.string.chest);
        }
        else if(info.getCategory() == 12) {
            exCategory = getString(R.string.back);
        }
        else if(info.getCategory() == 13) {
            exCategory = getString(R.string.shoulders);
        }
        else if(info.getCategory() == 14) {
            exCategory = getString(R.string.calves);
        }
        else {
            exCategory = Constants.unknown;
        }
    }

    private void getMuscles(boolean isPrimary, ArrayList<String> musclesList) {
        ArrayList<Long> list;
        if(isPrimary) {
            list = info.getPrimaryMuscles();
        }
        else {
            list = info.getSecondaryMuscles();
        }

        for(Long id : list) {
            if(id == 1) {
                musclesList.add(getString(R.string.biceps_brachii));
            }
            if(id == 2) {
                musclesList.add(getString(R.string.anterior_deltoid));
            }
            if(id == 3) {
                musclesList.add(getString(R.string.serratus_anterior));
            }
            if(id == 4) {
                musclesList.add(getString(R.string.pectoralis_major));
            }
            if(id == 5) {
                musclesList.add(getString(R.string.tricpes_brachii));
            }
            if(id == 6) {
                musclesList.add(getString(R.string.rectus_abdominis));
            }
            if(id == 7) {
                musclesList.add(getString(R.string.gastrocnemius));
            }
            if(id == 8) {
                musclesList.add(getString(R.string.gluteus_maximus));
            }
            if(id == 9) {
                musclesList.add(getString(R.string.trapezius));
            }
            if(id == 10) {
                musclesList.add(getString(R.string.quadriceps_femoris));
            }
            if(id == 11) {
                musclesList.add(getString(R.string.biceps_femoris));
            }
            if(id == 12) {
                musclesList.add(getString(R.string.latissimus_dorsi));
            }
            if(id == 13) {
                musclesList.add(getString(R.string.brachialis));
            }
            if(id == 14) {
                musclesList.add(getString(R.string.obliquus));
            }
            if(id == 15) {
                musclesList.add(getString(R.string.soleus));
            }
        }

        if (musclesList.isEmpty()) {
            musclesList.add(Constants.unknown);
        }
    }

    private void getEquipment() {
        ArrayList<Long> list = info.getEquipment();
        for(Long id : list) {
            if(id == 1) {
                equipmentList.add(getString(R.string.barbell));
            }
            if(id == 2) {
                equipmentList.add(getString(R.string.sz_bar));
            }
            if(id == 3) {
                equipmentList.add(getString(R.string.dumbell));
            }
            if(id == 4) {
                equipmentList.add(getString(R.string.gym_mat));
            }
            if(id == 5) {
                equipmentList.add(getString(R.string.swiss_ball));
            }
            if(id == 6) {
                equipmentList.add(getString(R.string.pull_up_bar));
            }
            if(id == 7) {
                equipmentList.add(getString(R.string.none));
            }
            if(id == 8) {
                equipmentList.add(getString(R.string.bench));
            }
            if(id == 9) {
                equipmentList.add(getString(R.string.incline_bench));
            }
            if(id == 10) {
                equipmentList.add(getString(R.string.kettlebell));
            }
        }

        if(equipmentList.isEmpty()) {
            equipmentList.add(Constants.unknown);
        }
    }

    //--------------------------------------------------------------------------------------------
    // TODO: Routine maintenance to check values match or just use API calls instead
    //--------------------------------------------------------------------------------------------


    private class FetchExerciseMetaData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(scrollView, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadImageFromWebOperations();
                getExerciseCategory();
                getMuscles(true, primaryMusclesList);
                getMuscles(false, secondaryMusclesList);
                getEquipment();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(scrollView, true, true);
                    modifyOnScreenData();
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

}
