package com.vidhyasagar.fitcentive.api_requests.fatsecret;

import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.vidhyasagar.fitcentive.parse.Constants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by vharihar on 2016-11-04.
 */
public class FatSecretMeals {

    final static private String APP_METHOD = "GET";
    final static private String APP_KEY = Constants.RestApiConsumerKey;
    final static private String APP_SECRET = Constants.RestApiSharedSecret;
    final static private String APP_URL = "http://platform.fatsecret.com/rest/server.api";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";


    //----------------------------------------------------------------------------
    // saved_meal.create request
    //----------------------------------------------------------------------------

    public long createMeal(String mealName, String mealDesc, String applicableMeals) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=saved_meal.create");
        params.add("saved_meal_name=" + mealName);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));
        if(mealDesc != null) {
            params.add("saved_meal_description=" + mealDesc);
        }
        if(applicableMeals != null) {
            params.add("meals=" + applicableMeals);
        }

        JSONObject meal = null;
        long mealId = -1;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject("saved_meal_id");
            mealId = meal.getLong("value");
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return mealId;
    }

    //----------------------------------------------------------------------------
    // saved_meal.delete request
    //----------------------------------------------------------------------------

    public boolean deleteMeal(long mealId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=saved_meal.delete");
        params.add("saved_meal_id=" + mealId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));

        JSONObject meal = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }

    //----------------------------------------------------------------------------
    // saved_meal.edit request
    //----------------------------------------------------------------------------

    public boolean editMeal(long mealId, String mealName, String mealDesc, String applicableMeals) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=saved_meal.create");
        params.add("saved_meal_id=" + mealId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));
        if(mealName != null) {
            params.add("saved_meal_name=" + mealName);
        }
        if(mealDesc != null) {
            params.add("saved_meal_description=" + mealDesc);
        }
        if(applicableMeals != null) {
            params.add("meals=" + applicableMeals);
        }

        JSONObject meal = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // saved_meals.get request
    //----------------------------------------------------------------------------

    public JSONObject getMeals(String meal) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=saved_meals.get");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));
        if(meal != null) {
            params.add("meal=" + meal);
        }

        JSONObject retrievedMeals = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            Log.i("getmeals", mealGet.toString());
            retrievedMeals = mealGet.getJSONObject("saved_meals");
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return retrievedMeals;
    }


    //----------------------------------------------------------------------------
    // saved_meal_item.add request
    //----------------------------------------------------------------------------

    public long addSavedMealItem(long mealId, long foodId, String foodName, long servingId, float numUnits) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("saved_meal_id=" + mealId);
        params.add("food_id=" + foodId);
        params.add("food_entry_name=" + foodName);
        params.add("serving_id=" + servingId);
        params.add("number_of_units=" + numUnits);
        params.add("method=saved_meal_item.add");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));

        JSONObject meal = null;
        long savedMealItemId = -1;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject("saved_meal_item_id");
            savedMealItemId = meal.getLong("value");
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return savedMealItemId;
    }


    //----------------------------------------------------------------------------
    // saved_meal_item.add request
    //----------------------------------------------------------------------------

    public boolean deleteSavedMealItem(long savedMealItemId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("saved_meal_item_id=" + savedMealItemId);
        params.add("method=saved_meal_item.delete");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));

        JSONObject meal = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // saved_meal_item.edit request
    //----------------------------------------------------------------------------

    public boolean editSavedMealItem(long savedMealItemId, String savedMealItemName, float numUnits) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("saved_meal_item_id=" + savedMealItemId);
        params.add("method=saved_meal_item.edit");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));
        if(savedMealItemName != null) {
            params.add("saved_meal_item_name=" + savedMealItemName);
        }
        if(numUnits != -1) {
            params.add("number_of_units=" + numUnits);
        }

        JSONObject meal = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            meal = mealGet.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // saved_meal_items.get request
    //----------------------------------------------------------------------------

    public JSONObject getSavedMealItems(long savedMealItemId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("saved_meal_item_id=" + savedMealItemId);
        params.add("method=saved_meal_item.delete");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template)));

        JSONObject savedMealItems = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject mealGet = new JSONObject(builder.toString());
            savedMealItems = mealGet.getJSONObject("saved_meal_items");
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return savedMealItems;
    }




    //----------------------------------------------------------------------------
    // COMMON HELPERS
    //----------------------------------------------------------------------------

    private static String[] generateAuthParams() {
        return new String[]{
                "oauth_consumer_key=" + APP_KEY,        // Your API key when you registered as a developer
                "oauth_signature_method=HMAC-SHA1",     //The method used to generate the signature (only HMAC-SHA1 is supported)
                "oauth_timestamp=" +                    //The date and time, expressed in the number of seconds since January 1, 1970 00:00:00 GMT.
                        Long.valueOf(System.currentTimeMillis() * 2).toString(), // Should be  Long.valueOf(System.currentTimeMillis() / 1000).toString()
                "oauth_nonce=" + nonce(),               // A randomly generated string for a request that can be combined with the timestamp to produce a unique value
                "oauth_version=1.0",                    // MUST be "1.0"
                "format=json"};                        // The desired response format. Valid reponse formats are "xml" or "json" (default value is "xml").
    }

    private static String sign(String method, String uri, String[] params) {
        String[] p = {method, Uri.encode(uri), Uri.encode(paramify(params))};
        String s = join(p, "&");
        String key = APP_SECRET + "&" + Constants.userOauthSecret;
        SecretKey sk = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        try {
            Mac m = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            m.init(sk);
            return Uri.encode(new String(Base64.encode(m.doFinal(s.getBytes()), Base64.DEFAULT)).trim());
        } catch (java.security.NoSuchAlgorithmException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        } catch (java.security.InvalidKeyException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        }
    }

    private static String paramify(String[] params) {
        String[] p = Arrays.copyOf(params, params.length);
        Arrays.sort(p);
        return join(p, "&");
    }

    private static String join(String[] array, String separator) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0)
                b.append(separator);
            b.append(array[i]);
        }
        return b.toString();
    }

    private static String nonce() {
        Random r = new Random();
        StringBuilder n = new StringBuilder();
        for (int i = 0; i < r.nextInt(8) + 2; i++)
            n.append(r.nextInt(26) + 'a');
        return n.toString();
    }
}
