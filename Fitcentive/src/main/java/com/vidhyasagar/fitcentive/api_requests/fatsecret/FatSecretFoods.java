package com.vidhyasagar.fitcentive.api_requests.fatsecret;

import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.vidhyasagar.fitcentive.parse.Constants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class FatSecretFoods {
    /**
     * FatSecret Authentication
     * http://platform.fatsecret.com/api/default.aspx?screen=rapiauth
     * Reference
     * https://github.com/ethan-james/cookbox/blob/master/src/com/vitaminc4/cookbox/FatSecret.java
     */
    final static private String APP_METHOD = "GET";
    final static private String APP_KEY = Constants.RestApiConsumerKey;
    final static private String APP_SECRET = Constants.RestApiSharedSecret;
    final static private String APP_URL = "http://platform.fatsecret.com/rest/server.api";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private static int MAX_RESULTS = 50;

    //----------------------------------------------------------------------------

    private String cleanUp(String string) {
        return string.replace(" ", "%20");
    }

    //----------------------------------------------------------------------------
    // food.find_id_for_barcode request
    //----------------------------------------------------------------------------

    public long getFoodIdFromBarcode(String barcode) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=food.find_id_for_barcode");
        params.add("barcode=" + barcode);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        long value = -1;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            Log.i("barcodelog", foodGet.toString());
            value = foodGet.getJSONObject(Constants.foodId).getLong(Constants.value);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return value;
    }

    //----------------------------------------------------------------------------
    // foods.autocomplete request
    //----------------------------------------------------------------------------

    public JSONObject autoComplete(String exp, int maxResults) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=foods.autocomplete");
        params.add("expression=" + cleanUp(exp));
        params.add("max_results=" + maxResults);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), false));
        JSONObject suggestions = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            Log.i("autocompletelog", foodGet.toString());
            suggestions = foodGet.getJSONObject(Constants.suggestions);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return suggestions;
    }


    //----------------------------------------------------------------------------
    // food.get request
    //----------------------------------------------------------------------------

    public JSONObject getFood(Long foodId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=food.get");
        params.add("food_id=" + foodId);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), false));
        JSONObject food = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            Log.i("debuglog", foodGet.toString());
            food = foodGet.getJSONObject(Constants.food);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return food;
    }

    //----------------------------------------------------------------------------
    // foods.search request
    //----------------------------------------------------------------------------

    public JSONObject searchFoods(String searchFood, int page) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("page_number=" + page);                     // The zero-based offset into the results for the query. Use this parameter with max_results to request successive pages of search results (default value is 0).
        params.add("max_results=" + MAX_RESULTS);              // The maximum number of results to return (default value is 20). This number cannot be greater than 50.
        params.add("method=foods.search");
        params.add("search_expression=" + Uri.encode(searchFood));
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), false));

        JSONObject foods = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject food = new JSONObject(builder.toString());   // { first
            Log.i("searchfoods", food.toString());
            foods = food.getJSONObject(Constants.foods);                    // { second
        } catch (Exception exception) {
            Log.e("FatSecret Error", exception.toString());
            exception.printStackTrace();
        }
        return foods;
    }

    //----------------------------------------------------------------------------
    // food.add_favorite request
    //----------------------------------------------------------------------------
    // TODO : Add support for servings and number of units
    public boolean addFavoriteFood(Long foodId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=food.add_favorite");
        params.add("food_id=" + foodId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject response = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject result = new JSONObject(builder.toString());
//            Log.i("addfavoritefood", result.toString());
            response = result.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // food.delete_favorite request
    //----------------------------------------------------------------------------
    // TODO : Add support for servings and number of units
    public boolean deleteFavoriteFood(Long foodId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=food.delete_favorite");
        params.add("food_id=" + foodId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject response = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject result = new JSONObject(builder.toString());
//            Log.i("removefavoritefood", result.toString());
            response = result.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // foods.get_favorites request
    //----------------------------------------------------------------------------
    public JSONObject getFavoriteFoods() {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=foods.get_favorites");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject foods = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            foods = foodGet.getJSONObject(Constants.foods);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return foods;
    }


    //----------------------------------------------------------------------------
    // foods.get_most_eaten request
    //----------------------------------------------------------------------------
    public JSONObject getMostEaten(String meal) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        if(meal != null) {
            params.add("meal=" + meal);
        }
        params.add("method=foods.get_most_eaten");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject foods = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            Log.i("mosteatenlog", foodGet.toString());
            foods = foodGet.getJSONObject(Constants.foods);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return foods;
    }


    //----------------------------------------------------------------------------
    // foods.get_recently_eaten request
    //----------------------------------------------------------------------------
    public JSONObject getRecentlyEaten(String meal) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        if(meal != null) {
            params.add("meal=" + meal);
        }
        params.add("method=foods.get_recently_eaten");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject foods = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject foodGet = new JSONObject(builder.toString());
            Log.i("recentlyeatenlog", foodGet.toString());
            foods = foodGet.getJSONObject(Constants.foods);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return foods;
    }





    //----------------------------------------------------------------------------
    // COMMON HELPERS
    //----------------------------------------------------------------------------

    private static String[] generateAuthParams() {
        return new String[]{
                "oauth_consumer_key=" + APP_KEY,        // Your API key when you registered as a developer
                "oauth_signature_method=HMAC-SHA1",     //The method used to generate the signature (only HMAC-SHA1 is supported)
                "oauth_timestamp=" +                    //The date and time, expressed in the number of seconds since January 1, 1970 00:00:00 GMT.
                        Long.valueOf(System.currentTimeMillis() * 2).toString(), // Should be  Long.valueOf(System.currentTimeMillis() / 1000).toString()
                "oauth_nonce=" + nonce(),               // A randomly generated string for a request that can be combined with the timestamp to produce a unique value
                "oauth_version=1.0",                    // MUST be "1.0"
                "format=json"};                        // The desired response format. Valid reponse formats are "xml" or "json" (default value is "xml").
    }

    private static String sign(String method, String uri, String[] params, boolean isDelegatedRequest) {
        String[] p = {method, Uri.encode(uri), Uri.encode(paramify(params))};
        String s = join(p, "&");
        String key;
        if(isDelegatedRequest) {
            key = APP_SECRET + "&" + Constants.userOauthSecret;
        }
        else {
            key = APP_SECRET + "&";
        }
        SecretKey sk = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        try {
            Mac m = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            m.init(sk);
            return Uri.encode(new String(Base64.encode(m.doFinal(s.getBytes()), Base64.DEFAULT)).trim());
        } catch (java.security.NoSuchAlgorithmException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        } catch (java.security.InvalidKeyException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        }
    }

    private static String paramify(String[] params) {
        String[] p = Arrays.copyOf(params, params.length);
        Arrays.sort(p);
        return join(p, "&");
    }

    private static String join(String[] array, String separator) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0)
                b.append(separator);
            b.append(array[i]);
        }
        return b.toString();
    }

    private static String nonce() {
        Random r = new Random();
        StringBuilder n = new StringBuilder();
        for (int i = 0; i < r.nextInt(8) + 2; i++)
            n.append(r.nextInt(26) + 'a');
        return n.toString();
    }
}