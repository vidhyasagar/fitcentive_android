package com.vidhyasagar.fitcentive.api_requests.fatsecret;

import android.net.Uri;
import android.util.Base64;
import android.util.Log;

import com.vidhyasagar.fitcentive.parse.Constants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by vharihar on 2016-11-04.
 */
public class FatSecretRecipes {

    final static private String APP_METHOD = "GET";
    final static private String APP_KEY = Constants.RestApiConsumerKey;
    final static private String APP_SECRET = Constants.RestApiSharedSecret;
    final static private String APP_URL = "http://platform.fatsecret.com/rest/server.api";
    private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";

    private static int MAX_RESULTS = 50;


    //----------------------------------------------------------------------------
    // recipe.get request
    //----------------------------------------------------------------------------

    public JSONObject getRecipe(Long recipeId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=recipe.get");
        params.add("recipe_id=" + recipeId);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), false));
        JSONObject recipe = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject recipeGet = new JSONObject(builder.toString());
//            Log.i("recipeget", recipeGet.toString());
            recipe = recipeGet.getJSONObject(Constants.recipe);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return recipe;
    }


    //----------------------------------------------------------------------------
    // recipes.search request
    //----------------------------------------------------------------------------

    public JSONObject searchRecipes(String searchRecipe, int page, String recipeType) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        if(recipeType != null) {
            params.add("recipe_type=" + recipeType);
        }
        params.add("page_number=" + page);                     // The zero-based offset into the results for the query. Use this parameter with max_results to request successive pages of search results (default value is 0).
        params.add("max_results=" + MAX_RESULTS);              // The maximum number of results to return (default value is 20). This number cannot be greater than 50.
        params.add("method=recipes.search");
        params.add("search_expression=" + Uri.encode(searchRecipe));
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), false));

        JSONObject recipes = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject recipesGet = new JSONObject(builder.toString());   // { first
//            Log.i("searchrecipes", recipesGet.toString());
            recipes = recipesGet.getJSONObject(Constants.recipes);                    // { second
        } catch (Exception exception) {
            Log.e("FatSecret Error", exception.toString());
            exception.printStackTrace();
        }
        return recipes;
    }


    //----------------------------------------------------------------------------
    // recipe.add_favorite request
    //----------------------------------------------------------------------------
    public boolean addFavoriteRecipe(Long recipeId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=recipe.add_favorite");
        params.add("recipe_id=" + recipeId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject response = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject result = new JSONObject(builder.toString());
//            Log.i("addfavoriterecipe", result.toString());
            response = result.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // recipe.delete_favorite request
    //----------------------------------------------------------------------------
    public boolean deleteFavoriteRecipe(Long recipeId) {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=recipe.delete_favorite");
        params.add("recipe_id=" + recipeId);
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject response = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject result = new JSONObject(builder.toString());
            response = result.getJSONObject(Constants.success);
            return true;
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
            return false;
        }
    }


    //----------------------------------------------------------------------------
    // recipes.get_favorites request
    //----------------------------------------------------------------------------
    public JSONObject getFavoriteRecipes() {
        List<String> params = new ArrayList<>(Arrays.asList(generateAuthParams()));
        String[] template = new String[1];
        // Method specific parameters
        params.add("method=recipes.get_favorites");
        params.add("oauth_token=" + Constants.userOauthToken);
        params.add("oauth_signature=" + sign(APP_METHOD, APP_URL, params.toArray(template), true));
        JSONObject recipes = null;
        try {
            URL url = new URL(APP_URL + "?" + paramify(params.toArray(template)));
            URLConnection api = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            JSONObject recipesGet = new JSONObject(builder.toString());
            recipes = recipesGet.getJSONObject(Constants.recipes);
        } catch (Exception e) {
            Log.w("Fit", e.toString());
            e.printStackTrace();
        }
        return recipes;
    }


    //----------------------------------------------------------------------------
    // COMMON HELPERS
    //----------------------------------------------------------------------------

    private static String[] generateAuthParams() {
        return new String[]{
                "oauth_consumer_key=" + APP_KEY,        // Your API key when you registered as a developer
                "oauth_signature_method=HMAC-SHA1",     //The method used to generate the signature (only HMAC-SHA1 is supported)
                "oauth_timestamp=" +                    //The date and time, expressed in the number of seconds since January 1, 1970 00:00:00 GMT.
                        Long.valueOf(System.currentTimeMillis() * 2).toString(), // Should be  Long.valueOf(System.currentTimeMillis() / 1000).toString()
                "oauth_nonce=" + nonce(),               // A randomly generated string for a request that can be combined with the timestamp to produce a unique value
                "oauth_version=1.0",                    // MUST be "1.0"
                "format=json"};                        // The desired response format. Valid reponse formats are "xml" or "json" (default value is "xml").
    }

    private static String sign(String method, String uri, String[] params, boolean isDelegated) {
        String[] p = {method, Uri.encode(uri), Uri.encode(paramify(params))};
        String s = join(p, "&");
        String key;
        if(isDelegated) {
            key = APP_SECRET + "&" + Constants.userOauthSecret;
        }
        else {
            key = APP_SECRET + "&";
        }
        SecretKey sk = new SecretKeySpec(key.getBytes(), HMAC_SHA1_ALGORITHM);
        try {
            Mac m = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            m.init(sk);
            return Uri.encode(new String(Base64.encode(m.doFinal(s.getBytes()), Base64.DEFAULT)).trim());
        } catch (java.security.NoSuchAlgorithmException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        } catch (java.security.InvalidKeyException e) {
            Log.w("FatSecret_TEST FAIL", e.getMessage());
            return null;
        }
    }

    private static String paramify(String[] params) {
        String[] p = Arrays.copyOf(params, params.length);
        Arrays.sort(p);
        return join(p, "&");
    }

    private static String join(String[] array, String separator) {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i > 0)
                b.append(separator);
            b.append(array[i]);
        }
        return b.toString();
    }

    private static String nonce() {
        Random r = new Random();
        StringBuilder n = new StringBuilder();
        for (int i = 0; i < r.nextInt(8) + 2; i++)
            n.append(r.nextInt(26) + 'a');
        return n.toString();
    }
}
