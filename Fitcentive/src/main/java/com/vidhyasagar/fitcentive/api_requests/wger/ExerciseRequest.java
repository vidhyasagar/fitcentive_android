package com.vidhyasagar.fitcentive.api_requests.wger;

import android.net.Uri;
import android.util.Log;

import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.FilterInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by vharihar on 2016-12-09.
 */

// TODO: This should fetch exercises, create URLs based on the builder pattern, and return parsed data in the form of an ArrayList<ExerciseInfo>
public class ExerciseRequest {

   public static final int LIMIT = 50;

   public static final String exerciseImageRoute = "exerciseimage/";
   public static final String exerciseRoute = "exercise/";

   public JSONObject wgerExerciseSearchAll(String url) {
      JSONObject exercise = null;
      try {
         URL Url = new URL(url);
         disableSSLCertificateChecking();
         HttpsURLConnection api = (HttpsURLConnection) Url.openConnection();
         api.connect();
         String line;
         StringBuilder builder = new StringBuilder();
         BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
         while ((line = reader.readLine()) != null) {
            builder.append(line);
         }
         exercise = new JSONObject(builder.toString());
         Log.i("searchexercises", exercise.toString());
      } catch (Exception exception) {
         Log.e("Wger Error", exception.toString());
         exception.printStackTrace();
      }
      return exercise;
   }

   public JSONObject wgerSearchExerciseWithFilter(long language, long page, FilterInfo filter) {
      String Url = getBaseUrl() + exerciseRoute;
      String[] template = new String[1];
      List<String> params = new ArrayList<>();
      params.add("limit="+ LIMIT);
      if(language != -1) {
         params.add("language=" + language);
      }
      if(page != -1) {
         params.add("page=" + page);
      }
      addFiltersToParams(params, filter);
//      Uncomment this to get only approved workouts
//      params.add("status=2");
      Url = Url + "?" + paramify(params.toArray(template));
      JSONObject exercise = null;
      try {
         URL url = new URL(Url);
         disableSSLCertificateChecking();
         HttpsURLConnection api = (HttpsURLConnection) url.openConnection();
         api.connect();
         String line;
         StringBuilder builder = new StringBuilder();
         BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
         while ((line = reader.readLine()) != null) {
            builder.append(line);
         }
         exercise = new JSONObject(builder.toString());
         Log.i("searchexercises", exercise.toString());
      } catch (Exception exception) {
         Log.e("Wger Error", exception.toString());
         exception.printStackTrace();
      }
      return exercise;
   }

   public JSONObject wgerExerciseSearchAll(long language, long page, long category, long equipment) {
      String Url = getBaseUrl() + exerciseRoute;
      String[] template = new String[1];
      List<String> params = new ArrayList<>();
      params.add("limit="+ LIMIT);
      if(language != -1) {
         params.add("language=" + language);
      }
      if(page != -1) {
         params.add("page=" + page);
      }
      if(category != -1) {
         params.add("category=" + category);
      }
      if(equipment != -1) {
         params.add("equipment=" + equipment);
      }
//      Uncomment this to get only approved workouts
//      params.add("status=2");
      Url = Url + "?" + paramify(params.toArray(template));
      JSONObject exercise = null;
      try {
         URL url = new URL(Url);
         disableSSLCertificateChecking();
         HttpsURLConnection api = (HttpsURLConnection) url.openConnection();
         api.connect();
         String line;
         StringBuilder builder = new StringBuilder();
         BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
         while ((line = reader.readLine()) != null) {
            builder.append(line);
         }
         exercise = new JSONObject(builder.toString());
         Log.i("searchexercises", exercise.toString());
      } catch (Exception exception) {
         Log.e("Wger Error", exception.toString());
         exception.printStackTrace();
      }
      return exercise;
   }

   public JSONObject wgerGetExerciseInfo(long id) {
      String Url = getBaseUrl() + exerciseRoute + id + "/";
      JSONObject exercise = null;
      try {
         URL url = new URL(Url);
         disableSSLCertificateChecking();
         HttpsURLConnection api = (HttpsURLConnection) url.openConnection();
         api.connect();
         String line;
         StringBuilder builder = new StringBuilder();
         BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
         while ((line = reader.readLine()) != null) {
            builder.append(line);
         }
         exercise = new JSONObject(builder.toString());
         Log.i("getexerciseinfo", exercise.toString());
      } catch (Exception exception) {
         Log.e("Wger Error", exception.toString());
         exception.printStackTrace();
      }
      return exercise;
   }

   public ArrayList<String> wgerGetExerciseImageUrls(long id) {
      String Url = getBaseUrl() + exerciseImageRoute;
      List<String> params = new ArrayList<>();
      String[] template = new String[1];
      params.add("exercise=" + id);
      Url = Url + "?" + paramify(params.toArray(template));
      ArrayList<String> urls = new ArrayList<>();
      try {
         URL url = new URL(Url);
         disableSSLCertificateChecking();
         HttpsURLConnection api = (HttpsURLConnection) url.openConnection();
         api.connect();
         String line;
         StringBuilder builder = new StringBuilder();
         BufferedReader reader = new BufferedReader(new InputStreamReader(api.getInputStream()));
         while ((line = reader.readLine()) != null) {
            builder.append(line);
         }
         JSONObject exercise = new JSONObject(builder.toString());
         Log.i("getExerciseImage", exercise.toString());
         JSONArray array = exercise.optJSONArray(Constants.results);
         if(array != null && array.length() != 0) {
            for(int i = 0; i < array.length(); i++) {
               JSONObject object = array.getJSONObject(i);
               urls.add(object.optString(Constants.image, Constants.unknown));
            }
         }
      } catch (Exception exception) {
         Log.e("Wger Error", exception.toString());
         exception.printStackTrace();
      }
      return urls;
   }




   /**
    * Disables the SSL certificate checking for new instances of {@link HttpsURLConnection} This has been created to
    * aid testing on a local box, not for use on production.
    */
   private static void disableSSLCertificateChecking() {
      TrustManager[] trustAllCerts = new TrustManager[] {
              new X509TrustManager() {

                 @Override
                 public void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                    // not implemented
                 }

                 @Override
                 public void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws java.security.cert.CertificateException {
                    // not implemented
                 }

                 @Override
                 public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                 }

              }
      };

      try {

         HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {

            @Override
            public boolean verify(String s, SSLSession sslSession) {
               return true;
            }

         });
         SSLContext sc = SSLContext.getInstance("TLS");
         sc.init(null, trustAllCerts, new java.security.SecureRandom());
         HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      } catch (KeyManagementException e) {
         e.printStackTrace();
      } catch (NoSuchAlgorithmException e) {
         e.printStackTrace();
      }
   }

   private static String getBaseUrl() {
      return Constants.wgerBaseUrl;
   }

   private static String paramify(String[] params) {
      String[] p = Arrays.copyOf(params, params.length);
      Arrays.sort(p);
      return join(p, "&");
   }

   private static String join(String[] array, String separator) {
      StringBuilder b = new StringBuilder();
      for (int i = 0; i < array.length; i++) {
         if (i > 0)
            b.append(separator);
         b.append(array[i]);
      }
      return b.toString();
   }

   private void addFiltersToParams(List<String> params, FilterInfo filter) {

      for(int i = 0; i < filter.getCategories().size(); i++) {
         params.add("category=" + filter.getCategories().get(i));
      }

      for(int i = 0; i < filter.getEquipment().size(); i++) {
         params.add("equipment=" + filter.getEquipment().get(i));
      }

      for(int i = 0; i < filter.getPrimaryMuscles().size(); i++) {
         params.add("muscles=" + filter.getPrimaryMuscles().get(i));
      }

      for(int i = 0; i < filter.getSecondaryMuscles().size(); i++) {
         params.add("muscles_secondary=" + filter.getSecondaryMuscles().get(i));
      }

   }

}
