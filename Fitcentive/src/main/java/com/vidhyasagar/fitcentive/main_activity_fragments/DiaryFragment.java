package com.vidhyasagar.fitcentive.main_activity_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryFragment extends Fragment {


    public DiaryFragment() {
        // Required empty public constructor
    }

    public static final String TAG = "DIARY_FRAGMENT";
    public static final int MIDDLE_PAGE = 250;

    private final int NO_PAGES = 500;
    private final int PAGE_MIDDLE = 250;

    int argumentDayOffset;

    FragmentManager fragmentManager;
    ViewPager viewPager;
    Adapter adapter;
    ProgressBar progressBar;


    private void bindViews() {
        fragmentManager = getChildFragmentManager();
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
    }

    private void setUpToolbarTitle() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.diary));
    }

    private void retrieveArguments() {
        Bundle args = getArguments();
        if(args != null) {
            argumentDayOffset = args.getInt(Constants.DAY_OFFSET);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_diary, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_diary, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                ((DiaryPageFragment) adapter.getFragment(viewPager.getCurrentItem())).validateExistingFoods();
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        retrieveArguments();
        bindViews();
        setUpToolbarTitle();
        setUpViewPager();
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    public void setUpViewPager() {
        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        adapter = new Adapter(fragmentManager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(PAGE_MIDDLE + argumentDayOffset);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public DiaryPageFragment getCurrentFragment() {
        return (DiaryPageFragment) adapter.getFragment(viewPager.getCurrentItem());
    }

    public void showMainLayout(boolean show) {
        setViewVisibility(progressBar, !show, true);
        setViewVisibility(viewPager, show, true);
    }

    //-----------------
    // ADAPTER(S)
    //-----------------

    public class Adapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        public Adapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        @Override
        public Fragment getItem(int position) {
            return DiaryPageFragment.newInstance(position - PAGE_MIDDLE);
        }

        @Override
        public int getCount() {
            return NO_PAGES;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }
    }


    //-----------------
    // ASYNCTASK(S)
    //-----------------

}
