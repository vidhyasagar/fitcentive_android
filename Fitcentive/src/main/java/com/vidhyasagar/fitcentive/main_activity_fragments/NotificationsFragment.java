package com.vidhyasagar.fitcentive.main_activity_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.NotificationsAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.NotificationInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationsFragment extends Fragment {

    public static String TAG = "NOTIFICATIONS_FRAGMENT";

    int RETRY_COUNT = 0;
    RecyclerView recyclerView;
    LinearLayoutManager llm;
    ProgressBar postsProgressBar;
    CardView emptyCardView;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<NotificationInfo> notificationInfo;
    NotificationsAdapter rvAdapter;

    HashMap<String, Bitmap> pictureTable;

    public NotificationsFragment() {
        // Required empty public constructor
        pictureTable = new HashMap<>();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public void bindViews() {
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.notifications_recycler);
        postsProgressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
        emptyCardView = (CardView) getActivity().findViewById(R.id.card_view);
        swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeContainer);
    }

    public void fetchNotifications() throws Exception {
        notificationInfo.clear();
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Notification);
        query.whereEqualTo(Constants.receiverUsername, ParseUser.getCurrentUser().getUsername());
        query.orderByDescending(Constants.createdAt);
        query.setLimit(getContext().getResources().getInteger(R.integer.notifications_limit));
        List<ParseObject> result = query.find();
        for(ParseObject notification : result) {
            String receiverUsername = notification.getString(Constants.receiverUsername);
            String providerUsername = notification.getString(Constants.providerUsername);
            String type = notification.getString(Constants.type);
            String postId = notification.getString(Constants.postId);
            String changedCaption = notification.getString(Constants.changedCaption);
            Date notificationDate = notification.getCreatedAt();
            boolean isActive = notification.getBoolean(Constants.isActive);
            Bitmap image;
            ParseFile file = null;
            if(pictureTable.containsKey(providerUsername)) {
                image = pictureTable.get(providerUsername);
            }
            else {
                file = notification.getParseFile(Constants.image);
                if(file != null) {
                    byte[] bytes = file.getData();
                    image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    if(!pictureTable.containsKey(providerUsername)) {
                        pictureTable.put(providerUsername, image);
                    }

                }

                else {
                    image = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_logo);
                }
            }

            NotificationInfo.NOTIFICATION_TYPE notificationType = type.equals(getString(R.string.follow_request))
                    ? NotificationInfo.NOTIFICATION_TYPE.FOLLOW_REQUEST : (type.equals(getString(R.string.comment_request))
                    ? NotificationInfo.NOTIFICATION_TYPE.COMMENT : NotificationInfo.NOTIFICATION_TYPE.LIKE);

            ParseQuery<ParseUser> query2 = ParseUser.getQuery();
            query2.whereEqualTo(Constants.username, receiverUsername);
            ParseUser user1 = query2.getFirst();
            String receiverName = user1.getString(Constants.firstname) + " " + user1.getString(Constants.lastname);

            ParseQuery<ParseUser> query3 = ParseUser.getQuery();
            query3.whereEqualTo(Constants.username, providerUsername);
            ParseUser user2 = query3.getFirst();
            String providerName = user2.getString(Constants.firstname) + " " + user2.getString(Constants.lastname);

            NotificationInfo newInfo = new NotificationInfo(notificationType, receiverUsername, providerUsername,
                    postId, image, receiverName, providerName, isActive, changedCaption, notificationDate);
            notificationInfo.add(newInfo);
        }
    }

    public void setUpRecycler() {
        notificationInfo = new ArrayList<>();
        llm = new LinearLayoutManager(getContext());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);
        rvAdapter = new NotificationsAdapter(notificationInfo, getContext());
        recyclerView.setAdapter(rvAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifications, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        bindViews();
        setUpRecycler();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.notifications));

        swipeRefreshLayout.setColorSchemeResources(R.color.app_theme);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchNotifications(false).execute();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchNotifications(true).execute();
    }

    //==================================================
    // ASYNCTASK
    //==================================================

    private class FetchNotifications extends AsyncTask<Void, Void, Boolean> {

        boolean shouldShowProgressBar;

        public FetchNotifications(boolean shouldShowProgressBar) {
            this.shouldShowProgressBar = shouldShowProgressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pictureTable.clear();
            setViewVisibility(recyclerView, false, true);
            if(shouldShowProgressBar) {
                setViewVisibility(postsProgressBar, true, true);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchNotifications();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            try {
                super.onPostExecute(aBoolean);
                setViewVisibility(postsProgressBar, false, true);
                if (aBoolean) {
                    rvAdapter.setPictureTable(pictureTable);
                    rvAdapter.setPostsCount(getContext().getResources().getInteger(R.integer.post_query_init_limit));
                    rvAdapter.notifyDataSetChanged();
                    if (!notificationInfo.isEmpty()) {
                        setViewVisibility(emptyCardView, false, true);
                        setViewVisibility(recyclerView, true, true);
                    } else {
                        setViewVisibility(recyclerView, false, true);
                        setViewVisibility(emptyCardView, true, true);
                    }

                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchNotifications(true).execute();
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
