package com.vidhyasagar.fitcentive.main_activity_fragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.BarcodeActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.activities.SearchFoodActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.CreatedFoodResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.MealResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.RecipeResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.utilities.MySearchSuggestion;
import com.vidhyasagar.fitcentive.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.vidhyasagar.fitcentive.activities.SearchFoodActivity.BARCODE_RESULT;
import static com.vidhyasagar.fitcentive.activities.SearchFoodActivity.IS_FROM_BARCODE;
import static com.vidhyasagar.fitcentive.activities.SearchFoodActivity.MAX_RESULTS_AUTOCOMPLETE;
import static com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_ALL;
import static com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_FOODS;

/**
 * A simple {@link Fragment} subclass.
 */
public class NutritionFragment extends Fragment {

    public static NutritionFragment newInstance(int dayOffset) {
        NutritionFragment fragment = new NutritionFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        fragment.setArguments(args);
        return fragment;
    }

    public NutritionFragment() {
        // Required empty public constructor
    }

    public static final String TAG = "NUTRITION_FRAGMENT";

    private static final int NO_OF_TABS = 4;
    private static final int ALL_TAB = 0;
    private static final int FOOD_TAB = 1;
    private static final int MEAL_TAB = 2;
    private static final int RECIPE_TAB = 3;


    int dayOffset;

    SearchFoodFragmentsAdapter adapter;
    FragmentManager manager;
    ViewPager viewPager;
    TabLayout tabLayout;
    FloatingSearchView floatingSearchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_nutrition, container, false);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search_food, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_barcode) {
            startBarcodeActivity();
            return true;
        }
        else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpToolbarTitle();
        setUpViewPager();
        setUpSearchBar();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String format = data.getStringExtra(BarcodeActivity.FORMAT);
        String rawData = data.getStringExtra(BarcodeActivity.RAW_DATA);
        String gtin13number = null;
        if(format.equals(Constants.upc_a)) {
            gtin13number = Utilities.convertFromUPC_A(rawData);
        }
        else if (format.equals(Constants.upc_e)) {
            gtin13number = Utilities.convertFromUPC_E(rawData);
        }
        else if(format.equals(Constants.ean_8)) {
            gtin13number = Utilities.convertFromEAN_8(rawData);
        }
        else if(format.equals(Constants.ean_13)) {
            gtin13number = rawData;
        }
        searchForFoodByBarcode(gtin13number);
    }

    private void searchForFoodByBarcode(String barcode) {
        if(barcode == null) {
            return;
        }
        new SearchForFoodByBarcode(barcode).execute();

    }


    private void startBarcodeActivity() {
        Intent i = new Intent(getContext(), BarcodeActivity.class);
        startActivityForResult(i, BARCODE_RESULT);
    }


    private void retrieveArguments() {
        this.dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
    }

    private void bindViews(View v) {
        viewPager = (ViewPager) v.findViewById(R.id.pager);
        tabLayout = (TabLayout) v.findViewById(R.id.tabs);
        floatingSearchView = (FloatingSearchView) v.findViewById(R.id.floating_search_view);
    }

    private void fetchSuggestions(String exp, List<MySearchSuggestion> suggestions) throws Exception {
        FatSecretFoods foods = new FatSecretFoods();
        JSONObject result = foods.autoComplete(exp, MAX_RESULTS_AUTOCOMPLETE);
        if(result != null) {
            Object object = result.get(Constants.suggestion);
            if(object instanceof JSONArray) {
                JSONArray tempArray = (JSONArray) object;
                int size = tempArray.length();
                for(int i = 0; i < size; i++) {
                    suggestions.add(new MySearchSuggestion(tempArray.getString(i)));
                }
            }
            else if(object instanceof String) {
                String sug = (String) object;
                suggestions.add(new MySearchSuggestion(sug));
            }
        }
    }

    private void setUpToolbarTitle() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.nutrition));
    }


    public void setUpViewPager() {

        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new SearchFoodFragmentsAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Change hint here
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(v, getActivity());
            }
        });

    }

    private void callRecipeFragmentToSearchForRecipeViaTextChanged(String query) {
        RecipeResultsFragment fragment = (RecipeResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForRecipe(query, false);
    }

    private void callRightFragmentToSearchForFood(String query, boolean isOkForAll) {
        if(viewPager.getCurrentItem() == ALL_TAB) {
            SearchFoodResultsFragment fragment = (SearchFoodResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
            fragment.searchForFood(query);

        }
        else if(viewPager.getCurrentItem() == RECIPE_TAB) {
            RecipeResultsFragment fragment = (RecipeResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
            fragment.searchForRecipe(query, true);
        }
    }

    private void callFoodFragmentToSearch(String query) {
        CreatedFoodResultsFragment fragment = (CreatedFoodResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForFood(query);
    }

    private void callMealFragmentToSearchForMeal(String query) {
        MealResultsFragment fragment = (MealResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForMeal(query);
    }

    private void setUpSearchBar() {
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                if(viewPager.getCurrentItem() == RECIPE_TAB) {
                    callRecipeFragmentToSearchForRecipeViaTextChanged(newQuery.trim());
                }
                else if(viewPager.getCurrentItem() == ALL_TAB) {
                    if(!TextUtils.isEmpty(newQuery.trim())){
                        new FetchSuggestions(newQuery.trim()).execute();
                    }
                }
                else if(viewPager.getCurrentItem() == FOOD_TAB) {
                    callFoodFragmentToSearch(newQuery.trim());
                }
                else if(viewPager.getCurrentItem() == MEAL_TAB) {
                    callMealFragmentToSearchForMeal(newQuery.trim());
                }
            }
        });

        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                callRightFragmentToSearchForFood(searchSuggestion.getBody().trim(), true);
            }

            @Override
            public void onSearchAction(String currentQuery) {
                callRightFragmentToSearchForFood(currentQuery.trim(), true);
                Utilities.hideKeyboard(floatingSearchView, getActivity());
            }
        });

        floatingSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                Utilities.hideKeyboard(floatingSearchView, getActivity());
                // This is the search button itself
                callRightFragmentToSearchForFood(floatingSearchView.getQuery().trim(), true);
            }
        });

    }

    private void startViewFoodActivity(long foodId) {
        Intent i = new Intent(getContext(), EditFoodEntryActivity.class);
        i.putExtra(ExpandedSearchFoodActivity.FOOD_ID, foodId);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, getTypeBasedOnTimeOfDay());
        i.putExtra(IS_FROM_BARCODE, true);
        startActivity(i);
    }

    private void createNoResultsFoundDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.no_results_found))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .setMessage(getString(R.string.no_barcode_results_found_prompt));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private DiaryPageFragment.ENTRY_TYPE getTypeBasedOnTimeOfDay() {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        if(hour >=4 && hour <= 11) {
            return DiaryPageFragment.ENTRY_TYPE.BREAKFAST;
        }
        else if(hour >= 11 && hour <= 15) {
            return DiaryPageFragment.ENTRY_TYPE.LUNCH;
        }
        else if(hour >= 15 && hour <= 17) {
            return DiaryPageFragment.ENTRY_TYPE.SNACKS;
        }
        else {
            return DiaryPageFragment.ENTRY_TYPE.DINNER;
        }
    }

    public class SearchFoodFragmentsAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public SearchFoodFragmentsAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position){
                case 0:
                    fragment =  SearchFoodResultsFragment.newInstance(TYPE_ALL, getTypeBasedOnTimeOfDay(), dayOffset, false);
                    break;
                case 1:
                    fragment = CreatedFoodResultsFragment.newInstance(TYPE_FOODS, getTypeBasedOnTimeOfDay(), dayOffset);
                    break;
                case 2:
                    fragment =   MealResultsFragment.newInstance(dayOffset, getTypeBasedOnTimeOfDay());
                    break;
                case 3:
                    fragment =  RecipeResultsFragment.newInstance(dayOffset, getTypeBasedOnTimeOfDay(), false);
                    break;
                default:
                    fragment = null;
                    break;
            }
            // This should never fail
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case ALL_TAB:
                    return getString(R.string.all);
                case FOOD_TAB:
                    return getString(R.string.foods);
                case MEAL_TAB:
                    return getString(R.string.meals);
                case RECIPE_TAB:
                    return getString(R.string.recipes);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }

    }

    //------------------------------------------------------
    // TASK THAT FETCHES AUTCOMPLETE RESULTS FROM FATSECRET
    //------------------------------------------------------

    private class FetchSuggestions extends AsyncTask<Void, Void, Boolean> {

        String expression;
        List<MySearchSuggestion> suggestions;

        public FetchSuggestions(String exp) {
            this.expression = exp;
            suggestions = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            floatingSearchView.swapSuggestions(suggestions);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchSuggestions(expression, suggestions);
                if(suggestions.isEmpty()) {
                    suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                }
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    floatingSearchView.swapSuggestions(suggestions);
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class SearchForFoodByBarcode extends AsyncTask<Void, Void, Long> {

        String barcode;

        public SearchForFoodByBarcode(String barcode) {
            this.barcode = barcode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(Void... params) {
            FatSecretFoods foods = new FatSecretFoods();
            return foods.getFoodIdFromBarcode(barcode);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            if(isAdded()) {
                if (aLong == 0) {
                    // This means that no results were found
                    createNoResultsFoundDialog();
                } else {
                    startViewFoodActivity(aLong);
                }
                super.onPostExecute(aLong);
            }
        }
    }


}
