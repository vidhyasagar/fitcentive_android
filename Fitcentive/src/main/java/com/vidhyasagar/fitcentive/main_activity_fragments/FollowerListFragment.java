package com.vidhyasagar.fitcentive.main_activity_fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.FollowersAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.FollowerInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowerListFragment extends Fragment {


    public static FollowerListFragment newInstance() {
        FollowerListFragment fragment = new FollowerListFragment();
        return fragment;
    }

    public FollowerListFragment() {
        // Required empty public constructor
    }

    int RETRY_COUNT = 0;

    ArrayList<FollowerInfo> followersList;
    FollowersAdapter adapter;

    ProgressBar progressBar;
    RelativeLayout mainLayout;
    RecyclerView followersRecycler;
    CardView noResultsCardView;
    FloatingSearchView floatingSearchView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_follower_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        setActionBarTitle();
        bindViews(view);
        setUpRecycler();
        setUpSearchViewBehaviour();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchFollowersInfo().execute();
    }

    private void retrieveArguments() {

    }

    private void setActionBarTitle() {
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.followers));
    }

    private void filterFromFollowers(String query) {
        if(followersList.isEmpty()) {
            return;
        }

        ArrayList<FollowerInfo> list = new ArrayList<>();

        for(FollowerInfo info : followersList) {
            if(info.getUserFullName().toLowerCase().contains(query.toLowerCase()) ||
                    info.getUsername().toLowerCase().contains(query.toLowerCase())) {
                list.add(info);
            }
        }

        adapter.replaceDataSet(list);

        if(adapter.getIfDataSetEmpty()) {
            Utilities.setViewVisibility(followersRecycler, false, true, getContext());
            Utilities.setViewVisibility(noResultsCardView, true, true, getContext());
        }
        else {
            Utilities.setViewVisibility(followersRecycler, true, true, getContext());
            Utilities.setViewVisibility(noResultsCardView, false, true, getContext());
        }
    }

    private void setUpSearchViewBehaviour() {
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                filterFromFollowers(newQuery.trim());
            }
        });
    }

    private void bindViews(View v) {
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        followersRecycler = (RecyclerView) v.findViewById(R.id.followers_recycler);
        noResultsCardView = (CardView) v.findViewById(R.id.no_results_card_view);
        floatingSearchView = (FloatingSearchView) v.findViewById(R.id.floating_search_view);
    }

    private void setUpRecycler() {
        followersList = new ArrayList<>();
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        followersRecycler.setHasFixedSize(true);
        followersRecycler.setLayoutManager(llm);
        adapter = new FollowersAdapter(followersList, getContext());
        followersRecycler.setAdapter(adapter);
    }

    private boolean getIsFollowingCurrentUser(String follower, List<String> following) {
        return following.contains(follower);
    }

    private void fetchFollowersInfo() throws Exception {
        followersList.clear();
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Follow);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());

        ParseObject result = query.getFirst();
        List<String> followers = result.getList(Constants.followers);
        List<String> following = result.getList(Constants.following);
        // Now for each follower in followers, must fetch all of their info
        // Must also fetch follow status of the follower with respect to our CURRENT USER
        // In short, we must know whether CURRENT_USER follows USER with username 'followers.get(i)'

        for(String follower : followers) {
            ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
            userQuery.whereEqualTo(Constants.username, follower);
            ParseObject userObject = userQuery.getFirst();
            FollowerInfo followerInfo = new FollowerInfo();
            followerInfo.setUsername(follower);
            followerInfo.setUserFullName(userObject.getString(Constants.firstname) + " " + userObject.getString(Constants.lastname));

            Bitmap image;
            ParseFile file = null;
            file = userObject.getParseFile(Constants.profilePicture);
            if(file != null) {
                byte[] bytes = file.getData();
                image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            }
            else {
                image = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_logo);
            }
            followerInfo.setUserPicture(image);

            followerInfo.setFollowingCurrentUser(getIsFollowingCurrentUser(follower, following));

            followersList.add(followerInfo);
        }

    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(mainLayout, show, true, getContext());
        Utilities.setViewVisibility(progressBar, !show, true, getContext());
    }

    //---------------------------------------------------
    // ASYNCTASK(S)
    //---------------------------------------------------

    private class FetchFollowersInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchFollowersInfo();
                return true;
            }
            catch  (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if(aBoolean) {
                    adapter.notifyDataSetChanged();
                    showMainLayout(true);
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchFollowersInfo().execute();
                    }
                }
            }
        }
    }

}
