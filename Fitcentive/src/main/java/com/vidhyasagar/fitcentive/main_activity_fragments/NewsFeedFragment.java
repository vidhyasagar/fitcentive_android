package com.vidhyasagar.fitcentive.main_activity_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.NewsFeedAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFeedFragment extends Fragment {

    public static String TAG = "NEWS_FEED_FRAGMENT";

    int RETRY_COUNT = 0;
    boolean isNewsFeed;
    RecyclerView recycler;
    LinearLayoutManager lm;
    ArrayList<NewsFeedInfo> news;
    List<String> following;
    NewsFeedAdapter rvAdapter;
    ProgressBar postsProgressBar;
    CardView emptyCardView;
    SwipeRefreshLayout swipeRefreshLayout;

    public NewsFeedFragment() {
        // Required empty public constructor
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news_feed, container, false);
    }


    public void bindViews() {
        recycler = (RecyclerView) getActivity().findViewById(R.id.newsfeed_recycler);
        postsProgressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
        emptyCardView = (CardView) getActivity().findViewById(R.id.card_view);
        swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.swipeContainer);
    }

    public void getUsersToFetchPostsFrom() throws ParseException {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        List<ParseObject> result = query.find();
        if(result.isEmpty()) {
            following = new ArrayList<>();
        }
        else {
            if(this.isNewsFeed) {
                following = result.get(0).getList(Constants.following);
            }
            else {
                following = result.get(0).getList(Constants.followers);
            }
        }
        if(this.isNewsFeed) {
            if(following == null) {
                following = new ArrayList<>();
            }
            following.add(ParseUser.getCurrentUser().getUsername());
        }
    }

    public void populateNews() throws ParseException, Exception {
        // Name, caption, comments, likes, date, image
        news.clear();
        ParseQuery<ParseObject> postsQuery = new ParseQuery<>(Constants.Post);
        // Post query check where username is list of usernames of people who current user follows
        if(following == null) {
            return;
        }
        postsQuery.whereContainedIn(Constants.username, following);
        postsQuery.orderByDescending(Constants.createdAt);
        postsQuery.setLimit(getContext().getResources().getInteger(R.integer.post_query_init_limit));
        List<ParseObject> objects = postsQuery.find();

        if(objects.isEmpty()) {
            return;
        }

        for(ParseObject post : objects) {
            String objectId = post.getObjectId();
            String username = post.getString(Constants.username);
            String caption = post.getString(Constants.caption);
            int numberComments = post.getInt(Constants.numberComments);
            int numberLikes = post.getInt(Constants.numberLikes);
            Date date = post.getCreatedAt();
            ParseFile image = post.getParseFile(Constants.image);
            List<String> likersList = post.getList(Constants.likers);
            NewsFeedInfo newPost;
            if(image != null) {
                byte[] byteArray = image.getData();
                Bitmap postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, postBitmap, objectId);
            }
            else {
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, null, objectId);
            }
            if(likersList == null) {
                newPost.setPostLikeStatus(false);
            }
            else {
                // If currentusername is in the list of likers for the post, then set it to true
                if(NewsFeedAdapter.checkLikeStatus(ParseUser.getCurrentUser().getUsername(), likersList)) {
                    newPost.setPostLikeStatus(true);
                }
                else {
                    newPost.setPostLikeStatus(false);
                }
            }

            if(likersList == null || likersList.size() == 0) {
                newPost.setFirstLiker(null);
            }
            else if(likersList.size() != 0) {
                // ParseQuery to extract user first name and last name from given user_name
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(Constants.username, likersList.get(0));

                // Since usernames are unique, this must return only ONE object
                List<ParseUser> foundUsers = query.find();
                String likerName = foundUsers.get(0).getString(Constants.firstname) + " " +
                        foundUsers.get(0).getString(Constants.lastname);
                newPost.setFirstLiker(likerName);
            }

            if(NewsFeedAdapter.checkCommentStatus(ParseUser.getCurrentUser().getUsername(), objectId)) {
                newPost.setPostCommentStatus(true);
            }
            else {
                newPost.setPostCommentStatus(false);
            }

            news.add(newPost);
        }
    }

    public void setUpRecycler() {
        news = new ArrayList<>();
        lm = new LinearLayoutManager(getContext());
        recycler.setHasFixedSize(true);
        recycler.setLayoutManager(lm);
        rvAdapter = new NewsFeedAdapter(news, getContext(), true);
        rvAdapter.setEmptyCardAndRecycler(recycler, emptyCardView);
        recycler.setAdapter(rvAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();

        bindViews();
        setUpRecycler();

        this.isNewsFeed = getArguments().getBoolean("isNewsfeed");

        if(this.isNewsFeed) {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.news_feed));
        }
        else {
            ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.followers));

        }

        // Fetching posts taken care of in onResume for robustness

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(R.color.app_theme);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchPosts(false).execute();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchPosts(true).execute();
    }

    // -------------------------------
    // OFF the UI thread
    // -------------------------------

    private class FetchPosts extends AsyncTask<Void, Void, Boolean> {

        boolean shouldShowProgressBar;

        public FetchPosts(boolean shouldShowProgressBar) {
            this.shouldShowProgressBar = shouldShowProgressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(recycler, false, true);
            if(shouldShowProgressBar) {
                setViewVisibility(postsProgressBar, true, true);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                getUsersToFetchPostsFrom();
                rvAdapter.setFollowingList(new ArrayList<String>(following));
                populateNews();
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            try {
            super.onPostExecute(aBoolean);
                if(isAdded()) {
                    setViewVisibility(postsProgressBar, false, true);
                    if (aBoolean) {
                        rvAdapter.setPostsCount(getContext().getResources().getInteger(R.integer.post_query_init_limit));
                        rvAdapter.notifyDataSetChanged();
                        if (!news.isEmpty()) {
                            setViewVisibility(emptyCardView, false, true);
                            setViewVisibility(recycler, true, true);
                        } else {
                            setViewVisibility(recycler, false, true);
                            setViewVisibility(emptyCardView, true, true);
                        }

                        swipeRefreshLayout.setRefreshing(false);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                        RETRY_COUNT++;
                        if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                            new FetchPosts(true).execute();
                        }
                    }
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
