package com.vidhyasagar.fitcentive.search_food_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditFoodActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.CreatedFoodsAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.FoodResultsInfo;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment.DAY_OFFSET_STRING;
import static com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment.ENTRY_TYPE_STRING;
import static com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment.RESULT_TYPE;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatedFoodResultsFragment extends Fragment {


    public static CreatedFoodResultsFragment newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM type, DiaryPageFragment.ENTRY_TYPE entryType,
                                                         int dayOffset) {
        CreatedFoodResultsFragment f = new CreatedFoodResultsFragment();
        Bundle args = new Bundle();
        args.putSerializable(RESULT_TYPE, type);
        args.putSerializable(ENTRY_TYPE_STRING, entryType);
        args.putInt(DAY_OFFSET_STRING, dayOffset);
        f.setArguments(args);
        return f;
    }

    public CreatedFoodResultsFragment() {
        // Required empty public constructor
    }

    RelativeLayout noCreatedFoodRelativeLayout, mainRelativeLayout;
    Button createNewFoodButton, createFoodButton;
    RecyclerView createdFoodsRecycler;
    CreatedFoodsAdapter adapter;
    ProgressBar progressBar;
    CardView noResultsCardView;

    int dayOffset;
    DiaryPageFragment.ENTRY_TYPE type;
    FatSecretFoods foods;
    FatSecretRecipes recipes;

    int RETRY_COUNT = 0;
    ArrayList<FoodResultsInfo> createdFoodsList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_food_results, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpRecycler();
        setUpFoodCreateButton();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_meals_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_meal:
                startCreateNewFoodActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void startCreateNewFoodActivity() {
        Intent i = new Intent(getContext(), CreateOrEditFoodActivity.class);
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, false);
        startActivityForResult(i, CreateOrEditFoodActivity.CREATE_FOOD);
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchCreatedFoods().execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_CREATE) {
//            new FetchCreatedMeals().execute();
        }
        else if(resultCode == RESULT_OK && requestCode == CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_EDIT) {
            // This actually doesn't get caught. However things work out the way it's supposed to so I'm not complaining :P
        }
    }

    private void retrieveArguments() {
        this.dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        this.type = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(SearchFoodResultsFragment.ENTRY_TYPE_STRING);
    }

    private void bindViews(View v) {
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        noCreatedFoodRelativeLayout = (RelativeLayout) v.findViewById(R.id.no_meals_relative_layout);
        mainRelativeLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        createNewFoodButton = (Button) v.findViewById(R.id.create_meal_button);
        createFoodButton = (Button) v.findViewById(R.id.create_new_meal_button);
        createdFoodsRecycler = (RecyclerView) v.findViewById(R.id.meal_results_recycler);
        noResultsCardView = (CardView) v.findViewById(R.id.no_results_card_view);
        foods = new FatSecretFoods();
        recipes = new FatSecretRecipes();
    }

    public void searchForFood(String query) {
        filterFromCreatedFoods(query);
    }

    private void filterFromCreatedFoods(String query) {
        if(createdFoodsList.isEmpty()) {
            return;
        }

        ArrayList<FoodResultsInfo> newList = new ArrayList<>();

        for(FoodResultsInfo info : createdFoodsList) {
            if(info.getFoodName().toLowerCase().contains(query.toLowerCase()) ||
                    info.getBrandName().toLowerCase().contains(query.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);
        if(adapter.getIfDataSetEmpty()) {
            showNoResultsCardView(true);
        }
        else {
            showNoResultsCardView(false);
        }
    }

    private void setUpFoodCreateButton() {
        createNewFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateNewFoodActivity();
            }
        });
        createFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateNewFoodActivity();
            }
        });
    }

    private void setUpRecycler() {
        createdFoodsList = new ArrayList<>();
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        createdFoodsRecycler.setLayoutManager(lm);
        adapter = new CreatedFoodsAdapter(createdFoodsList, getContext(), SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_MEALS, type, dayOffset);
        createdFoodsRecycler.setAdapter(adapter);
    }

    private String getServingSizeFrom(double servingSize, String unit) {
        return String.valueOf(servingSize) + " " + unit;
    }


    private void fetchCreatedFoods() throws Exception {
        createdFoodsList.clear();

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedFood);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());

        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            FoodResultsInfo info = new FoodResultsInfo(
                    object.getString(Constants.foodNameField),
                    object.getString(Constants.foodBrand),
                    object.getString(Constants.foodDescriptionField),
                    getServingSizeFrom(object.getDouble(Constants.servingSizeField), object.getString(Constants.servingSizeUnit)),
                    Double.valueOf(object.getDouble(Constants.caloriesField)).intValue());

            info.setObjectId(object.getObjectId());

            createdFoodsList.add(info);
        }

    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void showMainLayout(boolean show) {
        setViewVisibility(mainRelativeLayout, show, true);
        setViewVisibility(progressBar, !show, true);
    }

    private void showNoResultsCardView(boolean show) {
        setViewVisibility(createdFoodsRecycler, !show, true);
        setViewVisibility(noResultsCardView, show, true);
    }

    private void showGetStartedPage(boolean show) {
        setViewVisibility(noCreatedFoodRelativeLayout, show, true);
        setViewVisibility(mainRelativeLayout, !show, true);
    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class FetchCreatedFoods extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchCreatedFoods();
                return true;
            }
            catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    if (createdFoodsList.isEmpty()) {
                        showGetStartedPage(true);
                        setViewVisibility(progressBar, false, true);
                    } else {
                        showMainLayout(true);
                        showGetStartedPage(false);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedFoods().execute();
                    }
                }
            }
        }
    }

}
