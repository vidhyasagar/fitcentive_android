package com.vidhyasagar.fitcentive.search_food_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.RecipeResultsAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.RecipeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeResultsFragment extends Fragment {

    public static String TAG = "RECIPE_RESULTS_FRAGMENT";

    int dayOffset;
    DiaryPageFragment.ENTRY_TYPE mealType;

    boolean isReturningToCreateMealActivity;

    int RETRY_COUNT = 0;
    boolean HAS_SEARCH_HAPPENED = false;

    ArrayList<RecipeInfo> recipeList;
    LinearLayoutManager llm;
    RecipeResultsAdapter adapter;

    FatSecretRecipes recipes;
    ProgressBar progressBar;
    RelativeLayout relativeLayout;
    RecyclerView recyclerView;
    CardView noResultsCardView;
    TextView noResultsTextView;
    TextView redoPrompt;

    public static RecipeResultsFragment newInstance(int dayOffset, DiaryPageFragment.ENTRY_TYPE type,
                                                    boolean isReturningToCreateMealActivity) {
        RecipeResultsFragment fragment = new RecipeResultsFragment();
        Bundle args = new Bundle();
        args.putInt(SearchFoodResultsFragment.DAY_OFFSET_STRING, dayOffset);
        args.putSerializable(Constants.mealType, type);
        args.putBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL,  isReturningToCreateMealActivity);
        fragment.setArguments(args);
        return fragment;
    }

    public RecipeResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recipe_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpRecycler();
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!HAS_SEARCH_HAPPENED) {
            new InitRecipeResults().execute();
        }
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(SearchFoodResultsFragment.DAY_OFFSET_STRING);
        mealType = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(Constants.mealType);
        this.isReturningToCreateMealActivity = getArguments().getBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL);
    }

    private void bindViews(View view) {
        recipes = new FatSecretRecipes();
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.main_relative_layout);
        recyclerView = (RecyclerView) view.findViewById(R.id.exercise_results_recycler);
        noResultsCardView = (CardView) view.findViewById(R.id.no_results_card_view);
        noResultsTextView = (TextView) view.findViewById(R.id.no_results_found_textview);
        redoPrompt = (TextView) view.findViewById(R.id.no_results_redo_prompt);
    }

    private void setUpRecycler() {
        recipeList = new ArrayList<>();
        llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new RecipeResultsAdapter(recipeList, getActivity(), dayOffset, mealType,
                isReturningToCreateMealActivity);
        recyclerView.setAdapter(adapter);
    }

    public void searchForRecipe(String recipeName, boolean hasSearchButtonBeenClicked) {
        if(hasSearchButtonBeenClicked) {
            new SearchRecipes(recipeName).execute();
        }
        else {
            filterOutFromFavorites(recipeName);
        }
    }

    private void fatSecretRecipeSearch(String searchText) throws JSONException {
        recipeList.clear();
        HAS_SEARCH_HAPPENED = true;
        JSONObject result = recipes.searchRecipes(searchText, 0, null);
        if(result != null) {
            JSONArray resultArray = result.optJSONArray(Constants.recipe);
            if(resultArray != null) {
                int size = resultArray.length();
                for (int i = 0; i < size; i++) {
                    JSONObject recipeItem = resultArray.getJSONObject(i);
                    long recipeId = recipeItem.optLong(Constants.recipeId, -1);
                    String recipeDescription = recipeItem.optString(Constants.recipeDescription, Constants.unknown);
                    String recipeImage = recipeItem.optString(Constants.recipeImage, null);
                    String recipeName = recipeItem.optString(Constants.recipeName, Constants.unknown);
                    String recipeUrl = recipeItem.optString(Constants.recipeUrl, null);
                    RecipeInfo newFood = new RecipeInfo(recipeId, recipeName, recipeDescription, recipeUrl, recipeImage);
                    newFood.setFavorite(false);
                    recipeList.add(newFood);
                }
            }
        }
    }


    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void hideOrShowRecyclerAndShowEmptyCard(boolean showRecycler) {
        setViewVisibility(recyclerView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
        noResultsTextView.setText(getString(R.string.no_results_found));
        redoPrompt.setText(getString(R.string.no_results_redo_prompt));
    }

    private void initRecipes() throws JSONException {
        recipeList.clear();
        JSONObject result = recipes.getFavoriteRecipes();

        if(result != null) {
            Object tempObject = result.get(Constants.recipe);

            if(tempObject instanceof JSONArray) {

                JSONArray resultArray = result.optJSONArray(Constants.recipe);
                if (resultArray != null) {
                    int size = resultArray.length();
                    for (int i = 0; i < size; i++) {
                        JSONObject foodItem = resultArray.getJSONObject(i);
                        long recipeId = foodItem.optLong(Constants.recipeId, -1);
                        String recipeName = foodItem.optString(Constants.recipeName, Constants.unknown);
                        String recipeUrl = foodItem.optString(Constants.recipeUrl, Constants.unknown);
                        String recipeDescription = foodItem.optString(Constants.recipeDescription, Constants.unknown);
                        String recipeImage = foodItem.optString(Constants.recipeImage, Constants.unknown);
                        RecipeInfo newObject = new RecipeInfo(recipeId, recipeName, recipeDescription, recipeUrl, recipeImage);
                        newObject.setFavorite(true);
                        recipeList.add(newObject);
                    }
                }
            }

            else {
                JSONObject foodItem = result.getJSONObject(Constants.recipe);
                if (foodItem != null) {
                    long recipeId = foodItem.optLong(Constants.recipeId, -1);
                    String recipeName = foodItem.optString(Constants.recipeName, Constants.unknown);
                    String recipeUrl = foodItem.optString(Constants.recipeUrl, Constants.unknown);
                    String recipeDescription = foodItem.optString(Constants.recipeDescription, Constants.unknown);
                    String recipeImage = foodItem.optString(Constants.recipeImage, Constants.unknown);
                    RecipeInfo newObject = new RecipeInfo(recipeId, recipeName, recipeDescription, recipeUrl, recipeImage);
                    newObject.setFavorite(true);
                    recipeList.add(newObject);
                }
            }

        }
    }

    // IF this is executed, then it means recipeList is NOT empty initally
    // IT could be empty with more filtering
    private void filterOutFromFavorites(String searchText) {
        if(recipeList.isEmpty()) {
            return;
        }

        if(HAS_SEARCH_HAPPENED) {
            return;
        }

        ArrayList<RecipeInfo> newList = new ArrayList<>();

        for(RecipeInfo info : recipeList) {
            if(info.getRecipeName().toLowerCase().contains(searchText.toLowerCase()) ||
                    info.getRecipeDescription().toLowerCase().contains(searchText.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);
        if(adapter.getIfDataSetEmpty()) {
            hideOrShowRecyclerAndShowEmptyCard(false);
        }
        else {
            hideOrShowRecyclerAndShowEmptyCard(true);
        }
    }

    private void hideOrShowRecyclerAndShowEmptyCardAndChangeText(boolean showRecycler) {
        setViewVisibility(recyclerView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
        noResultsTextView.setText(getString(R.string.no_favorite_recipes_found));
        redoPrompt.setText(getString(R.string.start_typing));
    }

    //--------------------------------
    // ASYNCTASK(S)
    //--------------------------------


    private class SearchRecipes extends AsyncTask<Void, Void, Boolean> {

        private String searchText;

        public SearchRecipes(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(relativeLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                adapter.setSearchText(searchText);
                // 1 because our initial search starts with page 0
                adapter.setCurrentPage(1);
                fatSecretRecipeSearch(searchText);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(relativeLayout, true, true);
                if(recipeList.isEmpty()) {
                    hideOrShowRecyclerAndShowEmptyCard(false);
                }
                else {
                    hideOrShowRecyclerAndShowEmptyCard(true);
                    adapter.replaceDataSet(recipeList);
                }
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new SearchRecipes(this.searchText).execute();
                }
            }
        }
    }

    private class InitRecipeResults extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(relativeLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                initRecipes();
                return true;
            } catch(JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(relativeLayout, true, true);
                    if (recipeList.isEmpty()) {
                        hideOrShowRecyclerAndShowEmptyCardAndChangeText(false);
                    } else {
                        hideOrShowRecyclerAndShowEmptyCardAndChangeText(true);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new InitRecipeResults().execute();
                    }
                }
            }
        }
    }
}
