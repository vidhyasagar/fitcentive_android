package com.vidhyasagar.fitcentive.search_food_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.FoodSearchResultsAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.FoodResultsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFoodResultsFragment extends Fragment {

    public enum RESULT_TYPE_ENUM {TYPE_ALL, TYPE_RECENT, TYPE_FREQUENT, TYPE_MEALS, TYPE_FAVORITES, TYPE_RECIPES, TYPE_FOODS}
    public static String RESULT_TYPE = "RESULT_TYPE";
    public static String ENTRY_TYPE_STRING = "ENTRY_TYPE";
    public static String IS_RETURNING_TO_CREATE_MEAL = "IS_RETURNING_TO_CREATE_MEAL";
    public static String DAY_OFFSET_STRING = "DAY_OFFSET";
    public static String TAG = "SEARCH_FOOD_RESULTS_FRAGMENT";

    RESULT_TYPE_ENUM type;
    DiaryPageFragment.ENTRY_TYPE entryType;
    int RETRY_COUNT = 0;
    int dayOffset;
    boolean isReturningToCreateMealActivity;
    // This is only used if the above is true

    FatSecretFoods food;
    ArrayList<FoodResultsInfo> foodList;

    ProgressBar progressBar;
    RelativeLayout relativeLayout;
    RecyclerView recyclerView;
    CardView noResultsCardView;
    TextView noResultsTextView, redoPrompt;
    LinearLayoutManager llm;
    FoodSearchResultsAdapter adapter;

    public SearchFoodResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_food_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        retrieveArguments();
        bindViews(view);
        setUpRecycler();

        new InitFoodResults().execute();
    }

    public RESULT_TYPE_ENUM getResultType() {
        return type;
    }

    public static SearchFoodResultsFragment newInstance(RESULT_TYPE_ENUM type, DiaryPageFragment.ENTRY_TYPE entryType,
                                                        int dayOffset, boolean isReturningToCreateMealActivity) {
        SearchFoodResultsFragment f = new SearchFoodResultsFragment();
        Bundle args = new Bundle();
        args.putSerializable(RESULT_TYPE, type);
        args.putSerializable(ENTRY_TYPE_STRING, entryType);
        args.putInt(DAY_OFFSET_STRING, dayOffset);
        args.putBoolean(IS_RETURNING_TO_CREATE_MEAL, isReturningToCreateMealActivity);
        f.setArguments(args);
        return f;
    }

    private void retrieveArguments() {
        this.type =  (RESULT_TYPE_ENUM) getArguments().getSerializable(RESULT_TYPE);
        this.entryType = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(ENTRY_TYPE_STRING);
        this.dayOffset = getArguments().getInt(DAY_OFFSET_STRING);
        this.isReturningToCreateMealActivity = getArguments().getBoolean(IS_RETURNING_TO_CREATE_MEAL);
    }

    private void setUpRecycler() {
        foodList = new ArrayList<>();
        llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new FoodSearchResultsAdapter(foodList, getActivity(), type, entryType,
                dayOffset, isReturningToCreateMealActivity);
        // UNCOMMENT THIS LINE TO SACRIFICE STABILITY FOR BETTER PERFORMANCE
        // adapter.setHasStableIds(true);
        // THIS IS FOR BETTER PERFORMANCE AGAIN
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    private void bindViews(View view) {
        food = new FatSecretFoods();
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.main_relative_layout);
        recyclerView = (RecyclerView) view.findViewById(R.id.food_results_recycler);
        noResultsCardView = (CardView) view.findViewById(R.id.no_results_card_view);
        noResultsTextView = (TextView) view.findViewById(R.id.no_results_found_textview);
        redoPrompt = (TextView) view.findViewById(R.id.no_results_redo_prompt);
    }

    public static String getMealType(DiaryPageFragment.ENTRY_TYPE entryType) {
        switch (entryType) {
            case BREAKFAST: return "breakfast";
            case LUNCH: return "lunch";
            case DINNER: return "dinner";
            default: return "other";
        }
    }

    private void initFoods() throws JSONException {
        foodList.clear();
        JSONObject result = null;
        switch (type) {
            case TYPE_RECENT:
                result = food.getRecentlyEaten(getMealType(entryType));
                break;
            case TYPE_FREQUENT:
                result = food.getMostEaten(getMealType(entryType));
                break;
            case TYPE_ALL:
                return;
            case TYPE_FAVORITES:
                result = food.getFavoriteFoods();
                break;
            default: break;
        }

        if(result != null) {
            Object tempObject = result.get(Constants.food);

            if(tempObject instanceof JSONArray) {

                JSONArray resultArray = result.optJSONArray(Constants.food);
                if (resultArray != null) {
                    int size = resultArray.length();
                    for (int i = 0; i < size; i++) {
                        JSONObject foodItem = resultArray.getJSONObject(i);
                        long foodId = foodItem.optLong(Constants.foodId, -1);
                        long servingId = foodItem.optLong(Constants.servingId, -1);
                        DetailedFoodResultsInfo newObject = fetchDetailedFoodInfo(foodId, servingId);
                        if (newObject != null) {
                            FoodResultsInfo newFood = new FoodResultsInfo(newObject.getBrandName(), null, newObject.getFoodName(), newObject.getFoodType(), newObject.getFoodUrl(), foodId);
                            newFood.setCalories((Double.valueOf(newObject.getCalories()).intValue()));
                            foodList.add(newFood);
                        } else {
                            return;
                        }
                    }
                }
                else {
                    // In this case now,
                    foodList.add(null);
                }

            }

            else {
                JSONObject foodItem = result.getJSONObject(Constants.food);
                if (foodItem != null) {
                    long foodId = foodItem.optLong(Constants.foodId, -1);
                    long servingId = foodItem.optLong(Constants.servingId, -1);
                    DetailedFoodResultsInfo newObject = fetchDetailedFoodInfo(foodId, servingId);
                    if (newObject != null) {
                        FoodResultsInfo newFood = new FoodResultsInfo(newObject.getBrandName(), null, newObject.getFoodName(), newObject.getFoodType(), newObject.getFoodUrl(), foodId);
                        newFood.setCalories((Double.valueOf(newObject.getCalories()).intValue()));
                        foodList.add(newFood);
                    } else {
                        return;
                    }
                }
                else {
                    // In this case now,
                    foodList.add(null);
                }
            }

        }
    }

    public void searchForFood(String foodName) {
        new SearchFoods(foodName).execute();
    }

    private void fatSecretFoodSearchAll(String foodName) throws JSONException {
        foodList.clear();
        JSONObject result = food.searchFoods(foodName, 0);
        if(result != null) {
            JSONArray resultArray = result.optJSONArray(Constants.food);
            if(resultArray != null) {
                int size = resultArray.length();
                for (int i = 0; i < size; i++) {
                    JSONObject foodItem = resultArray.getJSONObject(i);
                    String brandName = foodItem.optString(Constants.brandName, Constants.unknown);
                    String foodDescription = foodItem.optString(Constants.foodDescription, Constants.unknown);
                    String fooDName = foodItem.optString(Constants.foodName, Constants.unknown);
                    String foodType = foodItem.optString(Constants.foodType, Constants.unknown);
                    String foodUrl = foodItem.optString(Constants.foodUrl, Constants.noUrlFound);
                    long foodId = foodItem.optLong(Constants.foodId, -1);
                    FoodResultsInfo newFood = new FoodResultsInfo(brandName, foodDescription, fooDName, foodType, foodUrl, foodId);
                    foodList.add(newFood);
                }
            }
            else {
                foodList.add(null);
            }
        }
    }

    private void fatSecretFoodSearchRecentAndFrequentAndFavorites(String foodQuery) throws JSONException {
        // Filters out from existing foodList items that match searchQuery
        ArrayList<FoodResultsInfo> newList = new ArrayList<>();

        for(FoodResultsInfo info : foodList) {
            if(info.getBrandName().toLowerCase().contains(foodQuery.toLowerCase()) || info.getFoodName().toLowerCase().contains(foodQuery.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);

    }


    private DetailedFoodResultsInfo extractDetailedInfoFromServingArray(JSONArray servingsArray, String brandName, String foodName,
                                                                         long foodId, long servingId) throws JSONException {

        int size = servingsArray.length();

        for(int i = 0; i < size; i++) {
            JSONObject servingObject = servingsArray.getJSONObject(i);

            if(servingId == servingObject.optLong(Constants.servingId, -1)) {
                return extractDetailedInfoFromServingObject(servingObject, brandName, foodName, foodId, servingId);
            }

        }

        return null;
    }

    private DetailedFoodResultsInfo extractDetailedInfoFromServingObject(JSONObject servingObject, String brandName, String foodName,
                                                                         long foodId, long servingId) throws JSONException {

        String servingUrl = servingObject.optString(Constants.servingUrl, Constants.noUrlFound);
        String servingDescription = servingObject.optString(Constants.servingDescription, Constants.unknown);
        double metricServingAmount = servingObject.optDouble(Constants.metricServingAmount, -1);
        String metricServingUnit = servingObject.optString(Constants.metricServingUnit, Constants.unknown);
        String measurementDescription = servingObject.optString(Constants.measurementDescription, Constants.unknown);
        double numberOfUnits = servingObject.optDouble(Constants.numberOfUnits);
        DetailedFoodResultsInfo newInfo = new DetailedFoodResultsInfo(foodId, servingId, servingUrl, servingDescription,
                metricServingAmount, metricServingUnit, measurementDescription, numberOfUnits, getContext());

        // Setting up the heavy stuff
        newInfo.setBrandName(brandName);
        newInfo.setFoodName(foodName);
        newInfo.setCalories(servingObject.optDouble(Constants.calories, -1));
        newInfo.setCarbs(servingObject.optDouble(Constants.carbs, -1));
        newInfo.setProtein(servingObject.optDouble(Constants.protein, -1));
        newInfo.setFat(servingObject.optDouble(Constants.fat, -1));
        newInfo.setBonusFact();

        return newInfo;

    }


    private DetailedFoodResultsInfo fetchDetailedFoodInfo(long foodId, long servingId) throws JSONException {
        JSONObject result = food.getFood(foodId);
        if(result != null) {
            String brandName = result.optString(Constants.brandName, Constants.unknown);
            String foodName = result.optString(Constants.foodName, Constants.unknown);
            JSONObject servingInfo = result.optJSONObject(Constants.servings);

            if(servingInfo != null) {
                JSONObject servingObject = null;
                JSONArray servingArray = null;
                Object tempObject = servingInfo.get(Constants.serving);
                if(tempObject != null) {
                    if(tempObject instanceof JSONObject) {
                        servingObject = (JSONObject) tempObject;
                    }
                    else {
                        servingArray = (JSONArray) tempObject;
                    }
                }

                if(servingArray != null) {
                    return extractDetailedInfoFromServingArray(servingArray, brandName, foodName, foodId, servingId);
                }
                else {
                    return extractDetailedInfoFromServingObject(servingObject, brandName, foodName, foodId, servingId);

                }
            }

        }
        return null;
    }

    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void hideOrShowRecyclerAndShowEmptyCard(boolean showRecycler) {
        setViewVisibility(recyclerView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
        noResultsTextView.setText(getString(R.string.no_results_found));
        redoPrompt.setText(getString(R.string.no_results_redo_prompt));
    }

    private void hideOrShowRecyclerAndShowEmptyCardAndChangeText(boolean showRecycler) {
        setViewVisibility(recyclerView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
        noResultsTextView.setText(getString(R.string.search_for_all_foods));
        redoPrompt.setText(getString(R.string.start_typing));
    }


    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class SearchFoods extends AsyncTask<Void, Void, Boolean> {

        private String searchText;

        public SearchFoods(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(relativeLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                adapter.setSearchText(searchText);
                // 1 because our initial search starts at page 0
                adapter.setCurrentPage(1);
                switch(type) {
                    case TYPE_ALL:
                        fatSecretFoodSearchAll(searchText);
                        break;
                    case TYPE_RECENT:
                    case TYPE_FREQUENT:
                    case TYPE_FAVORITES:
                        fatSecretFoodSearchRecentAndFrequentAndFavorites(searchText);
                        break;

                    default: break;
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(relativeLayout, true, true);
                if(type == RESULT_TYPE_ENUM.TYPE_ALL) {
                    if ((foodList.size() == 1 && foodList.get(0) == null) || foodList.isEmpty()) {
                        if(searchText.isEmpty()) {
                            hideOrShowRecyclerAndShowEmptyCardAndChangeText(false);
                        }
                        else {
                            hideOrShowRecyclerAndShowEmptyCard(false);
                        }
                    } else {
                        hideOrShowRecyclerAndShowEmptyCard(true);
                        adapter.notifyDataSetChanged();
                    }
                }
                else {
                    if(adapter.getIfDataSetEmpty()) {
                        hideOrShowRecyclerAndShowEmptyCard(false);
                    }
                    else {
                        hideOrShowRecyclerAndShowEmptyCard(true);
                    }
                }
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new SearchFoods(this.searchText).execute();
                }
            }
        }
    }

    private class InitFoodResults extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(relativeLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }


        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                initFoods();
                return true;
            } catch(JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(relativeLayout, true, true);
                    if ((foodList.size() == 1 && foodList.get(0) == null) || foodList.isEmpty()) {
                        if(type == RESULT_TYPE_ENUM.TYPE_ALL) {
                            hideOrShowRecyclerAndShowEmptyCardAndChangeText(false);
                        }
                        else {
                            hideOrShowRecyclerAndShowEmptyCard(false);
                        }
                    } else {
                        hideOrShowRecyclerAndShowEmptyCard(true);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new InitFoodResults().execute();
                    }
                }
            }
        }
    }

}
