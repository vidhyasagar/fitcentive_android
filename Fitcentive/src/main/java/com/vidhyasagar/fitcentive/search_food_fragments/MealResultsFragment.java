package com.vidhyasagar.fitcentive.search_food_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.MealResultsAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.MealInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_CREATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MealResultsFragment extends Fragment {


    public static MealResultsFragment newInstance(int dayOffset, DiaryPageFragment.ENTRY_TYPE type) {
        MealResultsFragment fragment = new MealResultsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putSerializable(SearchFoodResultsFragment.ENTRY_TYPE_STRING, type);
        fragment.setArguments(args);
        return fragment;
    }

    public MealResultsFragment() {
        // Required empty public constructor
    }


    RelativeLayout noMealsRelativeLayout, mainRelativeLayout;
    Button createNewMealButton, createMealButton;
    RecyclerView mealsRecycler;
    MealResultsAdapter adapter;
    ProgressBar progressBar;
    CardView noResultsCardView;

    int dayOffset;
    DiaryPageFragment.ENTRY_TYPE type;
    FatSecretFoods foods;
    FatSecretRecipes recipes;

    int RETRY_COUNT = 0;
    ArrayList<MealInfo> mealsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_meal_results, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpMealCreateButton();
        setUpRecycler();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchCreatedMeals().execute();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_meals_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_meal:
                startCreateNewMealActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_CREATE) {
//            new FetchCreatedMeals().execute();
        }
        else if(resultCode == RESULT_OK && requestCode == CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_EDIT) {
            // This actually doesn't get caught. However things work out the way it's supposed to so I'm not complaining :P
        }
    }

    private void retrieveArguments() {
        this.dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        this.type = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(SearchFoodResultsFragment.ENTRY_TYPE_STRING);
    }

    private void bindViews(View v) {
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        noMealsRelativeLayout = (RelativeLayout) v.findViewById(R.id.no_meals_relative_layout);
        mainRelativeLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        createNewMealButton = (Button) v.findViewById(R.id.create_meal_button);
        createMealButton = (Button) v.findViewById(R.id.create_new_meal_button);
        mealsRecycler = (RecyclerView) v.findViewById(R.id.meal_results_recycler);
        noResultsCardView = (CardView) v.findViewById(R.id.no_results_card_view);
        foods = new FatSecretFoods();
        recipes = new FatSecretRecipes();
    }

    public void searchForMeal(String query) {
        filterOutFromMeals(query);
    }

    private void filterOutFromMeals(String query) {
        if(mealsList.isEmpty()) {
            return;
        }

        ArrayList<MealInfo> newList = new ArrayList<>();

        for(MealInfo info : mealsList) {
            if(info.getMealName().toLowerCase().contains(query.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);
        if(adapter.getIfDataSetEmpty()) {
            showNoResultsCardView(true);
        }
        else {
            showNoResultsCardView(false);
        }
    }

    private void setUpMealCreateButton() {
        createNewMealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateNewMealActivity();
            }
        });
        createMealButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateNewMealActivity();
            }
        });
    }

    private void setUpRecycler() {
        mealsList = new ArrayList<>();
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        mealsRecycler.setLayoutManager(lm);
        adapter = new MealResultsAdapter(mealsList, getContext(), SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_MEALS, type, dayOffset);
        mealsRecycler.setAdapter(adapter);
    }

    private void startCreateNewMealActivity() {
        Intent i = new Intent(getContext(), CreateOrEditMealActivity.class);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        i.putExtra(SearchFoodResultsFragment.ENTRY_TYPE_STRING, type);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_CREATE);
        startActivityForResult(i, CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_CREATE);
    }


    private String fetchServingSize(JSONArray array, long foodServingId, Number foodNumberOfServings) throws Exception {
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            long servingId = object.optLong(Constants.servingId, -1);
            if(servingId == foodServingId) {
                // DIRTY HACK IN PLAY, Can fail when things cannot be pluralized with the addition of an 's'
                try {
                    if((Integer) foodNumberOfServings == 1) {
                        return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown);
                    }
                    else {
                        return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                    }
                } catch(ClassCastException e) {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                }
            }
        }
        return foodNumberOfServings + " " + Constants.unknown;
    }

    private String fetchServingSize(JSONObject object, long foodServingId, Number foodNumberOfServings) throws Exception {
        long servingId = object.optLong(Constants.servingId, -1);
        if(servingId == foodServingId) {
            // DIRTY HACK IN PLAY. Can fail when things cannot be pluralized with the addition of an 's'
            try {
                if((Integer) foodNumberOfServings == 1) {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown);
                }
                else {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                }
            } catch(ClassCastException e) {
                return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
            }
        }
        else {
            return foodNumberOfServings + " " + Constants.unknown;
        }
    }

    private String fetchRecipeServingSize(JSONArray array, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        JSONObject object = array.getJSONObject(recipeServingOption);
        // This was the original line (below))
//        return foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown);
        // Doing this DIRTY HACK over here because all recipe's seem to have serving sizes of '1 serving'
        String servingSize = foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown).substring(2);
        try {
            if ((Integer) foodNumberOfServings == 1) {
                return servingSize;
            } else {
                return servingSize + "s";
            }
        }
        catch(ClassCastException e) {
            return servingSize + "s";
        }
    }

    private String fetchRecipeServingSize(JSONObject object, int recipeServingOption, Number foodNumberOfServings) throws Exception {
//        return foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown);
        // Doing this DIRTY HACK over here because all recipe's seem to have serving sizes of '1 serving'
        String servingSize = foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown).substring(2);
        try {
            if((Integer) foodNumberOfServings == 1) {
                return servingSize;
            } else {
                return servingSize + "s";
            }
        }
        catch (ClassCastException e) {
            return servingSize + "s";
        }
    }

    private double getCaloriesForFood(JSONArray array, long foodServingId, Number foodNumberOfServings) throws Exception {
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            long servingId = object.optLong(Constants.servingId, -1);
            if(servingId == foodServingId) {
                try {
                    return ((Double) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
                } catch (ClassCastException e) {
                    return ((Integer) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
                }
            }
        }
        return 0;
    }

    private double getCaloriesForFood(JSONObject object, long foodServingId, Number foodNumberOfServings) throws Exception {
        long servingId = object.optLong(Constants.servingId, -1);
        if(servingId == foodServingId) {
            try {
                return ((Double) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
            } catch (ClassCastException e) {
                return ((Integer) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
            }
        }
        else {
            return 0;
        }
    }

    private double getCaloriesForRecipe(JSONArray array, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        try {
            return array.getJSONObject(recipeServingOption).optDouble(Constants.calories, 0) * ((Double) foodNumberOfServings);
        } catch (ClassCastException e) {
            return array.getJSONObject(recipeServingOption).optDouble(Constants.calories, 0) * ((Integer) foodNumberOfServings);
        }
    }

    private double getCaloriesForRecipe(JSONObject object, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        try {
            return object.optDouble(Constants.calories, 0) * ((Double) foodNumberOfServings);
        } catch (ClassCastException e) {
            return object.optDouble(Constants.calories, 0) * ((Integer) foodNumberOfServings);
        }
    }

    private void fetchDetailedInformation(List<Integer> foodIds, List<Integer> foodServingIds, List<Number> foodNumberOfServings,
                                          List<Integer> recipeIds, List<Integer> recipeServingOptions,
                                          List<Number> recipeNumberOfServings, MealInfo mealInfo) throws Exception{

        List<String> foodAndRecipeNames = new ArrayList<>();
        List<String> servingSizes = new ArrayList<>();
        double totalNumberOfCaloriesForThisMeal = 0;

        // First we operate on foods, then we operate on recipes
        int foodSize = foodIds.size();
        for(int i = 0; i < foodSize; i++) {
            JSONObject result = foods.getFood(Long.valueOf(foodIds.get(i)));
            if(result != null) {
                foodAndRecipeNames.add(result.optString(Constants.foodName, Constants.unknown));
                JSONObject servingInfo = result.optJSONObject(Constants.servings);
                if(servingInfo != null) {
                    Object object = servingInfo.opt(Constants.serving);
                    if (object instanceof JSONObject) {
                        servingSizes.add(fetchServingSize((JSONObject) object, foodServingIds.get(i), foodNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForFood((JSONObject) object, foodServingIds.get(i), foodNumberOfServings.get(i));
                    } else {
                        servingSizes.add(fetchServingSize((JSONArray) object, foodServingIds.get(i), foodNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForFood((JSONArray) object, foodServingIds.get(i), foodNumberOfServings.get(i));
                    }
                }
            }
        }

        int recipeSize = recipeIds.size();
        for(int i = 0; i < recipeSize; i++) {
            JSONObject result = recipes.getRecipe(Long.valueOf(recipeIds.get(i)));
            if(result != null) {
                foodAndRecipeNames.add(result.optString(Constants.recipeName, Constants.unknown));
                JSONObject servingInfo = result.optJSONObject(Constants.servingSizes);
                if(servingInfo != null) {
                    Object object = servingInfo.opt(Constants.serving);
                    if (object instanceof JSONObject) {
                        servingSizes.add(fetchRecipeServingSize((JSONObject) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForRecipe((JSONObject) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i));
                    } else {
                        servingSizes.add(fetchRecipeServingSize((JSONArray) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForRecipe((JSONArray) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i));
                    }
                }
            }
        }

        mealInfo.setServingSizes(servingSizes);
        mealInfo.setFoodAndRecipeNames(foodAndRecipeNames);
        mealInfo.setTotalNumberOfCalories(Double.valueOf(totalNumberOfCaloriesForThisMeal).intValue());

        mealsList.add(mealInfo);

    }

    private void fetchCreatedMeals() throws Exception {
        mealsList.clear();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedMeal);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            String mealName = object.getString(Constants.mealName);
            String mealObjectId = object.getObjectId();

            List<Integer> foodIds = object.getList(Constants.foodIds);
            List<Integer> foodServingIds = object.getList(Constants.foodServingIds);
            List<Number> foodNumberOfServings = object.getList(Constants.foodNumberOfServings);

            List<Integer> recipeIds = object.getList(Constants.recipeIds);
            List<Number> recipeNumberOfServings = object.getList(Constants.recipeNumberOfServings);
            List<Integer> recipeServingOptions = object.getList(Constants.recipeServingOptions);

            MealInfo newMeal = new MealInfo();
            newMeal.setMealName(mealName);
            newMeal.setMealObjectId(mealObjectId);

            // Now for each of the foodIds and recipeIds, we must fetch food data... :(
            fetchDetailedInformation(foodIds, foodServingIds, foodNumberOfServings, recipeIds,
                    recipeServingOptions, recipeNumberOfServings, newMeal);
        }

    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void showMainLayout(boolean show) {
        setViewVisibility(mainRelativeLayout, show, true);
        setViewVisibility(progressBar, !show, true);
    }

    private void showNoResultsCardView(boolean show) {
        setViewVisibility(mealsRecycler, !show, true);
        setViewVisibility(noResultsCardView, show, true);
    }

    private void showGetStartedPage(boolean show) {
        setViewVisibility(noMealsRelativeLayout, show, true);
        setViewVisibility(mainRelativeLayout, !show, true);
    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class FetchCreatedMeals extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchCreatedMeals();
                return true;
            }
            catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    if (mealsList.isEmpty()) {
                        showGetStartedPage(true);
                        setViewVisibility(progressBar, false, true);
                    } else {
                        showMainLayout(true);
                        showGetStartedPage(false);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedMeals().execute();
                    }
                }
            }
        }
    }
}
