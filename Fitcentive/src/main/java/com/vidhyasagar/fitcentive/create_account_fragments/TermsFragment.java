package com.vidhyasagar.fitcentive.create_account_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsFragment extends Fragment {

    public static final String TAG = "fragment_terms";


    Button button;

    public TermsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_terms, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        button = (Button) getActivity().findViewById(R.id.button);
        button.setText(getString(R.string.s_finish));

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_terms));
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
