package com.vidhyasagar.fitcentive.create_account_fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BirthdayFragment extends Fragment {

    public static final String TAG = "fragment_birthday";

    Button button, birthdayButton;
    TextView birthdayDay, birthdayMonth, birthdayYear;
    boolean birthdayConfirmation = false;

    private int mYear, mMonth, mDay;

    public void updateOnScreenDate(int day, int month, int year) {
        birthdayDay.setText(String.valueOf(day));
        birthdayYear.setText(String.valueOf(year));
        String monthString;
        switch(month) {
            case 0: monthString = "Jan";
                    break;
            case 1: monthString = "Feb";
                break;
            case 2: monthString = "Mar";
                    break;
            case 3: monthString = "Apr";
                    break;
            case 4: monthString = "May";
                    break;
            case 5: monthString = "Jun";
                    break;
            case 6: monthString = "Jul";
                    break;
            case 7: monthString = "Aug";
                    break;
            case 8: monthString = "Sep";
                    break;
            case 9: monthString = "Oct";
                    break;
            case 10: monthString = "Nov";
                    break;
            case 11: monthString = "Dec";
                    break;
            default: monthString = "Unk";
                    break;
        }
        birthdayMonth.setText(monthString);
    }

    public int getBirthdayMonth() {
        String tempBirthday = birthdayMonth.getText().toString();
        switch(tempBirthday) {
            case "Jan" : return 1;
            case "Feb" : return 2;
            case "Mar" : return 3;
            case "Apr" : return 4;
            case "May" : return 5;
            case "Jun" : return 6;
            case "Jul" : return 7;
            case "Aug" : return 8;
            case "Sep" : return 9;
            case "Oct" : return 10;
            case "Nov" : return 11;
            case "Dec" : return 12;
            default    : return  0;

        }

    }


    public void createBirthdayConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.birthday_confirmation_heading))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        birthdayConfirmation = true;
                        ((CreateAccountActivity)getActivity()).replaceNextFragment(new View(getActivity()));
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.birthday_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getActivity().getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void popupDatePickerDialog() {

        // Get Current Date from the display on screen
        mYear = Integer.valueOf(birthdayYear.getText().toString());
        mMonth = getBirthdayMonth() - 1;
        mDay = Integer.valueOf(birthdayDay.getText().toString());

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateOnScreenDate(dayOfMonth, monthOfYear, year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    public boolean isBirthdayValid() {
        if(Integer.valueOf(birthdayDay.getText().toString()) == 12
                && Integer.valueOf(birthdayYear.getText().toString()) == 1995
                && getBirthdayMonth() == 10 && !birthdayConfirmation) {
            createBirthdayConfirmationDialog();
            if(birthdayConfirmation) {
                return true;
            }
            else {
                return false;
            }

        }
        else {
            return true;
        }
    }

    public BirthdayFragment() {
        // Required empty public constructor
    }

    public void getBirthday() {

        ((CreateAccountActivity)getActivity()).setBirthday(Integer.valueOf(birthdayDay.getText().toString()),
                getBirthdayMonth(),
                Integer.valueOf(birthdayYear.getText().toString()));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_birthday, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        button = (Button) getActivity().findViewById(R.id.button);
        birthdayButton = (Button) getActivity().findViewById(R.id.set_birthday_button);
        birthdayDay = (TextView) getActivity().findViewById(R.id.birthday_day);
        birthdayMonth = (TextView) getActivity().findViewById(R.id.birthday_month);
        birthdayYear = (TextView) getActivity().findViewById(R.id.birthday_year);

        birthdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupDatePickerDialog();
            }
        });

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) button.getLayoutParams();
        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.topMargin = 60;
        button.setLayoutParams(lp);

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_birthday));

    }

    @Override
    public void onStop() {
        getBirthday();
        super.onStop();

    }
}
