package com.vidhyasagar.fitcentive.create_account_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PasswordFragment extends Fragment {

    public static final String TAG = "fragment_password";


    EditText userPassword;
    EditText userPasswordConfirm;
    Button button;

    public boolean isPasswordValid() {
        if (TextUtils.isEmpty(userPassword.getText().toString())) {
            userPassword.setError(getString(R.string.error_field_required));
            userPassword.requestFocus();
            return false;
        }
        else if(!(userPassword.getText().toString().length() >= 6)) {
            userPassword.setError(getString(R.string.error_invalid_password));
            userPassword.requestFocus();
            return false;
        }
        else if (TextUtils.isEmpty(userPasswordConfirm.getText().toString())) {
            userPasswordConfirm.setError(getString(R.string.error_field_required));
            userPasswordConfirm.requestFocus();
            return false;
        }
        else if(!(userPasswordConfirm.getText().toString().length() >= 6)) {
            userPasswordConfirm.setError(getString(R.string.error_invalid_password));
            userPasswordConfirm.requestFocus();
            return false;
        }
        else if(!userPassword.getText().toString().equals(userPasswordConfirm.getText().toString())) {
            userPasswordConfirm.setError(getString(R.string.error_passwords_do_not_match));
            userPasswordConfirm.requestFocus();
            return false;
        }
        return true;
    }

    public PasswordFragment() {
        // Required empty public constructor
    }

    public void getPassword() {
        ((CreateAccountActivity)getActivity()).setPassword(userPassword.getText().toString());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_password, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        userPassword = (EditText) getActivity().findViewById(R.id.password);
        userPasswordConfirm = (EditText) getActivity().findViewById(R.id.password_confirm);
        button = (Button) getActivity().findViewById(R.id.button);

        // Resetting button margins
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) button.getLayoutParams();
        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.topMargin = 60;
        button.setLayoutParams(lp);

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_password));
    }

    @Override
    public void onStop() {
        getPassword();
        super.onStop();
    }
}
