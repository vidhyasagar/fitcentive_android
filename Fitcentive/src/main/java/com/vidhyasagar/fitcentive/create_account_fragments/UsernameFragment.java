package com.vidhyasagar.fitcentive.create_account_fragments;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;
import com.vidhyasagar.fitcentive.parse.Constants;

import java.util.concurrent.ExecutionException;

/**
 * A simple {@link Fragment} subclass.
 */
public class UsernameFragment extends Fragment {

    public static final String TAG = "fragment_username";


    AutoCompleteTextView userName;
    ProgressBar mProgressView;
    TextView textView;
    TextView textView2;
    TextView verifyingAccount;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // TODO : Future enhancement - animate hide and show of elements
    private void showUsernameValidationProgress() {
        userName.setVisibility(View.GONE);
        textView.setVisibility(View.GONE);
        textView2.setVisibility(View.GONE);
        verifyingAccount.setVisibility(View.VISIBLE);
        mProgressView.setVisibility(View.VISIBLE);
    }

    private void hideUsernameValidationProgress() {
        userName.setVisibility(View.VISIBLE);
        textView.setVisibility(View.VISIBLE);
        textView2.setVisibility(View.VISIBLE);
        verifyingAccount.setVisibility(View.GONE);
        mProgressView.setVisibility(View.GONE);
    }

    public boolean isUsernameValid() {
        String tempUsername = userName.getText().toString().trim();
        if (tempUsername.length() < 6) {
            userName.setError(getString(R.string.error_username_too_small));
            userName.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(tempUsername)) {
            userName.setError(getString(R.string.error_field_required));
            userName.requestFocus();
            return false;
        }

        if (tempUsername.contains(" ")) {
            userName.setError(getString(R.string.error_username_has_spaces));
            userName.requestFocus();
            return false;
        }

        CheckUsernameTask task = new CheckUsernameTask();
        boolean returnValue = false;
        try {
            returnValue = task.execute(tempUsername).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public UsernameFragment() {
        // Required empty public constructor
    }

    public void getUsername() {
        ((CreateAccountActivity)getActivity()).setUsername(userName.getText().toString().trim());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_username, container, false);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        userName = (AutoCompleteTextView) getActivity().findViewById(R.id.username);
        mProgressView = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
        textView = (TextView) getActivity().findViewById(R.id.textView);
        textView2 = (TextView) getActivity().findViewById(R.id.textView2);
        verifyingAccount = (TextView) getActivity().findViewById(R.id.verify_textview);

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_username));

    }

    @Override
    public void onStop() {
        getUsername();
        super.onStop();
    }

    private class CheckUsernameTask extends AsyncTask<String, Integer, Boolean> {

        boolean isNetworkConnectionAvailable = true;

        @Override
        protected Boolean doInBackground(String... params) {
            if(isNetworkAvailable()) {
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(Constants.username, params[0]);
                try {
                    if (query.count() != 0) {
                        // Username already exists, user must choose a different one
                        return false;
                    } else {
                        return true;
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else {
                isNetworkConnectionAvailable = false;
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean && isNetworkConnectionAvailable) {
                userName.setError(getString(R.string.error_username_taken));
                userName.requestFocus();
            }
            if(!isNetworkConnectionAvailable) {
                Toast.makeText(getActivity(), getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
            }
            hideUsernameValidationProgress();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showUsernameValidationProgress();
        }
    }

}
