package com.vidhyasagar.fitcentive.create_account_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class GenderFragment extends Fragment {

    public static final String TAG = "fragment_gender";


    RadioGroup radioGroup;
    Button button;
    boolean isMale;

    public boolean isGenderValid() {
        // Is the button now checked?
        if(radioGroup.getCheckedRadioButtonId() == -1) {
            Toast.makeText(getActivity(), getString(R.string.error_choose_gender), Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            return true;
        }
    }

    public GenderFragment() {
        // Required empty public constructor
    }

    public void onRadioButtonClicked() {
        // Is the button now checked?
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        boolean checked = ((RadioButton) radioButton).isChecked();

        // Check which radio button was clicked
        switch(radioButton.getId()) {
            case R.id.radio_male:
                if (checked)
                    isMale = true;
                    break;
            case R.id.radio_female:
                if (checked)
                    isMale = false;
                    break;
        }
    }

    public void getGender() {
        ((CreateAccountActivity)getActivity()).setGender(this.isMale);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gender, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        button = (Button) getActivity().findViewById(R.id.button);
        radioGroup = (RadioGroup) getActivity().findViewById(R.id.radio_group);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                onRadioButtonClicked();
            }
        });

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) button.getLayoutParams();
        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.topMargin = 60;
        button.setLayoutParams(lp);

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_gender));
    }

    @Override
    public void onStop() {
        getGender();
        super.onStop();
    }
}
