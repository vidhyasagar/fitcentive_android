package com.vidhyasagar.fitcentive.create_account_fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateAccountActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class NameFragment extends Fragment {

    public static final String TAG = "fragment_name";


    EditText userFirstName;
    EditText userLastName;

    public boolean isNameValid() {
        if (TextUtils.isEmpty(userFirstName.getText().toString())) {
            userFirstName.setError(getString(R.string.error_field_required));
            userFirstName.requestFocus();
            return false;
        }
        else if (TextUtils.isEmpty(userLastName.getText().toString())) {
            userLastName.setError(getString(R.string.error_field_required));
            userLastName.requestFocus();
            return false;
        }
        return true;
    }

    public NameFragment() {
        // Required empty public constructor
    }

    public void getNames() {
        ((CreateAccountActivity)getActivity()).setName(userFirstName.getText().toString().trim(), userLastName.getText().toString().trim());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_name, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        userFirstName = (EditText) getActivity().findViewById(R.id.first_name);
        userLastName = (EditText) getActivity().findViewById(R.id.last_name);

        android.support.v7.app.ActionBar bar = ((CreateAccountActivity) getActivity()).getSupportActionBar();
        bar.setTitle(getString(R.string.title_name));
    }

    @Override
    public void onStop() {
        getNames();
        super.onStop();
    }
}
