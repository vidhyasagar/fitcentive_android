package com.vidhyasagar.fitcentive.search_exercise_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.EditCardioActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.CardioAdapter;
import com.vidhyasagar.fitcentive.adapters.ExerciseResultsAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatedExercisesFragment extends Fragment {


    public static CreatedExercisesFragment newInstance(int dayOffset, boolean isReturningToWorkoutFragment) {
        CreatedExercisesFragment fragment = new CreatedExercisesFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public CreatedExercisesFragment() {
        // Required empty public constructor
    }

    int dayOffset;
    int RETRY_COUNT = 0;
    boolean isReturningToWorkoutFragment;

    HashMap<String, Double> createdCardioMETs;
    HashMap<String, String> createdCardioObjectIds;
    ArrayList<ExerciseInfo> exerciseList;
    ArrayList<String> cardioList;
    ExerciseResultsAdapter exerciseAdapter;
    CardioAdapter cardioAdapter;
    Switch cardioSwitch;
    RelativeLayout mainLayout, noExercisesLayout;
    ProgressBar progressBar;
    CardView noResultsCardView;
    RecyclerView exerciseResultsRecycler;
    ListView cardioResultsListView;
    Button createNewExerciseButton, createExerciseButton;
    ImageView exerciseLogo;
    TextView noExTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_created_exercises, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpExerciseCreateButton();
        setUpRecyclerAndListview();
        setUpSwitchListener();
        setUpCardioListListener();
        setUpMainLayoutKeyboardHideListener();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchCreatedExercises().execute();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        isReturningToWorkoutFragment = getArguments().getBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, false);
    }

    private void bindViews(View v) {
        createExerciseButton = (Button) v.findViewById(R.id.create_exercise_button);
        createNewExerciseButton = (Button) v.findViewById(R.id.create_new_ex_button);
        noResultsCardView = (CardView) v.findViewById(R.id.no_results_card_view);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        noExercisesLayout = (RelativeLayout) v.findViewById(R.id.no_created_exercises_rel_layout);
        exerciseResultsRecycler = (RecyclerView) v.findViewById(R.id.ex_results_recycler);
        cardioResultsListView = (ListView) v.findViewById(R.id.cardio_results_recycler);
        cardioSwitch = (Switch) v.findViewById(R.id.switch2);
        noExTextView = (TextView) v.findViewById(R.id.no_ex_text_view);
        exerciseLogo = (ImageView) v.findViewById(R.id.exercise_logo);
    }

    private void setUpSwitchListener() {
        cardioSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    showCardioGetStartedPageIfNeeded();
                }
                else {
                    showStrengthGetStartedPageIfNeeded();
                }
                showEmptyCardViewIfNeeded(isChecked);
            }
        });
    }

    private void setUpMainLayoutKeyboardHideListener() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });
    }

    public View getLayoutForSnackbar() {
        return this.mainLayout;
    }

    private void setNoExercisesImageView(boolean isCardio) {
        if(isCardio) {
            exerciseLogo.setImageResource(R.drawable.ic_cardio);
        }
        else {
            exerciseLogo.setImageResource(R.drawable.ic_exercise);
        }
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    // TODO: BUG WILL EXIST WHEN THERE ARE MULTIPLE EXERCISES WITH SAME NAME
    private void setUpCardioListListener() {
        cardioResultsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startDetailedCardioActivity(cardioList.get(position),
                        createdCardioMETs.get(cardioList.get(position)), createdCardioObjectIds.get(cardioList.get(position)));
            }
        });

        cardioResultsListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                bringUpLongClickOptionsMenu(cardioList.get(position), createdCardioObjectIds.get(cardioList.get(position)), position);
                return true;
            }
        });
    }

    private void bringUpLongClickOptionsMenu(final String name, final String createdCardioObjectId, final int position) {
        final CharSequence [] choices;
        choices = getResources().getStringArray(R.array.created_exercise_list_choices);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);

        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: copyToClipBoard(name);
                        break;
                    case 1: startEditCreatedCardioExerciseActivity(createdCardioObjectId);
                        break;
                    case 2: deleteCreatedCardioExercise(createdCardioObjectId, position);
                        break;
                    default: break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    // Deletion done in background here instead of using Asynctask
    private void deleteCreatedCardioExercise(final String objectId, int position) {
        // Delete from database, delete from current data set, notifyDataSetChanged
        final ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                object.deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null) {
                            e.printStackTrace();
                        }
                        else {
                            Utilities.showSnackBar(getString(R.string.exercise_deleted_successfully), mainLayout);
                        }
                    }
                });

                // Now proceeding to delete all diary entries which have THIS createdExercise in it
                ParseQuery<ParseObject> deleteQuery = new ParseQuery<>(Constants.ExerciseDiary);
                deleteQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
                deleteQuery.whereEqualTo(Constants.isCreated, true);
                deleteQuery.whereEqualTo(Constants.createdExerciseObjectId, objectId);

                deleteQuery.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if(e == null) {
                            for(ParseObject temp : objects) {
                                temp.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e != null) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        cardioList.remove(position);
        cardioAdapter.notifyDataSetChanged();
        showCardioGetStartedPageIfNeeded();
    }

    private void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(getString(R.string.food_name), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), getString(R.string.exercise_name_copied), Toast.LENGTH_SHORT).show();
    }

    private void showCardioGetStartedPageIfNeeded() {
        if(cardioList.isEmpty()) {
            showGetStartedPage(true, true);
        }
        else {
            showGetStartedPage(false, true);
            noExTextView.setText(getString(R.string.no_created_strength_exercises_yet));
            setNoExercisesImageView(true);
            setViewVisibility(exerciseResultsRecycler, false, true);
            setViewVisibility(cardioResultsListView, true, true);
        }
    }

    public void showStrengthGetStartedPageIfNeeded() {
        if(exerciseList.isEmpty()) {
            showGetStartedPage(true, false);
        }
        else {
            showGetStartedPage(false, false);
            noExTextView.setText(getString(R.string.no_created_strength_exercises_yet));
            setNoExercisesImageView(false);
            setViewVisibility(cardioResultsListView, false, true);
            setViewVisibility(exerciseResultsRecycler, true, true);
        }
    }

    private void showEmptyCardViewIfNeeded(boolean isCardio) {
        if(isCardio) {
            setViewVisibility(exerciseResultsRecycler, false, true);
            if(cardioAdapter.getIfDataSetEmpty()) {
                setViewVisibility(noResultsCardView, true, true);
            }
            else {
                setViewVisibility(noResultsCardView, false, true);
            }
        }
        else {
            setViewVisibility(cardioResultsListView, false, true);
            if(exerciseAdapter.getIfDataSetEmpty()) {
                setViewVisibility(noResultsCardView, true, true);
            }
            else {
                setViewVisibility(noResultsCardView, false, true);
            }
        }
    }

    private void startEditCreatedCardioExerciseActivity(String objectId) {
        Intent i = new Intent(getContext(), CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_EDIT);
        i.putExtra(Constants.isCardio, true);
        i.putExtra(Constants.createdExerciseObjectId, objectId);
        startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

    // This is opening edit cardio activity to take in info to save to diary
    private void startDetailedCardioActivity(String createdCardioExerciseName, double createdCardioMET ,
                                             String createdCardioObjectId) {
        Intent i = new Intent(getContext(), EditCardioActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(EditCardioActivity.createdCardioExerciseName, createdCardioExerciseName);
        i.putExtra(CardioFragment.CREATED_CARDIO_MET, createdCardioMET);
        i.putExtra(CardioFragment.CREATED_CARDIO_OBJECT_ID, createdCardioObjectId);
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        i.putExtra(Constants.isCreated, true);
        if(isReturningToWorkoutFragment) {
            getActivity().startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
        }
        else {
            startActivity(i);
        }
    }

    private void setUpExerciseCreateButton() {
        createExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateExerciseActivity(cardioSwitch.isChecked());
            }
        });

        createNewExerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateExerciseActivity(cardioSwitch.isChecked());
            }
        });
    }

    private void startCreateExerciseActivity(boolean isCardio) {
        Intent i = new Intent(getContext(), CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_CREATE);
        i.putExtra(Constants.isCardio, isCardio);
        startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

    private void setUpRecyclerAndListview() {
        cardioList = new ArrayList<>();
        exerciseList = new ArrayList<>();
        createdCardioMETs = new HashMap<>();
        createdCardioObjectIds = new HashMap<>();

        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        exerciseResultsRecycler.setLayoutManager(lm);

        cardioAdapter = new CardioAdapter(getContext(), -1, cardioList);
        exerciseAdapter = new ExerciseResultsAdapter(exerciseList, getContext(), dayOffset, true, this, isReturningToWorkoutFragment);

        cardioResultsListView.setAdapter(cardioAdapter);
        exerciseResultsRecycler.setAdapter(exerciseAdapter);

    }

    // This function fetches both cardio and strength exercises but into separate views
    private void fetchCreatedExercises() throws Exception {
        fetchCreatedStrengthExercises();
        fetchCreatedCardioExercises();
    }

    private ArrayList<Long> convertToArrayList(List<Integer> list) {
        ArrayList<Long> templist = new ArrayList<>();
        for(Integer number : list) {
            templist.add(Long.valueOf(number));
        }
        return templist;
    }

    private void fetchCreatedStrengthExercises() throws Exception {
        exerciseList.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.isCardio, false);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            ExerciseInfo exerciseInfo = new ExerciseInfo();
            exerciseInfo.setDescription(object.getString(Constants.exerciseDescription));
            exerciseInfo.setName(object.getString(Constants.exerciseName));
            exerciseInfo.setOriginalName(object.getString(Constants.exerciseName));
            exerciseInfo.setId(-1);
            exerciseInfo.setCategory(object.getLong(Constants.category));
            List<Integer> pmList = object.getList(Constants.primaryMusclesList);
            List<Integer> smList = object.getList(Constants.secondaryMusclesList);
            List<Integer> eqList = object.getList(Constants.equipmentList);
            exerciseInfo.setPrimaryMuscles(convertToArrayList(pmList));
            exerciseInfo.setSecondaryMuscles(convertToArrayList(smList));
            exerciseInfo.setEquipment(convertToArrayList(eqList));
            exerciseInfo.setCreated(true);
            exerciseInfo.setCreatedExerciseObjectId(object.getObjectId());

            exerciseList.add(exerciseInfo);

        }
    }

    private void fetchCreatedCardioExercises() throws Exception {
        cardioList.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.isCardio, true);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            cardioList.add(object.getString(Constants.exerciseName));
            createdCardioMETs.put(object.getString(Constants.exerciseName), object.getDouble(Constants.metValue));
            createdCardioObjectIds.put(object.getString(Constants.exerciseName), object.getObjectId());
        }
    }

    public void searchForExerciseAndCardio(String query) {
        filterFromDataSets(query);
    }

    private void filterFromDataSets(String query) {
        filterFromExercises(query);
        filterFromCardios(query);
    }

    private void filterFromExercises(String query) {
        if(exerciseList.isEmpty()) {
            return;
        }

        ArrayList<ExerciseInfo> tempList = new ArrayList<>();

        for(ExerciseInfo info : exerciseList) {
            if(info.getName().toLowerCase().contains(query.toLowerCase())) {
                tempList.add(info);
            }
        }

        exerciseAdapter.replaceDataSet(tempList);
        if(!cardioSwitch.isChecked()) {
            if (exerciseAdapter.getIfDataSetEmpty()) {
                showNoResultsCardView(true, false);
            } else {
                showNoResultsCardView(false, false);
            }
        }

    }

    private void filterFromCardios(String query) {
        if(cardioList.isEmpty()) {
            return;
        }

        ArrayList<String> tempList = new ArrayList<>();
        for(String name : cardioList) {
            if(name.toLowerCase().contains(query.toLowerCase())) {
                tempList.add(name);
            }
        }

        cardioAdapter.replaceDataSet(tempList);
        if(cardioSwitch.isChecked()) {
            if (cardioAdapter.getIfDataSetEmpty()) {
                showNoResultsCardView(true, true);
            } else {
                showNoResultsCardView(false, true);
            }
        }

    }

    private void showMainLayout(boolean show) {
        setViewVisibility(mainLayout, show, true);
        setViewVisibility(progressBar, !show, true);
    }

    private void showNoResultsCardView(boolean show, boolean isCardio) {
        if(isCardio) {
            setViewVisibility(cardioResultsListView, !show, true);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) noResultsCardView.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.cardio_results_listview);
            noResultsCardView.setLayoutParams(params);
        }
        else {
            setViewVisibility(exerciseResultsRecycler, !show, true);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) noResultsCardView.getLayoutParams();
            params.addRule(RelativeLayout.BELOW, R.id.exercise_results_recycler);
            noResultsCardView.setLayoutParams(params);
        }
        setViewVisibility(noResultsCardView, show, true);
    }

    private void showGetStartedPage(boolean show, boolean isCardio) {
        setViewVisibility(noExercisesLayout, show, true);
        setViewVisibility(mainLayout, !show, true);

        if(isCardio) {
            noExTextView.setText(getString(R.string.no_created_cardio_exercises_yet));
        }
        else {
            noExTextView.setText(getString(R.string.no_created_strength_exercises_yet));
        }
    }

    public boolean getIfCardio() {
        return cardioSwitch.isChecked();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class FetchCreatedExercises extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchCreatedExercises();
                return true;
            }
            catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    if(cardioSwitch.isChecked()) {
                        if(cardioList.isEmpty()) {
                            showGetStartedPage(true, true);
                            setViewVisibility(progressBar, false, true);
                        }
                        else {
                            showMainLayout(true);
                            showGetStartedPage(false, true);
                            setViewVisibility(cardioResultsListView, true, true);
                            setViewVisibility(exerciseResultsRecycler, false, true);
                            setViewVisibility(noResultsCardView, false, true);
                            cardioAdapter.notifyDataSetChanged();
                            exerciseAdapter.notifyDataSetChanged();
                        }
                    }
                    else {
                        if(exerciseList.isEmpty()) {
                            showGetStartedPage(true, false);
                            setViewVisibility(progressBar, false, true);
                        }
                        else {
                            showMainLayout(true);
                            showGetStartedPage(false, false);
                            setViewVisibility(cardioResultsListView, false, true);
                            setViewVisibility(exerciseResultsRecycler, true, true);
                            setViewVisibility(noResultsCardView, false, true);
                            cardioAdapter.notifyDataSetChanged();
                            exerciseAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedExercises().execute();
                    }
                }
            }
        }
    }

}
