package com.vidhyasagar.fitcentive.search_exercise_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ExerciseFilterActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.ExerciseResultsAdapter;
import com.vidhyasagar.fitcentive.api_requests.wger.ExerciseRequest;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;
import com.vidhyasagar.fitcentive.wrappers.FilterInfo;
import com.vidhyasagar.fitcentive.wrappers.RecipeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExerciseResultsFragment extends Fragment {

    public static String TAG = "EXERCISE_RESULTS_FRAGMENT";
    public static final String IS_RETURNING_TO_WORKOUT = "IS_RETURNING_TO_WORKOUT";

    public static final int REQUEST_FILTER = 47;

    public static long GERMAN = 1;
    public static long ENGLISH = 2;

    public static long PAGE_NUMBER = 1;

    int dayOffset;

    boolean isReturningToWorkoutFragment;
    int RETRY_COUNT = 0;

    ExerciseRequest exerciseRequest;

    ArrayList<ExerciseInfo> exerciseList;
    LinearLayoutManager llm;
    ExerciseResultsAdapter adapter;

    ProgressBar progressBar;
    RelativeLayout relativeLayout;
    RecyclerView recyclerView;
    CardView noResultsCardView;
    TextView noResultsTextView;
    TextView redoPrompt;

    // Filter info
    FilterInfo filterInfo;
    Button filterButton;
    boolean isFilterSet;

    public static ExerciseResultsFragment newInstance(int dayOffset, boolean isReturningToWorkoutFragment) {
        ExerciseResultsFragment fragment = new ExerciseResultsFragment();
        Bundle args = new Bundle();
        args.putInt(SearchFoodResultsFragment.DAY_OFFSET_STRING, dayOffset);
        args.putBoolean(IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public ExerciseResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpRecycler();
        setUpFilterButton();

        isFilterSet = false;

        new FetchAllExercises().execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_FILTER && resultCode == RESULT_OK) {
            filterInfo =  data.getParcelableExtra(ExerciseFilterActivity.FILTER_INFO);
            filterButton.setText(getString(R.string.clear_filters));
            isFilterSet = true;
            adapter.resetFetchFlag();
            new FetchAllExercises().execute();
        }
    }

    private void startFilterActivity() {
        Intent i = new Intent(getContext(), ExerciseFilterActivity.class);
        startActivityForResult(i, REQUEST_FILTER);
    }

    private void clearFilters() {
        filterInfo = null;
        isFilterSet = false;
        filterButton.setText(getString(R.string.set_filters));
        adapter.resetFetchFlag();
        new FetchAllExercises().execute();
    }

    private void setUpFilterButton() {
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isFilterSet) {
                    clearFilters();
                }
                else {
                    startFilterActivity();
                }
            }
        });
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(SearchFoodResultsFragment.DAY_OFFSET_STRING);
        isReturningToWorkoutFragment = getArguments().getBoolean(IS_RETURNING_TO_WORKOUT, false);
    }

    private void bindViews(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.main_relative_layout);
        recyclerView = (RecyclerView) view.findViewById(R.id.exercise_results_recycler);
        noResultsCardView = (CardView) view.findViewById(R.id.no_results_card_view);
        noResultsTextView = (TextView) view.findViewById(R.id.no_results_found_textview);
        redoPrompt = (TextView) view.findViewById(R.id.no_results_redo_prompt);
        filterButton = (Button) view.findViewById(R.id.filter_button);
        exerciseRequest = new ExerciseRequest();
    }

    private void setUpRecycler() {
        exerciseList = new ArrayList<>();
        llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        adapter = new ExerciseResultsAdapter(exerciseList, getActivity(), dayOffset, false, isReturningToWorkoutFragment);
        recyclerView.setAdapter(adapter);
    }

    private void filterFromDataSet(String exerciseName) {
        if(exerciseList.isEmpty()) {
            return;
        }

        ArrayList<ExerciseInfo> newList = new ArrayList<>();

        for(ExerciseInfo info : exerciseList) {
            if(info.getName().toLowerCase().contains(exerciseName.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);
        if(adapter.getIfDataSetEmpty()) {
            hideOrShowRecyclerAndShowEmptyCard(false);
        }
        else {
            hideOrShowRecyclerAndShowEmptyCard(true);
        }
    }

    public void searchForExercise(String exerciseName) {
        filterFromDataSet(exerciseName);
    }

    public static void extractIntoList(JSONArray array, ArrayList<Long> list) throws JSONException {
        int size = array.length();
        for(int i = 0; i < size; i++) {
            list.add(array.getLong(i));
        }
    }

    public static String padDescription(String desc) {
        desc = "<html><body>" + desc + "</body></html>";
        return desc;
    }

    private void searchForExercises() throws JSONException {
        exerciseList.clear();
        String nextUrl;
        JSONObject response;
        if(isFilterSet) {
            response = exerciseRequest.wgerSearchExerciseWithFilter(ENGLISH, PAGE_NUMBER, filterInfo);
        }
        else {
            response = exerciseRequest.wgerExerciseSearchAll(ENGLISH, PAGE_NUMBER, -1, -1);
        }
        if(response != null) {
            nextUrl = response.optString(Constants.next, Constants.unknown);
            JSONArray results = response.optJSONArray(Constants.results);
            if (results != null) {
                int size = results.length();
                for (int i = 0; i < size; i++) {
                    JSONObject object = results.getJSONObject(i);

                    String description = object.optString(Constants.description, Constants.unknown);
                    description = padDescription(description);
                    String name = object.optString(Constants.name, Constants.unknown);
                    long id = object.optLong(Constants.id, -1);
                    String originalName = object.optString(Constants.originalName, Constants.unknown);
                    long category = object.optLong(Constants.category, -1);

                    JSONArray musclesArray = object.optJSONArray(Constants.muscles);
                    JSONArray secondaryMusclesArray = object.optJSONArray(Constants.secondaryMuscles);
                    JSONArray equipmentArray = object.optJSONArray(Constants.equipment);

                    ArrayList<Long> muscles = new ArrayList<>();
                    ArrayList<Long> secondaryMuscles = new ArrayList<>();
                    ArrayList<Long> equipment = new ArrayList<>();
                    extractIntoList(musclesArray, muscles);
                    extractIntoList(secondaryMusclesArray, secondaryMuscles);
                    extractIntoList(equipmentArray, equipment);

                    ExerciseInfo exerciseInfo = new ExerciseInfo();
                    exerciseInfo.setDescription(description);
                    exerciseInfo.setName(name);
                    exerciseInfo.setOriginalName(originalName);
                    exerciseInfo.setId(id);
                    exerciseInfo.setCategory(category);
                    exerciseInfo.setPrimaryMuscles(muscles);
                    exerciseInfo.setSecondaryMuscles(secondaryMuscles);
                    exerciseInfo.setEquipment(equipment);
                    exerciseInfo.setCreated(false);

                    exerciseList.add(exerciseInfo);

                }
            }
            adapter.setNextUrlGlobal(nextUrl);
        }
    }


    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void hideOrShowRecyclerAndShowEmptyCard(boolean showRecycler) {
        setViewVisibility(recyclerView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
        noResultsTextView.setText(getString(R.string.no_results_found));
        redoPrompt.setText(getString(R.string.no_results_redo_prompt));
    }


    //--------------------------------
    // ASYNCTASK(S)
    //--------------------------------


    private class FetchAllExercises extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(relativeLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                searchForExercises();
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(relativeLayout, true, true);
                    if (exerciseList.isEmpty()) {
                        hideOrShowRecyclerAndShowEmptyCard(false);
                    } else {
                        hideOrShowRecyclerAndShowEmptyCard(true);
                        adapter.replaceDataSet(exerciseList);
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchAllExercises().execute();
                    }
                }
            }
        }
    }

}
