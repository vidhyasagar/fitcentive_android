package com.vidhyasagar.fitcentive.search_exercise_fragments;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.WorkoutResultsAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.ArrayList;
import java.util.List;

import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_CREATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class WorkoutsFragment extends Fragment {

    public static WorkoutsFragment newInstance(int dayOffset) {
        WorkoutsFragment fragment = new WorkoutsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        fragment.setArguments(args);
        return fragment;
    }

    public WorkoutsFragment() {
        // Required empty public constructor
    }

    int RETRY_COUNT = 0;
    int dayOffset;

    ArrayList<CreatedWorkoutInfo> workoutList;

    ProgressBar progressBar;
    RelativeLayout noWorkoutsRelativeLayout, mainRelativeLayout;
    RecyclerView workoutsRecycler;
    WorkoutResultsAdapter adapter;
    CardView noResultsCardView;
    Button createNewWorkoutButton, createWorkoutButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_workouts, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpCreateButtons();
        setUpRecycler();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchCreatedWorkouts().execute();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
    }

    private void bindViews(View v) {
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        noWorkoutsRelativeLayout = (RelativeLayout) v.findViewById(R.id.no_workouts_relative_layout);
        mainRelativeLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        createNewWorkoutButton = (Button) v.findViewById(R.id.create_workout_button);
        createWorkoutButton = (Button) v.findViewById(R.id.create_new_workout_button);
        workoutsRecycler = (RecyclerView) v.findViewById(R.id.workout_results_recycler);
        noResultsCardView = (CardView) v.findViewById(R.id.no_results_card_view);
    }

    private void setUpCreateButtons() {
        createNewWorkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateWorkoutActivity();
            }
        });

        createWorkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startCreateWorkoutActivity();
            }
        });
    }

    public void startCreateWorkoutActivity() {
        Intent i = new Intent(getContext(), CreateOrEditWorkoutActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_CREATE);
        startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }

    private void setUpRecycler() {
        workoutList = new ArrayList<>();
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        workoutsRecycler.setLayoutManager(lm);
        adapter = new WorkoutResultsAdapter(workoutList, getContext(), dayOffset);
        workoutsRecycler.setAdapter(adapter);
    }

    public void searchForWorkout(String query) {
        filterOutFromWorkouts(query);
    }

    private void filterOutFromWorkouts(String query) {
        if(workoutList.isEmpty()) {
            return;
        }

        ArrayList<CreatedWorkoutInfo> newList = new ArrayList<>();

        for(CreatedWorkoutInfo info : workoutList) {
            if(info.getWorkoutName().toLowerCase().contains(query.toLowerCase())) {
                newList.add(info);
            }
        }

        adapter.replaceDataSet(newList);
        if(adapter.getIfDataSetEmpty()) {
            showNoResultsCardView(true);
        }
        else {
            showNoResultsCardView(false);
        }
    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(mainRelativeLayout, show, true, getContext());
        Utilities.setViewVisibility(noWorkoutsRelativeLayout, show, true, getContext());
        Utilities.setViewVisibility(progressBar, !show, true, getContext());
    }

    private void showNoResultsCardView(boolean show) {
        Utilities.setViewVisibility(workoutsRecycler, !show, true, getContext());
        Utilities.setViewVisibility(noResultsCardView, show, true, getContext());
    }

    private void showGetStartedPage(boolean show) {
        Utilities.setViewVisibility(noWorkoutsRelativeLayout, show, true, getContext());
        Utilities.setViewVisibility(mainRelativeLayout, !show, true, getContext());
    }

    private String cardioFormat(String name, double minutes) {
        return name + ", " + Double.valueOf(minutes).intValue() + " minutes";
    }

    private void fetchCardioComponents(CreatedWorkoutInfo info, List<String> names) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.workoutId, info.getWorkoutObjectId());
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());

        double totalNumberOfCalories = 0;
        List<ParseObject> result = query.find();

        for(ParseObject object : result) {

            CardioInfo newInfo = new CardioInfo();
            newInfo.setName(object.getString(Constants.exerciseName));
            newInfo.setMinutesPerformed(object.getDouble(Constants.minutesPerformed));
            newInfo.setCaloriesBurned(object.getDouble(Constants.caloriesBurned));

            names.add(cardioFormat(newInfo.getName(), newInfo.getMinutesPerformed()));
            totalNumberOfCalories += newInfo.getCaloriesBurned();
        }

        info.setTotalNumberOfCalories(Double.valueOf(totalNumberOfCalories).intValue());
    }

    // Note that while adding exerciseName to workoutList, the name is formatted. Refer to CreatedWorkoutInfo.java
    private void fetchStrengthComponents(CreatedWorkoutInfo info, List<String> names) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, info.getWorkoutObjectId());

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {

            ExerciseInfo newInfo = new ExerciseInfo();
            newInfo.setName(object.getString(Constants.exerciseName));
            newInfo.setNumberOfSets(object.getInt(Constants.numberOfSets));
            newInfo.setRepsPerSet(object.getDouble(Constants.repsPerSet));
            names.add(fiestyFormat(newInfo.getName(), newInfo.getNumberOfSets(), newInfo.getRepsPerSet()));
        }
    }

    private String fiestyFormat(String name, int numberOfSets, double repsPerSet) {
        return name + ", " + numberOfSets + " sets, " + Double.valueOf(repsPerSet).intValue() + " reps/set";
    }

    private void fetchMoreDetailedWorkoutInfo(CreatedWorkoutInfo info) throws Exception{
        List<String> exNames = new ArrayList<>();
        fetchCardioComponents(info, exNames);
        fetchStrengthComponents(info, exNames);
        info.setExerciseNames(exNames);
    }

    private void fetchCreatedWorkouts() throws Exception {
        workoutList.clear();
        // Fetch all objectIds of created workout
        // Fetch individual components of each created workout
        // Model closely resembles that of MealResultsFragment
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkout);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());

        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            CreatedWorkoutInfo workoutInfo = new CreatedWorkoutInfo();
            workoutInfo.setWorkoutName(object.getString(Constants.workoutName));
            workoutInfo.setWorkoutObjectId(object.getObjectId());
            fetchMoreDetailedWorkoutInfo(workoutInfo);
            workoutList.add(workoutInfo);
        }
    }

    private class FetchCreatedWorkouts extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchCreatedWorkouts();
                return true;
            }
            catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    if (workoutList.isEmpty()) {
                        showGetStartedPage(true);
                        Utilities.setViewVisibility(progressBar, false, true, getContext());
                    } else {
                        showMainLayout(true);
                        showGetStartedPage(false);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedWorkouts().execute();
                    }
                }
            }
        }
    }

}
