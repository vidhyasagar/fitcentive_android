package com.vidhyasagar.fitcentive.search_exercise_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.MainActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.CardioAdapter;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.FitnessFormulae;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardioFragment extends Fragment {

    public static final String TAG = "CARDIO_FRAGMENT";
    public static final String CREATED_CARDIO_NAME = "CREATED_CARDIO_NAME";
    public static final String CREATED_CARDIO_OBJECT_ID = "CREATED_CARDIO_OBJECT_ID";
    public static final String CREATED_CARDIO_MET = "CREATED_CARDIO_MET";

    public static CardioFragment newInstance(int dayOffset, boolean isReturningToWorkoutFragment) {
        CardioFragment fragment = new CardioFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public static CardioFragment newInstance(int dayOffset, CardioInfo info, boolean isCreated,  boolean isReturningToWorkoutFragment) {
        CardioFragment fragment = new CardioFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putBoolean(Constants.isCreated, isCreated);
        args.putBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        args.putParcelable(ExpandedExerciseActivity.INFO, info);
        fragment.setArguments(args);
        return fragment;
    }

    public static CardioFragment newInstance(int dayOffset, String createdCardioName,
                                             double createdCardioMET, boolean isCreated,
                                             String createdExerciseObjectId, boolean isReturningToWorkoutFragment) {
        CardioFragment fragment = new CardioFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putString(CREATED_CARDIO_NAME, createdCardioName);
        args.putString(CREATED_CARDIO_OBJECT_ID, createdExerciseObjectId);
        args.putDouble(CREATED_CARDIO_MET, createdCardioMET);
        args.putBoolean(Constants.isCreated, isCreated);
        args.putBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
        fragment.setArguments(args);
        return fragment;
    }

    public CardioFragment() {
        // Required empty public constructor
    }

    int dayOffset;
    int RETRY_COUNT = 0;
    CardioInfo info;
    boolean isEditMode;
    boolean isCreated;
    boolean isReturningToCreateWorkoutActivity;

    String createdCardioName;
    double createdCardioMET;
    String createdCardioObjectId;

    ArrayList<String> cardioNames;
    HashMap<String, Double> cardioMETs;

    TextView cardioName;
    ImageView selectButton;
    RelativeLayout nameLayout;

    EditText minutesPerformed, caloriesBurnt;
    SeekBar seekBar;
    RelativeLayout mainLayout;
    Button addToDiaryButton;
    ProgressBar progressBar;

    CardView noResultsCardView;
    ListView cardioListView;
    CardioAdapter adapter;
    FloatingSearchView floatingSearchView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_cardio, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        fetchCardioExercisesFromFile();
        sortCardioExercises();
        bindViews(view);
        setUpSeekBarBehaviour();
        setUpMinutesPerformedBehaviour();
        setUpNameLayoutListener();
        setUpAddToDiaryButton();
        setUpCardioName();
        changeLayoutIfNeedToFillUp();

        if(isEditMode) {
            populateOnScreenData();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_cardio, menu);
        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(dayOffset, getContext())));

        if(isReturningToCreateWorkoutActivity) {
            dateItem.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                validateAndSaveCurrentCardio();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            default:
                return false;
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), addToDiaryButton);
        }
        else {
            if(mMonth == month) {
                dayOffset = day - mDay;
            }
            else {
                dayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, dayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        getActivity().invalidateOptionsMenu();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        isCreated = getArguments().getBoolean(Constants.isCreated, false);
        isReturningToCreateWorkoutActivity = getArguments().getBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, false);
        createdCardioName = getArguments().getString(CREATED_CARDIO_NAME, Constants.unknown);
        createdCardioObjectId = getArguments().getString(CREATED_CARDIO_OBJECT_ID, Constants.unknown);
        createdCardioMET = getArguments().getDouble(CREATED_CARDIO_MET, -1);
        info = getArguments().getParcelable(ExpandedExerciseActivity.INFO);
        if(info != null) {
            isEditMode = true;
        }
        else {
            isEditMode = false;
        }
    }

    private void bindViews(View view) {
        caloriesBurnt = (EditText) view.findViewById(R.id.calories_burnt_edittext);
        minutesPerformed = (EditText) view.findViewById(R.id.minutes_performed_edittext);
        cardioName = (TextView) view.findViewById(R.id.cardio_name);
        selectButton = (ImageView) view.findViewById(R.id.expand_button);
        nameLayout = (RelativeLayout) view.findViewById(R.id.name_layout);
        seekBar = (SeekBar) view.findViewById(R.id.seek_bar);
        mainLayout = (RelativeLayout) view.findViewById(R.id.main_relative_layout);
        addToDiaryButton = (Button) view.findViewById(R.id.add_to_diary_button);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
    }

    private void setUpCardioName() {
        cardioName.setText(cardioNames.get(0));
    }

    private void populateOnScreenData() {
        if(isCreated) {
            cardioName.setText(info.getName());
            minutesPerformed.setText(String.valueOf(info.getMinutesPerformed()));
            caloriesBurnt.setText(String.valueOf(info.getCaloriesBurned()));
            // Work still needs to be done, we need to fetch the MET value
            // This can be done silently in the background
            // TODO: POTENTIAL SOURCE OF BUGS, IF USER UPDATES MINUTES BEFORE THIS IS DONE
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        ParseQuery<ParseObject> createdQuery = new ParseQuery<ParseObject>(Constants.CreatedExercise);
                        ParseObject created = createdQuery.get(info.getCreatedCardioObjectId());
                        createdCardioMET = created.getDouble(Constants.metValue);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }
        else {
            cardioName.setText(info.getName());
            minutesPerformed.setText(String.valueOf(info.getMinutesPerformed()));
            caloriesBurnt.setText(String.valueOf(info.getCaloriesBurned()));
        }
    }

    private void changeLayoutIfNeedToFillUp() {
        if(!createdCardioName.equals(Constants.unknown)) {
            nameLayout.setOnClickListener(null);
            selectButton.setVisibility(View.GONE);
            cardioName.setText(createdCardioName);
        }

        // isCreated only comes into play when createdCardioName is UNKNOWN
        if(isCreated) {
            nameLayout.setOnClickListener(null);
            selectButton.setVisibility(View.GONE);
        }
    }

    private void fetchCardioExercisesFromFile() {
        cardioNames = new ArrayList<>();
        cardioMETs = new HashMap<>();
        InputStream is = getResources().openRawResource(R.raw.cardio_exercises);
        String str = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            if (is != null) {
                while ((str = reader.readLine()) != null) {
                    String[] parts = str.split(":");
                    cardioNames.add(parts[0]);
                    cardioMETs.put(parts[0], Double.valueOf(parts[1]));
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try { is.close(); } catch (Throwable ignore) {}
        }
    }

    private void sortCardioExercises() {
        Collections.sort(cardioNames, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                return lhs.compareToIgnoreCase(rhs);
            }
        });
    }

    private void setUpSeekBarBehaviour() {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                minutesPerformed.setText(String.valueOf(progress));
                minutesPerformed.setSelection(minutesPerformed.getText().length());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
    }

    private void setUpMinutesPerformedBehaviour() {
        minutesPerformed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                caloriesBurnt.setText(getCaloriesBurnt(s.toString()));
                if(s.toString().isEmpty()) {
                    seekBar.setProgress(0);
                }
                else{
                    seekBar.setProgress(Double.valueOf(s.toString()).intValue());
                }
            }
        });
    }


    // TODO: Make weight a pipelined input from USERPROFILE: TBI
    private String getCaloriesBurnt(String s) {
        double weightInPounds = 180;
        double minutes;
        double calories;
        if(s.isEmpty()) {
            minutes = 0;
        }
        else {
            minutes = Double.valueOf(s);
        }

        if(createdCardioName.equals(Constants.unknown)) {
            if(isCreated) {
                calories = FitnessFormulae.getCaloriesBurned(weightInPounds, createdCardioMET, minutes);
            }
            else {
                calories = FitnessFormulae.getCaloriesBurned(weightInPounds, cardioMETs.get(cardioName.getText().toString()), minutes);
            }
        }
        else {
            calories = FitnessFormulae.getCaloriesBurned(weightInPounds, createdCardioMET, minutes);
        }
        return String.valueOf(Math.round(calories * 100.0) / 100.0);
    }

    private void setUpNameLayoutListener() {
        nameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCardioListDialog();
            }
        });
    }

    public void createCardioListDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.cardio_activity));
        builder.setPositiveButton(getString(R.string.close), null);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_cardio_list_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_serving_size_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();

        noResultsCardView = (CardView) dialog.findViewById(R.id.no_results_card_view);
        cardioListView = (ListView) dialog.findViewById(R.id.cardio_results_listview);
        floatingSearchView = (FloatingSearchView) dialog.findViewById(R.id.floating_search_view);

        adapter = new CardioAdapter(getContext(), -1, cardioNames);
        cardioListView.setAdapter(adapter);

        cardioListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedCardio = adapter.getDataSet().get(position);
                replaceOnScreenSelectedCardio(selectedCardio);
                caloriesBurnt.setText(getCaloriesBurnt(minutesPerformed.getText().toString()));
                dialog.dismiss();
            }
        });

        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                // Filters out our current dataset
                filterFromDataSet(newQuery.trim());
            }
        });

        floatingSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                View v = dialog.getCurrentFocus();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                View v = dialog.getCurrentFocus();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

            @Override
            public void onSearchAction(String currentQuery) {
                View v = dialog.getCurrentFocus();
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

    }

    // METHOD IS A PART OF DIALOG CREATED
    private void filterFromDataSet(String query) {
        if(cardioNames.isEmpty()) {
            return;
        }

        ArrayList<String> templist = new ArrayList<>();
        for(String name : cardioNames) {
            if(name.toLowerCase().contains(query.toLowerCase())) {
                templist.add(name);
            }
        }

        adapter.replaceDataSet(templist);
        if(adapter.getIfDataSetEmpty()) {
            hideOrShowRecyclerAndShowEmptyCard(false);
        }
        else {
            hideOrShowRecyclerAndShowEmptyCard(true);
        }

    }

    // METHOD IS A PART OF THE DIALOG CREATED
    private void hideOrShowRecyclerAndShowEmptyCard(boolean showRecycler) {
        setViewVisibility(cardioListView, showRecycler, true);
        setViewVisibility(noResultsCardView, !showRecycler, true);
    }

    private void replaceOnScreenSelectedCardio(String name) {
        cardioName.setText(name);
    }

    private void editCurrentCardioActivity() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        ParseObject object = query.get(info.getObjectId());

        object.put(Constants.exerciseName, cardioName.getText().toString());
        object.put(Constants.minutesPerformed, Double.valueOf(minutesPerformed.getText().toString()));
        object.put(Constants.caloriesBurned, Double.valueOf(caloriesBurnt.getText().toString()));
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));

        object.save();
    }

    private void saveCurrentCreatedCardioExerciseTemporarily() throws Exception {
        ParseObject newObject = new ParseObject(Constants.ExerciseDiary);

        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.exerciseType, getString(R.string.cardio_type));

        newObject.put(Constants.exerciseName, cardioName.getText().toString());
        newObject.put(Constants.minutesPerformed, Double.valueOf(minutesPerformed.getText().toString()));
        newObject.put(Constants.caloriesBurned, Double.valueOf(caloriesBurnt.getText().toString()));

        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        newObject.put(Constants.isCreated, true);

        // Object specific details pertaining to created exercises
        newObject.put(Constants.createdExerciseObjectId, createdCardioObjectId);

        newObject.save();
    }

    private void saveCurrentCardioExerciseTemporarily() throws Exception {
        ParseObject newObject = new ParseObject(Constants.ExerciseDiary);

        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.exerciseType, getString(R.string.cardio_type));

        newObject.put(Constants.exerciseName, cardioName.getText().toString());
        newObject.put(Constants.minutesPerformed, Double.valueOf(minutesPerformed.getText().toString()));
        newObject.put(Constants.caloriesBurned, Double.valueOf(caloriesBurnt.getText().toString()));

        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        newObject.put(Constants.isCreated, false);

        newObject.save();
    }

    private void returnToWorkoutComponentsFragment() {
        Intent i = new Intent();
        CardioInfo info = new CardioInfo();
        info.setName(cardioName.getText().toString());
        info.setMinutesPerformed(Double.valueOf(minutesPerformed.getText().toString()));
        info.setCaloriesBurned(Double.valueOf(caloriesBurnt.getText().toString()));
        info.setCreated(isCreated);
        if(isCreated) {
            info.setCreatedCardioObjectId(createdCardioObjectId);
        }
        i.putExtra(ExpandedExerciseActivity.INFO, info);

        if(isCreated) {
            getActivity().setResult(CreateOrEditWorkoutActivity.RESULT_CREATED_CARDIO, i);
        }
        else {
            getActivity().setResult(CreateOrEditWorkoutActivity.RESULT_CARDIO, i);
        }
        getActivity().finish();
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(getContext(), MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        startActivity(i);
        getActivity().finish();
    }

    private void setUpAddToDiaryButton() {
        addToDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndSaveCurrentCardio();
            }
        });

        if(isEditMode) {
            addToDiaryButton.setText(getString(R.string.save));
        }

        if(isReturningToCreateWorkoutActivity) {
            addToDiaryButton.setText(getString(R.string.add_to_workout));
        }
    }

    private void validateAndSaveCurrentCardio() {
        if(isValid()) {
            new AddOrEditCurrentCardioExerciseToDiary().execute();
        }
        else {
            handleErrors();
        }
    }

    private boolean isValid() {
        return (!minutesPerformed.getText().toString().isEmpty() && Double.valueOf(minutesPerformed.getText().toString()) != 0
                && !caloriesBurnt.getText().toString().isEmpty());
    }

    private void handleErrors() {
        if(minutesPerformed.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.minutes_performed_required));
        }
        else if(Double.valueOf(minutesPerformed.getText().toString()) == 0) {
            showSnackBar(getString(R.string.minutes_performed_cannot_be_0));
        }
        else if(caloriesBurnt.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.calories_burnt_required));
        }
    }

    private void showSnackBar(String message) {
        Snackbar snackbar =  Snackbar.make(mainLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }


    //-----------------------------------------
    // ASYNCTASK(S)
    //-----------------------------------------

    private class AddOrEditCurrentCardioExerciseToDiary extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isReturningToCreateWorkoutActivity) {
                    if (createdCardioName.equals(Constants.unknown)) {
                        if (isEditMode) {
                            editCurrentCardioActivity();
                        } else {
                            saveCurrentCardioExerciseTemporarily();
                        }
                    } else {
                        // This is the special case in which a created Cardio exercise needs more input
                        // before it is saved to diary. Currenty now proceed to save it
                        saveCurrentCreatedCardioExerciseTemporarily();
                    }
                }
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if(aBoolean) {
                    if(!isReturningToCreateWorkoutActivity) {
                            returnToDiaryPage();
                    }
                    else {
                        returnToWorkoutComponentsFragment();
                    }
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new AddOrEditCurrentCardioExerciseToDiary().execute();
                    }
                }
            }
        }
    }
}
