package com.vidhyasagar.fitcentive.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.activities.SearchFoodActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.CarbsDetailedInfoDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.FatBreakdownDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.VitaminsAndMineralsDialogFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailedFoodSearchResultsFragment extends Fragment {

    public static String TAG = "DETAILED_SEARCH_RESULTS_FRAGMENT";

    int CURRENT_SERVING_OPTION = 0;
    int EXTRACT_DATA_FUNCTION_COUNT = 0;
    boolean IS_PART_OF_VIEWPAGER;
    boolean isFromBarcode;

    long foodId;
    long servingIdGlobal;
    double numberOfServings;

    // This is an arraylist because it could contain multiple different serving descriptions
    ArrayList<DetailedFoodResultsInfo> detailedInfoList;
    ArrayList<Entry> breakdown;
    double totalFatCarbsAndProteins;
    FatSecretFoods food;

    PieChart pieChart;
    TextView foodBrand, foodName, foodServingSize;
    TextView fats, carbs, proteins, calories;
    Button previousButton, nextButton, addToDiaryButton;
    EditText servingSizeEditText;
    TextView servingSize;
    TextView vitaminsAndMinerals;
    Spinner dialogServingSizeSpinner;
    LinearLayout linearLayout;
    LinearLayout parentLayout;
    LinearLayout carbsLayout, proteinsLayout, fatsLayout;
    ProgressBar progressBar;

    public DetailedFoodSearchResultsFragment() {
        // Required empty public constructor
    }


    public static DetailedFoodSearchResultsFragment newInstance(long foodIdLong, boolean isPartOfViewPager) {
        DetailedFoodSearchResultsFragment fragment = new DetailedFoodSearchResultsFragment();
        Bundle args = new Bundle();
        args.putLong(ExpandedSearchFoodActivity.FOOD_ID, foodIdLong);
        args.putBoolean(ExpandedSearchFoodActivity.IS_PART_OF_VIEWPAGER, isPartOfViewPager);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailedFoodSearchResultsFragment newInstance(long foodIdLong, boolean isPartOfViewPager,
                                                                long servingId, double numberOfServings,
                                                                boolean isFromBarcode) {
        DetailedFoodSearchResultsFragment fragment = new DetailedFoodSearchResultsFragment();
        Bundle args = new Bundle();
        args.putLong(ExpandedSearchFoodActivity.FOOD_ID, foodIdLong);
        args.putBoolean(ExpandedSearchFoodActivity.IS_PART_OF_VIEWPAGER, isPartOfViewPager);
        args.putBoolean(SearchFoodActivity.IS_FROM_BARCODE, isFromBarcode);
        args.putLong(Constants.servingId, servingId);
        args.putDouble(Constants.numberServings, numberOfServings);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detailed_search_results, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpButtons();
        setUpClickableTextView();
        setUpClickableMacros();
        new FetchDetailedInfo(this.foodId).execute();
    }

    private void showDetailedVitaminsAndMineralsBreakdown() {
        FragmentManager fm = getFragmentManager();
        VitaminsAndMineralsDialogFragment vitFragment = VitaminsAndMineralsDialogFragment.newInstance(
                detailedInfoList.get(CURRENT_SERVING_OPTION).getSodium(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getPotassium(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getCalcium(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getIron(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getVitamin_a(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getVitamin_c(), Double.valueOf(servingSize.getText().toString()));

        if(vitFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            vitFragment.show(fm, VitaminsAndMineralsDialogFragment.TAG);
        }
    }

    private void setUpClickableTextView() {
        vitaminsAndMinerals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedVitaminsAndMineralsBreakdown();
            }
        });
    }

    public void createServingDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.serving_size))
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_serving_size_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_serving_size_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();


        servingSizeEditText = (EditText) dialog.findViewById(R.id.number_of_servings);
        dialogServingSizeSpinner = (Spinner) dialog.findViewById(R.id.serving_size);

        servingSizeEditText.setText(servingSize.getText().toString());
        servingSizeEditText.setSelection(servingSizeEditText.getText().length());

        setUpSpinner();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(servingSizeEditText.getText()) && Double.valueOf(servingSizeEditText.getText().toString()) != 0) {
                    updateNumberOfServings();
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_invalid_number_servings), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUpSpinner() {

        List<String> spinnerArray =  new ArrayList<>();
        for(int i = 0; i < detailedInfoList.size(); i++) {
            spinnerArray.add(detailedInfoList.get(i).getServingDescription());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dialogServingSizeSpinner.setAdapter(adapter);

        dialogServingSizeSpinner.setSelection(CURRENT_SERVING_OPTION);

        dialogServingSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CURRENT_SERVING_OPTION = position;
//                calculateTotal();
                foodServingSize.setText(detailedInfoList.get(CURRENT_SERVING_OPTION).getServingDescription());
//                refreshPieChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void refreshPieChart() {
        PieData data = pieChart.getData();
        PieDataSet dataSet = (PieDataSet) data.getDataSet();
        for(int i = 0; i < 3; i++) {
            dataSet.removeEntry(i);
        }
        Entry carbs = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getCarbs() / totalFatCarbsAndProteins) * 100, 0); // 0 == Carbs
        Entry fats = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getFat() / totalFatCarbsAndProteins) * 100, 1); // 0 == Fat
        Entry proteins = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getProtein() / totalFatCarbsAndProteins) * 100, 2); // 0 == Protein

        dataSet.addEntry(carbs);
        dataSet.addEntry(fats);
        dataSet.addEntry(proteins);

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));

        data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        pieChart.setData(data);
        pieChart.invalidate();

    }


    private void updateNumberOfServings() {
        servingSize.setText(servingSizeEditText.getText().toString());
//        Commented out because redundant, activity calls it before saving anyway
//        sendDataToParentActivity();
        fats.setText(String.valueOf(roundUp(detailedInfoList.get(CURRENT_SERVING_OPTION).getFat() * Double.valueOf(servingSize.getText().toString()))));
        carbs.setText(String.valueOf(roundUp(detailedInfoList.get(CURRENT_SERVING_OPTION).getCarbs() * Double.valueOf(servingSize.getText().toString()))));
        proteins.setText(String.valueOf(roundUp(detailedInfoList.get(CURRENT_SERVING_OPTION).getProtein() * Double.valueOf(servingSize.getText().toString()))));
        calories.setText(String.valueOf(roundUp(detailedInfoList.get(CURRENT_SERVING_OPTION).getCalories() * Double.valueOf(servingSize.getText().toString()))));
    }

    public static double roundUp(double value) {
        return Math.round( value * 100.0 ) / 100.0;

    }

    public void sendDataToParentActivity(boolean isExpandedSearchFoodActivity) {
        if(isExpandedSearchFoodActivity) {
            ((ExpandedSearchFoodActivity) getActivity()).setChosenServingId(detailedInfoList.get(CURRENT_SERVING_OPTION).getServingId());
            ((ExpandedSearchFoodActivity) getActivity()).setChosenNumberServings(Double.valueOf(servingSize.getText().toString()));
        }
        else {
            ((EditFoodEntryActivity) getActivity()).setChosenServingId(detailedInfoList.get(CURRENT_SERVING_OPTION).getServingId());
            ((EditFoodEntryActivity) getActivity()).setChosenNumberServings(Double.valueOf(servingSize.getText().toString()));
        }
    }

    private void setUpServingMenu() {
        if(numberOfServings == -1) {
            servingSize.setText(String.valueOf(getContext().getResources().getInteger(R.integer.default_serving_size)));
        }
        else {
            servingSize.setText(String.valueOf(numberOfServings));
        }
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServingDialog();
            }
        });
    }

    private void setUpButtons() {

        if(!IS_PART_OF_VIEWPAGER) {
            previousButton.setVisibility(View.INVISIBLE);
            nextButton.setVisibility(View.INVISIBLE);
            if(isFromBarcode) {
                addToDiaryButton.setText(getString(R.string.add_to_diary));
            }
            else {
                addToDiaryButton.setText(getString(R.string.save));
            }
            addToDiaryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((EditFoodEntryActivity) getActivity()).addCurrentFood();
                }
            });
            return;
        }

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExpandedSearchFoodActivity) getActivity()).goPreviousViewpagerPage();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExpandedSearchFoodActivity) getActivity()).goNextViewpagerPage();
            }
        });

        addToDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ExpandedSearchFoodActivity) getActivity()).addCurrentFood();
            }
        });
    }

    private void setUpOnScreenData() {
        foodName.setText(detailedInfoList.get(CURRENT_SERVING_OPTION).getFoodName());
        foodBrand.setText(String.format(getString(R.string.food_brand_name), detailedInfoList.get(CURRENT_SERVING_OPTION).getBrandName()));
        foodServingSize.setText(detailedInfoList.get(CURRENT_SERVING_OPTION).getServingDescription());

        fats.setText(String.format(getString(R.string.two_decimal_places),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getFat() * Double.valueOf(servingSize.getText().toString())));
        carbs.setText(String.format(getString(R.string.two_decimal_places),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getCarbs() * Double.valueOf(servingSize.getText().toString())));
        proteins.setText(String.format(getString(R.string.two_decimal_places),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getProtein() * Double.valueOf(servingSize.getText().toString())));
        calories.setText(String.format(getString(R.string.two_decimal_places),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getCalories() * Double.valueOf(servingSize.getText().toString())));
    }

    private void calculateTotal() {
        totalFatCarbsAndProteins = detailedInfoList.get(CURRENT_SERVING_OPTION).getProtein() +
                detailedInfoList.get(CURRENT_SERVING_OPTION).getCarbs() +
                detailedInfoList.get(CURRENT_SERVING_OPTION).getFat();
    }

    private void retrieveArguments() {
        this.foodId = getArguments().getLong(ExpandedSearchFoodActivity.FOOD_ID);
        this.IS_PART_OF_VIEWPAGER = getArguments().getBoolean(ExpandedSearchFoodActivity.IS_PART_OF_VIEWPAGER);
        this.servingIdGlobal = getArguments().getLong(Constants.servingId, -1);
        this.numberOfServings = getArguments().getDouble(Constants.numberServings, -1);
        this.isFromBarcode = getArguments().getBoolean(SearchFoodActivity.IS_FROM_BARCODE, false);
    }

    private void bindViews(View view) {
        foodBrand = (TextView) view.findViewById(R.id.food_brand);
        foodName = (TextView) view.findViewById(R.id.food_name);
        foodServingSize = (TextView) view.findViewById(R.id.food_serving_size);
        fats = (TextView) view.findViewById(R.id.fats_textview);
        carbs = (TextView) view.findViewById(R.id.carbs_textview);
        proteins = (TextView) view.findViewById(R.id.proteins_textview);
        calories = (TextView) view.findViewById(R.id.calories_textview);
        pieChart = (PieChart) view.findViewById(R.id.pie_chart);
        previousButton = (Button) view.findViewById(R.id.previous_button);
        nextButton = (Button) view.findViewById(R.id.next_button);
        linearLayout = (LinearLayout) view.findViewById(R.id.serving_layout);
        servingSize = (TextView) view.findViewById(R.id.number_of_servings);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        parentLayout = (LinearLayout) view.findViewById(R.id.parent_linear_layout);
        addToDiaryButton = (Button) view.findViewById(R.id.add_to_diary_button);
        vitaminsAndMinerals = (TextView) view.findViewById(R.id.view_vitamins_textview);
        carbsLayout = (LinearLayout) view.findViewById(R.id.carbs_layout);
        proteinsLayout = (LinearLayout) view.findViewById(R.id.proteins_layout);
        fatsLayout = (LinearLayout) view.findViewById(R.id.fats_layout);
        food = new FatSecretFoods();
    }

    private void setUpClickableMacros() {
        carbsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_CARBS);
            }
        });
        fatsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_FATS);
            }
        });
        proteinsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_PROTEINS);
            }
        });
    }

    private void setUpPieChart() {
        calculateTotal();
        pieChart.setBackgroundColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription(getString(R.string.nutritional_breakdown));
        pieChart.setDescriptionColor(ContextCompat.getColor(getContext(), R.color.transp_black));
        pieChart.setDescriptionTextSize(11f);
        pieChart.setHoleRadius(5f);
        pieChart.setTransparentCircleRadius(1f);
        pieChart.setDrawSliceText(false);
//        pieChart.setCenterText(getString(R.string.nutritional_breakdown));
        pieChart.setCenterTextSize(13f);

        Legend l = pieChart.getLegend();
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_blue_light, android.R.color.holo_red_light, android.R.color.holo_green_light};
//        l.setCustom(colors, categories);


        breakdown = new ArrayList<Entry>();

        Entry carbs = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getCarbs() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_CARBS); // 0 == Carbs
        breakdown.add(carbs);
        Entry fats = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getFat() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_FATS); // 0 == Carbs
        breakdown.add(fats);
        Entry proteins = new Entry( (float) (detailedInfoList.get(CURRENT_SERVING_OPTION).getProtein() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_PROTEINS); // 0 == Carbs
        breakdown.add(proteins);

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
//        setBreakdown.setHighlightEnabled(true);
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, getContext());

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));
//
        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        if(!IS_PART_OF_VIEWPAGER) {
            pieChart.animateXY(750, 750);
        }

        pieChart.invalidate(); // refresh

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                // display msg when value selected
                if (e == null)
                    return;
                else {
                    showDetailedBreakDown(e.getXIndex());
                }
            }

            @Override
            public void onNothingSelected() {
                Log.i("applog", "Nothing is selected");
            }
        });
    }

    private void showDetailedBreakDown(int type) {
        switch (type) {
            case RecipeNutritionFragment.CASE_CARBS: // Carbs
                showDetailedCarbsBreakdown();
                break;
            case RecipeNutritionFragment.CASE_FATS: // Fats
                showDetailedFatsBreakdown();
                break;
            case RecipeNutritionFragment.CASE_PROTEINS: // Protein
                showDetailedProteinBreakdown();
                break;
            default: return;
        }
    }

    private void showDetailedCarbsBreakdown() {
        FragmentManager fm = getFragmentManager();
        CarbsDetailedInfoDialogFragment carbsFragment = CarbsDetailedInfoDialogFragment.newInstance(
                detailedInfoList.get(CURRENT_SERVING_OPTION).getCholestrol(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getSugar(),
                detailedInfoList.get(CURRENT_SERVING_OPTION).getFiber(), Double.valueOf(servingSize.getText().toString()));

        if(carbsFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            carbsFragment.show(fm, CarbsDetailedInfoDialogFragment.TAG);
        }
    }

    private void showDetailedFatsBreakdown() {
        FragmentManager fm = getFragmentManager();
        FatBreakdownDialogFragment fatFragment =
                FatBreakdownDialogFragment.newInstance(detailedInfoList.get(CURRENT_SERVING_OPTION).getPolyUnsaturatedFat(),
                        detailedInfoList.get(CURRENT_SERVING_OPTION).getMonoUnsaturatedFat(),
                        detailedInfoList.get(CURRENT_SERVING_OPTION).getTransFat(),
                        detailedInfoList.get(CURRENT_SERVING_OPTION).getSaturatedFat(), Double.valueOf(servingSize.getText().toString()));
        if(fatFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            fatFragment.show(fm, FatBreakdownDialogFragment.TAG);
        }
    }

    private void showDetailedProteinBreakdown() {
        Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.protein_data_text), Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void extractData(JSONArray servingArray, long foodId, String foodName, String brandName) throws Exception {
        int size = servingArray.length();
        for(int i = 0; i < size; i++) {
            extractData(servingArray.getJSONObject(i), foodId, foodName, brandName);
        }
    }

    private void extractData(JSONObject servingObject, long foodId, String foodName, String brandName) throws Exception {
        long servingId = servingObject.optLong(Constants.servingId, -1);
        String servingUrl = servingObject.optString(Constants.servingUrl, Constants.noUrlFound);
        String servingDescription = servingObject.optString(Constants.servingDescription, Constants.unknown);
        double metricServingAmount = servingObject.optDouble(Constants.metricServingAmount, -1);
        String metricServingUnit = servingObject.optString(Constants.metricServingUnit, Constants.unknown);
        String measurementDescription = servingObject.optString(Constants.measurementDescription, Constants.unknown);
        double numberOfUnits = servingObject.optDouble(Constants.numberOfUnits);
        DetailedFoodResultsInfo newInfo = new DetailedFoodResultsInfo(foodId, servingId, servingUrl, servingDescription,
                                            metricServingAmount, metricServingUnit, measurementDescription, numberOfUnits, getContext());

        if(servingId == this.servingIdGlobal) {
            CURRENT_SERVING_OPTION = EXTRACT_DATA_FUNCTION_COUNT;
        }

        // Setting up the heavy stuff
        newInfo.setBrandName(brandName);
        newInfo.setFoodName(foodName);
        newInfo.setCalories(servingObject.optDouble(Constants.calories, -1));
        newInfo.setCarbs(servingObject.optDouble(Constants.carbs, -1));
        newInfo.setProtein(servingObject.optDouble(Constants.protein, -1));
        newInfo.setFat(servingObject.optDouble(Constants.fat, -1));
        newInfo.setSaturatedFat(servingObject.optDouble(Constants.saturatedFat, -1));
        newInfo.setPolyUnsaturatedFat(servingObject.optDouble(Constants.polyUnsaturatedFat, -1));
        newInfo.setMonoUnsaturatedFat(servingObject.optDouble(Constants.monoUnsaturatedFat, -1));
        newInfo.setTransFat(servingObject.optDouble(Constants.transFat, -1));
        newInfo.setCholestrol(servingObject.optDouble(Constants.cholestrol, -1));
        newInfo.setSodium(servingObject.optDouble(Constants.sodium, -1));
        newInfo.setPotassium(servingObject.optDouble(Constants.potassium, -1));
        newInfo.setFiber(servingObject.optDouble(Constants.fiber, -1));
        newInfo.setSugar(servingObject.optDouble(Constants.sugar, -1));
        newInfo.setVitamin_a(servingObject.optDouble(Constants.vitamin_a, -1));
        newInfo.setVitamin_c(servingObject.optDouble(Constants.vitamin_c, -1));
        newInfo.setCalcium(servingObject.optDouble(Constants.calcium, -1));
        newInfo.setIron(servingObject.optDouble(Constants.iron, -1));
        newInfo.setBonusFact();

        detailedInfoList.add(newInfo);
        EXTRACT_DATA_FUNCTION_COUNT++;
    }

    private void fetchDetailedInfo(long foodId) throws Exception {
        detailedInfoList = new ArrayList<>();
        JSONObject foodItem = food.getFood(foodId);
        String foodName = foodItem.optString(Constants.foodName, Constants.unknown);
        String brandName = foodItem.optString(Constants.brandName, Constants.unknown);
        JSONObject servingInfo = foodItem.optJSONObject(Constants.servings);
        if(servingInfo != null) {
            JSONObject servingObject = null;
            JSONArray servingArray = null;
            Object tempObject = servingInfo.get(Constants.serving);
            if(tempObject != null) {
                if(tempObject instanceof JSONObject) {
                    servingObject = (JSONObject) tempObject;
                }
                else {
                    servingArray = (JSONArray) tempObject;
                }
            }

            if(servingArray != null) {
                extractData(servingArray, foodId, foodName, brandName);
            }
            else {
                extractData(servingObject, foodId, foodName, brandName);
            }
        }

    }


    //-------------------------
    // ASYNCTASK(S)
    //-------------------------

    private class FetchDetailedInfo extends AsyncTask<Void, Void, Boolean> {

        long foodId;

        public FetchDetailedInfo(long foodId) {
            this.foodId = foodId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(parentLayout, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchDetailedInfo(foodId);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setUpPieChart();
                    setUpServingMenu();
                    setUpOnScreenData();
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(parentLayout, true, true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

}
