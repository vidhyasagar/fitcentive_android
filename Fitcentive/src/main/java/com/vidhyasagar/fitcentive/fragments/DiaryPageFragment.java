package com.vidhyasagar.fitcentive.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.AddWaterActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.activities.SearchExerciseActivity;
import com.vidhyasagar.fitcentive.activities.SearchFoodActivity;
import com.vidhyasagar.fitcentive.adapters.DiaryAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretDiary;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.api_requests.wger.ExerciseRequest;
import com.vidhyasagar.fitcentive.main_activity_fragments.DiaryFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.NoScrollLayoutManager;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;
import com.vidhyasagar.fitcentive.wrappers.Ingredients;
import com.vidhyasagar.fitcentive.wrappers.MealInfo;
import com.vidhyasagar.fitcentive.wrappers.ServingInfo;
import com.vidhyasagar.fitcentive.wrappers.WaterInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import github.nisrulz.recyclerviewhelper.RVHItemDividerDecoration;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryPageFragment extends Fragment {

    public static DiaryPageFragment newInstance(int offset) {
        DiaryPageFragment f = new DiaryPageFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, offset);
        f.setArguments(args);
        return f;
    }

    public DiaryPageFragment() {}

    public static String TAG = "DIARY_PAGE_FRAGMENT";
    public enum ENTRY_TYPE {BREAKFAST, LUNCH, DINNER, SNACKS, WATER, EXCERCISE}

    int RETRY_COUNT = 0;
    boolean HAS_UNSAVED_CHANGES = false;

    boolean isBreakfastDone, isLunchDone, isDinnerDone, isSnacksDone, isWaterDone, isExerciseDone;

    // TODO : this is temporary, must be an argument supplied upon fragment instantiation
    int TARGET_CALORIES_AMOUNT;
    FatSecretFoods foods;
    FatSecretDiary fatSecretDiary;
    FatSecretRecipes recipes;
    ExerciseRequest exerciseRequest;

    ViewPager viewPager;
    RelativeLayout mainLayout;
    ProgressBar progressBar;
    ScrollView scrollView;
    ImageView nextButton, previousButton;
    TextView dateTextView;
    TextView breakfastTotalCalories, lunchTotalCalories, dinnerTotalCalories;
    TextView snacksTotalCalories, waterTotalCalories, exerciseTotalCalories;
    RecyclerView breakfastRecycler, lunchRecycler, dinnerRecycler, snacksRecycler, waterRecycler, excerciseRecycler;
    DiaryAdapter breakfastAdapter, lunchAdapter, dinnerAdapter, snacksAdapter, waterAdapter, excerciseAdapter;
    TextView targetCalories, intakeCalories, exerciseCalories, netCalories;
    RelativeLayout addBreakfastButton, addLunchButton, addDinnerButton, addSnacksButton, addWaterButton, addExButton;
    Button headerBreakfast, headerLunch, headerDinner, headerSnacks, headerWater, headerEx;

    FragmentManager fragmentManager;
    Button breakfastButton, lunchButton, dinnerButton, snacksButton, waterButton, exerciseButton;

    int dayOffset;
    String date;
    ArrayList<Object> breakfastData, lunchData, dinnerData, snacksData, waterData, excerciseData;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_diary_page, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpDay();
        setUpButtons();
        initRecyclers();
        prepareAddDataButtons();
        setUpQuickButtons();
    }


    @Override
    public void onResume() {
        super.onResume();

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            new PopulateBreakfast().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new PopulateLunch().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new PopulateDinner().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new PopulateSnacks().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new PopulateWater().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            new PopulateExercise().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else {
            new PopulateBreakfast().execute();
            new PopulateLunch().execute();
            new PopulateDinner().execute();
            new PopulateSnacks().execute();
            new PopulateWater().execute();
            new PopulateExercise().execute();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        new DestroyTempDataFromDatabase().execute();
    }

    public void validateExistingFoods() {
        new ValidateExistingFoods().execute();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public boolean hasUnsavedChanges() {
        return this.HAS_UNSAVED_CHANGES;
    }

    private void initRecyclers() {
        initBreakfastRecycler();
        initLunchRecycler();
        initDinnerRecycler();
        initSnacksRecycler();
        initWaterRecycler();
        initExcerciseRecycler();
    }

    private void initBreakfastRecycler() {
        breakfastData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        breakfastAdapter = new DiaryAdapter(breakfastData, getContext(), breakfastRecycler, ENTRY_TYPE.BREAKFAST, this, breakfastTotalCalories, dayOffset);
        breakfastRecycler.setLayoutManager(lm);
        breakfastRecycler.setAdapter(breakfastAdapter);

        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(breakfastAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(breakfastRecycler);

        // Set the divider in the recyclerview
        breakfastRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    private void initLunchRecycler() {
        lunchData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        lunchAdapter = new DiaryAdapter(lunchData, getContext(), lunchRecycler, ENTRY_TYPE.LUNCH, this, lunchTotalCalories, dayOffset);
        lunchRecycler.setLayoutManager(lm);
        lunchRecycler.setAdapter(lunchAdapter);

        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(lunchAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(lunchRecycler);

        // Set the divider in the recyclerview
        lunchRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    private void initDinnerRecycler() {
        dinnerData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        dinnerAdapter = new DiaryAdapter(dinnerData, getContext(), dinnerRecycler, ENTRY_TYPE.DINNER, this, dinnerTotalCalories, dayOffset);
        dinnerRecycler.setLayoutManager(lm);
        dinnerRecycler.setAdapter(dinnerAdapter);

        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(dinnerAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(dinnerRecycler);

        // Set the divider in the recyclerview
        dinnerRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    private void initSnacksRecycler() {
        snacksData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        snacksAdapter = new DiaryAdapter(snacksData, getContext(), snacksRecycler, ENTRY_TYPE.SNACKS, this, snacksTotalCalories, dayOffset);
        snacksRecycler.setLayoutManager(lm);
        snacksRecycler.setAdapter(snacksAdapter);

        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(snacksAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(snacksRecycler);

        // Set the divider in the recyclerview
        snacksRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    private void initWaterRecycler() {
        waterData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        waterAdapter = new DiaryAdapter(waterData, getContext(), waterRecycler, ENTRY_TYPE.WATER, this, waterTotalCalories, dayOffset);
        waterRecycler.setLayoutManager(lm);
        waterRecycler.setAdapter(waterAdapter);

        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(waterAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(waterRecycler);

        // Set the divider in the recyclerview
        waterRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    private void initExcerciseRecycler() {
        excerciseData = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        excerciseAdapter = new DiaryAdapter(excerciseData, getContext(), excerciseRecycler, ENTRY_TYPE.EXCERCISE, this, exerciseTotalCalories, dayOffset);
        excerciseRecycler.setLayoutManager(lm);
        excerciseRecycler.setAdapter(excerciseAdapter);
        ItemTouchHelper.Callback callback = new RVHItemTouchHelperCallback(excerciseAdapter, true, false, true);
        ItemTouchHelper helper = new ItemTouchHelper(callback);
        helper.attachToRecyclerView(excerciseRecycler);

        // Set the divider in the recyclerview
        excerciseRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

    }

    // TODO : Performance improvement : make populating data fetch from individual threads
    private void prepareData() throws Exception {
        prepareBreakfastData();
        prepareLunchData();
        prepareDinnerData();
        prepareSnacksData();
        prepareWaterData();
        prepareExcerciseData();
    }

    private int getIntegerFromString(TextView textView) {
        if(textView.getVisibility() == View.GONE) {
            return 0;
        }
        else {
            String string = textView.getText().toString();
            if (string.equals("")) {
                return 0;
            } else {
                String newString = string.split(" ")[0];
                return Integer.valueOf(newString);
            }
        }
    }

    private void hideRecyclerIfEmptyDataSet() {
        if(breakfastData.isEmpty()) {
            setViewVisibility(breakfastRecycler, false, true);
        } else {
            setViewVisibility(breakfastRecycler, true, true);
        }

        if(lunchData.isEmpty()) {
            setViewVisibility(lunchRecycler, false, true);
        } else {
            setViewVisibility(lunchRecycler, true, true);
        }

        if(dinnerData.isEmpty()) {
            setViewVisibility(dinnerRecycler, false, true);
        } else {
            setViewVisibility(dinnerRecycler, true, true);
        }

        if(snacksData.isEmpty()) {
            setViewVisibility(snacksRecycler, false, true);
        } else {
            setViewVisibility(snacksRecycler, true, true);
        }

        if(excerciseData.isEmpty()) {
            setViewVisibility(excerciseRecycler, false, true);
        } else {
            setViewVisibility(excerciseRecycler, true, true);
        }

        if(waterData.isEmpty()) {
            setViewVisibility(waterRecycler, false, true);
        } else {
            setViewVisibility(waterRecycler, true, true);
        }
    }

    private void prepareAddDataButtons() {
        addBreakfastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.BREAKFAST);
            }
        });
        headerBreakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.BREAKFAST);
            }
        });

        addLunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.LUNCH);
            }
        });
        headerLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.LUNCH);
            }
        });

        addDinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.DINNER);
            }
        });
        headerDinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.DINNER);
            }
        });

        addSnacksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.SNACKS);
            }
        });
        headerSnacks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddFoodActivity(ENTRY_TYPE.SNACKS);
            }
        });

        addWaterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddWaterActivity();
            }
        });
        headerWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddWaterActivity();
            }
        });

        addExButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddExerciseActivity();
            }
        });
        headerEx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAddExerciseActivity();
            }
        });
    }

    private void startAddExerciseActivity() {
        Intent i = new Intent(getContext(), SearchExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        // Start activity for result?
        startActivity(i);
    }

    private void startAddFoodActivity(ENTRY_TYPE type) {
        Intent i = new Intent(getContext(), SearchFoodActivity.class);
        i.putExtra(Constants.type, type);
        i.putExtra(Constants.returnDate, dayOffset);
        startActivity(i);
    }

    private void startAddWaterActivity() {
        Intent i = new Intent(getContext(), AddWaterActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        startActivity(i);
    }

    private void extractData(JSONArray servingArray, long foodId, ArrayList<Object> data,
                             String foodName, String brandName, double numberOfServings, long servingId, boolean isFavorite) throws Exception {
        int size = servingArray.length();
        for(int i = 0; i < size; i++) {
            JSONObject servingObject = servingArray.getJSONObject(i);
            long tempServingId = servingObject.optLong(Constants.servingId, -1);
            if(servingId == tempServingId) {
                extractData(servingObject, foodId, data, foodName, brandName, numberOfServings, isFavorite);
            }
        }
    }

    private void extractData(JSONObject servingObject, long foodId, ArrayList<Object> data,
                            String foodName, String brandName, double numberOfServings, boolean isFavorite) throws Exception {

        long servingId = servingObject.optLong(Constants.servingId, -1);
        String servingUrl = servingObject.optString(Constants.servingUrl, Constants.noUrlFound);
        String servingDescription = servingObject.optString(Constants.servingDescription, Constants.unknown);
        double metricServingAmount = servingObject.optDouble(Constants.metricServingAmount, -1);
        String metricServingUnit = servingObject.optString(Constants.metricServingUnit, Constants.unknown);
        String measurementDescription = servingObject.optString(Constants.measurementDescription, Constants.unknown);
        double numberOfUnits = servingObject.optDouble(Constants.numberOfUnits, -1);
        DetailedFoodResultsInfo newInfo = new DetailedFoodResultsInfo(foodId, servingId, servingUrl, servingDescription,
                metricServingAmount, metricServingUnit, measurementDescription, numberOfUnits, getContext());

        // Setting up the heavy stuff
        newInfo.setBrandName(brandName);
        newInfo.setFoodName(foodName);
        newInfo.setNumberServings(numberOfServings);
        newInfo.setCalories(servingObject.optDouble(Constants.calories, -1));
        newInfo.setCarbs(servingObject.optDouble(Constants.carbs, -1));
        newInfo.setProtein(servingObject.optDouble(Constants.protein, -1));
        newInfo.setFat(servingObject.optDouble(Constants.fat, -1));
        newInfo.setSaturatedFat(servingObject.optDouble(Constants.saturatedFat, -1));
        newInfo.setPolyUnsaturatedFat(servingObject.optDouble(Constants.polyUnsaturatedFat, -1));
        newInfo.setMonoUnsaturatedFat(servingObject.optDouble(Constants.monoUnsaturatedFat, -1));
        newInfo.setTransFat(servingObject.optDouble(Constants.transFat, -1));
        newInfo.setCholestrol(servingObject.optDouble(Constants.cholestrol, -1));
        newInfo.setSodium(servingObject.optDouble(Constants.sodium, -1));
        newInfo.setPotassium(servingObject.optDouble(Constants.potassium, -1));
        newInfo.setFiber(servingObject.optDouble(Constants.fiber, -1));
        newInfo.setSugar(servingObject.optDouble(Constants.sugar, -1));
        newInfo.setVitamin_a(servingObject.optDouble(Constants.vitamin_a, -1));
        newInfo.setVitamin_c(servingObject.optDouble(Constants.vitamin_c, -1));
        newInfo.setCalcium(servingObject.optDouble(Constants.calcium, -1));
        newInfo.setIron(servingObject.optDouble(Constants.iron, -1));
        newInfo.setBonusFact();
        newInfo.setFavorite(isFavorite);
        newInfo.setCreatedFood(false);

        data.add(newInfo);
    }

    private void fetchDetailedInfo(long foodId, ArrayList<Object> data, double numberOfServings,
                                    long servingId, boolean isFavorite) throws Exception {
        JSONObject foodItem = foods.getFood(foodId);
        String foodName = foodItem.optString(Constants.foodName, Constants.unknown);
        String brandName = foodItem.optString(Constants.brandName, Constants.unknown);

        JSONObject servingInfo = foodItem.optJSONObject(Constants.servings);
        if(servingInfo != null) {
            JSONObject servingObject = null;
            JSONArray servingArray = null;
            Object tempObject = servingInfo.get(Constants.serving);
            if(tempObject != null) {
                if(tempObject instanceof JSONObject) {
                    servingObject = (JSONObject) tempObject;
                }
                else {
                    servingArray = (JSONArray) tempObject;
                }
            }

            if(servingArray != null) {
                extractData(servingArray, foodId, data, foodName, brandName, numberOfServings, servingId, isFavorite);
            }
            else {
                extractData(servingObject, foodId, data, foodName, brandName, numberOfServings, isFavorite);
            }
        }
    }


    private ArrayList<Ingredients> fetchIngredientsData(JSONArray array) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            Ingredients temp = new Ingredients();
            temp.setFoodId(object.optLong(Constants.foodId, -1));
            temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
            temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
            temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
            temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
            temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
            temp.setServingId(object.optLong(Constants.servingId, -1));
            ingredientsList.add(temp);
        }
        return ingredientsList;
    }

    private ArrayList<Ingredients> fetchIngredientsData(JSONObject object) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        Ingredients temp = new Ingredients();
        temp.setFoodId(object.optLong(Constants.foodId, -1));
        temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
        temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
        temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
        temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
        temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
        temp.setServingId(object.optLong(Constants.servingId, -1));

        ingredientsList.add(temp);
        return ingredientsList;
    }

    private void fetchIngredients(JSONObject ingredients, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(ingredients == null) {
            return;
        }
        ArrayList<Ingredients> ingredientsList;
        Object temp = ingredients.get(Constants.ingredient);
        if(temp instanceof JSONObject) {
            ingredientsList = fetchIngredientsData((JSONObject) temp);
        }
        else {
            ingredientsList = fetchIngredientsData((JSONArray) temp);
        }
        recipeInfo.setIngredientsList(ingredientsList);
    }

    private ArrayList<String> fetchDirectionsData(JSONArray array) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
            directionsList.add(directionDescription);
        }
        return directionsList;
    }

    private ArrayList<String> fetchDirectionsData(JSONObject object) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
        directionsList.add(directionDescription);
        return directionsList;
    }

    private void fetchDirections(JSONObject direction, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(direction == null) {
            return;
        }
        ArrayList<String> directionsList;
        Object temp = direction.get(Constants.direction);
        if(temp instanceof JSONObject) {
            // There is only one direction here
            directionsList = fetchDirectionsData((JSONObject) temp);
        }
        else {
            // There are multiple directions here, extract it from the JSONArray
            directionsList = fetchDirectionsData((JSONArray) temp);
        }
        recipeInfo.setDirections(directionsList);
    }

    private ArrayList<ServingInfo> extractServingData(JSONArray array) throws JSONException {
        ArrayList<ServingInfo> list = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject result = array.getJSONObject(i);
            ServingInfo newServingInfo = new ServingInfo();
            newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
            newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
            newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
            newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
            newServingInfo.setFat(result.optDouble(Constants.fat, -1));
            newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
            newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
            newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
            newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
            newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
            newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
            newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
            newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
            newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
            newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
            newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
            newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
            newServingInfo.setIron(result.optDouble(Constants.iron, -1));
            list.add(newServingInfo);
        }
        return list;
    }

    private ArrayList<ServingInfo> extractServingData(JSONObject result) {
        ArrayList<ServingInfo> list = new ArrayList<>();
        ServingInfo newServingInfo = new ServingInfo();
        newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
        newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
        newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
        newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
        newServingInfo.setFat(result.optDouble(Constants.fat, -1));
        newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
        newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
        newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
        newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
        newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
        newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
        newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
        newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
        newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
        newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
        newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
        newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
        newServingInfo.setIron(result.optDouble(Constants.iron, -1));
        list.add(newServingInfo);
        return list;
    }

    private void fetchServingSizeInfo(JSONObject servings, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(servings == null) {
            return;
        }

        ArrayList<ServingInfo> servingsList;
        Object temp = servings.get(Constants.serving);
        if(temp instanceof JSONObject) {
            servingsList = extractServingData((JSONObject) temp);
        }
        else {
            servingsList = extractServingData((JSONArray) temp);
        }

        recipeInfo.setServingsList(servingsList);
    }

    private void fetchDetailedRecipeInfo(long recipeId, boolean isFavorite, double numberOfServings, int recipeServingOption, ArrayList<Object> data) throws JSONException {
        JSONObject result = recipes.getRecipe(recipeId);
        DetailedRecipeResultsInfo recipeInfo = new DetailedRecipeResultsInfo();
        if(result != null) {
            recipeInfo.setRecipeId(result.optLong(Constants.recipeId, -1));
            recipeInfo.setRecipeName(result.optString(Constants.recipeName, Constants.unknown));
            recipeInfo.setRecipeUrl(result.optString(Constants.recipeUrl, Constants.unknown));
            recipeInfo.setRecipeDescription(result.optString(Constants.recipeDescription, Constants.unknown));
            recipeInfo.setNumberOfServings(result.optDouble(Constants.numberOfServings, -1));
            recipeInfo.setPrepTime(result.optInt(Constants.prepTime, -1));
            recipeInfo.setCookingTime(result.optInt(Constants.cookingTime, -1));
            recipeInfo.setRating(result.optInt(Constants.rating, -1));
            recipeInfo.setRecipeType(result.optString(Constants.recipeType, Constants.unknown));
            recipeInfo.setRecipeImageUrl(result.optString(Constants.recipeImage, Constants.unknown));
            recipeInfo.setChosenNumberOfServings(numberOfServings);
            recipeInfo.setFavorite(isFavorite);
            recipeInfo.setSelectedServingOption(recipeServingOption);
            fetchServingSizeInfo(result.optJSONObject(Constants.servingSizes), recipeInfo);
            // Directions and ingredients array lists. The method sets it on the object directly as recipeInfo is GLOBAL
            // NOT taking into account category type and similar stuff because tbh it's useless
            fetchDirections(result.optJSONObject(Constants.directions), recipeInfo);
            fetchIngredients(result.optJSONObject(Constants.ingredients), recipeInfo);

            recipeInfo.setBonusFact();
            data.add(recipeInfo);
        }
    }


// ----------------------------------------------------------

    private String fetchServingSize(JSONArray array, long foodServingId, Number foodNumberOfServings) throws Exception {
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            long servingId = object.optLong(Constants.servingId, -1);
            if(servingId == foodServingId) {
                // DIRTY HACK IN PLAY, Can fail when things cannot be pluralized with the addition of an 's'
                try {
                    if((Integer) foodNumberOfServings == 1) {
                        return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown);
                    }
                    else {
                        return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                    }
                } catch(ClassCastException e) {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                }
            }
        }
        return foodNumberOfServings + " " + Constants.unknown;
    }

    private String fetchServingSize(JSONObject object, long foodServingId, Number foodNumberOfServings) throws Exception {
        long servingId = object.optLong(Constants.servingId, -1);
        if(servingId == foodServingId) {
            // DIRTY HACK IN PLAY. Can fail when things cannot be pluralized with the addition of an 's'
            try {
                if((Integer) foodNumberOfServings == 1) {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown);
                }
                else {
                    return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
                }
            } catch(ClassCastException e) {
                return foodNumberOfServings + " " + object.optString(Constants.measurementDescription, Constants.unknown) + "s";
            }
        }
        else {
            return foodNumberOfServings + " " + Constants.unknown;
        }
    }

    private String fetchRecipeServingSize(JSONArray array, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        JSONObject object = array.getJSONObject(recipeServingOption);
        // This was the original line (below))
//        return foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown);
        // Doing this DIRTY HACK over here because all recipe's seem to have serving sizes of '1 serving'
        String servingSize = foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown).substring(2);
        try {
            if ((Integer) foodNumberOfServings == 1) {
                return servingSize;
            } else {
                return servingSize + "s";
            }
        }
        catch(ClassCastException e) {
            return servingSize + "s";
        }
    }

    private String fetchRecipeServingSize(JSONObject object, int recipeServingOption, Number foodNumberOfServings) throws Exception {
//        return foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown);
        // Doing this DIRTY HACK over here because all recipe's seem to have serving sizes of '1 serving'
        String servingSize = foodNumberOfServings + " " + object.optString(Constants.servingSize, Constants.unknown).substring(2);
        try {
            if((Integer) foodNumberOfServings == 1) {
                return servingSize;
            } else {
                return servingSize + "s";
            }
        }
        catch (ClassCastException e) {
            return servingSize + "s";
        }
    }

    private double getCaloriesForFood(JSONArray array, long foodServingId, Number foodNumberOfServings) throws Exception {
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            long servingId = object.optLong(Constants.servingId, -1);
            if(servingId == foodServingId) {
                try {
                    return ((Double) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
                } catch (ClassCastException e) {
                    return ((Integer) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
                }
            }
        }
        return 0;
    }

    private double getCaloriesForFood(JSONObject object, long foodServingId, Number foodNumberOfServings) throws Exception {
        long servingId = object.optLong(Constants.servingId, -1);
        if(servingId == foodServingId) {
            try {
                return ((Double) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
            } catch (ClassCastException e) {
                return ((Integer) foodNumberOfServings) * object.optDouble(Constants.calories, 0);
            }
        }
        else {
            return 0;
        }
    }

    private double getCaloriesForRecipe(JSONArray array, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        try {
            return array.getJSONObject(recipeServingOption).optDouble(Constants.calories, 0) * ((Double) foodNumberOfServings);
        } catch (ClassCastException e) {
            return array.getJSONObject(recipeServingOption).optDouble(Constants.calories, 0) * ((Integer) foodNumberOfServings);
        }
    }

    private double getCaloriesForRecipe(JSONObject object, int recipeServingOption, Number foodNumberOfServings) throws Exception {
        try {
            return object.optDouble(Constants.calories, 0) * ((Double) foodNumberOfServings);
        } catch (ClassCastException e) {
            return object.optDouble(Constants.calories, 0) * ((Integer) foodNumberOfServings);
        }
    }

    private void fetchDetailedInformation(List<Integer> foodIds, List<Integer> foodServingIds, List<Number> foodNumberOfServings,
                                          List<Integer> recipeIds, List<Integer> recipeServingOptions,
                                          List<Number> recipeNumberOfServings, MealInfo mealInfo,
                                          ArrayList<Object> data) throws Exception{

        List<String> foodAndRecipeNames = new ArrayList<>();
        List<String> servingSizes = new ArrayList<>();
        double totalNumberOfCaloriesForThisMeal = 0;

        // First we operate on foods, then we operate on recipes
        int foodSize = foodIds.size();
        for(int i = 0; i < foodSize; i++) {
            JSONObject result = foods.getFood(Long.valueOf(foodIds.get(i)));
            if(result != null) {
                foodAndRecipeNames.add(result.optString(Constants.foodName, Constants.unknown));
                JSONObject servingInfo = result.optJSONObject(Constants.servings);
                if(servingInfo != null) {
                    Object object = servingInfo.opt(Constants.serving);
                    if (object instanceof JSONObject) {
                        servingSizes.add(fetchServingSize((JSONObject) object, foodServingIds.get(i), foodNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForFood((JSONObject) object, foodServingIds.get(i), foodNumberOfServings.get(i));
                    } else {
                        servingSizes.add(fetchServingSize((JSONArray) object, foodServingIds.get(i), foodNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForFood((JSONArray) object, foodServingIds.get(i), foodNumberOfServings.get(i));
                    }
                }
            }
        }

        int recipeSize = recipeIds.size();
        for(int i = 0; i < recipeSize; i++) {
            JSONObject result = recipes.getRecipe(Long.valueOf(recipeIds.get(i)));
            if(result != null) {
                foodAndRecipeNames.add(result.optString(Constants.recipeName, Constants.unknown));
                JSONObject servingInfo = result.optJSONObject(Constants.servingSizes);
                if(servingInfo != null) {
                    Object object = servingInfo.opt(Constants.serving);
                    if (object instanceof JSONObject) {
                        servingSizes.add(fetchRecipeServingSize((JSONObject) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForRecipe((JSONObject) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i));
                    } else {
                        servingSizes.add(fetchRecipeServingSize((JSONArray) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i)));
                        totalNumberOfCaloriesForThisMeal+= getCaloriesForRecipe((JSONArray) object, recipeServingOptions.get(i), recipeNumberOfServings.get(i));
                    }
                }
            }
        }

        mealInfo.setServingSizes(servingSizes);
        mealInfo.setFoodAndRecipeNames(foodAndRecipeNames);
        mealInfo.setTotalNumberOfCalories(Double.valueOf(totalNumberOfCaloriesForThisMeal).intValue());

        data.add(mealInfo);

    }

    private String getServingDescription(double servingSize, String servingUnit, double numberOfServings) {
        return String.valueOf(servingSize * numberOfServings) + " " + servingUnit;
    }

    private void fetchCreatedFoodEntry(String createdFoodId, double numberOfServings, ArrayList<Object> data) throws Exception {

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedFood);
        ParseObject object = query.get(createdFoodId);

        String foodName = object.getString(Constants.foodNameField);
        String foodBrand = object.getString(Constants.foodBrand);
        String foodDescription = object.getString(Constants.foodDescriptionField);
        double servingSize = object.getDouble(Constants.servingSizeField);
        String servingUnit = object.getString(Constants.servingSizeUnit);
        double calories = object.getDouble(Constants.caloriesField);

        DetailedFoodResultsInfo food = new DetailedFoodResultsInfo();
        food.setFoodName(foodName);
        food.setBrandName(foodBrand);
        food.setFoodDescription(foodDescription);
        food.setServingDescription(getServingDescription(servingSize, servingUnit, numberOfServings));
        food.setCalories(calories);
        food.setCreatedFood(true);
        food.setNumberOfServings(numberOfServings);
        food.setObjectId(createdFoodId);

        data.add(food);
    }

    private void fetchCreatedMeal(String mealId, ArrayList<Object> data) throws Exception {

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedMeal);
        ParseObject object = query.get(mealId);


        String mealName = object.getString(Constants.mealName);
        String mealObjectId = object.getObjectId();

        List<Integer> foodIds = object.getList(Constants.foodIds);
        List<Integer> foodServingIds = object.getList(Constants.foodServingIds);
        List<Number> foodNumberOfServings = object.getList(Constants.foodNumberOfServings);

        List<Integer> recipeIds = object.getList(Constants.recipeIds);
        List<Number> recipeNumberOfServings = object.getList(Constants.recipeNumberOfServings);
        List<Integer> recipeServingOptions = object.getList(Constants.recipeServingOptions);

        MealInfo newMeal = new MealInfo();
        newMeal.setMealName(mealName);
        newMeal.setMealObjectId(mealObjectId);

        // Now for each of the foodIds and recipeIds, we must fetch food data... :(
        fetchDetailedInformation(foodIds, foodServingIds, foodNumberOfServings, recipeIds,
                recipeServingOptions, recipeNumberOfServings, newMeal, data);

    }

// ----------------------------------------------------------

    private ArrayList<Long> fetchFromJSONArray(JSONObject object, String key) throws Exception {
        JSONArray array = object.optJSONArray(key);
        ArrayList<Long> list = new ArrayList<>();
        if(array != null) {
            int size = array.length();
            for(int i = 0; i < size; i++) {
                list.add(array.getLong(i));
            }
        }

        return list;
    }

    private ArrayList<Long> extractFromList(List<Integer> list) {
        ArrayList<Long> newList = new ArrayList<>();
        for(Integer number : list) {
            newList.add(Long.valueOf(number));
        }
        return newList;
    }

    private void fetchDetailedCreatedExerciseInfo(ExerciseInfo info, String createdExerciseObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);

        info.setDescription(object.getString(Constants.exerciseDescription));
        info.setOriginalName(object.getString(Constants.exerciseName));
        info.setCategory(object.getLong(Constants.category));

        List<Integer> temp = object.getList(Constants.equipmentList);
        info.setEquipment(extractFromList(temp));
        temp = object.getList(Constants.primaryMusclesList);
        info.setPrimaryMuscles(extractFromList(temp));
        temp = object.getList(Constants.secondaryMusclesList);
        info.setSecondaryMuscles(extractFromList(temp));

        excerciseData.add(info);
    }

    private void fetchDetailedExerciseInfo(ExerciseInfo info) throws Exception {
        JSONObject object = exerciseRequest.wgerGetExerciseInfo(info.getId());
        if(object != null) {
            info.setDescription(object.optString(Constants.description, Constants.unknown));
            info.setOriginalName(object.optString(Constants.originalName, Constants.unknown));
            info.setCategory(object.optLong(Constants.category, -1));
            info.setEquipment(fetchFromJSONArray(object, Constants.equipment));
            info.setPrimaryMuscles(fetchFromJSONArray(object, Constants.muscles));
            info.setSecondaryMuscles(fetchFromJSONArray(object, Constants.secondaryMuscles));
        }

        excerciseData.add(info);
    }


    private void prepareBreakfastData() throws Exception {
        breakfastData.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereContains(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.mealType, getString(R.string.breakfast));
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject foodItem : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!foodItem.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                // For each resulting foodId, look up the food
                if(foodItem.getString(Constants.entryType).equals(getString(R.string.recipe))) {
                    long recipeId = foodItem.getLong(Constants.recipeIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    int recipeServingOption = foodItem.getInt(Constants.recipeServingOption);
                    fetchDetailedRecipeInfo(recipeId, isFavorite, numberOfServings, recipeServingOption, breakfastData);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.food))) {
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    long servingId = foodItem.getLong(Constants.servingIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    fetchDetailedInfo(foodItem.getLong(Constants.foodIdNumber), breakfastData, numberOfServings, servingId, isFavorite);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.meal))) {
                    String mealObjectId = foodItem.getString(Constants.mealObjectId);
                    fetchCreatedMeal(mealObjectId, breakfastData);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.created_food))) {
                    String createdFoodObjectId = foodItem.getString(Constants.createdFoodObjectId);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    fetchCreatedFoodEntry(createdFoodObjectId, numberOfServings, breakfastData);
                }
            }
        }

    }

    private void prepareLunchData() throws Exception {
        lunchData.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereContains(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.mealType, getString(R.string.lunch));
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject foodItem : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!foodItem.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                // For each resulting foodId, look up the food
                if(foodItem.getString(Constants.entryType).equals(getString(R.string.recipe))) {
                    long recipeId = foodItem.getLong(Constants.recipeIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    int recipeServingOption = foodItem.getInt(Constants.recipeServingOption);
                    fetchDetailedRecipeInfo(recipeId, isFavorite, numberOfServings, recipeServingOption, lunchData);
                }
                else if(foodItem.getString(Constants.entryType).equals(getString(R.string.food))){
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    long servingId = foodItem.getLong(Constants.servingIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    fetchDetailedInfo(foodItem.getLong(Constants.foodIdNumber), lunchData, numberOfServings, servingId, isFavorite);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.meal))) {
                    String mealObjectId = foodItem.getString(Constants.mealObjectId);
                    fetchCreatedMeal(mealObjectId, lunchData);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.created_food))) {
                    String createdFoodObjectId = foodItem.getString(Constants.createdFoodObjectId);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    fetchCreatedFoodEntry(createdFoodObjectId, numberOfServings, lunchData);
                }
            }
        }

    }

    private void prepareDinnerData() throws Exception {
        dinnerData.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereContains(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.mealType, getString(R.string.dinner));
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject foodItem : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!foodItem.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                // For each resulting foodId, look up the food
                if(foodItem.getString(Constants.entryType).equals(getString(R.string.recipe))) {
                    long recipeId = foodItem.getLong(Constants.recipeIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    int recipeServingOption = foodItem.getInt(Constants.recipeServingOption);
                    fetchDetailedRecipeInfo(recipeId, isFavorite, numberOfServings, recipeServingOption, dinnerData);
                }
                else if(foodItem.getString(Constants.entryType).equals(getString(R.string.food))) {
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    long servingId = foodItem.getLong(Constants.servingIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    fetchDetailedInfo(foodItem.getLong(Constants.foodIdNumber), dinnerData, numberOfServings, servingId, isFavorite);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.meal))) {
                    String mealObjectId = foodItem.getString(Constants.mealObjectId);
                    fetchCreatedMeal(mealObjectId, dinnerData);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.created_food))) {
                    String createdFoodObjectId = foodItem.getString(Constants.createdFoodObjectId);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    fetchCreatedFoodEntry(createdFoodObjectId, numberOfServings, dinnerData);
                }
            }
        }


    }

    private void prepareSnacksData() throws Exception {
        snacksData.clear();
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereContains(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.mealType, getString(R.string.snacks));
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject foodItem : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!foodItem.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                // For each resulting foodId, look up the food
                if(foodItem.getString(Constants.entryType).equals(getString(R.string.recipe))) {
                    long recipeId = foodItem.getLong(Constants.recipeIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    int recipeServingOption = foodItem.getInt(Constants.recipeServingOption);
                    fetchDetailedRecipeInfo(recipeId, isFavorite, numberOfServings, recipeServingOption, snacksData);
                }
                else if(foodItem.getString(Constants.entryType).equals(getString(R.string.food))) {
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    long servingId = foodItem.getLong(Constants.servingIdField);
                    boolean isFavorite = foodItem.getBoolean(Constants.isFavorite);
                    fetchDetailedInfo(foodItem.getLong(Constants.foodIdNumber), snacksData, numberOfServings, servingId, isFavorite);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.meal))) {
                    String mealObjectId = foodItem.getString(Constants.mealObjectId);
                    fetchCreatedMeal(mealObjectId, snacksData);
                }
                else if (foodItem.getString(Constants.entryType).equals(getString(R.string.created_food))) {
                    String createdFoodObjectId = foodItem.getString(Constants.createdFoodObjectId);
                    double numberOfServings = foodItem.getDouble(Constants.numberServings);
                    fetchCreatedFoodEntry(createdFoodObjectId, numberOfServings, snacksData);
                }
            }
        }

    }

    private void prepareWaterData() throws Exception {
        waterData.clear();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.WaterDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));

        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!object.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                WaterInfo newInfo = new WaterInfo(object.getDouble(Constants.cupsOfWater), object.getObjectId());
                waterData.add(newInfo);
            }
        }

    }

    private void prepareExcerciseData() throws Exception {
        excerciseData.clear();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));

        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {
                // If ANY object is NOT permanent, then we update our boolean flag
                if(!object.getBoolean(Constants.isPermanent)) {
                    HAS_UNSAVED_CHANGES = true;
                }

                //For each resulting exercise in diary, look up exerciseType
                if(object.getString(Constants.exerciseType).equals(getString(R.string.weights_type))) {
                    ExerciseInfo newInfo = new ExerciseInfo();
                    newInfo.setName(object.getString(Constants.exerciseName));
                    newInfo.setId(object.getLong(Constants.exerciseId));
                    newInfo.setNumberOfSets(object.getInt(Constants.numberOfSets));
                    newInfo.setRepsPerSet(object.getDouble(Constants.repsPerSet));
                    newInfo.setWeightPerSet(object.getDouble(Constants.weightPerSet));
                    newInfo.setWeightUnit(object.getString(Constants.weightUnit));
                    newInfo.setObjectId(object.getObjectId());
                    newInfo.setCreated(object.getBoolean(Constants.isCreated));
                    if(object.getBoolean(Constants.isCreated)) {
                        newInfo.setCreatedExerciseObjectId(object.getString(Constants.createdExerciseObjectId));
                        fetchDetailedCreatedExerciseInfo(newInfo, object.getString(Constants.createdExerciseObjectId));
                    }
                    else {
                        fetchDetailedExerciseInfo(newInfo);
                    }
                }
                else if(object.getString(Constants.exerciseType).equals(getString(R.string.cardio_type))) {
                    CardioInfo info = new CardioInfo();
                    info.setObjectId(object.getObjectId());
                    info.setName(object.getString(Constants.exerciseName));
                    info.setMinutesPerformed(object.getDouble(Constants.minutesPerformed));
                    info.setCaloriesBurned(object.getDouble(Constants.caloriesBurned));
                    info.setCreated(object.getBoolean(Constants.isCreated));
                    if(object.getBoolean(Constants.isCreated)) {
                        info.setCreatedCardioObjectId(object.getString(Constants.createdExerciseObjectId));
                    }
                    excerciseData.add(info);
                }
                else if(object.getString(Constants.exerciseType).equals(getString(R.string.workout))) {
                    CreatedWorkoutInfo info = new CreatedWorkoutInfo();
                    info.setObjectId(object.getObjectId());
                    info.setWorkoutObjectId(object.getString(Constants.workoutId));
                    info.setWorkoutName(object.getString(Constants.workoutName));
                    getWorkoutDetailedInfo(info);
                    excerciseData.add(info);
                }
            }
        }
    }

    private void getCardioComponentsInfo(CreatedWorkoutInfo info, List<String> names) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, info.getWorkoutObjectId());

        double totalNumberOfCalories = 0;

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            names.add(object.getString(Constants.exerciseName));
            totalNumberOfCalories += object.getDouble(Constants.caloriesBurned);
        }

        info.setTotalNumberOfCalories(Double.valueOf(totalNumberOfCalories).intValue());
    }

    private void getStrengthComponentsInfo(CreatedWorkoutInfo info, List<String> names) throws Exception{
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, info.getWorkoutObjectId());

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            names.add(object.getString(Constants.exerciseName));
        }
    }

    private void getWorkoutDetailedInfo(CreatedWorkoutInfo info) throws Exception {
        // Must get names of all exercises and total number of calories associated with it
        List<String> names = new ArrayList<>();
        getCardioComponentsInfo(info, names);
        getStrengthComponentsInfo(info, names);
        info.setExerciseNames(names);
    }

    public void updateCalorieCountOnHeader() {
        int totalOverallCalorieCount = getIntegerFromString(breakfastTotalCalories) +
                getIntegerFromString(lunchTotalCalories) +
                getIntegerFromString(dinnerTotalCalories) +
                getIntegerFromString(snacksTotalCalories) +
                getIntegerFromString(waterTotalCalories);

        int totalExerciseCalories = getIntegerFromString(exerciseTotalCalories);

        targetCalories.setText(String.valueOf(TARGET_CALORIES_AMOUNT));
        intakeCalories.setText(String.valueOf(totalOverallCalorieCount));
        exerciseCalories.setText(String.valueOf(totalExerciseCalories));
        netCalories.setText(String.valueOf(TARGET_CALORIES_AMOUNT - totalOverallCalorieCount + totalExerciseCalories));
    }

    public void updateRecyclerHeaderCalorieCount(ArrayList<Object> data, TextView textView) {
        double totalCalorieCount = 0;
        DetailedFoodResultsInfo newInfo;
        DetailedRecipeResultsInfo newRecipeInfo;
        for(Object info : data) {
            if(info instanceof DetailedFoodResultsInfo) {
                newInfo = (DetailedFoodResultsInfo) info;
                totalCalorieCount += (newInfo.getCalories() * newInfo.getNumberOfServings());
            }
            else if (info instanceof  DetailedRecipeResultsInfo) {
                newRecipeInfo = (DetailedRecipeResultsInfo) info;
                totalCalorieCount += (newRecipeInfo.getServingsList().get(newRecipeInfo.getSelectedServingOption()).getCalories()
                                        * newRecipeInfo.getChosenNumberOfServings());
            }
            else if (info instanceof MealInfo) {
                totalCalorieCount += ((MealInfo) info).getTotalNumberOfCalories();
            }
            else if(info instanceof CardioInfo) {
                totalCalorieCount += ((CardioInfo) info).getCaloriesBurned();
            }
            else if(info instanceof CreatedWorkoutInfo) {
                totalCalorieCount += ((CreatedWorkoutInfo) info).getTotalNumberOfCalories();
            }
        }

        if(totalCalorieCount == 0) {
            textView.setVisibility(View.GONE);
        }
        else {
            Double d = totalCalorieCount;
            textView.setVisibility(View.VISIBLE);
            textView.setText(String.format(getString(R.string.x_kcals), d.intValue()));
        }
    }

    private void setUpButtons() {

        previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(dayOffset + DiaryFragment.MIDDLE_PAGE - 1);
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(dayOffset + DiaryFragment.MIDDLE_PAGE + 1);
            }
        });
    }


    public static long getDateInDays(int offset) {
        Date epoch = new Date(0);
        long days = TimeUnit.MILLISECONDS.toDays(getTimeInMillisWhileAccountingForTimezone() - epoch.getTime());
        return days + offset;
    }

    public static long getTimeInMillisWhileAccountingForTimezone() {
        Calendar c = Calendar.getInstance();
        TimeZone timeZone = c.getTimeZone();
        long millis = c.getTimeInMillis();
        return millis + timeZone.getRawOffset();
    }

    public void setUpDay() {
        if(dayOffset == 0 ){
            date = getString(R.string.today);
        }
        else if(dayOffset == -1) {
            date = getString(R.string.yesterday);

        }
        else if(dayOffset == 1) {
            date = getString(R.string.tomorrow);

        }
        else {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            c.add(Calendar.DATE, dayOffset);
            date = sdf.format(c.getTime());
        }

        dateTextView.setText(date);
    }

    private void setUpQuickButtons() {
        breakfastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addBreakfastButton.getTop());            }
        });
        lunchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addLunchButton.getTop());            }
        });
        dinnerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addDinnerButton.getTop());            }
        });
        snacksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addSnacksButton.getTop());
            }
        });
        waterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addWaterButton.getTop());            }
        });
        exerciseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, addExButton.getTop());            }
        });
    }

    private void retrieveArguments() {
        Bundle args = getArguments();
        dayOffset = args.getInt(Constants.DAY_OFFSET);
        TARGET_CALORIES_AMOUNT = args.getInt(Constants.TOTAL_CALORIES_AMOUNT, Constants.defaultTargetCalories);
    }

    private void bindViews(View view) {
        fatSecretDiary = new FatSecretDiary();
        viewPager = (ViewPager) getActivity().findViewById(R.id.viewpager);
        nextButton = (ImageView) view.findViewById(R.id.next_button);
        previousButton = (ImageView) view.findViewById(R.id.previous_button);
        dateTextView = (TextView) view.findViewById(R.id.day_text_view);
        breakfastRecycler = (RecyclerView) view.findViewById(R.id.breakfast_recycler);
        lunchRecycler = (RecyclerView) view.findViewById(R.id.lunch_recycler);
        dinnerRecycler = (RecyclerView) view.findViewById(R.id.dinner_recycler);
        snacksRecycler = (RecyclerView) view.findViewById(R.id.snacks_recycler);
        waterRecycler = (RecyclerView) view.findViewById(R.id.water_recycler);
        excerciseRecycler = (RecyclerView) view.findViewById(R.id.excercise_recycler);
        addBreakfastButton = (RelativeLayout) view.findViewById(R.id.breakfast_header);
        addLunchButton = (RelativeLayout) view.findViewById(R.id.lunch_header);
        addDinnerButton = (RelativeLayout) view.findViewById(R.id.dinner_header);
        addSnacksButton = (RelativeLayout) view.findViewById(R.id.snacks_header);
        addWaterButton = (RelativeLayout) view.findViewById(R.id.water_header);
        addExButton = (RelativeLayout) view.findViewById(R.id.excercise_header);
        scrollView = (ScrollView) view.findViewById(R.id.parent_scrollview);
        mainLayout = (RelativeLayout) view.findViewById(R.id.main_relative_layout);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        breakfastTotalCalories = (TextView) view.findViewById(R.id.breakfast_total_calorie_count);
        lunchTotalCalories = (TextView) view.findViewById(R.id.lunch_total_calorie_count);
        dinnerTotalCalories = (TextView) view.findViewById(R.id.dinner_total_calorie_count);
        exerciseTotalCalories = (TextView) view.findViewById(R.id.exercise_total_calorie_count);
        waterTotalCalories = (TextView) view.findViewById(R.id.water_total_calorie_count);
        snacksTotalCalories = (TextView) view.findViewById(R.id.snacks_total_calorie_count);
        targetCalories = (TextView) view.findViewById(R.id.target_calories_textview);
        exerciseCalories = (TextView) view.findViewById(R.id.exercise_calories_textview);
        intakeCalories = (TextView) view.findViewById(R.id.intake_calories_textview);
        netCalories = (TextView) view.findViewById(R.id.net_calories_textview);
        breakfastButton = (Button) view.findViewById(R.id.button_breakfast);
        lunchButton = (Button) view.findViewById(R.id.button_lunch);
        dinnerButton = (Button) view.findViewById(R.id.button_dinner);
        snacksButton = (Button) view.findViewById(R.id.button_snacks);
        exerciseButton = (Button) view.findViewById(R.id.button_exercise);
        waterButton = (Button) view.findViewById(R.id.button_water);
        headerBreakfast = (Button) view.findViewById(R.id.add_bf_button);
        headerLunch = (Button) view.findViewById(R.id.add_lun_button);
        headerDinner = (Button) view.findViewById(R.id.add_din_button);
        headerWater = (Button) view.findViewById(R.id.add_wat_button);
        headerEx = (Button) view.findViewById(R.id.add_ex_button);
        headerSnacks = (Button) view.findViewById(R.id.add_sna_button);
        fragmentManager = getFragmentManager();
        foods = new FatSecretFoods();
        recipes = new FatSecretRecipes();
        exerciseRequest = new ExerciseRequest();
    }

    private void destroyTempDataFromDatabase() throws Exception {
        // FOOD DATA
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        query.whereEqualTo(Constants.isPermanent, false);
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {
                object.delete();
            }
        }

        // WATER DATA
        ParseQuery<ParseObject> waterQuery = new ParseQuery<ParseObject>(Constants.WaterDiary);
        waterQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        waterQuery.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        waterQuery.whereEqualTo(Constants.isPermanent, false);
        if(waterQuery.count() != 0) {
            List<ParseObject> result = waterQuery.find();
            for(ParseObject object : result) {
                object.delete();
            }
        }

        // EXERCISE DATA
        ParseQuery<ParseObject> exQuery = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        exQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        exQuery.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        exQuery.whereEqualTo(Constants.isPermanent, false);
        if(exQuery.count() != 0) {
            List<ParseObject> result = exQuery.find();
            for(ParseObject object : result) {
                object.delete();
            }
        }
    }

    private void validateExerciseEntries() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        query.whereEqualTo(Constants.isPermanent, false);
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {
                object.put(Constants.isPermanent, true);
                object.save();
            }
        }
    }

    private void validateWaterEntries() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.WaterDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        query.whereEqualTo(Constants.isPermanent, false);
        if(query.count() != 0) {
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {
                object.put(Constants.isPermanent, true);
                object.save();
            }
        }
    }

    private void validateExistingFoodsInDatabase() throws Exception{
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, getDateInDays(dayOffset));
        query.whereEqualTo(Constants.isPermanent, false);
        if(query.count() != 0 ){
            List<ParseObject> result = query.find();
            for(ParseObject object : result) {

                object.put(Constants.isPermanent, true);

                if(object.getString(Constants.entryType).equals(getString(R.string.food))) {
                    String mealType = null;
                    if(object.getString(Constants.mealType).equals(getString(R.string.breakfast)) ||
                            object.getString(Constants.mealType).equals(getString(R.string.lunch)) ||
                            object.getString(Constants.mealType).equals(getString(R.string.dinner))) {
                        mealType = object.getString(Constants.mealType).toLowerCase();
                    }
                    else {
                        mealType = Constants.other;
                    }
                    // API CALL TO CREATE A FOOD ENTRY IN FATSECRET DATABASE
                    long foodEntryId = fatSecretDiary.createFoodEntry(object.getLong(Constants.foodIdNumber),
                            Constants.placeholderName, object.getLong(Constants.servingIdField),
                            object.getDouble(Constants.numberServings), mealType, getDateInDays(dayOffset));
                    object.put(Constants.foodEntryIdField, foodEntryId);
                }
                object.save();
            }
        }
    }

    private boolean allDataLoadingIsComplete() {
        return isBreakfastDone && isLunchDone && isDinnerDone && isSnacksDone && isWaterDone && isExerciseDone;
    }

    private void updateOnScreenData() {
        hideRecyclerIfEmptyDataSet();
        updateRecyclerHeaderCalorieCount(breakfastData, breakfastTotalCalories);
        updateRecyclerHeaderCalorieCount(lunchData, lunchTotalCalories);
        updateRecyclerHeaderCalorieCount(dinnerData, dinnerTotalCalories);
        updateRecyclerHeaderCalorieCount(snacksData, snacksTotalCalories);
        updateRecyclerHeaderCalorieCount(excerciseData, exerciseTotalCalories);
        updateCalorieCountOnHeader();
        setViewVisibility(progressBar, false, true);
        setViewVisibility(mainLayout, true, true);
        scrollView.smoothScrollTo(0,0);
    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class DestroyTempDataFromDatabase extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                destroyTempDataFromDatabase();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(isAdded()) {
                if (!aBoolean) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class ValidateExistingFoods extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            ((DiaryFragment) getParentFragment()).showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                validateExistingFoodsInDatabase();
                validateWaterEntries();
                validateExerciseEntries();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if(aBoolean) {
                Snackbar snackbar =  Snackbar.make(viewPager, getString(R.string.diary_updated), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                HAS_UNSAVED_CHANGES = false;
//                Toast.makeText(getContext(), getString(R.string.diary_updated), Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
//            ((DiaryFragment) getParentFragment()).showMainLayout(true);
        }
    }

    private class PopulateBreakfast extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isBreakfastDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareBreakfastData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isBreakfastDone = true;
                breakfastAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateBreakfast().execute();
                    }
                }
            }
        }
    }

    private class PopulateLunch extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isLunchDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareLunchData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isLunchDone = true;
                lunchAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateLunch().execute();
                    }
                }
            }
        }
    }

    private class PopulateDinner extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isDinnerDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareDinnerData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isDinnerDone = true;
                dinnerAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateDinner().execute();
                    }
                }
            }
        }
    }

    private class PopulateSnacks extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isSnacksDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareSnacksData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isSnacksDone = true;
                snacksAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateSnacks().execute();
                    }
                }
            }
        }
    }

    private class PopulateWater extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isWaterDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareWaterData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isWaterDone = true;
                waterAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateWater().execute();
                    }
                }
            }
        }
    }

    private class PopulateExercise extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(mainLayout, false, true);
            isExerciseDone = false;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                prepareExcerciseData();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                isExerciseDone = true;
                excerciseAdapter.notifyDataSetChanged();
                if(isAdded()) {
                    if(allDataLoadingIsComplete()) {
                        updateOnScreenData();
                    }
                }
            }
            else {
                if(isAdded()) {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if (RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new PopulateExercise().execute();
                    }
                }
            }
        }
    }

}
