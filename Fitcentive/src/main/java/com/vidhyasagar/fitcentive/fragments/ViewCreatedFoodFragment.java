package com.vidhyasagar.fitcentive.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditFoodActivity;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.MainActivity;
import com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.CarbsDetailedInfoDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.FatBreakdownDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.VitaminsAndMineralsDialogFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.

 Created on 02-12-2016 by Vidhyasagar
 */

// TODO: Handle gracefully if object not found, same goes for meals too
public class ViewCreatedFoodFragment extends Fragment {

    public static String TAG = "VIEW_CREATED_FOOD_FRAGMENT";
    public static int RETURN_TO_VIEW_CREATED_FOOD = 21;

    public static ViewCreatedFoodFragment newInstance (String objectId, double numberOfServings,
                                                       boolean isEditMode, int dayOffset,
                                                       DiaryPageFragment.ENTRY_TYPE entryType) {
        ViewCreatedFoodFragment fragment = new ViewCreatedFoodFragment();
        Bundle args = new Bundle();
        args.putString(Constants.objectId, objectId);
        args.putDouble(Constants.numberOfServings, numberOfServings);
        args.putBoolean(ExpandedRecipeActivity.IS_EDITING, isEditMode);
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putSerializable(Constants.entryType, entryType);
        fragment.setArguments(args);
        return fragment;
    }

    public ViewCreatedFoodFragment() {
        // Required empty public constructor
    }

    int dayOffset, newDayOffset;

    DiaryPageFragment.ENTRY_TYPE entryType, newEntryType;
    String objectId;
    boolean isEditMode;
    double numberOfServings;

    DetailedFoodResultsInfo detailedFoodInfo;
    double totalFatCarbsAndProteins;

    PieChart pieChart;
    TextView foodBrand, foodName, foodServingSize;
    TextView fats, carbs, proteins, calories;
    Button previousButton, nextButton, addToDiaryButton;
    EditText servingSizeEditText;
    TextView servingSize;
    TextView vitaminsAndMinerals;
    Spinner dialogServingSizeSpinner;
    LinearLayout linearLayout;
    LinearLayout parentLayout;
    LinearLayout carbsLayout, proteinsLayout, fatsLayout;
    ProgressBar progressBar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_created_food, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpButtons();
        setUpClickableTextView();
        setUpClickableMacros();

    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchDetailedInfo().execute();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_view_created_food, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                startEditCreatedFoodActivity();
                break;
            case R.id.action_add_food:
                addFoodToDiary();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startEditCreatedFoodActivity() {
        Intent i = new Intent(getContext(), CreateOrEditFoodActivity.class);
        i.putExtra(Constants.objectId, objectId);
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, true);
        startActivityForResult(i, RETURN_TO_VIEW_CREATED_FOOD);
    }

    private void addFoodToDiary() {
        ((EditFoodEntryActivity) getActivity()).addCurrentFood();
    }

    private void retrieveArguments() {
        objectId = getArguments().getString(Constants.objectId);
        numberOfServings = getArguments().getDouble(Constants.numberOfServings);
        isEditMode = getArguments().getBoolean(ExpandedRecipeActivity.IS_EDITING, false);
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        entryType = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(Constants.entryType);
        newEntryType = entryType;
        newDayOffset = dayOffset;
    }

    private void bindViews(View view) {
        foodBrand = (TextView) view.findViewById(R.id.food_brand);
        foodName = (TextView) view.findViewById(R.id.food_name);
        foodServingSize = (TextView) view.findViewById(R.id.food_serving_size);
        fats = (TextView) view.findViewById(R.id.fats_textview);
        carbs = (TextView) view.findViewById(R.id.carbs_textview);
        proteins = (TextView) view.findViewById(R.id.proteins_textview);
        calories = (TextView) view.findViewById(R.id.calories_textview);
        pieChart = (PieChart) view.findViewById(R.id.pie_chart);
        previousButton = (Button) view.findViewById(R.id.previous_button);
        nextButton = (Button) view.findViewById(R.id.next_button);
        linearLayout = (LinearLayout) view.findViewById(R.id.serving_layout);
        servingSize = (TextView) view.findViewById(R.id.number_of_servings);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        parentLayout = (LinearLayout) view.findViewById(R.id.parent_linear_layout);
        addToDiaryButton = (Button) view.findViewById(R.id.add_to_diary_button);
        vitaminsAndMinerals = (TextView) view.findViewById(R.id.view_vitamins_textview);
        carbsLayout = (LinearLayout) view.findViewById(R.id.carbs_layout);
        proteinsLayout = (LinearLayout) view.findViewById(R.id.proteins_layout);
        fatsLayout = (LinearLayout) view.findViewById(R.id.fats_layout);
    }

    private void setUpButtons() {
        previousButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        addToDiaryButton.setText(getString(R.string.save));
        addToDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditFoodEntryActivity) getActivity()).addCurrentFood();
            }
        });
    }

    public void saveCurrentFoodToDiary() {
        new SaveOrEditCurrentFoodToDiay().execute();
    }

    private void setUpClickableMacros() {
        carbsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_CARBS);
            }
        });
        fatsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_FATS);
            }
        });
        proteinsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(RecipeNutritionFragment.CASE_PROTEINS);
            }
        });
    }

    private void showDetailedBreakDown(int type) {
        switch (type) {
            case RecipeNutritionFragment.CASE_CARBS: // Carbs
                showDetailedCarbsBreakdown();
                break;
            case RecipeNutritionFragment.CASE_FATS: // Fats
                showDetailedFatsBreakdown();
                break;
            case RecipeNutritionFragment.CASE_PROTEINS: // Protein
                showDetailedProteinBreakdown();
                break;
            default: return;
        }
    }

    private void showDetailedCarbsBreakdown() {
        FragmentManager fm = getFragmentManager();
        CarbsDetailedInfoDialogFragment carbsFragment = CarbsDetailedInfoDialogFragment.newInstance(
                detailedFoodInfo.getCholestrol(),
                detailedFoodInfo.getSugar(),
                detailedFoodInfo.getFiber(), Double.valueOf(servingSize.getText().toString()));

        if(carbsFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            carbsFragment.show(fm, CarbsDetailedInfoDialogFragment.TAG);
        }
    }

    private void showDetailedFatsBreakdown() {
        FragmentManager fm = getFragmentManager();
        FatBreakdownDialogFragment fatFragment =
                FatBreakdownDialogFragment.newInstance(detailedFoodInfo.getPolyUnsaturatedFat(),
                        detailedFoodInfo.getMonoUnsaturatedFat(),
                        detailedFoodInfo.getTransFat(),
                        detailedFoodInfo.getSaturatedFat(), Double.valueOf(servingSize.getText().toString()));
        if(fatFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            fatFragment.show(fm, FatBreakdownDialogFragment.TAG);
        }
    }

    private void showDetailedProteinBreakdown() {
        Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.protein_data_text), Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void setUpClickableTextView() {
        vitaminsAndMinerals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedVitaminsAndMineralsBreakdown();
            }
        });
    }

    private void showDetailedVitaminsAndMineralsBreakdown() {
        FragmentManager fm = getFragmentManager();
        VitaminsAndMineralsDialogFragment vitFragment = VitaminsAndMineralsDialogFragment.newInstance(
                detailedFoodInfo.getSodium(),
                detailedFoodInfo.getPotassium(),
                detailedFoodInfo.getCalcium(),
                detailedFoodInfo.getIron(),
                detailedFoodInfo.getVitamin_a(),
                detailedFoodInfo.getVitamin_c(), Double.valueOf(servingSize.getText().toString()));

        if(vitFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            vitFragment.show(fm, VitaminsAndMineralsDialogFragment.TAG);
        }
    }

    private void calculateTotal() {
        totalFatCarbsAndProteins = detailedFoodInfo.getProtein() +
                detailedFoodInfo.getCarbs() +
                detailedFoodInfo.getFat();
    }

    private void setUpPieChart() {
        calculateTotal();
        pieChart.setBackgroundColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription(getString(R.string.nutritional_breakdown));
        pieChart.setDescriptionColor(ContextCompat.getColor(getContext(), R.color.transp_black));
        pieChart.setDescriptionTextSize(11f);
        pieChart.setHoleRadius(5f);
        pieChart.setTransparentCircleRadius(1f);
        pieChart.setDrawSliceText(false);
//        pieChart.setCenterText(getString(R.string.nutritional_breakdown));
        pieChart.setCenterTextSize(13f);

        Legend l = pieChart.getLegend();
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_blue_light, android.R.color.holo_red_light, android.R.color.holo_green_light};
//        l.setCustom(colors, categories);


        ArrayList<Entry> breakdown = new ArrayList<Entry>();

        Entry carbs = new Entry( (float) (detailedFoodInfo.getCarbs() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_CARBS); // 0 == Carbs
        breakdown.add(carbs);
        Entry fats = new Entry( (float) (detailedFoodInfo.getFat() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_FATS); // 0 == Carbs
        breakdown.add(fats);
        Entry proteins = new Entry( (float) (detailedFoodInfo.getProtein() / totalFatCarbsAndProteins) * 100, RecipeNutritionFragment.CASE_PROTEINS); // 0 == Carbs
        breakdown.add(proteins);

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
//        setBreakdown.setHighlightEnabled(true);
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, getContext());

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));
//
        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.animateXY(750, 750);

        pieChart.invalidate(); // refresh

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                // display msg when value selected
                if (e == null)
                    return;
                else {
                    showDetailedBreakDown(e.getXIndex());
                }
            }

            @Override
            public void onNothingSelected() {
                Log.i("applog", "Nothing is selected");
            }
        });
    }

    private void setUpOnScreenData() {
        foodName.setText(detailedFoodInfo.getFoodName());
        if(detailedFoodInfo.getBrandName().isEmpty()) {
            foodBrand.setVisibility(View.GONE);
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) foodServingSize.getLayoutParams();
            params.setMarginStart(0);
            foodServingSize.setLayoutParams(params);
        }
        else {
            foodBrand.setVisibility(View.VISIBLE);
            foodBrand.setText(String.format(getString(R.string.food_brand_name), detailedFoodInfo.getBrandName()));
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) foodServingSize.getLayoutParams();
            int dpValue = 5; // margin in dips
            float d = getContext().getResources().getDisplayMetrics().density;
            int margin = (int)(dpValue * d); // margin in pixels
            params.setMarginStart(margin);
            foodServingSize.setLayoutParams(params);
        }
        foodServingSize.setText(detailedFoodInfo.getServingDescription());

        fats.setText(String.format(getString(R.string.two_decimal_places), detailedFoodInfo.getFat() * Double.valueOf(servingSize.getText().toString())));
        carbs.setText(String.format(getString(R.string.two_decimal_places), detailedFoodInfo.getCarbs() * Double.valueOf(servingSize.getText().toString())));
        proteins.setText(String.format(getString(R.string.two_decimal_places), detailedFoodInfo.getProtein() * Double.valueOf(servingSize.getText().toString())));
        calories.setText(String.format(getString(R.string.two_decimal_places), detailedFoodInfo.getCalories() * Double.valueOf(servingSize.getText().toString())));
    }

    private void setUpServingMenu() {
        if(numberOfServings == -1) {
            servingSize.setText(String.valueOf(getContext().getResources().getInteger(R.integer.default_serving_size)));
        }
        else {
            servingSize.setText(String.valueOf(numberOfServings));
        }
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServingDialog();
            }
        });
    }

    public void createServingDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.serving_size))
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_serving_size_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_serving_size_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();


        servingSizeEditText = (EditText) dialog.findViewById(R.id.number_of_servings);
        dialogServingSizeSpinner = (Spinner) dialog.findViewById(R.id.serving_size);

        servingSizeEditText.setText(servingSize.getText().toString());
        servingSizeEditText.setSelection(servingSizeEditText.getText().length());

        setUpSpinner();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(servingSizeEditText.getText()) && Double.valueOf(servingSizeEditText.getText().toString()) != 0) {
                    updateNumberOfServings();
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_invalid_number_servings), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateNumberOfServings() {
        servingSize.setText(servingSizeEditText.getText().toString());
        fats.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(detailedFoodInfo.getFat() * Double.valueOf(servingSize.getText().toString()))));
        carbs.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(detailedFoodInfo.getCarbs() * Double.valueOf(servingSize.getText().toString()))));
        proteins.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(detailedFoodInfo.getProtein() * Double.valueOf(servingSize.getText().toString()))));
        calories.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(detailedFoodInfo.getCalories() * Double.valueOf(servingSize.getText().toString()))));
    }


    private void setUpSpinner() {

        List<String> spinnerArray =  new ArrayList<>();
        spinnerArray.add(detailedFoodInfo.getServingDescription());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dialogServingSizeSpinner.setAdapter(adapter);

    }

    private String getServingDescription(double servingSize, String unit) {
        return String.valueOf(servingSize) + " " + unit;
    }

    private void fetchDetailedInfo() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedFood);
        ParseObject food = query.get(objectId);

        DetailedFoodResultsInfo info = new DetailedFoodResultsInfo(-1, -1, Constants.unknown,
                getServingDescription(food.getDouble(Constants.servingSizeField), food.getString(Constants.servingSizeUnit)),
                        -1, null, null, -1, getContext());
        info.setFoodName(food.getString(Constants.foodNameField));
        info.setBrandName(food.getString(Constants.foodBrand));
        info.setFoodDescription(food.getString(Constants.foodDescriptionField));

        info.setCalories(food.getDouble(Constants.caloriesField));
        info.setFat(food.getDouble(Constants.fatsField));
        info.setProtein(food.getDouble(Constants.proteinsField));
        info.setCarbs(food.getDouble(Constants.carbsField));

        info.setPolyUnsaturatedFat(food.getDouble(Constants.polyFats));
        info.setMonoUnsaturatedFat(food.getDouble(Constants.monoFats));
        info.setTransFat(food.getDouble(Constants.transFats));
        info.setSaturatedFat(food.getDouble(Constants.satFats));

        info.setCholestrol(food.getDouble(Constants.cholesterolField));
        info.setSugar(food.getDouble(Constants.sugar));
        info.setFiber(food.getDouble(Constants.fiber));
        info.setSodium(food.getDouble(Constants.sodium));
        info.setPotassium(food.getDouble(Constants.potassium));
        info.setCalcium(food.getDouble(Constants.calcium));
        info.setIron(food.getDouble(Constants.iron));
        info.setVitamin_a(food.getDouble(Constants.vitaminA));
        info.setVitamin_c(food.getDouble(Constants.vitaminC));

        detailedFoodInfo = info;

    }

    public void setNewDayOffset(int offset) {
        this.newDayOffset = offset;
    }

    public void setNewEntryType(DiaryPageFragment.ENTRY_TYPE type) {
        this.newEntryType = type;
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.newDayOffset);
        getActivity().startActivity(i);
        getActivity().finish();
    }

    private void editCurrentFoodEntryInDiary() throws Exception{
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.numberServings, numberOfServings);
        query.whereEqualTo(Constants.entryType, getString(R.string.created_food));
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        switch (entryType) {
            case BREAKFAST:
                query.whereEqualTo(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                query.whereEqualTo(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                query.whereEqualTo(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                query.whereEqualTo(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                query.whereEqualTo(Constants.mealType, getString(R.string.water));
                break;
            default:
                break;
        }

        ParseObject object = query.getFirst();
        object.put(Constants.numberServings, Double.valueOf(servingSize.getText().toString()));
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        switch (newEntryType) {
            case BREAKFAST:
                object.put(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                object.put(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                object.put(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                object.put(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                object.put(Constants.mealType, getString(R.string.water));
                break;
        }
        object.save();

    }

    private void addCurrentFoodEntryToDiary() throws Exception {
        ParseObject newObject = new ParseObject(Constants.FoodDiary);
        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.entryType, getString(R.string.created_food));
        newObject.put(Constants.createdFoodObjectId, objectId);
        switch (newEntryType) {
            case BREAKFAST:
                newObject.put(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                newObject.put(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                newObject.put(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                newObject.put(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                newObject.put(Constants.mealType, getString(R.string.water));
                break;
        }
        newObject.put(Constants.numberServings, Double.valueOf(servingSize.getText().toString()));
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        newObject.save();
    }

    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    //-------------------------
    // ASYNCTASK(S)
    //-------------------------

    private class FetchDetailedInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(parentLayout, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchDetailedInfo();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                if(e instanceof ParseException) {
                    if(((ParseException) e).getCode() == ParseException.OBJECT_NOT_FOUND) {
                        returnToDiaryPage();
                        return true;
                    }
                }
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setUpPieChart();
                    setUpServingMenu();
                    setUpOnScreenData();
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(parentLayout, true, true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private class SaveOrEditCurrentFoodToDiay extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(parentLayout, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(isEditMode) {
                    editCurrentFoodEntryInDiary();
                }
                else {
                    addCurrentFoodEntryToDiary();
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    setViewVisibility(progressBar, false, true);
                    setViewVisibility(parentLayout, true, true);
                    returnToDiaryPage();
                } else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
