package com.vidhyasagar.fitcentive.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.MainActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.UserResultsAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.UserResultsInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserResultsFragment extends Fragment {

    RecyclerView recyclerView;
    ArrayList<UserResultsInfo> uData;
    UserResultsAdapter rAdapter;
    LinearLayoutManager lm;
    ProgressBar progressBar;
    FetchUserResults fetchUserResultsTask;

    boolean isMainActivity;

    public UserResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_results, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();

        Bundle bundle = getArguments();
        uData = new ArrayList<>();
        recyclerView = (RecyclerView) getActivity().findViewById(R.id.results_recycler);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar_res_fragment);

        rAdapter = new UserResultsAdapter(uData, getContext());
        lm = new LinearLayoutManager(getContext());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(rAdapter);

        fetchUserResultsTask = new FetchUserResults();
        isMainActivity = bundle.getBoolean("isMainActivity", false);
        String query = bundle.getString("query");
        if(!query.isEmpty()) {
            fetchUserResultsTask.execute(bundle.getString("query"));
        }
    }


    // TODO : Make integer constants INTEGER RESOURCES
    // TODO : Handle graceful asynctask cancel exit
    private class FetchUserResults extends AsyncTask<String, Void, Boolean> {

        Bitmap fetchedImage;
        ParseFile fetchedImageFile;
        byte[] byteArray;

        private void addToFoundUsersList(List<ParseUser> users) throws ParseException {
            for(ParseUser user : users) {
                fetchedImageFile = user.getParseFile(Constants.profilePicture);
                if(fetchedImageFile != null) {
                    byteArray = fetchedImageFile.getData();
                    fetchedImage = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                    uData.add(new UserResultsInfo(user.getString(Constants.firstname) + " " + user.getString(Constants.lastname),
                            user.getUsername(), fetchedImage));
                }
                else {
                    uData.add(new UserResultsInfo(user.getString(Constants.firstname) + " " + user.getString(Constants.lastname),
                            user.getUsername(), BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_logo)));
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                recyclerView.setVisibility(View.GONE);
                recyclerView.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        recyclerView.setVisibility(View.GONE);
                    }
                });
                progressBar.setVisibility(View.VISIBLE);
                progressBar.animate().setDuration(shortAnimTime).alpha(1).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressBar.setVisibility(View.VISIBLE);
                    }
                });
            }
            else {
                recyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String input = params[0];
            ParseQuery<ParseUser> userNameQuery = ParseUser.getQuery();
            ParseQuery<ParseUser> firstNameQuery = ParseUser.getQuery();
            ParseQuery<ParseUser> lastNameQuery = ParseUser.getQuery();
            List<ParseUser> usernameQueryResult;
            List<ParseUser> firstNameQueryResult;
            List<ParseUser> lastNameQueryResult;
            try {
                if (ProfileActivity.containsWhitespace(input)) {
                    // There is a first-name last-name combination here
                    // Capitalize first and last name before search
                    // Currently only support for first-name/last-name combo
                    // Code will crash if there is 3 names encountered
                    String[] names = input.split("\\s+");
                    String firstname = names[0];
                    String lastname = names[1];
                    firstname = firstname.substring(0,1).toUpperCase() + firstname.substring(1);
                    lastname = lastname.substring(0,1).toUpperCase() + lastname.substring(1);
                    ParseQuery<ParseUser> query = ParseUser.getQuery();
                    query.whereContains(Constants.firstname, firstname);
                    query.whereContains(Constants.lastname, lastname);
                    List<ParseUser> result = query.find();
                    if(result.size() == 0) {
                        // No users found, do some logic here
                        uData.add(null);
                    }
                    else {
                        addToFoundUsersList(result);
                    }
                } else {
                    // Can be just first name, just last name or username
                    if (input.charAt(0) == '@') {
                        // Username search
                        userNameQuery.whereContains(Constants.username, input.substring(1));
                        usernameQueryResult = userNameQuery.find();
                        if(usernameQueryResult.size() == 0) {
                            // No users found, do some logic here
                            uData.add(null);
                        }
                        else {
                            addToFoundUsersList(usernameQueryResult);
                        }

                    } else {
                        // perform both first-name and last-name search
                        firstNameQuery.whereContains(Constants.firstname, input.substring(0,1).toUpperCase() + input.substring(1));
                        lastNameQuery.whereContains(Constants.lastname, input.substring(0,1).toUpperCase() + input.substring(1));
                        firstNameQueryResult = firstNameQuery.find();
                        lastNameQueryResult = lastNameQuery.find();

                        if(firstNameQueryResult.size() == 0 && lastNameQueryResult.size() == 0) {
                            // No users found, do some logic here
                            uData.add(null);
                        }
                        else {
                            if(firstNameQueryResult.size() != 0) {
                                addToFoundUsersList(firstNameQueryResult);
                            }
                            if(lastNameQueryResult.size() != 0) {
                                addToFoundUsersList(lastNameQueryResult);
                            }
                        }

                    }
                }
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if (aBoolean) {
                    // Hiding progress bar and showing recylcer
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                        progressBar.setVisibility(View.GONE);
                        progressBar.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.animate().setDuration(shortAnimTime).alpha(1).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                recyclerView.setVisibility(View.VISIBLE);
                            }
                        });
                    } else {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    // Updating data set
                    rAdapter.notifyDataSetChanged();
                    // Resizing framelayout as needed
                    if (uData.size() < 5) {
                        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (uData.size() * 70), getContext().getResources().getDisplayMetrics());
                        if (isMainActivity) {
                            ((MainActivity) getActivity()).setFrameLayoutWidth(height);
                        } else {
                            ((ProfileActivity) getActivity()).setFrameLayoutWidth(height);
                        }
                    } else {
                        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 350, getContext().getResources().getDisplayMetrics());
                        if (isMainActivity) {
                            ((ProfileActivity) getActivity()).setFrameLayoutWidth(height);
                        } else {
                            ((ProfileActivity) getActivity()).setFrameLayoutWidth(height);
                        }
                    }
                } else {
                    // An error occurred, handle it
                    Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                    // Hide progress bar and do NOT show the recycler either
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                        progressBar.setVisibility(View.GONE);
                        progressBar.animate().setDuration(shortAnimTime).alpha(0).setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }
        }
    }
}
