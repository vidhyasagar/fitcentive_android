package com.vidhyasagar.fitcentive.utilities;

/**
 * Created by vharihar on 2016-12-13.
 */

public class FitnessFormulae {

    // BMR CONSTANTS - BASAL METABOLIC RATE
    public static final int BMR_BASE_MEN = 66;
    public static final double BMR_WEIGHT_MEN = 6.23;
    public static final double BMR_HEIGHT_MEN = 12.7;
    public static final double BMR_AGE_MEN = 6.8;
    public static final int BMR_BASE_WOMEN = 655;
    public static final double BMR_WEIGHT_WOMEN = 4.35;
    public static final double BMR_AGE_AND_HEIGHT_WOMEN = 4.7;

    // ACTIVITY MULTIPLIERS
    public static final double little = 1.2;
    public static final double light = 1.375;
    public static final double moderate = 1.55;
    public static final double heavy = 1.725;

    public static final double LBS_TO_KGS = 2.2;
    public static final double MINS_TO_HOURS = 60;

    // This returns your Basal Metabolic Rate, the calories a body burns while at REST
    // Refer to http://www.builtlean.com/2010/03/14/how-to-calculate-your-calorie-burn/
    // Uses the Harris Benedict Method
    public static double getBMR(boolean isMen, double weightInPounds, double heightInInches, double ageInYears) {
        if(isMen) {
            return BMR_BASE_MEN + (BMR_WEIGHT_MEN * weightInPounds) + (BMR_HEIGHT_MEN * heightInInches) - (BMR_AGE_MEN * ageInYears);
        }
        else {
            return BMR_BASE_WOMEN + (BMR_WEIGHT_WOMEN * weightInPounds) + (BMR_AGE_AND_HEIGHT_WOMEN * heightInInches) - (BMR_AGE_AND_HEIGHT_WOMEN * ageInYears);
        }
    }

    public static double getCaloriesBurned(double weightInPounds, double METvalue, double timeInMinutes) {
        double weightInKgs = weightInPounds / LBS_TO_KGS;
        double timeInHours = timeInMinutes / MINS_TO_HOURS;
        return weightInKgs * METvalue * timeInHours;
    }

}
