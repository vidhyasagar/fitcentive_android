package com.vidhyasagar.fitcentive.utilities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by vharihar on 2016-12-14.
 */

// Contains functions common to everyone

public class Utilities {
    public static void setViewVisibility(final View v, final boolean show, final boolean isGone, Context mContext) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = mContext.getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public static void showSnackBar(String message, View layout) {
        Snackbar snackbar =  Snackbar.make(layout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void hideKeyboard(View v, Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static ArrayList<Integer> toIntegerArrayList(List<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for(Integer i : list) {
            newList.add(i);
        }
        return newList;
    }

    public static String convertFromUPC_E_to_UPC_A(String rawData) {
        if(rawData.length() != 8) {
            return null;
        }
        if(rawData.charAt(0) != '0' && rawData.charAt(0) != '1') {
            return null;
        }

        if(rawData.charAt(6) == '0' || rawData.charAt(6) == '1' || rawData.charAt(6) == '2') {
            return rawData.substring(0, 3) + rawData.charAt(6) + "0000" + rawData.substring(3, 6) + rawData.charAt(7);
        }
        else if(rawData.charAt(6) == '3') {
            return rawData.substring(0, 4) + "00000" + rawData.substring(4, 6) + rawData.charAt(7);
        }
        else if(rawData.charAt(6) == '4') {
            return rawData.substring(0, 5) + "00000" + rawData.charAt(5) + rawData.charAt(7);
        }
        else if(rawData.charAt(6) == '5' || rawData.charAt(6) == '6' || rawData.charAt(6) == '7' ||
                rawData.charAt(6) == '8' || rawData.charAt(6) == '9') {
            return rawData.substring(0, 6) + "0000" + rawData.charAt(6) + rawData.charAt(7);
        }
        else {
            return null;
        }
    }

    public static String convertFromUPC_A(String rawData) {
        int toPad = 13 - rawData.length();
        String paddedZeroes = "";
        for(int i = 0; i < toPad; i++) {
            paddedZeroes += "0";
        }
        return paddedZeroes + rawData;
    }

    public static String convertFromUPC_E(String rawData) {
        String upcAformat = convertFromUPC_E_to_UPC_A(rawData);
        return convertFromUPC_A(upcAformat);
    }

    public static String convertFromEAN_8(String rawData) {
        int toPad = 13 - rawData.length();
        String paddedZeroes = "";
        for(int i = 0; i < toPad; i++) {
            paddedZeroes += "0";
        }
        return paddedZeroes + rawData;
    }

    public static String getDateFor(int dayOffset) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, dayOffset);
        return sdf.format(c.getTime());
    }

    public static String getDateString(int dayOffset, Context context) {
        switch (dayOffset) {
            case 0:
                return context.getString(R.string.today);
            case 1:
                return context.getString(R.string.tomorrow);
            case -1:
                return context.getString(R.string.yesterday);
            default:
                return getDateFor(dayOffset);
        }
    }

    public static String getEntryTypeString(DiaryPageFragment.ENTRY_TYPE type, Context context) {
        switch (type) {
            case BREAKFAST:
                return context.getString(R.string.breakfast);
            case LUNCH:
                return context.getString(R.string.lunch);
            case DINNER:
                return context.getString(R.string.dinner);
            case WATER:
                return context.getString(R.string.water);
            case SNACKS:
                return context.getString(R.string.snacks);
            default:
                return null;
        }
    }

    public static int getCheckedItem(DiaryPageFragment.ENTRY_TYPE type) {
        switch (type) {
            case BREAKFAST:
                return 0;
            case LUNCH:
                return 1;
            case DINNER:
                return 2;
            case SNACKS:
                return 3;
            default:
                return -1;
        }
    }

    // TODO: BEWARE OF AND CHECK FOR OFF BY ONE ERRORS
    // TODO: ADD SUPPORT FOR YEAR TRANSITIONS
    // ASSUMPTION : chosenMonth != currentMonth and the different between them is less than or equal to 8 months
    //                                      21          11              21                 9
    public static int computeDayOffset(int today, int currentMonth, int chosenDay, int chosenMonth, int year) {
        // chosen day can be in the FUTURE or PAST
        int totalDays = 0;
        if(currentMonth < chosenMonth) {
            // Case when chosen month is in the FUTURE
            for(int i = currentMonth; i < chosenMonth; i++) {
                totalDays += getDaysInMonth(i, year);
            }
            totalDays += chosenDay;
            totalDays -= today;
            return totalDays;
        }

        else {
            // Case when chosen month is in the PAST
            for(int i = chosenMonth; i < currentMonth; i++) {
                totalDays += getDaysInMonth(i, year);
            }
            totalDays += today;
            totalDays -= chosenDay;
            return -totalDays;
        }
    }

    private static int getDaysInMonth(int month, int year) {
        switch (month) {
            case 0:
            case 2:
            case 4:
            case 6:
            case 7:
            case 9:
            case 11:
                return 31;
            case 3:
            case 5:
            case 8:
            case 10:
                return 30;
            case 1:
                return leapYear(year) ? 29 : 28;
            default:
                return 0;
        }
    }

    private static boolean leapYear(int year) {
        return (year % 4 == 0);
    }


}
