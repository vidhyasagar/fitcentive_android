package com.vidhyasagar.fitcentive.utilities;

import android.content.Context;

import com.squareup.otto.Bus;

/**
 * Created by vidxyz on 7/11/17.
 */

public class EventBus {

    private static Bus bus;

    public static Bus getEventBus() {
        if(bus == null) {
            bus = new Bus();
        }
        return bus;
    }

}
