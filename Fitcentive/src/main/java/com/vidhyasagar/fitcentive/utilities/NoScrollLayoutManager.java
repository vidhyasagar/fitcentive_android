package com.vidhyasagar.fitcentive.utilities;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;

/**
 * Created by vharihar on 2016-11-01.
 */
public class NoScrollLayoutManager extends LinearLayoutManager {

        private boolean isScrollEnabled = true;

        public NoScrollLayoutManager(Context context) {
            super(context);
        }

        public void setScrollEnabled(boolean flag) {
            this.isScrollEnabled = flag;
        }

        @Override
        public boolean canScrollVertically() {
            //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
            return isScrollEnabled && super.canScrollVertically();
        }
}
