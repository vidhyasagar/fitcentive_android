package com.vidhyasagar.fitcentive.utilities;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

/**
 * Created by vharihar on 2016-10-03.
 */
public class MySearchSuggestion implements SearchSuggestion {

    private String suggestion;

    public MySearchSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    @Override
    public String getBody() {
        return this.suggestion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
