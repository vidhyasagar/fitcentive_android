package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vidxyz on 11/23/16.
 */

public class ServingInfo implements Parcelable {

    String servingSize;

    // Nutrient values
    double calories;
    double carbs;
    double protein;
    double fat;
    double saturatedFat;
    double polyUnsaturatedFat;
    double monoUnsaturatedFat;
    double transFat;
    double cholestrol;
    double sodium;
    double potassium;
    double fiber;
    double sugar;
    double vitamin_a;
    double vitamin_c;
    double calcium;
    double iron;

    public ServingInfo() {}

    protected ServingInfo(Parcel in) {
        calories = in.readDouble();
        carbs = in.readDouble();
        protein = in.readDouble();
        fat = in.readDouble();
        saturatedFat = in.readDouble();
        polyUnsaturatedFat = in.readDouble();
        monoUnsaturatedFat = in.readDouble();
        transFat = in.readDouble();
        cholestrol = in.readDouble();
        sodium = in.readDouble();
        potassium = in.readDouble();
        fiber = in.readDouble();
        sugar = in.readDouble();
        vitamin_a = in.readDouble();
        vitamin_c = in.readDouble();
        calcium = in.readDouble();
        iron = in.readDouble();
        servingSize = in.readString();
    }

    public static final Creator<ServingInfo> CREATOR = new Creator<ServingInfo>() {
        @Override
        public ServingInfo createFromParcel(Parcel in) {
            return new ServingInfo(in);
        }

        @Override
        public ServingInfo[] newArray(int size) {
            return new ServingInfo[size];
        }
    };

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }


    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public double getCarbs() {
        return carbs;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public double getSaturatedFat() {
        return saturatedFat;
    }

    public void setSaturatedFat(double saturatedFat) {
        this.saturatedFat = saturatedFat;
    }

    public double getPolyUnsaturatedFat() {
        return polyUnsaturatedFat;
    }

    public void setPolyUnsaturatedFat(double polyUnsaturatedFat) {
        this.polyUnsaturatedFat = polyUnsaturatedFat;
    }

    public double getMonoUnsaturatedFat() {
        return monoUnsaturatedFat;
    }

    public void setMonoUnsaturatedFat(double monoUnsaturatedFat) {
        this.monoUnsaturatedFat = monoUnsaturatedFat;
    }

    public double getTransFat() {
        return transFat;
    }

    public void setTransFat(double transFat) {
        this.transFat = transFat;
    }

    public double getCholestrol() {
        return cholestrol;
    }

    public void setCholestrol(double cholestrol) {
        this.cholestrol = cholestrol;
    }

    public double getSodium() {
        return sodium;
    }

    public void setSodium(double sodium) {
        this.sodium = sodium;
    }

    public double getPotassium() {
        return potassium;
    }

    public void setPotassium(double potassium) {
        this.potassium = potassium;
    }

    public double getFiber() {
        return fiber;
    }

    public void setFiber(double fiber) {
        this.fiber = fiber;
    }

    public double getSugar() {
        return sugar;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public double getVitamin_a() {
        return vitamin_a;
    }

    public void setVitamin_a(double vitamin_a) {
        this.vitamin_a = vitamin_a;
    }

    public double getVitamin_c() {
        return vitamin_c;
    }

    public void setVitamin_c(double vitamin_c) {
        this.vitamin_c = vitamin_c;
    }

    public double getCalcium() {
        return calcium;
    }

    public void setCalcium(double calcium) {
        this.calcium = calcium;
    }

    public double getIron() {
        return iron;
    }

    public void setIron(double iron) {
        this.iron = iron;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(this.calories);
        dest.writeDouble(this.carbs);
        dest.writeDouble(this.protein);
        dest.writeDouble(this.fat);
        dest.writeDouble(this.saturatedFat);
        dest.writeDouble(this.polyUnsaturatedFat);
        dest.writeDouble(this.monoUnsaturatedFat);
        dest.writeDouble(this.transFat);
        dest.writeDouble(this.cholestrol);
        dest.writeDouble(this.sodium);
        dest.writeDouble(this.potassium);
        dest.writeDouble(this.fiber);
        dest.writeDouble(this.sugar);
        dest.writeDouble(this.vitamin_a);
        dest.writeDouble(this.vitamin_a);
        dest.writeDouble(this.calcium);
        dest.writeDouble(this.iron);
        dest.writeString(this.servingSize);
    }
}
