package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-16.
 */

public class CreatedWorkoutInfo implements Parcelable{

    private String objectId;
    private String workoutObjectId;
    private String workoutName;

    // Note that this is an augmented list. It will be of two types
    // -  CASE STRENGTH : Benchpress dumbells, 3 sets, 5 reps/set
    // -  CASE CARDIO   : Stairmaster, 30 mins
    private List<String> exerciseNames;
    private int totalNumberOfCalories;

    public CreatedWorkoutInfo() {
        exerciseNames = new ArrayList<>();
    }

    public String getWorkoutObjectId() {
        return workoutObjectId;
    }

    public void setWorkoutObjectId(String workoutObjectId) {
        this.workoutObjectId = workoutObjectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }


    public String getWorkoutName() {
        return workoutName;
    }

    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }

    public List<String> getExerciseNames() {
        return exerciseNames;
    }

    public void setExerciseNames(List<String> exerciseNames) {
        this.exerciseNames = exerciseNames;
    }

    public int getTotalNumberOfCalories() {
        return totalNumberOfCalories;
    }

    public void setTotalNumberOfCalories(int totalNumberOfCalories) {
        this.totalNumberOfCalories = totalNumberOfCalories;
    }

    public void addToExerciseNames(String name) {
        exerciseNames.add(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(workoutObjectId);
        dest.writeString(workoutName);
        dest.writeInt(totalNumberOfCalories);
        dest.writeStringList(exerciseNames);
        dest.writeString(objectId);
    }

    protected CreatedWorkoutInfo(Parcel in) {
        workoutObjectId = in.readString();
        workoutName = in.readString();
        exerciseNames = in.createStringArrayList();
        totalNumberOfCalories = in.readInt();
        objectId = in.readString();
    }

    public static final Creator<CreatedWorkoutInfo> CREATOR = new Creator<CreatedWorkoutInfo>() {
        @Override
        public CreatedWorkoutInfo createFromParcel(Parcel in) {
            return new CreatedWorkoutInfo(in);
        }

        @Override
        public CreatedWorkoutInfo[] newArray(int size) {
            return new CreatedWorkoutInfo[size];
        }
    };
}
