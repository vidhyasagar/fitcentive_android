package com.vidhyasagar.fitcentive.wrappers;

import java.util.List;

/**
 * Created by vharihar on 2016-11-28.
 */

public class MealInfo {

    String mealObjectId;
    String mealName;
    List<String> foodAndRecipeNames;
    List<String> servingSizes;
    int totalNumberOfCalories;

    public MealInfo() {}

    public String getMealObjectId() {
        return mealObjectId;
    }

    public void setMealObjectId(String mealObjectId) {
        this.mealObjectId = mealObjectId;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public List<String> getFoodAndRecipeNames() {
        return foodAndRecipeNames;
    }

    public void setFoodAndRecipeNames(List<String> foodAndRecipeNames) {
        this.foodAndRecipeNames = foodAndRecipeNames;
    }

    public List<String> getServingSizes() {
        return servingSizes;
    }

    public void setServingSizes(List<String> servingSizes) {
        this.servingSizes = servingSizes;
    }

    public int getTotalNumberOfCalories() {
        return totalNumberOfCalories;
    }

    public void setTotalNumberOfCalories(int totalNumberOfCalories) {
        this.totalNumberOfCalories = totalNumberOfCalories;
    }
}
