package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by vharihar on 2016-11-11.
 */
public class FoodResultsInfo implements Parcelable{

    enum FACT_TYPE {PROTEIN, FAT, CARBS};

    String objectId;

    // Primary types
    private String brandName;
    private String foodDescription;
    private long foodId;
    private String foodName;
    private String foodType;
    private String foodUrl;

    // Secondary derived types
    private String servingSize;
    private int calories;
    private float fats, carbs, proteins;

    //
    private String interestingFact = null;
    int THRESHOLD = 40;
    FACT_TYPE type;

    public FoodResultsInfo(String brandName, String foodDescription, String foodName, String foodType, String foodUrl, long foodId) {
        this.brandName = brandName;
        this.foodDescription = foodDescription;
        this.foodName = foodName;
        this.foodType = foodType;
        this.foodUrl = foodUrl;
        this.foodId = foodId;

        if(this.foodDescription != null) {
            this.manipulateDescription();
        }
    }

    public FoodResultsInfo(String foodName, String brandName, String foodDescription, String servingSize, int calories) {
        this.foodDescription = foodDescription;
        this.foodName = foodName;
        this.brandName = brandName;
        this.servingSize = servingSize;
        this.calories = calories;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getBrandName() {
        return this.brandName;
    }

    public String getFoodDescription() {
        return this.foodDescription;
    }

    public String getFoodName(){
        return this.foodName;
    }

    public String getFoodType() {
        return this.foodType;
    }

    public String getFoodUrl() {
        return this.foodUrl;
    }

    public long getFoodId() {
        return this.foodId;
    }

    // This function parses the food description to extract the servingSize, calories, fats, carbs, and proteins
    private void manipulateDescription() {
        try {
            String[] parts = foodDescription.split("-");
            String preServing = parts[0];
            this.servingSize = preServing.substring(3).trim();

            String preBreakdown = parts[1];
            String[] components = preBreakdown.split("\\|");
            this.calories = Integer.valueOf(components[0].replaceAll("\\D+", ""));

            String preFats = components[1].trim().split("Fat:")[1].trim();
            this.fats = Float.valueOf(preFats.substring(0, preFats.length() - 1));

            String preCarbs = components[2].trim().split("Carbs:")[1].trim();
            this.carbs = Float.valueOf(preCarbs.substring(0, preCarbs.length() - 1));

            String preProteins = components[3].trim().split("Protein:")[1].trim();
            this.proteins = Float.valueOf(preProteins.substring(0, preProteins.length() - 1));

            if (this.proteins / (this.proteins + this.carbs + this.fats) > THRESHOLD) {
                this.interestingFact = "This food is high in protein content";
                this.type = FACT_TYPE.PROTEIN;
            }

            if (this.fats / (this.proteins + this.carbs + this.fats) > THRESHOLD) {
                this.interestingFact = "This food is high in fat content";
                this.type = FACT_TYPE.FAT;
            }

            if (this.carbs / (this.proteins + this.carbs + this.fats) > THRESHOLD) {
                this.interestingFact = "This food is high in carbohydrate content";
                this.type = FACT_TYPE.CARBS;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getInterestingFact() {
        return this.interestingFact;
    }

    public FACT_TYPE getType() {
        return this.type;
    }

    public String getServingSize() {
        return this.servingSize;
    }

    public int getCalories() {
        return this.calories;
    }

    public float getFats() {
        return this.fats;
    }

    public float getCarbs() {
        return this.carbs;
    }

    public float getProteins() {
        return this.proteins;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }


    // PARCELABLE METHODS

    public FoodResultsInfo(Parcel in){
        this.brandName = in.readString();
        this.foodDescription = in.readString();
        this.foodName = in.readString();
        this.foodType = in.readString();
        this.foodUrl = in.readString();
        this.servingSize = in.readString();
        this.foodId = in.readLong();
        this.calories = in.readInt();
        this.fats = in.readFloat();
        this.carbs = in.readFloat();
        this.proteins = in.readFloat();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(brandName);
        dest.writeString(foodDescription);
        dest.writeString(foodName);
        dest.writeString(foodType);
        dest.writeString(foodUrl);
        dest.writeString(servingSize);
        dest.writeLong(foodId);
        dest.writeInt(calories);
        dest.writeFloat(fats);
        dest.writeFloat(carbs);
        dest.writeFloat(proteins);
    }

    public static final Creator<FoodResultsInfo> CREATOR = new Creator<FoodResultsInfo>() {
        @Override
        public FoodResultsInfo createFromParcel(Parcel in) {
            return new FoodResultsInfo(in);
        }

        @Override
        public FoodResultsInfo[] newArray(int size) {
            return new FoodResultsInfo[size];
        }
    };


}
