package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vharihar on 2016-11-23.
 */

public class Ingredients implements Parcelable {

    long foodId;
    String foodName;
    long servingId;
    double numberOfUnits;
    String measurementDescription;
    String ingredientUrl;
    String ingredientDescription;

    public Ingredients() {}

    public long getFoodId() {
        return foodId;
    }

    public void setFoodId(long foodId) {
        this.foodId = foodId;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public long getServingId() {
        return servingId;
    }

    public void setServingId(long servingId) {
        this.servingId = servingId;
    }

    public double getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(double numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public String getMeasurementDescription() {
        return measurementDescription;
    }

    public void setMeasurementDescription(String measurementDescription) {
        this.measurementDescription = measurementDescription;
    }

    public String getIngredientUrl() {
        return ingredientUrl;
    }

    public void setIngredientUrl(String ingredientUrl) {
        this.ingredientUrl = ingredientUrl;
    }

    public String getIngredientDescription() {
        return ingredientDescription;
    }

    public void setIngredientDescription(String ingredientDescription) {
        this.ingredientDescription = ingredientDescription;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(foodId);
        dest.writeLong(servingId);
        dest.writeDouble(numberOfUnits);
        dest.writeString(foodName);
        dest.writeString(measurementDescription);
        dest.writeString(ingredientUrl);
        dest.writeString(ingredientDescription);
    }

    public Ingredients(Parcel in) {
        foodId = in.readLong();
        servingId = in.readLong();
        numberOfUnits = in.readDouble();
        foodName = in.readString();
        measurementDescription = in.readString();
        ingredientUrl = in.readString();
        ingredientDescription = in.readString();
    }

    public static final Creator<Ingredients> CREATOR = new Creator<Ingredients>() {
        @Override
        public Ingredients createFromParcel(Parcel in) {
            return new Ingredients(in);
        }

        @Override
        public Ingredients[] newArray(int size) {
            return new Ingredients[size];
        }
    };
}
