package com.vidhyasagar.fitcentive.wrappers;

import android.graphics.Bitmap;
import android.text.format.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by vharihar on 2016-09-28.
 */
public class NewsFeedInfo {

    private String objectId;
    private String userName;
    private String postCaption;
    private int numberLikes;
    private int numberComments;
    private Bitmap postImage;
    private Date postDate;
    private boolean isPostLikedBySelf;
    private boolean isCommentedOnBySelf;
    private String firstLiker;

    public NewsFeedInfo(String userName, String postCaption, int numberComments,
                        int numberLikes, Date postDate, Bitmap postImage, String objectId) {

        this.userName = userName;
        this.postCaption = postCaption;
        this.numberComments = numberComments;
        this.numberLikes = numberLikes;
        this.postDate =  postDate;
        this.postImage = postImage;
        this.objectId = objectId;
    }

    public String getFirstLiker() {
        return this.firstLiker;
    }

    public void setFirstLiker(String firstLiker) {
        this.firstLiker = firstLiker;
    }

    public boolean getPostLikeStatus() {
        return this.isPostLikedBySelf;
    }

    public boolean getPostCommentStatus() {
        return this.isCommentedOnBySelf;
    }

    public void setPostLikeStatus(boolean liked) {
        this.isPostLikedBySelf = liked;
    }

    public void setPostCommentStatus(boolean commented) {
        this.isCommentedOnBySelf = commented;
    }

    public String getObjectId() {
        return this.objectId;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getPostCaption() {
        return this.postCaption;
    }

    public int getNumberLikes() {
        return numberLikes;
    }

    public void addNumberComments(int numberCommentsToAdd) {
        this.numberComments += numberCommentsToAdd;
    }

    public void addNumberLikes(int numLikesToAdd) {
        this.numberLikes+= numLikesToAdd;
    }

    public int getNumberComments() {
        return numberComments;
    }

    public String getPostDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy  -  hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("EST"));
        // Dirty hack for seemingly harmless server offset
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.postDate);
        cal.add(Calendar.HOUR_OF_DAY, 1);
        return sdf.format(cal.getTime());
    }

    public Bitmap getPostImage() {
        return this.postImage;
    }

    public void setPostCaption(String postCaption) {
        this.postCaption = postCaption;
    }

    public void setPostImage(Bitmap postImage) {
        this.postImage = postImage;
    }

}
