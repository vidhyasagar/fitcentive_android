package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by vidxyz on 12/18/16.
 */

public class FilterInfo implements Parcelable{

    private ArrayList<Integer> categories;
    private ArrayList<Integer> primaryMuscles;
    private ArrayList<Integer> secondaryMuscles;
    private ArrayList<Integer> equipment;

    public FilterInfo() {}

    public ArrayList<Integer> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Integer> categories) {
        this.categories = categories;
    }

    public ArrayList<Integer> getPrimaryMuscles() {
        return primaryMuscles;
    }

    public void setPrimaryMuscles(ArrayList<Integer> primaryMuscles) {
        this.primaryMuscles = primaryMuscles;
    }

    public ArrayList<Integer> getSecondaryMuscles() {
        return secondaryMuscles;
    }

    public void setSecondaryMuscles(ArrayList<Integer> secondaryMuscles) {
        this.secondaryMuscles = secondaryMuscles;
    }

    public ArrayList<Integer> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<Integer> equipment) {
        this.equipment = equipment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(categories);
        dest.writeSerializable(primaryMuscles);
        dest.writeSerializable(secondaryMuscles);
        dest.writeSerializable(equipment);
    }

    protected FilterInfo(Parcel in) {
        categories = (ArrayList<Integer>) in.readSerializable();
        primaryMuscles = (ArrayList<Integer>) in.readSerializable();
        secondaryMuscles = (ArrayList<Integer>) in.readSerializable();
        equipment = (ArrayList<Integer>) in.readSerializable();
    }

    public static final Creator<FilterInfo> CREATOR = new Creator<FilterInfo>() {
        @Override
        public FilterInfo createFromParcel(Parcel in) {
            return new FilterInfo(in);
        }

        @Override
        public FilterInfo[] newArray(int size) {
            return new FilterInfo[size];
        }
    };
}
