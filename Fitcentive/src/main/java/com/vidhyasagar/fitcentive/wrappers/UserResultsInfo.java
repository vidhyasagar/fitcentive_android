package com.vidhyasagar.fitcentive.wrappers;

import android.graphics.Bitmap;
import android.provider.UserDictionary;

/**
 * Created by vharihar on 2016-10-04.
 */
public class UserResultsInfo {

    private String userFullName;
    private String userUserName;     // This can be location or username depending on search criteria
    private Bitmap userProfilePicture;

    public UserResultsInfo(String fullname, String username, Bitmap profilePicture) {
        this.userFullName = fullname;
        this.userUserName = username;
        this.userProfilePicture = profilePicture;
    }

    public String getUserFullName() {
        return this.userFullName;
    }

    public String getUserUserName() {
        return this.userUserName;
    }

    public Bitmap getUserProfilePicture() {
        return this.userProfilePicture;
    }
}
