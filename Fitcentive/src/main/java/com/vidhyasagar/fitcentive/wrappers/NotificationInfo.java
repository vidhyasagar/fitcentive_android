package com.vidhyasagar.fitcentive.wrappers;

import android.graphics.Bitmap;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vharihar on 2016-10-19.
 */
public class NotificationInfo {
    public enum NOTIFICATION_TYPE {COMMENT, LIKE, FOLLOW_REQUEST}

    private NOTIFICATION_TYPE type;
    private String receiverUsername;
    private String receiverName;
    private String providerUsername;
    private String providerName;
    private String associatedPostId;
    private Bitmap associatedImage;
    private boolean isActive;
    private String changedCaption;
    private Date notificationDate;

    public NotificationInfo(NOTIFICATION_TYPE type, String receiverUsername, String providerUsername,
                            String postId, Bitmap image, String receiverName, String providerName,
                            boolean isActive, String changedCaption, Date notificationDate) {
        this.type = type;
        this.associatedPostId = postId;
        this.providerUsername = providerUsername;
        this.receiverUsername = receiverUsername;
        this.associatedImage = image;
        this.receiverName = receiverName;
        this.providerName = providerName;
        this.changedCaption = changedCaption;
        this.isActive = isActive;
        this.notificationDate = notificationDate;
    }

    public String getNotificationDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a - dd-MMM-yyyy", Locale.US);
        sdf.setTimeZone(TimeZone.getTimeZone("EST"));
        // Dirty hack for seemingly harmless server offset
        Calendar cal = Calendar.getInstance();
        cal.setTime(this.notificationDate);
//        cal.add(Calendar.HOUR_OF_DAY, 1);
        return sdf.format(cal.getTime());
    }

    public String getReceiverUsername() {
        return this.receiverUsername;
    }

    public String getProviderUsername() {
        return this.providerUsername;
    }

    public String getAssociatedPostId() {
        return this.associatedPostId;
    }

    public NOTIFICATION_TYPE getType() {
        return this.type;
    }

    public Bitmap getAssociatedImage() {
        return this.associatedImage;
    }

    public String getReceiverName() {
        return this.receiverName;
    }

    public String getProviderName() {
        return this.providerName;
    }

    public boolean getStatus() {
        return this.isActive;
    }

    public String getChangedCaption(){
        return this.changedCaption;
    }

}
