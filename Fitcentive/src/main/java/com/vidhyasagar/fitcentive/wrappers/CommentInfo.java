package com.vidhyasagar.fitcentive.wrappers;

import android.graphics.Bitmap;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by vharihar on 2016-10-12.
 */
public class CommentInfo {

    private Bitmap userPhoto;
    private String commentUsername;
    private Date commentDate;
    private String comment;
    private String commentId;
    private boolean isDateFromServer;

    public CommentInfo(Bitmap userPhoto, String commentUsername, String comment, Date commentDate, boolean isDateFromServer, String commentId) {
        this.userPhoto = userPhoto;
        this.comment = comment;
        this.commentUsername = commentUsername;
        this.commentDate = commentDate;
        this.isDateFromServer = isDateFromServer;
        this.commentId = commentId;
    }

    public  Bitmap getUserPhoto() {
        return this.userPhoto;
    }

    public String getCommentUsername() {
        return this.commentUsername;
    }

    public String getComment() {
        return this.comment;
    }

    public String getCommentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a - dd-MMM-yyyy", Locale.US);
        if(isDateFromServer) {
            sdf.setTimeZone(TimeZone.getTimeZone("EST"));
            // Dirty hack for seemingly harmless server offset
            Calendar cal = Calendar.getInstance();
            cal.setTime(this.commentDate);
            cal.add(Calendar.HOUR_OF_DAY, 1);
            return sdf.format(cal.getTime());
        }
        else {
            return sdf.format(this.commentDate);
        }
    }

    public String getCommentId() {
        return this.commentId;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
