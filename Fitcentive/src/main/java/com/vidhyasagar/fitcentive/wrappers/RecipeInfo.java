package com.vidhyasagar.fitcentive.wrappers;

/**
 * Created by vharihar on 2016-11-22.
 */

public class RecipeInfo {

    private long recipeId;
    private String recipeName;
    private String recipeDescription;
    private String recipeUrl;
    private String recipeImageUrl;

    boolean isFavorite;


    public RecipeInfo(long recipeId, String recipeName, String recipeDescription, String recipeUrl, String recipeImageUrl) {
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.recipeDescription = recipeDescription;
        this.recipeUrl = recipeUrl;
        this.recipeImageUrl = recipeImageUrl;
    }

    public long getRecipeId() {
        return this.recipeId;
    }

    public String getRecipeName() {
        return this.recipeName;
    }

    public String getRecipeDescription() {
        return this.recipeDescription;
    }

    public String getRecipeUrl() {
        return this.recipeUrl;
    }

    public String getRecipeImageUrl() {
        return this.recipeImageUrl;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

}
