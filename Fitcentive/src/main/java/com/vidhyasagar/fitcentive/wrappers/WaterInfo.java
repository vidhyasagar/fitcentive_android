package com.vidhyasagar.fitcentive.wrappers;

/**
 * Created by vharihar on 2016-11-30.
 */

public class WaterInfo {

    double numberOfCups;
    String objectId;

    public WaterInfo(double numberOfCups, String objectId) {
        this.numberOfCups = numberOfCups;
        this.objectId = objectId;

    }

    public String getObjectId() {
        return objectId;
    }


    public double getNumberOfCups() {
        return numberOfCups;
    }
}
