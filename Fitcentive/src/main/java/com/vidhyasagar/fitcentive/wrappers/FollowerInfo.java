package com.vidhyasagar.fitcentive.wrappers;

import android.graphics.Bitmap;

/**
 * Created by vharihar on 2016-12-19.
 */

public class FollowerInfo {

    private String username;
    private String userFullName;
    private Bitmap userPicture;
    // This pertains to CURRENT USER's follow status on whom the current OBJECT refers to
    private boolean isFollowingCurrentUser;

    public FollowerInfo() {}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public Bitmap getUserPicture() {
        return userPicture;
    }

    public void setUserPicture(Bitmap userPicture) {
        this.userPicture = userPicture;
    }

    public boolean isFollowingCurrentUser() {
        return isFollowingCurrentUser;
    }

    public void setFollowingCurrentUser(boolean followingCurrentUser) {
        isFollowingCurrentUser = followingCurrentUser;
    }
}
