package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by vharihar on 2016-12-13.
 */

public class CardioInfo implements Parcelable{

    private String name;
    private String objectId;

    private double minutesPerformed;
    private double caloriesBurned;

    private boolean isCreated;
    private String createdCardioObjectId;


    public CardioInfo() {}

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isCreated() {
        return isCreated;
    }

    public void setCreated(boolean created) {
        isCreated = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getMinutesPerformed() {
        return minutesPerformed;
    }

    public void setMinutesPerformed(double minutesPerformed) {
        this.minutesPerformed = minutesPerformed;
    }

    public double getCaloriesBurned() {
        return caloriesBurned;
    }

    public void setCaloriesBurned(double caloriesBurned) {
        this.caloriesBurned = caloriesBurned;
    }


    public String getCreatedCardioObjectId() {
        return createdCardioObjectId;
    }

    public void setCreatedCardioObjectId(String createdCardioObjectId) {
        this.createdCardioObjectId = createdCardioObjectId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(objectId);
        dest.writeDouble(minutesPerformed);
        dest.writeDouble(caloriesBurned);
        dest.writeByte((byte) (isCreated ? 1 : 0));
        dest.writeString(createdCardioObjectId);
    }

    protected CardioInfo(Parcel in) {
        name = in.readString();
        objectId = in.readString();
        minutesPerformed = in.readDouble();
        caloriesBurned = in.readDouble();
        isCreated = in.readByte() != 0;
        createdCardioObjectId = in.readString();
    }

    public static final Creator<CardioInfo> CREATOR = new Creator<CardioInfo>() {
        @Override
        public CardioInfo createFromParcel(Parcel in) {
            return new CardioInfo(in);
        }

        @Override
        public CardioInfo[] newArray(int size) {
            return new CardioInfo[size];
        }
    };
}
