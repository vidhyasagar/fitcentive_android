package com.vidhyasagar.fitcentive.wrappers;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.vidhyasagar.fitcentive.R;

/**
 * Created by vharihar on 2016-11-15.
 */
public class DetailedFoodResultsInfo implements Parcelable {

    public static final Creator<DetailedFoodResultsInfo> CREATOR = new Creator<DetailedFoodResultsInfo>() {
        @Override
        public DetailedFoodResultsInfo createFromParcel(Parcel in) {
            return new DetailedFoodResultsInfo(in);
        }

        @Override
        public DetailedFoodResultsInfo[] newArray(int size) {
            return new DetailedFoodResultsInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(foodId);
        dest.writeString(brandName);
        dest.writeString(foodName);
        dest.writeString(foodType);
        dest.writeString(foodUrl);
        dest.writeString(bonusFact);
        dest.writeString(factType.name());
        dest.writeLong(servingId);

        dest.writeString(servingUrl);
        dest.writeString(servingDescription);
        dest.writeString(metricServingUnit);
        dest.writeString(measurementDescription);
        dest.writeDouble(metricServingAmount);
        dest.writeDouble(numberOfUnits);
        dest.writeDouble(numberOfServings);

        dest.writeDouble(calories);
        dest.writeDouble(carbs);
        dest.writeDouble(protein);
        dest.writeDouble(fat);
        dest.writeDouble(saturatedFat);
        dest.writeDouble(polyUnsaturatedFat);
        dest.writeDouble(monoUnsaturatedFat);
        dest.writeDouble(transFat);
        dest.writeDouble(cholestrol);
        dest.writeDouble(sodium);
        dest.writeDouble(potassium);
        dest.writeDouble(fiber);
        dest.writeDouble(sugar);
        dest.writeDouble(vitamin_a);
        dest.writeDouble(vitamin_c);
        dest.writeDouble(calcium);
        dest.writeDouble(iron);

        try {
            dest.writeString(foodDescription);
        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    public DetailedFoodResultsInfo (Parcel in) {
        foodId = in.readLong();
        brandName = in.readString();
        foodName = in.readString();
        foodType = in.readString();
        foodUrl = in.readString();
        bonusFact = in.readString();
        factType = FACT_TYPE.valueOf(in.readString());
        servingId = in.readLong();

        servingUrl = in.readString();
        servingDescription = in.readString();
        metricServingUnit = in.readString();
        measurementDescription = in.readString();
        metricServingAmount = in.readDouble();
        numberOfUnits = in.readDouble();
        numberOfServings = in.readDouble();

        calories = in.readDouble();
        carbs = in.readDouble();
        protein = in.readDouble();
        fat = in.readDouble();
        saturatedFat = in.readDouble();
        polyUnsaturatedFat = in.readDouble();
        monoUnsaturatedFat = in.readDouble();
        transFat = in.readDouble();
        cholestrol = in.readDouble();
        sodium = in.readDouble();
        potassium = in.readDouble();
        fiber = in.readDouble();
        sugar = in.readDouble();
        vitamin_a = in.readDouble();
        vitamin_c = in.readDouble();
        calcium = in.readDouble();
        iron = in.readDouble();

        try {
            foodDescription = in.readString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public enum FACT_TYPE {PROTEIN, FAT, CARBS};
    public static int THRESHOLD = 40;

    Context mContext;

    String objectId;

    boolean isFavorite;

    String foodDescription;
    boolean isCreatedFood;

    // Food data
    long foodId;
    String brandName;
    String foodName;
    String foodType;
    String foodUrl;

    String bonusFact;
    FACT_TYPE factType;

    // Serving data
    long servingId;
    String servingUrl;
    String servingDescription;
    double metricServingAmount;
    String metricServingUnit;
    String measurementDescription;
    double numberOfUnits;
    double numberOfServings;

    // Nutrient values
    double calories;
    double carbs;
    double protein;
    double fat;
    double saturatedFat;
    double polyUnsaturatedFat;
    double monoUnsaturatedFat;
    double transFat;
    double cholestrol;
    double sodium;
    double potassium;
    double fiber;
    double sugar;
    double vitamin_a;
    double vitamin_c;
    double calcium;
    double iron;

    public DetailedFoodResultsInfo() {

    }

    public DetailedFoodResultsInfo(long foodId, long servingId, String servingUrl, String servingDescription, double metricServingAmount,
                                   String metricServingUnit, String measurementDescription, double numberOfUnits, Context mContext) {
        this.foodId = foodId;
        this.servingUrl = servingUrl;
        this.servingDescription = servingDescription;
        this.measurementDescription = measurementDescription;
        this.metricServingAmount = metricServingAmount;
        this.metricServingUnit = metricServingUnit;
        this.numberOfUnits = numberOfUnits;
        this.servingId = servingId;
        this.bonusFact = null;
        this.mContext = mContext;
    }

    // GETTERS AND SETTERS

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public boolean isCreatedFood() {
        return isCreatedFood;
    }

    public void setCreatedFood(boolean createdFood) {
        isCreatedFood = createdFood;
    }

    public Context getmContext() {
        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFoodId(long foodId) {
        this.foodId = foodId;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public void setFoodUrl(String foodUrl) {
        this.foodUrl = foodUrl;
    }

    public void setBonusFact(String bonusFact) {
        this.bonusFact = bonusFact;
    }

    public void setServingId(long servingId) {
        this.servingId = servingId;
    }

    public void setServingUrl(String servingUrl) {
        this.servingUrl = servingUrl;
    }

    public void setServingDescription(String servingDescription) {
        this.servingDescription = servingDescription;
    }

    public void setMetricServingAmount(double metricServingAmount) {
        this.metricServingAmount = metricServingAmount;
    }

    public void setMetricServingUnit(String metricServingUnit) {
        this.metricServingUnit = metricServingUnit;
    }

    public void setMeasurementDescription(String measurementDescription) {
        this.measurementDescription = measurementDescription;
    }

    public void setNumberOfUnits(double numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public void setNumberOfServings(double numberOfServings) {
        this.numberOfServings = numberOfServings;
    }

    public String getFoodDescription() {
        return foodDescription;
    }

    public void setFoodDescription(String foodDescription) {
        this.foodDescription = foodDescription;
    }

    public void setFactType(FACT_TYPE type) {
        this.factType = type;
    }

    public FACT_TYPE getFactType() {
        return this.factType;
    }

    public void setBonusFact() {
        try {
            if ((this.protein / (this.protein + this.carbs + this.fat)) * 100 > THRESHOLD) {
                this.bonusFact = mContext.getString(R.string.protein_bonus_fact);
                this.factType = FACT_TYPE.PROTEIN;
            }

            if ((this.fat / (this.protein + this.carbs + this.fat)) * 100 > THRESHOLD) {
                this.bonusFact = mContext.getString(R.string.fat_bonus_fact);
                this.factType = FACT_TYPE.FAT;
            }

            if ((this.carbs / (this.protein + this.carbs + this.fat)) * 100 > THRESHOLD) {
                this.bonusFact = mContext.getString(R.string.carbs_bonus_fact);
                this.factType = FACT_TYPE.CARBS;
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void setFavorite(boolean favorite) {
        this.isFavorite = favorite;
    }

    public boolean getIsFavorite() {
        return this.isFavorite;
    }

    public String getBonusFact() {
        return this.bonusFact;
    }

    public void setNumberServings(double num) {
        this.numberOfServings = num;
    }

    public double getNumberOfServings() {
        return this.numberOfServings;
    }

    public long getServingId() {
        return this.servingId;
    }

    public String getMeasurementDescription() {
        return this.measurementDescription;
    }

    public String getMetricServingUnit() {
        return this.metricServingUnit;
    }

    public String getServingDescription() {
        return this.servingDescription;
    }

    public double getNumberOfUnits() {
        return this.numberOfUnits;
    }

    public long getFoodId() {
        return this.foodId;
    }

    public String getServingUrl() {
        return this.servingUrl;
    }

    public String getBrandName() {
        return this.brandName;
    }

    public String getFoodName() {
        return this.foodName;
    }

    public String getFoodType() {
        return this.foodType;
    }

    public String getFoodUrl() {
        return this.foodUrl;
    }

    public double getMetricServingAmount() {
        return this.metricServingAmount;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public double getCalories() {
        return this.calories;
    }

    public void setCarbs(double carbs) {
        this.carbs = carbs;
    }

    public double getCarbs() {
        return this.carbs;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getProtein() {
        return this.protein;
    }

    public void setFat(double fats) {
        this.fat = fats;
    }

    public double getFat() {
        return this.fat;
    }

    public void setSaturatedFat(double saturatedFat) {
        this.saturatedFat = saturatedFat;
    }

    public double getSaturatedFat() {
        return this.saturatedFat;
    }

    public void setPolyUnsaturatedFat(double polyUnsaturatedFat) {
        this.polyUnsaturatedFat = polyUnsaturatedFat;
    }

    public double getPolyUnsaturatedFat() {
        return this.polyUnsaturatedFat;
    }

    public void setMonoUnsaturatedFat(double monoUnsaturatedFat) {
        this.monoUnsaturatedFat = monoUnsaturatedFat;
    }

    public double getMonoUnsaturatedFat() {
        return this.monoUnsaturatedFat;
    }

    public void setTransFat(double transFat) {
        this.transFat = transFat;
    }

    public double getTransFat() {
        return this.transFat;
    }

    public void setCholestrol(double cholestrol) {
        this.cholestrol = cholestrol;
    }

    public double getCholestrol() {
        return this.cholestrol;
    }

    public void setSodium(double sodium) {
        this.sodium = sodium;
    }

    public double getSodium() {
        return this.sodium;
    }

    public void setPotassium(double potassium) {
        this.potassium = potassium;
    }

    public double getPotassium() {
        return this.potassium;
    }

    public void setFiber(double fiber) {
        this.fiber = fiber;
    }

    public double getFiber() {
        return this.fiber;
    }

    public void setSugar(double sugar) {
        this.sugar = sugar;
    }

    public double getSugar() {
        return this.sugar;
    }

    public void setVitamin_a(double vitamin_a) {
        this.vitamin_a = vitamin_a;
    }

    public double getVitamin_a() {
        return this.vitamin_a;
    }

    public void setVitamin_c(double vitamin_c) {
        this.vitamin_c = vitamin_c;
    }

    public double getVitamin_c() {
        return this.vitamin_c;
    }

    public void setCalcium(double calcium) {
        this.calcium = calcium;
    }

    public double getCalcium() {
        return this.calcium;
    }

    public void setIron(double iron) {
        this.iron = iron;
    }

    public double getIron() {
        return this.iron;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }
}

