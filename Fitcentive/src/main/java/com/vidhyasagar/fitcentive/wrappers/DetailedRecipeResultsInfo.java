package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.vidhyasagar.fitcentive.R;

import java.util.ArrayList;

/**
 * Created by vharihar on 2016-11-23.
 */

public class DetailedRecipeResultsInfo implements Parcelable{

    double chosenNumberOfServings;
    String bonusFact;
    int selectedServingOption;
    boolean isFavorite;
    DetailedFoodResultsInfo.FACT_TYPE factType;

    private static int THRESHOLD = 40;

    long recipeId;
    String recipeName;
    String recipeUrl;
    String recipeDescription;
    double numberOfServings;
    int prepTime;
    int cookingTime;
    int rating;

    String recipeType;
    String recipeCategoryName;
    String recipeCategoryUrl;
    String recipeImageUrl;

    ArrayList<ServingInfo> servingsList;
    ArrayList<Ingredients> ingredientsList;
    ArrayList<String> directions;

    public DetailedRecipeResultsInfo() {

    }

    public DetailedFoodResultsInfo.FACT_TYPE getFactType() {
        return factType;
    }

    public void setFactType(DetailedFoodResultsInfo.FACT_TYPE factType) {
        this.factType = factType;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public double getChosenNumberOfServings() {
        return chosenNumberOfServings;
    }

    public void setChosenNumberOfServings(double chosenNumberOfServings) {
        this.chosenNumberOfServings = chosenNumberOfServings;
    }

    public String getBonusFact() {
        return bonusFact;
    }

    public void setBonusFact() {
        if((servingsList.get(selectedServingOption).getProtein() /
                (servingsList.get(selectedServingOption).getProtein()  +
                        servingsList.get(selectedServingOption).getCarbs()  +
                        servingsList.get(selectedServingOption).getFat() )) * 100 > THRESHOLD) {
            this.bonusFact = "Contains high amounts of protein";
            this.factType = DetailedFoodResultsInfo.FACT_TYPE.PROTEIN;
        }

        if((servingsList.get(selectedServingOption).getFat() /
                (servingsList.get(selectedServingOption).getProtein()  +
                        servingsList.get(selectedServingOption).getCarbs()  +
                        servingsList.get(selectedServingOption).getFat() )) * 100 > THRESHOLD) {
            this.bonusFact = "Contains high amounts of fats";
            this.factType = DetailedFoodResultsInfo.FACT_TYPE.FAT;
        }

        if((servingsList.get(selectedServingOption).getCarbs() /
                (servingsList.get(selectedServingOption).getProtein()  +
                        servingsList.get(selectedServingOption).getCarbs()  +
                        servingsList.get(selectedServingOption).getFat() )) * 100 > THRESHOLD) {
            this.bonusFact = "Contains high amounts of carbohydrates";
            this.factType = DetailedFoodResultsInfo.FACT_TYPE.CARBS;
        }

    }

    public int getSelectedServingOption() {
        return selectedServingOption;
    }

    public void setSelectedServingOption(int selectedServingOption) {
        this.selectedServingOption = selectedServingOption;
    }

    public long getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(long recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeUrl() {
        return recipeUrl;
    }

    public void setRecipeUrl(String recipeUrl) {
        this.recipeUrl = recipeUrl;
    }

    public String getRecipeDescription() {
        return recipeDescription;
    }

    public void setRecipeDescription(String recipeDescription) {
        this.recipeDescription = recipeDescription;
    }

    public double getNumberOfServings() {
        return numberOfServings;
    }

    public void setNumberOfServings(double numberOfServings) {
        this.numberOfServings = numberOfServings;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(int prepTime) {
        this.prepTime = prepTime;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getRecipeType() {
        return recipeType;
    }

    public void setRecipeType(String recipeType) {
        this.recipeType = recipeType;
    }

    public String getRecipeCategoryName() {
        return recipeCategoryName;
    }

    public void setRecipeCategoryName(String recipeCategoryName) {
        this.recipeCategoryName = recipeCategoryName;
    }

    public String getRecipeCategoryUrl() {
        return recipeCategoryUrl;
    }

    public void setRecipeCategoryUrl(String recipeCategoryUrl) {
        this.recipeCategoryUrl = recipeCategoryUrl;
    }

    public String getRecipeImageUrl() {
        return recipeImageUrl;
    }

    public void setRecipeImageUrl(String recipeImageUrl) {
        this.recipeImageUrl = recipeImageUrl;
    }

    public ArrayList<Ingredients> getIngredientsList() {
        return ingredientsList;
    }

    public void setIngredientsList(ArrayList<Ingredients> ingredientsList) {
        this.ingredientsList = ingredientsList;
    }

    public ArrayList<String> getDirections() {
        return directions;
    }

    public void setDirections(ArrayList<String> directions) {
        this.directions = directions;
    }

    public ArrayList<ServingInfo> getServingsList() {
        return servingsList;
    }

    public void setServingsList(ArrayList<ServingInfo> servingsList) {
        this.servingsList = servingsList;
    }

    //-------------------------
    // PARCELABLE IMPLEMETATION
    //-------------------------

    public DetailedRecipeResultsInfo(Parcel in) {
        recipeId = in.readLong();
        recipeName = in.readString();
        recipeUrl = in.readString();
        recipeDescription = in.readString();
        numberOfServings = in.readDouble();
        prepTime = in.readInt();
        cookingTime = in.readInt();
        rating = in.readInt();
        recipeType = in.readString();
        recipeCategoryName = in.readString();
        recipeCategoryUrl = in.readString();
        recipeImageUrl = in.readString();
        servingsList = in.createTypedArrayList(ServingInfo.CREATOR);
        ingredientsList = in.createTypedArrayList(Ingredients.CREATOR);
        directions = in.createStringArrayList();

        chosenNumberOfServings = in.readDouble();
        bonusFact = in.readString();
        selectedServingOption = in.readInt();
        isFavorite = in.readByte() != 0;
        factType = DetailedFoodResultsInfo.FACT_TYPE.valueOf(in.readString());
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(recipeId);
        dest.writeString(recipeName);
        dest.writeString(recipeUrl);
        dest.writeString(recipeDescription);
        dest.writeDouble(numberOfServings);
        dest.writeInt(prepTime);
        dest.writeInt(cookingTime);
        dest.writeInt(rating);
        dest.writeString(recipeType);
        dest.writeString(recipeCategoryName);
        dest.writeString(recipeCategoryUrl);
        dest.writeString(recipeImageUrl);
        dest.writeTypedList(servingsList);
        dest.writeTypedList(ingredientsList);
        dest.writeStringList(directions);

        dest.writeDouble(chosenNumberOfServings);
        dest.writeString(bonusFact);
        dest.writeInt(selectedServingOption);
        dest.writeByte((byte) (isFavorite ? 1 : 0));
        if(factType != null) {
            dest.writeString(factType.name());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<DetailedRecipeResultsInfo> CREATOR = new Creator<DetailedRecipeResultsInfo>() {
        @Override
        public DetailedRecipeResultsInfo createFromParcel(Parcel in) {
            return new DetailedRecipeResultsInfo(in);
        }

        @Override
        public DetailedRecipeResultsInfo[] newArray(int size) {
            return new DetailedRecipeResultsInfo[size];
        }
    };

}
