package com.vidhyasagar.fitcentive.wrappers;

import android.os.Parcel;
import android.os.Parcelable;

import com.vidhyasagar.fitcentive.parse.Constants;

import java.util.ArrayList;

/**
 * Created by vharihar on 2016-12-09.
 */

public class ExerciseInfo implements Parcelable {

    private String name;
    private long id;

    private String originalName;
    private String description;
    private long category;
    private ArrayList<Long> primaryMuscles;
    private ArrayList<Long> secondaryMuscles;
    private ArrayList<Long> equipment;

    private int numberOfSets;
    private double repsPerSet;
    private double weightPerSet;
    private String weightUnit;
    private String objectId;

    private boolean isCreated;
    private String createdExerciseObjectId;

    protected ExerciseInfo(Parcel in) {
        name = in.readString();
        originalName = in.readString();
        description = in.readString();

        category = in.readLong();
        id = in.readLong();

        primaryMuscles = (ArrayList<Long>) in.readSerializable();
        secondaryMuscles = (ArrayList<Long>) in.readSerializable();
        equipment = (ArrayList<Long>) in.readSerializable();

        numberOfSets = in.readInt();
        repsPerSet = in.readDouble();
        weightPerSet = in.readDouble();
        weightUnit = in.readString();
        objectId = in.readString();

        isCreated = in.readByte() != 0;
        createdExerciseObjectId = in.readString();
    }

    public static final Creator<ExerciseInfo> CREATOR = new Creator<ExerciseInfo>() {
        @Override
        public ExerciseInfo createFromParcel(Parcel in) {
            return new ExerciseInfo(in);
        }

        @Override
        public ExerciseInfo[] newArray(int size) {
            return new ExerciseInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(originalName);
        dest.writeString(description);

        dest.writeLong(category);
        dest.writeLong(id);

        dest.writeSerializable(primaryMuscles);
        dest.writeSerializable(secondaryMuscles);
        dest.writeSerializable(equipment);

        dest.writeInt(numberOfSets);
        dest.writeDouble(repsPerSet);
        dest.writeDouble(weightPerSet);
        dest.writeString(weightUnit);
        dest.writeString(objectId);

        dest.writeByte((byte) (isCreated ? 1 : 0));
        dest.writeString(createdExerciseObjectId);

    }

    public String getCreatedExerciseObjectId() {
        return createdExerciseObjectId;
    }

    public void setCreatedExerciseObjectId(String createdExerciseObjectId) {
        this.createdExerciseObjectId = createdExerciseObjectId;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public ExerciseInfo() {
        numberOfSets = -1;
        repsPerSet = -1;
        weightPerSet = -1;
        weightUnit = Constants.unknown;
        objectId = Constants.unknown;

    }

    public boolean isCreated() {
        return isCreated;
    }

    public void setCreated(boolean created) {
        isCreated = created;
    }

    public int getNumberOfSets() {
        return numberOfSets;
    }

    public void setNumberOfSets(int numberOfSets) {
        this.numberOfSets = numberOfSets;
    }

    public double getRepsPerSet() {
        return repsPerSet;
    }

    public void setRepsPerSet(double repsPerSet) {
        this.repsPerSet = repsPerSet;
    }

    public double getWeightPerSet() {
        return weightPerSet;
    }

    public void setWeightPerSet(double weightPerSet) {
        this.weightPerSet = weightPerSet;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCategory() {
        return category;
    }

    public void setCategory(long category) {
        this.category = category;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Long> getPrimaryMuscles() {
        return primaryMuscles;
    }

    public void setPrimaryMuscles(ArrayList<Long> primaryMuscles) {
        this.primaryMuscles = primaryMuscles;
    }

    public ArrayList<Long> getSecondaryMuscles() {
        return secondaryMuscles;
    }

    public void setSecondaryMuscles(ArrayList<Long> secondaryMuscles) {
        this.secondaryMuscles = secondaryMuscles;
    }

    public ArrayList<Long> getEquipment() {
        return equipment;
    }

    public void setEquipment(ArrayList<Long> equipment) {
        this.equipment = equipment;
    }

}
