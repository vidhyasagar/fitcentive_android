package com.vidhyasagar.fitcentive.dialog_fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.vidhyasagar.fitcentive.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CarbsDetailedInfoDialogFragment extends DialogFragment {

    public static String TAG = "CARBS_DETAILED_INFO_DIALOG_FRAGMENT";
    public static String cholesterolString = "CHOLESTEROL";
    public static String sugarString = "SUGAR";
    public static String fiberString = "FIBER";

    double cholesterol, sugar, fiber;
    double multiplier;

    BarChart barchart;
    TextView cholesterolTextView, sugarTextView, fiberTextView;

    public static CarbsDetailedInfoDialogFragment newInstance (double cholesterol, double sugar, double fiber, double multiplier) {
        cholesterol = cholesterol != -1 ? cholesterol : 0;
        sugar = sugar != -1 ? sugar : 0;
        fiber = fiber != -1 ? fiber : 0;

        if(cholesterol == 0 && sugar == 0 && fiber == 0) {
            return null;
        }

        CarbsDetailedInfoDialogFragment fragment = new CarbsDetailedInfoDialogFragment();
        Bundle args = new Bundle();
        args.putDouble(cholesterolString, cholesterol);
        args.putDouble(sugarString, sugar);
        args.putDouble(fiberString, fiber);
        args.putDouble(VitaminsAndMineralsDialogFragment.multiplierString, multiplier);
        fragment.setArguments(args);
        return fragment;
    }

    public CarbsDetailedInfoDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b =  new  AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom)
                .setTitle(getString(R.string.more_carb_info))
                .setIcon(R.drawable.ic_logo)
                .setPositiveButton(getContext().getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        LayoutInflater i = getActivity().getLayoutInflater();
        View v = i.inflate(R.layout.layout_carbs_detailed_info_dialog,null);
        b.setView(v);

        retrieveArguments();
        bindViews(v);
        setUpOnScreenData();
        setUpBarChart();

        return b.create();

    }

    private void retrieveArguments() {
        cholesterol = getArguments().getDouble(cholesterolString);
        sugar = getArguments().getDouble(sugarString);
        fiber = getArguments().getDouble(fiberString);
        multiplier = getArguments().getDouble(VitaminsAndMineralsDialogFragment.multiplierString);
    }

    private void bindViews(View v) {
        barchart = (BarChart) v.findViewById(R.id.bar_chart);
        cholesterolTextView = (TextView) v.findViewById(R.id.cholesterol_textview);
        sugarTextView = (TextView) v.findViewById(R.id.sugars_textview);
        fiberTextView = (TextView) v.findViewById(R.id.fibers_textview);
    }

    private void setUpOnScreenData() {
        cholesterolTextView.setText(String.format(getString(R.string.two_decimal_places), cholesterol * multiplier));
        sugarTextView.setText(String.format(getString(R.string.two_decimal_places), sugar * multiplier));
        fiberTextView.setText(String.format(getString(R.string.two_decimal_places), fiber * multiplier));
    }


    private void setUpBarChart() {
        barchart.setBackgroundColor(Color.TRANSPARENT);
        barchart.setDescription(getString(R.string.carbs_info));
        barchart.setDescriptionColor(ContextCompat.getColor(getContext(), R.color.transp_black));
        barchart.setDescriptionTextSize(11f);

        Legend l = barchart.getLegend();
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_red_light,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light};
//        l.setCustom(colors, categories);


        ArrayList<BarEntry> breakdown = new ArrayList<>();

        BarEntry cholesterolEntry = new BarEntry((float) (cholesterol * multiplier), 0);
        breakdown.add(cholesterolEntry);
        BarEntry sugarEntry = new BarEntry((float) (sugar * multiplier), 1);
        breakdown.add(sugarEntry);
        BarEntry fiberEntry = new BarEntry((float) (fiber * multiplier), 2);
        breakdown.add(fiberEntry);

        BarDataSet setBreakdown = new BarDataSet(breakdown, "");
//        setBreakdown.setHighlightEnabled(true);
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setColors(colors, getContext());

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.cholesterol));
        xVals.add(getString(R.string.sugar));
        xVals.add(getString(R.string.fiber));
//
        BarData data = new BarData(xVals, setBreakdown);
//        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        barchart.setData(data);
        barchart.setHighlightPerTapEnabled(true);
        barchart.animateXY(1000, 1000);

        barchart.invalidate(); // refresh

    }
}
