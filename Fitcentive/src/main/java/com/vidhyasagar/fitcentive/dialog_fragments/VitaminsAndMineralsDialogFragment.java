package com.vidhyasagar.fitcentive.dialog_fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.vidhyasagar.fitcentive.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class VitaminsAndMineralsDialogFragment extends DialogFragment {

    public static String TAG = "VITAMINS_AND_MINERALS_DIALOG_FRAGMENT";
    public static String sodiumString = "SODIUM";
    public static String potassiumString = "POTASSIUM";
    public static String calciumString = "CALCIUM";
    public static String ironString = "IRON";
    public static String vitaminAString = "VITAMIN_A";
    public static String vitaminCString = "VITAMIN_C";
    public static String multiplierString = "MULTIPLIER";

    double sodium, potassium, calcium, iron, vitamin_a, vitamin_c;
    double multiplier;

    TextView vitaminAText, vitaminCText, sodiumText, potassiumText, calciumText, ironText;

    public static VitaminsAndMineralsDialogFragment newInstance (double sodium, double potassium, double calcium,
                                                               double iron, double vitamin_a, double vitamin_c, double multiplier) {
        sodium = sodium != -1 ? sodium : 0;
        potassium = potassium != -1 ? potassium : 0;
        calcium = calcium != -1 ? calcium : 0;
        iron = iron != -1 ? iron : 0;
        vitamin_a = vitamin_a != -1 ? vitamin_a : 0;
        vitamin_c = vitamin_c != -1 ? vitamin_c : 0;

        if(sodium == 0 && potassium == 0 && calcium == 0 && iron == 0 && vitamin_a == 0 && vitamin_c == 0) {
            return null;
        }

        VitaminsAndMineralsDialogFragment fragment = new VitaminsAndMineralsDialogFragment();
        Bundle args = new Bundle();
        args.putDouble(sodiumString, sodium);
        args.putDouble(potassiumString, potassium);
        args.putDouble(calciumString, calcium);
        args.putDouble(ironString, iron);
        args.putDouble(vitaminAString, vitamin_a);
        args.putDouble(vitaminCString, vitamin_c);
        args.putDouble(multiplierString, multiplier);
        fragment.setArguments(args);
        return fragment;
    }

    public VitaminsAndMineralsDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b =  new  AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom)
                .setTitle(getString(R.string.vitamins_and_minerals))
                .setIcon(R.drawable.ic_logo)
                .setPositiveButton(getContext().getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        LayoutInflater i = getActivity().getLayoutInflater();
        View v = i.inflate(R.layout.layout_vitamins_and_minerals_dialog,null);
        b.setView(v);

        retrieveArguments();
        bindViews(v);
        setUpDataOnScreen();

        return b.create();

    }

    private void retrieveArguments() {
        sodium = getArguments().getDouble(sodiumString);
        potassium = getArguments().getDouble(potassiumString);
        calcium = getArguments().getDouble(calciumString);
        iron = getArguments().getDouble(ironString);
        vitamin_a = getArguments().getDouble(vitaminAString);
        vitamin_c = getArguments().getDouble(vitaminCString);
        multiplier = getArguments().getDouble(multiplierString);
    }

    private void bindViews(View v) {
        vitaminCText = (TextView) v.findViewById(R.id.vitaminc_value);
        vitaminAText = (TextView) v.findViewById(R.id.vitamina_value);
        sodiumText = (TextView) v.findViewById(R.id.sodium_value);
        potassiumText = (TextView) v.findViewById(R.id.potassium_value);
        calciumText = (TextView) v.findViewById(R.id.calcium_value);
        ironText = (TextView) v.findViewById(R.id.iron_value);
    }

    private void setUpDataOnScreen() {
        sodiumText.setText(String.format(getString(R.string.x_mg), sodium * multiplier));
        potassiumText.setText(String.format(getString(R.string.x_mg), potassium * multiplier));
        calciumText.setText(String.format(getString(R.string.x_percent), calcium * multiplier));
        ironText.setText(String.format(getString(R.string.x_percent), iron * multiplier));
        vitaminAText.setText(String.format(getString(R.string.x_percent), vitamin_a * multiplier));
        vitaminCText.setText(String.format(getString(R.string.x_percent), vitamin_c * multiplier));
    }

}
