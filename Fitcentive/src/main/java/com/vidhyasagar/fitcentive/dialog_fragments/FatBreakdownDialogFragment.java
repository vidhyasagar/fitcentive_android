package com.vidhyasagar.fitcentive.dialog_fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.vidhyasagar.fitcentive.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FatBreakdownDialogFragment extends DialogFragment {

    public static String TAG = "FAT_BREAKDOWN_DIALOG_FRAGMENT";
    public static String polyUnsaturatedFatsString = "POLY_UNSATURATED_FATS";
    public static String monoUnsaturatedFatsString = "MONO_UNSATURATED_FATS";
    public static String saturatedFatsString = "SATURATED_FATS";
    public static String transFatsString = "TRANS_FATS";

    double polyUnsaturated, monoUnsaturated, trans, saturated, total;
    double multiplier;

    PieChart pieChart;
    TextView polyFatsTextView, monoFatsTextView, transFatsTextView, saturatedFatsTextView;

    public static FatBreakdownDialogFragment newInstance(double polyUnsaturated, double monoUnsaturated,
                                                         double trans, double saturated, double multiplier) {

        polyUnsaturated = polyUnsaturated != -1 ? polyUnsaturated : 0;
        monoUnsaturated = monoUnsaturated != -1 ? monoUnsaturated : 0;
        trans = trans != -1 ? trans : 0;
        saturated = saturated != -1 ? saturated : 0;

        if(polyUnsaturated == 0 && monoUnsaturated == 0 & trans == 0 && saturated == 0) {
            return null;
        }

        FatBreakdownDialogFragment fragment = new FatBreakdownDialogFragment();
        Bundle args = new Bundle();
        args.putDouble(polyUnsaturatedFatsString, polyUnsaturated);
        args.putDouble(monoUnsaturatedFatsString, monoUnsaturated);
        args.putDouble(transFatsString, trans);
        args.putDouble(saturatedFatsString, saturated);
        args.putDouble(VitaminsAndMineralsDialogFragment.multiplierString, multiplier);
        fragment.setArguments(args);
        return fragment;
    }

    public FatBreakdownDialogFragment() {
        // Required empty public constructor
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b =  new  AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom)
                .setTitle(getString(R.string.detailed_fat_breakdown))
                .setIcon(R.drawable.ic_logo)
                .setPositiveButton(getContext().getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        LayoutInflater i = getActivity().getLayoutInflater();
        View v = i.inflate(R.layout.layout_fat_breakdown_dialog,null);
        b.setView(v);

        retrieveArguments();
        bindViews(v);
        setUpOnScreenData();
        setUpPieChart();

        return b.create();

    }

    private void retrieveArguments() {
        polyUnsaturated = getArguments().getDouble(polyUnsaturatedFatsString);
        monoUnsaturated = getArguments().getDouble(monoUnsaturatedFatsString);
        trans = getArguments().getDouble(transFatsString);
        saturated = getArguments().getDouble(saturatedFatsString);
        multiplier = getArguments().getDouble(VitaminsAndMineralsDialogFragment.multiplierString);
    }

    private void bindViews(View v) {
        pieChart = (PieChart) v.findViewById(R.id.pie_chart);
        polyFatsTextView = (TextView) v.findViewById(R.id.poly_fats_textview);
        monoFatsTextView = (TextView) v.findViewById(R.id.mono_fats_textview);
        transFatsTextView = (TextView) v.findViewById(R.id.trans_fats_textview);
        saturatedFatsTextView = (TextView) v.findViewById(R.id.saturated_fats_textview);
    }

    private void setUpOnScreenData() {
        polyFatsTextView.setText(String.format(getString(R.string.two_decimal_places), polyUnsaturated * multiplier));
        monoFatsTextView.setText(String.format(getString(R.string.two_decimal_places), monoUnsaturated * multiplier));
        transFatsTextView.setText(String.format(getString(R.string.two_decimal_places), trans * multiplier));
        saturatedFatsTextView.setText(String.format(getString(R.string.two_decimal_places), saturated * multiplier));
    }

    private void calculateTotal() {
        total = polyUnsaturated + monoUnsaturated + trans + saturated;
    }

    private void setUpPieChart() {
        calculateTotal();
        pieChart.setBackgroundColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription(getString(R.string.fat_breakdown));
        pieChart.setDescriptionColor(ContextCompat.getColor(getContext(), R.color.transp_black));
        pieChart.setDescriptionTextSize(11f);
        pieChart.setHoleRadius(5f);
        pieChart.setTransparentCircleRadius(1f);
        pieChart.setDrawSliceText(false);
//        pieChart.setCenterText(getString(R.string.nutritional_breakdown));
        pieChart.setCenterTextSize(13f);

        Legend l = pieChart.getLegend();
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_red_light,
                android.R.color.holo_blue_light,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light};
//        l.setCustom(colors, categories);


        ArrayList<Entry> breakdown = new ArrayList<Entry>();

        Entry saturatedFatsEntry = new Entry( (float) ((saturated * multiplier) / total) * 100, 0);
        breakdown.add(saturatedFatsEntry);
        Entry polyUn = new Entry( (float) ((polyUnsaturated * multiplier)/ total) * 100, 1);
        breakdown.add(polyUn);
        Entry monoUn = new Entry( (float) ((monoUnsaturated * multiplier)/ total) * 100, 2);
        breakdown.add(monoUn);
        Entry transFatsEntry = new Entry( (float) ((trans * multiplier)/ total) * 100, 3);
        breakdown.add(transFatsEntry);

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
//        setBreakdown.setHighlightEnabled(true);
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, getContext());

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.saturated));
        xVals.add(getString(R.string.poly));
        xVals.add(getString(R.string.mono));
        xVals.add(getString(R.string.trans));
//
        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.animateXY(1000, 1000);

        pieChart.invalidate(); // refresh

    }

}

