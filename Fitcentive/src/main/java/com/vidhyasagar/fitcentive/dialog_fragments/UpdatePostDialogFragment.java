package com.vidhyasagar.fitcentive.dialog_fragments;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.NewsFeedAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdatePostDialogFragment extends DialogFragment {

    public static String TAG = "UPDATE_POST_FRAGMENT";
    boolean HAS_IMAGE_CHANGED = false;
    boolean HAS_POST_CHANGED = false;
    String userText;

    NewsFeedInfo info;
    int position;
    NewsFeedAdapter adapter;

    Uri selectedImageUri;
    Bitmap selectedPostPicture;
    Button addImageButton;
    ImageView postImageView;
    ImageView deletePostButton;
    EditText postCaptionEditText;
    RelativeLayout rel;
    ProgressBar progressBar;

    InputMethodManager imm;
    AlertDialog dialog;

    public UpdatePostDialogFragment() {
    }

    public void setData(NewsFeedInfo info, int position, NewsFeedAdapter adapter) {
        this.info = info;
        this.position = position;
        this.adapter = adapter;
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void mayRequestWriteToStorage() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getContext().getString(R.string.confirm_write_access_denial))
                        .setMessage(getContext().getString(R.string.permission_confirmation_write))
                        .setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ProfileActivity.REQUEST_WRITE_PERMISSION);
                            }
                        })
                        .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        ProfileActivity.REQUEST_WRITE_PERMISSION);
            }
        }
        else {
            cameraIntent();
        }

    }

    private void mayRequestCamera() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getContext().getString(R.string.confirm_camera_access_denial))
                        .setMessage(getContext().getString(R.string.permission_confirmation_camera))
                        .setNegativeButton(getContext().getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions((Activity) getContext(),
                                        new String[]{Manifest.permission.CAMERA}, ProfileActivity.REQUEST_IMAGE_CAPTURE);
                            }
                        })
                        .setPositiveButton(getContext().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        ProfileActivity.REQUEST_IMAGE_CAPTURE);
            }
        }
        else {
            mayRequestWriteToStorage();
        }

    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        (getActivity()).startActivityForResult(Intent.createChooser(intent, getContext().getString(R.string.select_file)), ProfileActivity.SELECT_FILE);
    }

    public File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private void cameraIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            File photo;
            try
            {
                // place where to store camera taken picture
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            }
            catch(Exception e)
            {
                Toast.makeText(getContext(), getContext().getString(R.string.need_write_permission), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                return;
            }
            selectedImageUri = Uri.fromFile(photo);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
            //start camera intent
            getActivity().startActivityForResult(takePictureIntent, ProfileActivity.REQUEST_IMAGE_CAPTURE);
        }
        else {
            Toast.makeText(getContext(), getContext().getString(R.string.need_camera_for_action), Toast.LENGTH_SHORT).show();
        }

    }

    public void setPostPicture() {

        final CharSequence[] items = getContext().getResources().getStringArray(R.array.image_picking_choices);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getContext().getString(R.string.add_post_picture))
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            mayRequestCamera();
                        }
                        else if(which == 1) {
                            galleryIntent();
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void startCropActivity() {
        CropImage.activity(selectedImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(getContext().getString(R.string.crop_image_activity_title))
                .setAspectRatio(1, 1)
                .setFixAspectRatio(false)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(getActivity());
    }

    public Bitmap grabImage(Uri uri) {
        getContext().getContentResolver().notifyChange(uri, null);
        ContentResolver cr = getContext().getContentResolver();
        Bitmap bitmap = null;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, uri);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT).show();
        }

        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        return scaled;


    }

    public void onSelectedImageResult() {
        selectedPostPicture = grabImage(selectedImageUri);
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                postImageView.setImageBitmap(selectedPostPicture);
                postImageView.invalidate();
                setViewVisibility(addImageButton, false, true);
                setViewVisibility(deletePostButton, true, true);
                setViewVisibility(postImageView, true, true);
            }
        });
    }

    public void updatePost(String objectId, String caption, boolean isImageIncluded, int position) throws Exception {

        if(!info.getPostCaption().equals(caption) || HAS_IMAGE_CHANGED) {
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Post);
            query.whereEqualTo(Constants.objectId, objectId);
            ParseObject post = query.getFirst();
            post.put(Constants.caption, caption);
            if (!isImageIncluded) {
                post.remove(Constants.image);
                info.setPostImage(null);
            } else if (isImageIncluded && HAS_IMAGE_CHANGED) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                selectedPostPicture.compress(Bitmap.CompressFormat.PNG, getResources().getInteger(R.integer.bitmap_compression), stream);
                byte[] byte_data = stream.toByteArray();
                ParseFile image = new ParseFile(byte_data);
                post.put(Constants.image, image);
                info.setPostImage(selectedPostPicture);
            }

            post.save();
            info.setPostCaption(caption);
            HAS_POST_CHANGED = true;
        }
        else {
            HAS_POST_CHANGED = false;
        }

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b =  new  AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom)
                .setIcon(R.drawable.ic_logo)
                .setTitle(getContext().getString(R.string.edit_post))
                .setPositiveButton(getContext().getString(R.string.update),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        }
                )
                .setNegativeButton(getContext().getString(R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }
                );

        LayoutInflater i = getActivity().getLayoutInflater();
        View v = i.inflate(R.layout.layout_post_dialog,null);
        b.setView(v);

        addImageButton = (Button) v.findViewById(R.id.add_photo_button);
        deletePostButton = (ImageView) v.findViewById(R.id.post_image_delete_button);
        postImageView = (ImageView) v.findViewById(R.id.post_image);
        postCaptionEditText = (EditText) v.findViewById(R.id.post_text);
        rel = (RelativeLayout) v.findViewById(R.id.post_dialog_rel_layout);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);

        postCaptionEditText.setText(info.getPostCaption());
        postCaptionEditText.setSelection(info.getPostCaption().length());
        userText = info.getPostCaption();



        return b.create();

    }

    @Override
    public void onResume() {
        super.onResume();
        postCaptionEditText.setText(userText);
        postCaptionEditText.setSelection(postCaptionEditText.getText().toString().length());
    }


    @Override
    public void onStart() {
        super.onStart();

        postCaptionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                userText = s.toString().trim();
            }
        });

        dialog = (AlertDialog) getDialog();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(postCaptionEditText.getText().toString().trim())) {
                    // Perform saving of the post
                    boolean isImageIncluded = addImageButton.getVisibility() != View.VISIBLE;
                    new UpdatePost(info.getObjectId(), postCaptionEditText.getText().toString().trim(),
                            isImageIncluded, position).execute();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_empty_post), Toast.LENGTH_SHORT).show();
                }
            }
        });

        imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        rel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        if(info.getPostImage() != null) {
            setViewVisibility(postImageView, true, true);
            setViewVisibility(deletePostButton, true, true);
            setViewVisibility(addImageButton, false, true);
            postImageView.setImageBitmap(info.getPostImage());
        }
        else {
            setViewVisibility(postImageView, false, true);
            setViewVisibility(deletePostButton, false, true);
            setViewVisibility(addImageButton, true, true);
        }

        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HAS_IMAGE_CHANGED = true;
                setPostPicture();
            }
        });

        deletePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HAS_IMAGE_CHANGED = true;
                setViewVisibility(v, false, true);
                setViewVisibility(postImageView, false, true);
                setViewVisibility(addImageButton, true, true);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ProfileActivity.SELECT_FILE) {
                selectedImageUri = data.getData();
                startCropActivity();
            }
            else if (requestCode == ProfileActivity.REQUEST_IMAGE_CAPTURE) {
                startCropActivity();
            }
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                selectedImageUri = result.getUri();
                onSelectedImageResult();
            }
        }
        else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Exception error = result.getError();
            error.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
        }
    }

    // ===========================
    // ASYNCTASK
    // ===========================

    private class UpdatePost extends AsyncTask<Void, Void, Boolean> {

        String objectId;
        boolean isImageIncluded;
        String caption;
        int position;

        public UpdatePost(String objectId, String caption, boolean isImageIncluded, int position) {
            this.objectId = objectId;
            this.caption = caption;
            this.isImageIncluded = isImageIncluded;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(postImageView, false, false);
            setViewVisibility(deletePostButton, false, false);
            setViewVisibility(addImageButton, false, false);
            setViewVisibility(postCaptionEditText, false, false);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                updatePost(objectId, caption, isImageIncluded, position);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(HAS_POST_CHANGED) {
                    Toast.makeText(adapter.getmContext(), getString(R.string.post_updated), Toast.LENGTH_SHORT).show();
                    adapter.updateElementAt(position, info);
                }
                else {
                    Toast.makeText(adapter.getmContext(), getString(R.string.no_changes), Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
            else {
                Toast.makeText(adapter.getmContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
