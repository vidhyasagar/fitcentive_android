package com.vidhyasagar.fitcentive.create_exercise_fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BasicCreatedExerciseFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BasicCreatedExerciseFragment extends Fragment {

    public static BasicCreatedExerciseFragment newInstance(boolean isCardio, String createdExerciseObjectId) {
        BasicCreatedExerciseFragment fragment = new BasicCreatedExerciseFragment();
        Bundle args = new Bundle();
        args.putString(Constants.createdExerciseObjectId, createdExerciseObjectId);
        args.putBoolean(Constants.isCardio, isCardio);
        fragment.setArguments(args);
        return fragment;
    }

    public static BasicCreatedExerciseFragment newInstance(boolean isCardio) {
        BasicCreatedExerciseFragment fragment = new BasicCreatedExerciseFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.isCardio, isCardio);
        fragment.setArguments(args);
        return fragment;
    }

    public BasicCreatedExerciseFragment() {
        // Required empty public constructor
    }

    // If this is NOT NULL, then it is EDIT MODE
    String createdExerciseObjectId;
    boolean isCardio;
    int RETRY_COUNT = 0;

    // This only comes into play if createdExerciseObjectId is NOT NULL, ELSE UNUSED
    String createdExerciseDescription, createdExerciseName;

    Switch aSwitch;
    RelativeLayout mainLayout;
    FloatingActionButton nextButton;
    EditText exerciseName, exerciseDescription;
    TextView exerciseDescriptionLabel;
    ProgressBar progressBar;
    LinearLayout switchLayout;
    View linebreak;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_basic_exercise, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpMainLayoutKeyboardHide();
        setUpNextButton();
        setUpNameWatcher();
        flipSwitchIfNeeded();
        setUpSwitchListener();
        populateDataIfEditMode();
        hideSwitchIfNeeded();
    }

    private void retrieveArguments() {
        Bundle args = getArguments();
        if(args != null) {
            createdExerciseObjectId = args.getString(Constants.createdExerciseObjectId);
            isCardio = args.getBoolean(Constants.isCardio, false);
        }
    }

    private void bindViews(View v) {
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        nextButton = (FloatingActionButton) v.findViewById(R.id.next_button);
        exerciseName = (EditText) v.findViewById(R.id.exercise_name_edittext);
        exerciseDescription = (EditText) v.findViewById(R.id.ex_desc_edittext);
        exerciseDescriptionLabel = (TextView) v.findViewById(R.id.ex_desc_label);
        switchLayout = (LinearLayout) v.findViewById(R.id.switch_linear_layout);
        linebreak = v.findViewById(R.id.line_break_2);
        aSwitch = (Switch) v.findViewById(R.id.switch2);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
    }

    private void setUpMainLayoutKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void populateDataIfEditMode() {
        if(createdExerciseObjectId != null) {
            // This means there is a created object that needs to be loaded
            new FetchCreatedExerciseData().execute();
        }
    }

    private void hideSwitchIfNeeded() {
        if(createdExerciseObjectId != null) {
            switchLayout.setVisibility(View.GONE);
        }
        else {
            switchLayout.setVisibility(View.VISIBLE);
        }
    }

    private void flipSwitchIfNeeded() {
        aSwitch.setChecked(isCardio);
        hideDescription(!isCardio);
    }

    private void setUpSwitchListener() {
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    ((CreateOrEditExerciseActivity) getActivity()).setStrengthLayout(false);
                    hideDescription(false);
                }
                else {
                    ((CreateOrEditExerciseActivity) getActivity()).setStrengthLayout(true);
                    hideDescription(true);
                }
            }
        });
    }

    private void hideDescription(boolean hide) {
        Utilities.setViewVisibility(exerciseDescription, hide, true, getContext());
        Utilities.setViewVisibility(exerciseDescriptionLabel, hide, true, getContext());
        Utilities.setViewVisibility(linebreak, hide, true, getContext());

    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    private void setUpNameWatcher() {
        exerciseName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((CreateOrEditExerciseActivity) getActivity()).updateDetailsFragmentName(s.toString());
            }
        });
    }

    private void setUpNextButton() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    ((CreateOrEditExerciseActivity) getActivity()).goToNextFragment();
                }
                else {
                    handleErrors();
                }
            }
        });
    }

    public boolean isValid() {
        if(!aSwitch.isChecked()) {
            if (!exerciseName.getText().toString().isEmpty() && !exerciseDescription.getText().toString().isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
        else {
            if(!exerciseName.getText().toString().isEmpty()) {
                return true;
            }
            else {
                return false;
            }
        }
    }

    public void handleErrors() {
        if(!aSwitch.isChecked()) {
            if (exerciseName.getText().toString().isEmpty()) {
                Utilities.showSnackBar(getString(R.string.exercise_name_required), nextButton);
            }
            else if (exerciseDescription.getText().toString().isEmpty()) {
                Utilities.showSnackBar(getString(R.string.exercise_description_required), nextButton);
            }
        }
        else {
            if (exerciseName.getText().toString().isEmpty()) {
                Utilities.showSnackBar(getString(R.string.exercise_name_required), nextButton);
            }
        }
    }

    public boolean getIsStrength() {
        return !this.aSwitch.isChecked();
    }

    public String getExerciseName() {
        return this.exerciseName.getText().toString().trim();
    }

    public String getExerciseDescription() {
        return this.exerciseDescription.getText().toString().trim();
    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(mainLayout, show, true, getContext());
        Utilities.setViewVisibility(progressBar, !show, true, getContext());
    }

    private void fetchCreatedExerciseData() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);

        createdExerciseName = object.getString(Constants.exerciseName);
        if(!isCardio) {
            createdExerciseDescription = object.getString(Constants.exerciseDescription);
        }
    }

    private void fillUpOnScreenData() {
        exerciseName.setText(createdExerciseName);
        exerciseName.setSelection(createdExerciseName.length());
        if(!isCardio) {
            exerciseDescription.setText(createdExerciseDescription);
        }
    }

    //-----------------------------------------------------
    // ASYNCTASK(S)
    //-----------------------------------------------------

    private class FetchCreatedExerciseData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                fetchCreatedExerciseData();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if(aBoolean) {
                    showMainLayout(true);
                    fillUpOnScreenData();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedExerciseData().execute();
                    }
                }
            }
        }

    }

}
