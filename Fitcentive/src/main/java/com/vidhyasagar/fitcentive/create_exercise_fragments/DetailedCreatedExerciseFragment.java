package com.vidhyasagar.fitcentive.create_exercise_fragments;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.adapters.CreatedExerciseAdapter;
import com.vidhyasagar.fitcentive.adapters.CreatedExerciseCategoryAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailedCreatedExerciseFragment extends Fragment {

    public static final String STILL_CONFUSED_URL = "http://www.topendsports.com/weight-loss/energy-met.htm";

    public static DetailedCreatedExerciseFragment newInstance(boolean isCardio, String createdExerciseObjectId) {
        DetailedCreatedExerciseFragment fragment = new DetailedCreatedExerciseFragment();
        Bundle args = new Bundle();
        args.putString(Constants.createdExerciseObjectId, createdExerciseObjectId);
        args.putBoolean(Constants.isCardio, isCardio);
        fragment.setArguments(args);
        return fragment;
    }

    public static DetailedCreatedExerciseFragment newInstance(boolean isCardio) {
        DetailedCreatedExerciseFragment fragment = new DetailedCreatedExerciseFragment();
        Bundle args = new Bundle();
        args.putBoolean(Constants.isCardio, isCardio);
        fragment.setArguments(args);
        return fragment;
    }

    public DetailedCreatedExerciseFragment() {
        // Required empty public constructor
    }

    // This will be NOT NULL when it is EDIT MODE
    String createdExerciseObjectId;
    int RETRY_COUNT = 0;
    boolean isCardio;
    ArrayList<String> categories, equipment, primaryMuscles, secondaryMuscles;

    // These members come into play ONLY WHEN IT IS EDIT MODE :aka: createdExerciseObjectId is NOT NULL
    double metValueOfCreatedCardioExercise;
    ArrayList<Integer> equipmentArrayList, primaryMusclesArrayList, secondaryMusclesArrayList;
    int createdStrengthExerciseCategory;

    RelativeLayout mainLayout, strengthLayout, cardioLayout;
    FloatingActionButton backButton;
    ExpandableHeightListView categoryListView, equipmentListView, primaryMusclesListView, secondaryMusclesListView;
    CreatedExerciseAdapter equipmentAdapter, primaryMusclesAdapter, secondaryMusclesAdapter;
    CreatedExerciseCategoryAdapter categoryAdapter;
    ScrollView scrollView;
    ProgressBar progressBar;
    TextView name, stillConfused;
    EditText metValue;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detailed_exercise, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpMainLayoutKeyboardHide();
        setUpBackButton();
        populateLists();
        setUpStillConfused();
        flipLayoutIfNeeded();
        populateOnScreenDataIfEditMode();
    }

    private void retrieveArguments() {
        Bundle args = getArguments();
        if(args != null) {
            createdExerciseObjectId = args.getString(Constants.createdExerciseObjectId);
            isCardio = args.getBoolean(Constants.isCardio, false);
        }
    }

    private void bindViews(View v) {
        scrollView = (ScrollView) v.findViewById(R.id.scrollview);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        strengthLayout = (RelativeLayout) v.findViewById(R.id.strength_relative_layout);
        cardioLayout = (RelativeLayout) v.findViewById(R.id.cardio_relative_layout);
        backButton = (FloatingActionButton) v.findViewById(R.id.back_button);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);

        // Strength Layout
        name = (TextView) v.findViewById(R.id.exercise_name);
        categoryListView = (ExpandableHeightListView) v.findViewById(R.id.category_listview);
        equipmentListView = (ExpandableHeightListView) v.findViewById(R.id.equipment_listview);
        primaryMusclesListView = (ExpandableHeightListView) v.findViewById(R.id.primary_muscles_listview);
        secondaryMusclesListView = (ExpandableHeightListView) v.findViewById(R.id.secondary_muscles_listview);

        // Cardio Layout
        stillConfused = (TextView) v.findViewById(R.id.still_confused);
        metValue = (EditText) v.findViewById(R.id.met_value_edittext);

    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(mainLayout, show, true, getContext());
        Utilities.setViewVisibility(progressBar, !show, true, getContext());
    }

    private void setUpBackButton() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateOrEditExerciseActivity) getActivity()).goToPreviousFragment();
            }
        });
    }

    private void populateOnScreenDataIfEditMode() {
        if(createdExerciseObjectId != null) {
            new FetchCreatedExerciseData().execute();
        }
    }

    private void openUpWebView(String url) {
        new FinestWebView.Builder(getContext())
                .progressBarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .swipeRefreshColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .toolbarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .menuColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .menuTextColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .statusBarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .titleColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .urlColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .iconDefaultColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .show(url);
    }

    private void setUpStillConfused() {
        stillConfused.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUpWebView(STILL_CONFUSED_URL);
            }
        });
    }

    public boolean isValid() {
        if(cardioLayout.getVisibility() == View.VISIBLE) {
            if(metValue.getText().toString().isEmpty()) {
                return false;
            }
            else {
                return true;
            }
        }
        return true;
    }

    public void handleErrors() {
        // Left empty to handle design changes and subsequently, errors that arise, in the future
        // This will ONLY be invoked, by DESIGN, if cardioLayout is visible and isValid() returns false
        if(metValue.getText().toString().isEmpty()) {
            Utilities.showSnackBar(getString(R.string.met_value_required), backButton);
        }
    }

    public double getMetValue() {
        return Double.valueOf(metValue.getText().toString());
    }

    public void updateNameOnScreen(String name) {
        this.name.setText(name);
    }

    private void flipLayoutIfNeeded() {
        setLayout(!isCardio);
    }

    public void setLayout(boolean isStrength) {
        if(isStrength) {
            cardioLayout.setVisibility(View.GONE);
            strengthLayout.setVisibility(View.VISIBLE);
            CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) backButton.getLayoutParams();
            p.setAnchorId(R.id.strength_relative_layout);
            backButton.setLayoutParams(p);
        }
        else {
            strengthLayout.setVisibility(View.GONE);
            cardioLayout.setVisibility(View.VISIBLE);
            CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) backButton.getLayoutParams();
            p.setAnchorId(R.id.cardio_relative_layout);
            backButton.setLayoutParams(p);
        }
    }

    private void setUpMainLayoutKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    public int getSelectedCategory() {
        return categoryAdapter.getSelectedPosition();
    }

    public ArrayList<Integer> getSelectedEquipment() {
        return equipmentAdapter.getSelectedPositions();
    }

    public ArrayList<Integer> getSelectedPrimaryMuscles() {
        return primaryMusclesAdapter.getSelectedPositions();
    }

    public ArrayList<Integer> getSelectedSecondaryMuscles() {
        return secondaryMusclesAdapter.getSelectedPositions();
    }


    private void populateLists() {
        List<String> temp = Arrays.asList(getContext().getResources().getStringArray(R.array.exercise_categories));
        categories = new ArrayList<>(temp);

        temp = Arrays.asList(getContext().getResources().getStringArray(R.array.exercise_equipment));
        equipment = new ArrayList<>(temp);

        temp = Arrays.asList(getContext().getResources().getStringArray(R.array.exercise_muscles));
        primaryMuscles = new ArrayList<>(temp);

        temp = Arrays.asList(getContext().getResources().getStringArray(R.array.exercise_muscles));
        secondaryMuscles = new ArrayList<>(temp);

        categoryAdapter = new CreatedExerciseCategoryAdapter(getContext(), -1, categories);
        equipmentAdapter = new CreatedExerciseAdapter(getContext(), -1, equipment);
        primaryMusclesAdapter = new CreatedExerciseAdapter(getContext(), -1, primaryMuscles);
        secondaryMusclesAdapter = new CreatedExerciseAdapter(getContext(), -1, secondaryMuscles);

        categoryListView.setAdapter(categoryAdapter);
        equipmentListView.setAdapter(equipmentAdapter);
        primaryMusclesListView.setAdapter(primaryMusclesAdapter);
        secondaryMusclesListView.setAdapter(secondaryMusclesAdapter);

        categoryListView.setExpanded(true);
        equipmentListView.setExpanded(true);
        primaryMusclesListView.setExpanded(true);
        secondaryMusclesListView.setExpanded(true);

    }

    private ArrayList<Integer> getArrayListFrom(List<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for(Integer i : list) {
            newList.add(i);
        }
        return newList;
    }

    private void fetchCreatedExerciseData() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);

        if(isCardio) {
            metValueOfCreatedCardioExercise = object.getDouble(Constants.metValue);
        }
        else {
            createdStrengthExerciseCategory = object.getInt(Constants.category);
            List<Integer> temp = object.getList(Constants.equipmentList);
            equipmentArrayList = getArrayListFrom(temp);
            temp = object.getList(Constants.primaryMusclesList);
            primaryMusclesArrayList = getArrayListFrom(temp);
            temp = object.getList(Constants.secondaryMusclesList);
            secondaryMusclesArrayList = getArrayListFrom(temp);
        }
    }

    private void fillInSelectedChoices() {
        categoryAdapter.setPositionAsChecked(createdStrengthExerciseCategory);
        primaryMusclesAdapter.setPositionsAsChecked(primaryMusclesArrayList);
        secondaryMusclesAdapter.setPositionsAsChecked(secondaryMusclesArrayList);
        equipmentAdapter.setPositionsAsChecked(equipmentArrayList);
    }

    private void fillUpOnScreenData() {
        if(isCardio) {
            metValue.setText(String.valueOf(metValueOfCreatedCardioExercise));
            metValue.setSelection(metValue.getText().length());
        }
        else {
            fillInSelectedChoices();
        }
    }

    //-----------------------------------------------------
    // ASYNCTASK(S)
    //-----------------------------------------------------

    private class FetchCreatedExerciseData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                fetchCreatedExerciseData();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(isAdded()) {
                if(aBoolean) {
                    showMainLayout(true);
                    fillUpOnScreenData();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                    RETRY_COUNT++;
                    if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                        new FetchCreatedExerciseData().execute();
                    }
                }
            }
        }

    }

}
