package com.vidhyasagar.fitcentive.create_workout_fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.adapters.CreatedExerciseAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateWorkoutBasicFragment extends Fragment {

    public static CreateWorkoutBasicFragment newInstance(int dayOffset) {
        CreateWorkoutBasicFragment fragment = new CreateWorkoutBasicFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        fragment.setArguments(args);
        return fragment;
    }


    public static CreateWorkoutBasicFragment newInstance(int dayOffset, CreatedWorkoutInfo info,
                                                         ArrayList<Integer> daysList, CreateOrEditMealActivity.MODE mode) {
        CreateWorkoutBasicFragment fragment = new CreateWorkoutBasicFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putParcelable(ExpandedExerciseActivity.INFO, info);
        args.putIntegerArrayList(Constants.workoutDays, daysList);
        args.putSerializable(ExpandedRecipeActivity.MODE_TYPE, mode);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateWorkoutBasicFragment() {
        // Required empty public constructor
    }

    ArrayList<String> daysOfWeek;

    String flipCache;

    CreateOrEditMealActivity.MODE mode;
    int dayOffset;
    // These only come into play when the workout is ALREADY CREATED
    CreatedWorkoutInfo info;
    ArrayList<Integer> daysList;

    ExpandableHeightListView daysListView;
    FloatingActionButton nextButton;
    RelativeLayout mainLayout;
    TextInputEditText workoutName;
    CreatedExerciseAdapter daysAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_workout_basic, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpMainLayoutKeyboardHide();
        setUpDaysListView();
        setUpNextButton();

        if(mode != CreateOrEditMealActivity.MODE.MODE_CREATE) {
            Utilities.hideKeyboard(nextButton, getActivity());
            populateOnScreenData();
        }

        setUpTextWatcher();
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        info = getArguments().getParcelable(ExpandedExerciseActivity.INFO);
        daysList = getArguments().getIntegerArrayList(Constants.workoutDays);
        mode = (CreateOrEditMealActivity.MODE) getArguments().getSerializable(ExpandedRecipeActivity.MODE_TYPE);

        if(mode == null) {
            mode = CreateOrEditMealActivity.MODE.MODE_CREATE;
        }
    }

    private void bindViews(View v) {
        daysListView = (ExpandableHeightListView) v.findViewById(R.id.days_list);
        workoutName = (TextInputEditText) v.findViewById(R.id.workout_name_edittext);
        nextButton = (FloatingActionButton) v.findViewById(R.id.next_button);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
    }

    private void setUpMainLayoutKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(v, getActivity());
            }
        });
    }

    private ArrayList<Integer> applyNormalOffset(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for(Integer i : list) {
            newList.add(i + CreateOrEditExerciseActivity.NORMAL_OFFSET);
        }
        return newList;
    }

    private void populateOnScreenData() {
        workoutName.setText(info.getWorkoutName());
        workoutName.setSelection(info.getWorkoutName().length());
        daysAdapter.setPositionsAsChecked(applyNormalOffset(daysList));

        if(mode == CreateOrEditMealActivity.MODE.MODE_VIEW) {
            workoutName.setEnabled(false);
            daysAdapter.disableCheckboxes(true);
        }
        else if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            workoutName.setEnabled(true);
            daysAdapter.disableCheckboxes(false);
            flipCache = workoutName.getText().toString();
        }
    }

    private void setUpDaysListView() {
        String[] tempArray = getResources().getStringArray(R.array.days_of_week);
        List<String> temp = Arrays.asList(tempArray);
        daysOfWeek = new ArrayList<>(temp);

        daysAdapter = new CreatedExerciseAdapter(getContext(), -1, daysOfWeek);
        daysListView.setAdapter(daysAdapter);
        daysListView.setExpanded(true);
    }

    private void setUpNextButton() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValid()) {
                    ((CreateOrEditWorkoutActivity) getActivity()).goToNextFragment();
                }
                else {
                    handleErrors();
                }
            }
        });
    }

    public boolean isValid() {
        return !this.workoutName.getText().toString().isEmpty();
    }

    public void handleErrors() {
        if(this.workoutName.getText().toString().isEmpty()) {
            Utilities.showSnackBar(getString(R.string.workout_name_required), nextButton);
        }
    }

    public void sendBasicDataToParent() {
        ((CreateOrEditWorkoutActivity) getActivity()).setWorkoutName(workoutName.getText().toString().trim());
        ((CreateOrEditWorkoutActivity) getActivity()).setWorkoutDays(daysAdapter.getSelectedPositions());
    }

    private void setUpTextWatcher() {
        workoutName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((CreateOrEditWorkoutActivity) getActivity()).updateName(s.toString().trim());
            }
        });
    }

    public void flipViewMode() {
        if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            mode = CreateOrEditMealActivity.MODE.MODE_VIEW;
            workoutName.setEnabled(false);
            workoutName.setText(flipCache);
            daysAdapter.disableCheckboxes(true);
        }
        else {
            mode = CreateOrEditMealActivity.MODE.MODE_EDIT;
            workoutName.setEnabled(true);
            workoutName.setSelection(workoutName.getText().length());
            flipCache = workoutName.getText().toString().trim();
            daysAdapter.disableCheckboxes(false);
        }
    }

    public void forceFlipCache() {
        flipCache = workoutName.getText().toString().trim();
    }
}
