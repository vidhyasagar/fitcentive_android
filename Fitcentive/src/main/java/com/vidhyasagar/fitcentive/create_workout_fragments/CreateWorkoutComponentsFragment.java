package com.vidhyasagar.fitcentive.create_workout_fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.SearchExerciseActivity;
import com.vidhyasagar.fitcentive.adapters.WorkoutComponentsAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.NoScrollLayoutManager;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.ArrayList;

import github.nisrulz.recyclerviewhelper.RVHItemDividerDecoration;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateWorkoutComponentsFragment extends Fragment {

    public static String STRENGTH_COMPONENTS_LIST = "STRENGTH_COMPONENTS_LIST";
    public static String CARDIO_COMPONENTS_LIST = "CARDIO_COMPONENTS_LIST";

    public static CreateWorkoutComponentsFragment newInstance(int dayOffset) {
        CreateWorkoutComponentsFragment fragment = new CreateWorkoutComponentsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        fragment.setArguments(args);
        return fragment;
    }

    public static CreateWorkoutComponentsFragment newInstance(int dayOffset, String workoutName, CreateOrEditMealActivity.MODE mode,
                                                              ArrayList<CardioInfo> cardioComponents,
                                                              ArrayList<ExerciseInfo> strengthComponents) {
        CreateWorkoutComponentsFragment fragment = new CreateWorkoutComponentsFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.DAY_OFFSET, dayOffset);
        args.putString(Constants.workoutName, workoutName);
        args.putSerializable(ExpandedRecipeActivity.MODE_TYPE, mode);
        args.putParcelableArrayList(STRENGTH_COMPONENTS_LIST, strengthComponents);
        args.putParcelableArrayList(CARDIO_COMPONENTS_LIST, cardioComponents);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateWorkoutComponentsFragment() {
        // Required empty public constructor
    }

    int positionToUpdate;

    int dayOffset;
    CreateOrEditMealActivity.MODE viewMode;
    ItemTouchHelper.Callback callback;
    ItemTouchHelper helper;

    // These only come into play for WORKOUTS that are ALREADY CREATED
    ArrayList<CardioInfo> cardioComponents;
    ArrayList<ExerciseInfo> strengthComponents;
    String workoutNameString;

    ArrayList<Object> components;

    WorkoutComponentsAdapter componentsAdapter;
    RelativeLayout noComponentsLayout;
    TextView workoutName, workoutTotalCalories;
    RelativeLayout mainLayout;
    RecyclerView componentsRecycler;
    FloatingActionButton addButton;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_workout_components, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpAddExerciseButton();
        setUpMainLayoutKeyboardHide();
        setUpRecycler();

        if(viewMode != CreateOrEditMealActivity.MODE.MODE_CREATE) {
            populateOnScreenDataFromCreatedWorkout();
            setUpInteractiveSwipeBasedOnMode();
            if(viewMode == CreateOrEditMealActivity.MODE.MODE_VIEW) {
                componentsAdapter.setDisabled(true);
                addButton.hide();
            }
            else {
                componentsAdapter.setDisabled(false);
                addButton.show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT) {
            if(resultCode == CreateOrEditWorkoutActivity.RESULT_STRENGTH || resultCode == CreateOrEditWorkoutActivity.RESULT_CREATED_STRENGTH) {
                ExerciseInfo info = data.getExtras().getParcelable(ExpandedExerciseActivity.INFO);
                getChosenStrengthComponent(info, false);
            }
            else if(resultCode == CreateOrEditWorkoutActivity.RESULT_CARDIO ||  resultCode == CreateOrEditWorkoutActivity.RESULT_CREATED_CARDIO) {
                CardioInfo info = data.getExtras().getParcelable(ExpandedExerciseActivity.INFO);
                getChosenCardioComponent(info, false);
            }
        }
    }

    private void retrieveArguments() {
        dayOffset = getArguments().getInt(Constants.DAY_OFFSET);
        viewMode = (CreateOrEditMealActivity.MODE) getArguments().getSerializable(ExpandedRecipeActivity.MODE_TYPE);
        if(viewMode == null) {
            viewMode = CreateOrEditMealActivity.MODE.MODE_CREATE;
        }

        if(viewMode != CreateOrEditMealActivity.MODE.MODE_CREATE) {
            workoutNameString = getArguments().getString(Constants.workoutName);
            cardioComponents = getArguments().getParcelableArrayList(CARDIO_COMPONENTS_LIST);
            strengthComponents = getArguments().getParcelableArrayList(STRENGTH_COMPONENTS_LIST);
        }

    }

    private void bindViews(View v) {
        componentsRecycler = (RecyclerView) v.findViewById(R.id.workout_components_recyler);
        addButton = (FloatingActionButton) v.findViewById(R.id.add_component_button);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        workoutName = (TextView) v.findViewById(R.id.workout_name);
        workoutTotalCalories = (TextView) v.findViewById(R.id.workout_components_total_calories);
        noComponentsLayout = (RelativeLayout) v.findViewById(R.id.no_components_layout);
    }

    private void populateOnScreenDataFromCreatedWorkout() {
        workoutName.setText(workoutNameString);

        for(CardioInfo info : cardioComponents) {
            components.add(info);
        }

        for(ExerciseInfo info : strengthComponents) {
            components.add(info);
        }

        if(!components.isEmpty()) {
            componentsAdapter.notifyDataSetChanged();
            showRecycler(true);
        }

        Utilities.hideKeyboard(componentsRecycler, getActivity());
        updateCaloriesTextviewHeader();

    }

    private void updateCaloriesTextviewHeader() {
        componentsAdapter.updateTextViewHeader();
    }

    private void setUpMainLayoutKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(v, getActivity());
            }
        });
    }

    private void setUpAddExerciseButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSearchExerciseActivityForWorkout();
            }
        });
    }

    private void startSearchExerciseActivityForWorkout() {
        // CONTEXT BEING SENT HERE IS ACTIVITY AND NOT FRAGMENT CONTEXT
        Intent i = new Intent(getContext(), SearchExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(SearchExerciseActivity.NO_OF_TABS_STRING, SearchExerciseActivity.NO_OF_TABS_WORKOUT_MODE);
        startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }

    public void setPositionToUpdate(int positionToUpdate) {
        this.positionToUpdate = positionToUpdate;
    }

    public void updateName(String name) {
        workoutName.setText(name);
    }

    private void setUpInteractiveSwipeBasedOnMode() {
        if(viewMode == CreateOrEditMealActivity.MODE.MODE_VIEW) {
            callback = new RVHItemTouchHelperCallback(componentsAdapter, false, false, false);
            helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(componentsRecycler);
        }
        else {
            callback = new RVHItemTouchHelperCallback(componentsAdapter, true, false, true);
            helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(componentsRecycler);
        }
    }

    private void setUpRecycler() {
        components = new ArrayList<>();
        NoScrollLayoutManager lm = new NoScrollLayoutManager(getContext());
        lm.setScrollEnabled(false);
        componentsRecycler.setLayoutManager(lm);
        componentsAdapter = new WorkoutComponentsAdapter(components, getContext(), componentsRecycler, workoutTotalCalories, dayOffset, this);
        componentsRecycler.setAdapter(componentsAdapter);

        setUpInteractiveSwipeBasedOnMode();

        // Set the divider in the recyclerview
        componentsRecycler.addItemDecoration(new RVHItemDividerDecoration(getContext(), LinearLayoutManager.VERTICAL));

        // As it is initially empty
        showRecycler(false);
    }

    public void getChosenStrengthComponent(ExerciseInfo info, boolean fromActivity) {
        if(!fromActivity) {
            components.add(info);
        }
        else {
            components.remove(positionToUpdate);
            components.add(info);
        }
        componentsAdapter.notifyDataSetChanged();
        componentsAdapter.updateTextViewHeader();
        showRecycler(true);
    }

    public void getChosenCardioComponent(CardioInfo info, boolean fromActivity) {
        if(!fromActivity) {
            components.add(info);
        }
        else {
            components.remove(positionToUpdate);
            components.add(info);
        }
        componentsAdapter.notifyDataSetChanged();
        componentsAdapter.updateTextViewHeader();
        showRecycler(true);

    }

    private void showRecycler(boolean show) {
        Utilities.setViewVisibility(componentsRecycler, show, true, getContext());
        Utilities.setViewVisibility(noComponentsLayout, !show, true, getContext());
    }

    public boolean isValid() {
        return !components.isEmpty();
    }

    public void sendWorkoutDataToParent() {
        ((CreateOrEditWorkoutActivity) getActivity()).setWorkouts(components);
    }

    public void handleErrors() {
        Utilities.showSnackBar(getString(R.string.atleast_one_exercise_required), componentsRecycler);
    }

    public void flipViewMode() {
        if(viewMode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            viewMode = CreateOrEditMealActivity.MODE.MODE_VIEW;
            componentsAdapter.setDisabled(true);
            addButton.hide();
        }
        else {
            viewMode = CreateOrEditMealActivity.MODE.MODE_EDIT;
            componentsAdapter.setDisabled(false);
            addButton.show();
        }

        setUpInteractiveSwipeBasedOnMode();
    }
}
