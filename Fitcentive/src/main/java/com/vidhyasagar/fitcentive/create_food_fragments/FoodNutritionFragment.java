package com.vidhyasagar.fitcentive.create_food_fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditFoodActivity;
import com.vidhyasagar.fitcentive.parse.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodNutritionFragment extends Fragment {


    public static FoodNutritionFragment newInstance() {
        FoodNutritionFragment fragment = new FoodNutritionFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public static FoodNutritionFragment newInstance(String calories, String proteins, String fats, String carbs,
                                                    String polyFats, String monoFats, String satFats, String transFats,
                                                    String cholesterol, String sugar, String fiber, String sodium,
                                                    String potassium, String calcium, String iron, String vitaminA,
                                                    String vitaminC) {
        FoodNutritionFragment fragment = new FoodNutritionFragment();
        Bundle args = new Bundle();
        args.putString(Constants.calories, calories);
        args.putString(Constants.protein, proteins);
        args.putString(Constants.fat, fats);
        args.putString(Constants.carbs, carbs);
        args.putString(Constants.polyFats, polyFats);
        args.putString(Constants.monoFats, monoFats);
        args.putString(Constants.satFats, satFats);
        args.putString(Constants.transFats, transFats);
        args.putString(Constants.cholestrol, cholesterol);
        args.putString(Constants.sugar, sugar);
        args.putString(Constants.fiber, fiber);
        args.putString(Constants.sodium, sodium);
        args.putString(Constants.potassium, potassium);
        args.putString(Constants.calcium, calcium);
        args.putString(Constants.iron, iron);
        args.putString(Constants.vitaminA, vitaminA);
        args.putString(Constants.vitaminC, vitaminC);
        fragment.setArguments(args);
        return fragment;
    }

    public FoodNutritionFragment() {
        // Required empty public constructor
    }

    public enum NUTRITION_ERROR_TYPE {TYPE_CALORIES, TYPE_FATS, TYPE_PROTEINS, TYPE_CARBS};

    RelativeLayout mainLayout;
    EditText calories, proteins, fats, carbs;
    EditText polyFats, monoFats, transFats, satFats;
    EditText cholestrol, sugar, fiber, sodium, potassium, calcium, iron, vitaminA, vitaminC;
    FloatingActionButton backButton;

    String caloriesString, proteinsString, fatsString, carbsString, polyFatsString, monoFatsString;
    String transFatsString, satFatsString, cholesterolString, sugarString, fiberString, sodiumString;
    String potassiumString, calciumString, ironString, vitaminAString, vitaminCString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_food_nutrition, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpBackButton();
        setUpKeyboardHide();
        populateDataIfAny();
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    private void retrieveArguments() {
        caloriesString = getArguments().getString(Constants.calories, "");
        proteinsString = getArguments().getString(Constants.protein, "");
        fatsString = getArguments().getString(Constants.fat, "");
        carbsString = getArguments().getString(Constants.carbs, "");
        polyFatsString = getArguments().getString(Constants.polyFats, "");
        monoFatsString = getArguments().getString(Constants.monoFats, "");
        transFatsString = getArguments().getString(Constants.transFats, "");
        satFatsString = getArguments().getString(Constants.satFats, "");
        cholesterolString = getArguments().getString(Constants.cholestrol, "");
        sugarString = getArguments().getString(Constants.sugar, "");
        fiberString = getArguments().getString(Constants.fiber, "");
        sodiumString = getArguments().getString(Constants.sodium, "");
        potassiumString = getArguments().getString(Constants.potassium, "");
        calciumString = getArguments().getString(Constants.calcium, "");
        ironString = getArguments().getString(Constants.iron, "");
        vitaminAString = getArguments().getString(Constants.vitaminA, "");
        vitaminCString = getArguments().getString(Constants.vitaminC, "");
    }

    private void populateDataIfAny() {
        calories.setText(caloriesString);
        proteins.setText(proteinsString);
        fats.setText(fatsString);
        carbs.setText(carbsString);
        polyFats.setText(polyFatsString);
        monoFats.setText(monoFatsString);
        transFats.setText(transFatsString);
        satFats.setText(satFatsString);
        cholestrol.setText(cholesterolString);
        sugar.setText(sugarString);
        fiber.setText(fiberString);
        sodium.setText(sodiumString);
        potassium.setText(potassiumString);
        calcium.setText(calciumString);
        iron.setText(ironString);
        vitaminA.setText(vitaminAString);
        vitaminC.setText(vitaminCString);
    }

    private double getDoubleValue(EditText editText) {
        if(editText.getText().toString().trim().isEmpty()) {
            return 0;
        }
        else {
            return Double.valueOf(editText.getText().toString().trim());
        }
    }

    public void sendDataToParent() {
        ((CreateOrEditFoodActivity) getActivity()).sendAcrossBasicData(
                getDoubleValue(calories),
                getDoubleValue(fats),
                getDoubleValue(proteins),
                getDoubleValue(carbs)
        );

        ((CreateOrEditFoodActivity) getActivity()).sendAcrossDetailedData(
                getDoubleValue(satFats), getDoubleValue(polyFats),
                getDoubleValue(monoFats), getDoubleValue(transFats),
                getDoubleValue(cholestrol), getDoubleValue(potassium),
                getDoubleValue(sodium), getDoubleValue(fiber),
                getDoubleValue(sugar), getDoubleValue(vitaminA),
                getDoubleValue(vitaminC), getDoubleValue(calcium),
                getDoubleValue(iron)
        );
    }

    public boolean isNutritionalInfoValid() {
        if(!calories.getText().toString().trim().isEmpty() &&
                !proteins.getText().toString().trim().isEmpty() &&
                !fats.getText().toString().trim().isEmpty() &&
                !carbs.getText().toString().trim().isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public void makeSnackbarMessage(String message) {
        Snackbar snackbar = Snackbar.make(backButton, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public NUTRITION_ERROR_TYPE getErrorType() {
        if(calories.getText().toString().isEmpty()) {
            return NUTRITION_ERROR_TYPE.TYPE_CALORIES;
        }
        if(proteins.getText().toString().isEmpty()) {
            return NUTRITION_ERROR_TYPE.TYPE_PROTEINS;
        }
        if(fats.getText().toString().isEmpty()) {
            return NUTRITION_ERROR_TYPE.TYPE_FATS;
        }
        if(carbs.getText().toString().isEmpty()) {
            return NUTRITION_ERROR_TYPE.TYPE_CARBS;
        }
        return null;
    }

    public boolean validateData() {
        return isNutritionalInfoValid();
    }

    private void setUpBackButton() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateOrEditFoodActivity) getActivity()).goToPreviousFragment();
            }
        });
    }

    private void bindViews(View v) {
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
        backButton = (FloatingActionButton) v.findViewById(R.id.back_button);
        calories = (EditText) v.findViewById(R.id.calories_edittext);
        proteins = (EditText) v.findViewById(R.id.proteins_edittext);
        fats = (EditText) v.findViewById(R.id.fats_edittext);
        carbs = (EditText) v.findViewById(R.id.carbs_edittext);
        polyFats = (EditText) v.findViewById(R.id.poly_fats_edittext);
        monoFats = (EditText) v.findViewById(R.id.mono_fats_edittext);
        transFats = (EditText) v.findViewById(R.id.trans_fats_edittext);
        satFats = (EditText) v.findViewById(R.id.sat_fats_edittext);
        sodium = (EditText) v.findViewById(R.id.sodium_edittext);
        potassium = (EditText) v.findViewById(R.id.potassium_edittext);
        iron = (EditText) v.findViewById(R.id.iron_edittext);
        calcium = (EditText) v.findViewById(R.id.calcium_edittext);
        fiber = (EditText) v.findViewById(R.id.fiber_edittext);
        cholestrol = (EditText) v.findViewById(R.id.cholestrol_edittext);
        vitaminA = (EditText) v.findViewById(R.id.vitamin_a_edittext);
        vitaminC = (EditText) v.findViewById(R.id.vitamin_c_edittext);
        sugar = (EditText) v.findViewById(R.id.sugar_edittext);
    }

    private void setUpKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
    }
}
