package com.vidhyasagar.fitcentive.create_food_fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditFoodActivity;
import com.vidhyasagar.fitcentive.parse.Constants;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodBasicInfoFragment extends Fragment {

    public static FoodBasicInfoFragment newInstance() {
        FoodBasicInfoFragment foodBasicInfoFragment = new FoodBasicInfoFragment();
        Bundle args = new Bundle();
        foodBasicInfoFragment.setArguments(args);
        return foodBasicInfoFragment;
    }

    public static FoodBasicInfoFragment newInstance(String foodName, String brandName, String foodDescription,
                                                    String servingSize, String servingSizeUnit) {
        FoodBasicInfoFragment foodBasicInfoFragment = new FoodBasicInfoFragment();
        Bundle args = new Bundle();
        args.putString(Constants.foodName, foodName);
        args.putString(Constants.foodBrand, brandName);
        args.putString(Constants.foodDescription, foodDescription);
        args.putString(Constants.servingSize, servingSize);
        args.putString(Constants.servingSizeUnit, servingSizeUnit);
        foodBasicInfoFragment.setArguments(args);
        return foodBasicInfoFragment;
    }

    public FoodBasicInfoFragment() {
        // Required empty public constructor
    }

    public enum BASIC_ERROR_TYPE {TYPE_NAME, TYPE_SERVING_SIZE, TYPE_SERVING_UNIT, TYPE_DESCIPTION};

    FloatingActionButton nextButton;
    TextInputEditText foodName;
    EditText foodBrand, foodDescription;
    EditText foodServingSize, foodServingSizeUnit;
    RelativeLayout mainLayout;

    String foodname, foodbrand, fooddescription, foodservingsize, foodservingsizeunit;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_food_basic_info, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpNextButton();
        setUpKeyboardHide();
        populateDataIfAny();
        hideKeyboard();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mainLayout.getWindowToken(), 0);
    }

    private void retrieveArguments() {
        foodname = getArguments().getString(Constants.foodName, "");
        foodbrand = getArguments().getString(Constants.foodBrand, "");
        fooddescription = getArguments().getString(Constants.foodDescription, "");
        foodservingsize = getArguments().getString(Constants.servingSize, "");
        foodservingsizeunit = getArguments().getString(Constants.servingSizeUnit, "");
    }

    private void bindViews(View v) {
        nextButton = (FloatingActionButton) v.findViewById(R.id.next_button);
        foodName = (TextInputEditText) v.findViewById(R.id.food_name_edittext);
        foodBrand = (EditText) v.findViewById(R.id.food_brand_edittext);
        foodDescription = (EditText) v.findViewById(R.id.food_desc_edittext);
        foodServingSize = (EditText) v.findViewById(R.id.serving_size_edittext);
        foodServingSizeUnit = (EditText) v.findViewById(R.id.serving_unit_edittext);
        mainLayout = (RelativeLayout) v.findViewById(R.id.main_relative_layout);
    }

    private void populateDataIfAny() {
        foodName.setText(foodname);
        foodBrand.setText(foodbrand);
        foodDescription.setText(fooddescription);
        foodServingSize.setText(foodservingsize);
        foodServingSizeUnit.setText(foodservingsizeunit);
    }

    private void setUpKeyboardHide() {
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });
    }

    private void setUpNextButton() {
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextPageIfValid();
            }
        });
    }

    private boolean basicInfoIsValid() {
        if(!foodName.getText().toString().isEmpty()
                && !foodServingSize.getText().toString().isEmpty()
                && !foodServingSizeUnit.getText().toString().isEmpty()
                && !foodDescription.getText().toString().isEmpty()) {
            return true;
        }
        else {
            return false;
        }
    }

    public BASIC_ERROR_TYPE getErrorType() {
        if(foodName.getText().toString().isEmpty()) {
            return BASIC_ERROR_TYPE.TYPE_NAME;
        }
        if(foodDescription.getText().toString().isEmpty()) {
            return BASIC_ERROR_TYPE.TYPE_DESCIPTION;
        }
        if(foodServingSize.getText().toString().isEmpty()) {
            return BASIC_ERROR_TYPE.TYPE_SERVING_SIZE;
        }
        if(foodServingSizeUnit.getText().toString().isEmpty()) {
            return BASIC_ERROR_TYPE.TYPE_SERVING_UNIT;
        }
        return null;
    }

    private void goToNextPageIfValid() {
        if(basicInfoIsValid()) {
            ((CreateOrEditFoodActivity) getActivity()).goToNextFragment();
        }
        else {
            BASIC_ERROR_TYPE errorType = getErrorType();
            String errorMessage = null;
            switch (errorType) {
                case TYPE_NAME:
                    errorMessage = getString(R.string.food_error_name);
                    break;
                case TYPE_SERVING_SIZE:
                    errorMessage = getString(R.string.food_error_serving);
                    break;
                case TYPE_SERVING_UNIT:
                    errorMessage = getString(R.string.food_error_unit);
                    break;
                case TYPE_DESCIPTION:
                    errorMessage = getString(R.string.food_error_description);
                default:
                    break;
            }
            Snackbar snackbar = Snackbar.make(nextButton, errorMessage, Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
    }

    public void makeSnackbarMessage(String message) {
        Snackbar snackbar = Snackbar.make(nextButton, message, Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    public boolean validateData() {
        return basicInfoIsValid();
    }

    public void sendDataToParent() {
        ((CreateOrEditFoodActivity) getActivity()).sendAcrossData(foodName.getText().toString().trim(),
                                                foodBrand.getText().toString().trim(),
                                                foodDescription.getText().toString().trim(),
                                                foodServingSize.getText().toString().trim(),
                                                foodServingSizeUnit.getText().toString().trim());
    }
}
