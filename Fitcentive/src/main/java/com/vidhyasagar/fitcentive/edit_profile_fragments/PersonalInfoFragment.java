package com.vidhyasagar.fitcentive.edit_profile_fragments;


import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.parse.Constants;

import java.io.ByteArrayOutputStream;
import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInfoFragment extends Fragment {

    static String TAG = "PersonalInfoFragment";
    boolean IS_CAMERA_USED = false;

    Bundle arguments;
    boolean isEditable, isMale, isPublic;
    int birthdayDay, birthdayMonth, birthdayYear;
    ScrollView parentScrollView;
    RelativeLayout parentRelativeLayout;
    Button birthdayButton;
    InputMethodManager imm;
    TextInputEditText firstname, lastname, tagline, email, username, country, city;
    TextView bDay, bMonth, bYear;
    CircleImageView profileImage;
    String firstnameString, lastnameString, taglineString, emailString, usernameString, countryString, cityString;
    ProgressBar progressBar;
    Spinner spinner;
    Switch aSwitch;
    TextView switchHint, profileType;
    LinearLayout switchLinearLayout;
    ArrayAdapter<CharSequence> adapter;

    ParseFile profilePictureFile;
    byte[] profilePictureData;
    public Uri selectedImageUri;
    Bitmap selectedProfilePicture;

    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personal_info, container, false);
    }

    public void updateOnScreenDate(int day, int month, int year) {
        bDay.setText(String.valueOf(day));
        bYear.setText(String.valueOf(year));
        bMonth.setText(getBirthdayMonthString(month + 1));
    }

    public void createDateDialog() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateOnScreenDate(dayOfMonth, monthOfYear, year);

                    }
                }, Integer.valueOf(bYear.getText().toString()), getBirthdayMonthInt(bMonth.getText().toString()) - 1, Integer.valueOf(bDay.getText().toString()));
        datePickerDialog.show();
    }

    public String getBirthdayMonthString(int month) {
        String monthString;
        switch(month) {
            case 1: monthString = "Jan";
                break;
            case 2: monthString = "Feb";
                break;
            case 3: monthString = "Mar";
                break;
            case 4: monthString = "Apr";
                break;
            case 5: monthString = "May";
                break;
            case 6: monthString = "Jun";
                break;
            case 7: monthString = "Jul";
                break;
            case 8: monthString = "Aug";
                break;
            case 9: monthString = "Sep";
                break;
            case 10: monthString = "Oct";
                break;
            case 11: monthString = "Nov";
                break;
            case 12: monthString = "Dec";
                break;
            default: monthString = "Unk";
                break;
        }
        return monthString;
    }

    public int getBirthdayMonthInt(String month) {
        switch(month) {
            case "Jan" : return 1;
            case "Feb" : return 2;
            case "Mar" : return 3;
            case "Apr" : return 4;
            case "May" : return 5;
            case "Jun" : return 6;
            case "Jul" : return 7;
            case "Aug" : return 8;
            case "Sep" : return 9;
            case "Oct" : return 10;
            case "Nov" : return 11;
            case "Dec" : return 12;
            default    : return  0;
        }
    }

    public void bindViews() {
        parentScrollView = (ScrollView) getActivity().findViewById(R.id.parent_scrollview);
        parentRelativeLayout = (RelativeLayout) getActivity().findViewById(R.id.parent_relative_layout);
        firstname = (TextInputEditText) getActivity().findViewById(R.id.first_name_edittext);
        lastname = (TextInputEditText) getActivity().findViewById(R.id.last_name_edittext);
        tagline = (TextInputEditText) getActivity().findViewById(R.id.tagline_edittext);
        email = (TextInputEditText) getActivity().findViewById(R.id.email_edittext);
        username = (TextInputEditText) getActivity().findViewById(R.id.username_edittext);
        progressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
        profileImage = (CircleImageView) getActivity().findViewById(R.id.profile_image);
        spinner = (Spinner) getActivity().findViewById(R.id.gender_spinner);
        bDay = (TextView) getActivity().findViewById(R.id.birthday_day);
        bMonth = (TextView) getActivity().findViewById(R.id.birthday_month);
        bYear = (TextView) getActivity().findViewById(R.id.birthday_year);
        birthdayButton = (Button) getActivity().findViewById(R.id.change_birthday_button);
        country = (TextInputEditText) getActivity().findViewById(R.id.country_edittext);
        city = (TextInputEditText) getActivity().findViewById(R.id.city_edittext);
        aSwitch = (Switch) getActivity().findViewById(R.id.switch2);
        switchHint = (TextView) getActivity().findViewById(R.id.profile_type_hint);
        profileType = (TextView) getActivity().findViewById(R.id.profile_type);
        switchLinearLayout = (LinearLayout) getActivity().findViewById(R.id.switch_linear_layout);
        imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void retrieveArguments() {
        arguments = getArguments();
        this.usernameString = arguments.getString("username");
        this.isEditable = arguments.getBoolean("isEditable");
    }

    public void fetchUserInfo() throws ParseException, Exception{
        // Running in the background thread
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo(Constants.username, this.usernameString);
        ParseUser user = query.getFirst();
        this.firstnameString = user.getString(Constants.firstname);
        this.lastnameString = user.getString(Constants.lastname);
        this.emailString = user.getEmail();
        this.taglineString = user.getString(Constants.profileTagline);
        this.isMale = user.getBoolean(Constants.isMale);
        this.birthdayDay = user.getInt(Constants.birthdayDay);
        this.birthdayMonth = user.getInt(Constants.birthdayMonth);
        this.birthdayYear = user.getInt(Constants.birthdayYear);
        this.countryString = user.getString(Constants.country);
        this.cityString = user.getString(Constants.city);
        this.isPublic = user.getBoolean(Constants.isPublic);

        this.profilePictureFile = user.getParseFile(Constants.profilePicture);
        if(profilePictureFile != null) {
            profilePictureData = profilePictureFile.getData();
        }
        if(this.taglineString == null) {
            this.taglineString = getString(R.string.no_tagline);
        }
        if(this.countryString == null) {
            this.countryString = getString(R.string.unspecified);
        }
        if(this.cityString == null) {
            this.cityString = getString(R.string.unspecified);
        }

    }

    public void displayImage() {
        if(profilePictureData != null) {
            Bitmap tempSelectedProfilePicture = BitmapFactory.decodeByteArray(profilePictureData, 0, profilePictureData.length);
            if (tempSelectedProfilePicture != null) {
                profileImage.setImageBitmap(tempSelectedProfilePicture);
            } else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
        else {
            profileImage.setImageResource(R.drawable.ic_avatar);
        }
    }

    public void updateViewsWithUserInfo() {
        firstname.setText(firstnameString);
        lastname.setText(lastnameString);
        email.setText(emailString);
        tagline.setText(taglineString);
        username.setText(usernameString);
        bDay.setText(String.valueOf(this.birthdayDay));
        bMonth.setText(getBirthdayMonthString(this.birthdayMonth));
        bYear.setText(String.valueOf(this.birthdayYear));
        city.setText(cityString);
        country.setText(countryString);
        aSwitch.setChecked(this.isPublic);

        displayImage();

        if(this.isMale) {
            spinner.setSelection(adapter.getPosition(getString(R.string.male)));
        } else {
            spinner.setSelection(adapter.getPosition(getString(R.string.female)));
        }
    }

    public void setUpKeyboardBehavior() {
        // To make sure keyboard is hidden during launch
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // On touch listener to hide keyboard when user presses anywhere on screen
        parentRelativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });
    }

    public void initSpinner() {
        adapter = ArrayAdapter.createFromResource(getContext(),
                R.array.gender_choices, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    public void setUpChangeBirthdayButton() {
        birthdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDateDialog();
            }
        });
        bDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDateDialog();
            }
        });
        bMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDateDialog();
            }
        });
        bYear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDateDialog();
            }
        });
    }

    public void disableItemsIfNeeded() {
        if(!this.isEditable) {
            profileImage.setEnabled(false);
            firstname.setEnabled(false);
            lastname.setEnabled(false);
            tagline.setEnabled(false);
            spinner.setEnabled(false);
            bDay.setEnabled(false);
            bDay.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_color));
            bYear.setEnabled(false);
            bYear.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_color));
            bMonth.setEnabled(false);
            bMonth.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_color));
            birthdayButton.setEnabled(false);
            setViewVisibility(birthdayButton, false, true);
            city.setEnabled(false);
            country.setEnabled(false);
            switchLinearLayout.setVisibility(View.GONE);
            switchHint.setVisibility(View.GONE);
            profileType.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) profileImage.getLayoutParams();
            layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
            profileImage.setLayoutParams(layoutParams);
        }
    }

    public Bitmap grabImage(CircleImageView imageView) {
        getActivity().getContentResolver().notifyChange(selectedImageUri, null);
        ContentResolver cr = getActivity().getContentResolver();
        Bitmap bitmap = null;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImageUri);

        }
        catch (Exception e)
        {
            Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Failed to load", e);
        }

        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        imageView.setImageBitmap(scaled);
        return scaled;

    }

    private void onSelectedImageResult() {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        selectedProfilePicture = grabImage(profileImage);
        selectedProfilePicture.compress(Bitmap.CompressFormat.JPEG, getResources().getInteger(R.integer.bitmap_compression), bytes);
        selectedProfilePicture.compress(Bitmap.CompressFormat.PNG, getResources().getInteger(R.integer.bitmap_compression), stream);

        byte[] byte_data = stream.toByteArray();
        ParseFile file = new ParseFile(byte_data);
        new SavePicture().execute(file);

    }

    public void startProfileCropPictureActivity() {
        CropImage.activity(selectedImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(getString(R.string.crop_image_activity_title))
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setCropShape(CropImageView.CropShape.OVAL)
                .start(getContext(), this);

    }

    public File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    private void mayRequestWriteToStorage() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.confirm_write_access_denial))
                        .setMessage(getString(R.string.permission_confirmation_write))
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, ProfileActivity.REQUEST_WRITE_PERMISSION);
                            }
                        })
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        ProfileActivity.REQUEST_WRITE_PERMISSION);
            }
        }
        else {
            cameraIntent();
        }

    }

    private void mayRequestCamera() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.confirm_camera_access_denial))
                        .setMessage(getString(R.string.permission_confirmation_camera))
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.CAMERA}, ProfileActivity.REQUEST_IMAGE_CAPTURE);
                            }
                        })
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        ProfileActivity.REQUEST_IMAGE_CAPTURE);
            }
        }
        else {
            mayRequestWriteToStorage();
        }

    }

    private void cameraIntent() {
        IS_CAMERA_USED = true;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            File photo;
            try
            {
                // place where to store camera taken picture
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            }
            catch(Exception e)
            {
                Log.v(TAG, "Can't create file to take picture!");
                Toast.makeText(getContext(), getString(R.string.need_write_permission), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                return;
            }
            selectedImageUri = Uri.fromFile(photo);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
            //start camera intent
            startActivityForResult(takePictureIntent, ProfileActivity.REQUEST_IMAGE_CAPTURE);
        }
        else {
            Toast.makeText(getContext(), getString(R.string.need_camera_for_action), Toast.LENGTH_SHORT).show();
        }

    }

    private void galleryIntent() {
        IS_CAMERA_USED = false;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), ProfileActivity.SELECT_FILE);
    }

    public void createImagePickingDialog() {
        final CharSequence[] items = getResources().getStringArray(R.array.image_picking_choices);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.set_new_profile_picture))
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            mayRequestCamera();
                        }
                        else if(which == 1) {
                            galleryIntent();
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setUpProfilePictureSetUp() {
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createImagePickingDialog();
            }
        });
    }

    public void setUpSwitchHint() {
        switchHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar =  Snackbar.make(v, getString(R.string.profile_type_hint_full), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        });
    }

    public boolean isDataCleanToSave() {
        boolean toReturn = true;
        firstname.setError(null);
        lastname.setError(null);

        if(TextUtils.isEmpty(firstname.getText().toString())) {
            toReturn = false;
            firstname.setError(getString(R.string.error_field_required));
        }
        if(TextUtils.isEmpty(lastname.getText().toString())) {
            toReturn = false;
            lastname.setError(getString(R.string.error_field_required));
        }
        return toReturn;
    }

    public void saveData() {
        new SaveUserData().execute();
    }

    public void saveUserData() throws ParseException, Exception {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo(Constants.username, this.usernameString);
        ParseUser user = query.getFirst();
        user.put(Constants.firstname, firstname.getText().toString());
        user.put(Constants.lastname, lastname.getText().toString());
        user.put(Constants.profileTagline, tagline.getText().toString());
        user.put(Constants.isMale, spinner.getSelectedItem().toString().equals(getString(R.string.male)));
        user.put(Constants.birthdayDay, Integer.valueOf(bDay.getText().toString()));
        user.put(Constants.birthdayYear, Integer.valueOf(bYear.getText().toString()));
        user.put(Constants.birthdayMonth, getBirthdayMonthInt(bMonth.getText().toString()));
        user.put(Constants.city, city.getText().toString());
        user.put(Constants.country, country.getText().toString());
        user.put(Constants.isPublic, aSwitch.isChecked());
        user.save();
    }

    @Override
    public void onStart() {
        super.onStart();

        bindViews();
        setUpKeyboardBehavior();
        retrieveArguments();
        initSpinner();
        setUpChangeBirthdayButton();
        disableItemsIfNeeded();
        setUpProfilePictureSetUp();
        setUpSwitchHint();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchUserInfo().execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ProfileActivity.REQUEST_IMAGE_CAPTURE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mayRequestWriteToStorage();
                }
                break;
            }
            case ProfileActivity.REQUEST_WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            }
            default: break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ProfileActivity.SELECT_FILE || requestCode == ProfileActivity.REQUEST_IMAGE_CAPTURE) {
                if(!IS_CAMERA_USED) {
                    selectedImageUri = data.getData();
                }
                startProfileCropPictureActivity();

            }
            else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                selectedImageUri = result.getUri();
                onSelectedImageResult();
            }
        }
        else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Exception error = result.getError();
            error.printStackTrace();
            Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
        }
    }

    //----------------------------------------
    // Tasks to do separate from the UI thread
    //----------------------------------------

    private class FetchUserInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(parentScrollView, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchUserInfo();
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(parentScrollView, true, true);
                updateViewsWithUserInfo();
            }
            else {
                Toast.makeText(getContext(), R.string.error_unexpected_trying_again, Toast.LENGTH_SHORT).show();
                new FetchUserInfo().execute();
            }
        }
    }

    private class SavePicture extends AsyncTask<ParseFile, Void, Boolean> {

        @Override
        protected Boolean doInBackground(ParseFile... params) {
            ParseUser user = ParseUser.getCurrentUser();
            user.put(Constants.profilePicture, params[0]);
            try {
                user.save();
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar snackbar =  Snackbar.make(profileImage, getString(R.string.profile_picture_update), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class SaveUserData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(progressBar, true, true);
            setViewVisibility(parentScrollView, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                saveUserData();
                return true;
            }
            catch(ParseException e) {
                e.printStackTrace();
                return false;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(parentScrollView, true, true);
                setViewVisibility(progressBar, false, true);
                Snackbar snackbar =  Snackbar.make(profileImage, getString(R.string.update_details_successfull), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
            }
            else {
                Snackbar snackbar =  Snackbar.make(profileImage, getString(R.string.error_unexpected), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
