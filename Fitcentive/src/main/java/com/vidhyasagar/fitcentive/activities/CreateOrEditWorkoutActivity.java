package com.vidhyasagar.fitcentive.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.create_workout_fragments.CreateWorkoutBasicFragment;
import com.vidhyasagar.fitcentive.create_workout_fragments.CreateWorkoutComponentsFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateOrEditWorkoutActivity extends AppCompatActivity {

    public static final int RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT = 33;
    public static final int RETURN_TO_WORKOUT_FRAGMENT_AFTER_EDIT = 37;

    public static final int RESULT_STRENGTH = 1;
    public static final int RESULT_CARDIO = 2;
    public static final int RESULT_CREATED_STRENGTH = 3;
    public static final int RESULT_CREATED_CARDIO = 4;

    private static final int NO_OF_TABS = 2;

    int RETRY_COUNT = 0;

    boolean isPartOfDiary;
    int dayOffset;
    CreateOrEditMealActivity.MODE mode;

    //if isPartOfDiary
    String exerciseDiaryObjectId;

    // Used for already created workouts, MODE_VIEW AND MODE_EDIT
    String createdWorkoutObjectId;
    CreatedWorkoutInfo workoutInfo;
    ArrayList<Integer> workoutDaysList;
    ArrayList<CardioInfo> cardioComponents;
    ArrayList<ExerciseInfo> strengthComponents;

    FragmentManager manager;
    WorkoutFragmentsAdapter adapter;

    Toolbar toolbar;
    ProgressBar progressBar;
    TabLayout tabLayout;
    ViewPager viewPager;

    // Used for saving data
    String workoutName;
    ArrayList<Integer> workoutDays;
    ArrayList<Object> components;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_or_edit_workout);

        retrieveArguments();
        bindViews();
        setUpToolbar();

        if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
            setUpViewPager();
        }


        if(mode != CreateOrEditMealActivity.MODE.MODE_CREATE) {
            Utilities.hideKeyboard(viewPager, this);
            new LoadCreatedWorkout().execute();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_workout_activity, menu);

        switch(mode) {
            case MODE_CREATE:
                menu.findItem(R.id.action_delete).setVisible(false);
                menu.findItem(R.id.action_edit).setVisible(false);
                break;
            case MODE_VIEW:
                menu.findItem(R.id.action_delete).setVisible(false);
                break;
            case MODE_EDIT:
                menu.findItem(R.id.action_edit).setVisible(false);
                break;
        }

        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(dayOffset, this)));

        if (mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
            dateItem.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                    flipViewMode();
                }
                else {
                    onBackPressed();
                }
                return true;
            case R.id.action_save:
                validateAndSave();
                return true;
            case R.id.action_delete:
                createDeleteConfirmationDialog();
                return true;
            case R.id.action_edit:
                flipViewMode();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            flipViewMode();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT) {
            if(resultCode == CreateOrEditWorkoutActivity.RESULT_STRENGTH || resultCode == CreateOrEditWorkoutActivity.RESULT_CREATED_STRENGTH) {
                ExerciseInfo info = data.getExtras().getParcelable(ExpandedExerciseActivity.INFO);
                CreateWorkoutComponentsFragment fragment = (CreateWorkoutComponentsFragment) adapter.getFragment(1);
                fragment.getChosenStrengthComponent(info, true);
            }
            else if(resultCode == CreateOrEditWorkoutActivity.RESULT_CARDIO ||  resultCode == CreateOrEditWorkoutActivity.RESULT_CREATED_CARDIO) {
                CardioInfo info = data.getExtras().getParcelable(ExpandedExerciseActivity.INFO);
                CreateWorkoutComponentsFragment fragment = (CreateWorkoutComponentsFragment) adapter.getFragment(1);
                fragment.getChosenCardioComponent(info, true);
            }
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), toolbar);
        }
        else {
            if(mMonth == month) {
                dayOffset = day - mDay;
            }
            else {
                dayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, dayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        invalidateOptionsMenu();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void retrieveArguments() {
        dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        mode = (CreateOrEditMealActivity.MODE) getIntent().getExtras().getSerializable(ExpandedRecipeActivity.MODE_TYPE);
        if(mode != CreateOrEditMealActivity.MODE.MODE_CREATE) {
            createdWorkoutObjectId = getIntent().getExtras().getString(Constants.workoutId);
            isPartOfDiary = getIntent().getExtras().getBoolean(CreateOrEditMealActivity.IS_COMING_FROM_DIARY, false);
            if(isPartOfDiary) {
                exerciseDiaryObjectId = getIntent().getExtras().getString(Constants.objectId);
            }
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void setUpViewPager() {
        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new WorkoutFragmentsAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        switch (mode) {
            case MODE_CREATE:
                getSupportActionBar().setTitle(getString(R.string.create_workout));
                break;
            case MODE_EDIT:
                getSupportActionBar().setTitle(getString(R.string.edit_workout));
                break;
            case MODE_VIEW:
                getSupportActionBar().setTitle(getString(R.string.view_workout));
                break;
            default:
                break;
        }
    }

    private void flipViewMode() {
        ((CreateWorkoutBasicFragment) adapter.getFragment(0)).flipViewMode();
        ((CreateWorkoutComponentsFragment) adapter.getFragment(1)).flipViewMode();

        if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            mode = CreateOrEditMealActivity.MODE.MODE_VIEW;
        }
        else {
            mode = CreateOrEditMealActivity.MODE.MODE_EDIT;
        }

        invalidateOptionsMenu();
        setUpToolbar();
    }

    private void validateAndSave() {
        Utilities.hideKeyboard(viewPager, this);
        if(((CreateWorkoutBasicFragment) adapter.getFragment(0)).isValid() && ((CreateWorkoutComponentsFragment) adapter.getFragment(1)).isValid()) {
            ((CreateWorkoutBasicFragment) adapter.getFragment(0)).sendBasicDataToParent();
            ((CreateWorkoutComponentsFragment) adapter.getFragment(1)).sendWorkoutDataToParent();
            new SaveCreatedWorkout().execute();
        }
        else {
            if(!((CreateWorkoutBasicFragment) adapter.getFragment(0)).isValid()) {
                viewPager.setCurrentItem(0);
                ((CreateWorkoutBasicFragment) adapter.getFragment(0)).handleErrors();
            }
            else {
                viewPager.setCurrentItem(1);
                ((CreateWorkoutComponentsFragment) adapter.getFragment(1)).handleErrors();
            }
        }
    }

    private void createDeleteConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(CreateOrEditWorkoutActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.delete_workout))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new DeleteCurrentWorkoutObject().execute();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.delete_workout_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void goToNextFragment() {
        viewPager.setCurrentItem(1);
    }

    public void setWorkoutDays(ArrayList<Integer> list){
        this.workoutDays = list;
    }

    public void setWorkoutName(String name) {
        this.workoutName = name;
    }

    public void setWorkouts(ArrayList<Object> list) {
        this.components = list;
    }

    public void updateName(String name) {
        CreateWorkoutComponentsFragment fragment = (CreateWorkoutComponentsFragment) adapter.getFragment(1);
        fragment.updateName(name);
    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(tabLayout, show, true, this);
        Utilities.setViewVisibility(viewPager, show, true, this);
        Utilities.setViewVisibility(progressBar, !show, true, this);
    }

    private void fetchStrengthComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            ExerciseInfo info = new ExerciseInfo();
            info.setName(object.getString(Constants.exerciseName));
            info.setId(object.getLong(Constants.workoutId));
            info.setObjectId(object.getObjectId());

            info.setCreated(object.getBoolean(Constants.isCreated));

            if(info.isCreated()) {
                info.setCreatedExerciseObjectId(object.getString(Constants.createdExerciseObjectId));
            }

            info.setNumberOfSets(object.getInt(Constants.numberOfSets));
            info.setRepsPerSet(object.getDouble(Constants.repsPerSet));
            info.setWeightPerSet(object.getDouble(Constants.weightPerSet));
            info.setWeightUnit(object.getString(Constants.weightUnit));

            strengthComponents.add(info);
        }
    }

    private void fetchCardioComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            CardioInfo info = new CardioInfo();

            info.setMinutesPerformed(object.getDouble(Constants.minutesPerformed));
            info.setCaloriesBurned(object.getDouble(Constants.caloriesBurned));
            info.setObjectId(object.getObjectId());
            info.setName(object.getString(Constants.exerciseName));

            info.setCreated(object.getBoolean(Constants.isCreated));
            if(info.isCreated()) {
                info.setCreatedCardioObjectId(object.getString(Constants.createdExerciseObjectId));
            }

            cardioComponents.add(info);
        }
    }

    private void fetchWorkoutComponents() throws Exception{
        fetchStrengthComponents();
        fetchCardioComponents();
    }

    private void loadCreatedWorkout() throws Exception {
        workoutInfo = new CreatedWorkoutInfo();
        cardioComponents = new ArrayList<>();
        strengthComponents = new ArrayList<>();

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedWorkout);
        ParseObject workoutObject = query.get(createdWorkoutObjectId);
        workoutInfo.setWorkoutName(workoutObject.getString(Constants.workoutName));
        workoutInfo.setWorkoutObjectId(createdWorkoutObjectId);
        List<Integer> tempList = workoutObject.getList(Constants.workoutDays);
        workoutDaysList = Utilities.toIntegerArrayList(tempList);

        fetchWorkoutComponents();

    }

    private String createNewWorkoutObject() throws Exception {
        ParseObject object = new ParseObject(Constants.CreatedWorkout);
        object.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        object.put(Constants.workoutName, workoutName);
        object.put(Constants.workoutDays, workoutDays);
        object.save();

        return object.getObjectId();
    }

    private void addStrengthComponents(String createdWorkoutObjectIdLocal) throws Exception {
        for(Object comp : components) {
            if(comp instanceof ExerciseInfo) {
                ExerciseInfo info = (ExerciseInfo) comp;
                ParseObject object = new ParseObject(Constants.CreatedWorkoutStrengthComponent);
                object.put(Constants.workoutId, createdWorkoutObjectIdLocal);
                object.put(Constants.isCreated, info.isCreated());
                if(info.isCreated()) {
                    object.put(Constants.createdExerciseObjectId, info.getCreatedExerciseObjectId());
                }
                object.put(Constants.exerciseName, info.getName());
                object.put(Constants.exerciseId, info.getId());
                object.put(Constants.numberOfSets, info.getNumberOfSets());
                object.put(Constants.repsPerSet, info.getRepsPerSet());
                object.put(Constants.weightPerSet, info.getWeightPerSet());
                object.put(Constants.weightUnit, info.getWeightUnit());
                object.put(Constants.username, ParseUser.getCurrentUser().getUsername());
                object.save();
            }
        }

    }

    private void addCardioComponents(String createdWorkoutObjectIdLocal) throws Exception {
        for(Object comp : components) {
            if(comp instanceof CardioInfo) {
                CardioInfo info = (CardioInfo) comp;
                ParseObject object = new ParseObject(Constants.CreatedWorkoutCardioComponent);
                object.put(Constants.workoutId, createdWorkoutObjectIdLocal);
                object.put(Constants.isCreated, info.isCreated());
                if(info.isCreated()) {
                    object.put(Constants.createdExerciseObjectId, info.getCreatedCardioObjectId());
                }
                object.put(Constants.minutesPerformed, info.getMinutesPerformed());
                object.put(Constants.caloriesBurned, info.getCaloriesBurned());
                object.put(Constants.exerciseName, info.getName());
                object.put(Constants.username, ParseUser.getCurrentUser().getUsername());
                object.save();
            }
        }
    }

    private void saveCreatedWorkout() throws Exception {
        String createdWorkoutObjectId = createNewWorkoutObject();
        addStrengthComponents(createdWorkoutObjectId);
        addCardioComponents(createdWorkoutObjectId);
    }

    private void editStrengthComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }

        addStrengthComponents(createdWorkoutObjectId);
    }

    private void editCardioComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }

        addCardioComponents(createdWorkoutObjectId);
    }

    private void editCurrentWorkout() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkout);
        ParseObject object = query.get(createdWorkoutObjectId);

        object.put(Constants.workoutName, workoutName);
        object.put(Constants.workoutDays, workoutDays);

        object.save();

        editStrengthComponents();
        editCardioComponents();

        ((CreateWorkoutBasicFragment) adapter.getFragment(0)).forceFlipCache();

    }

    private void editWorkoutInExerciseDiary() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        ParseObject object = query.get(exerciseDiaryObjectId);

        // The only thing that can be changed in a workout entry is the date of the workout itself and nothing else
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        object.save();
    }

    private void addCurrentWorkoutToExerciseDiary() throws Exception {
        ParseObject object = new ParseObject(Constants.ExerciseDiary);

        object.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        object.put(Constants.isPermanent, false);
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        object.put(Constants.exerciseType, getString(R.string.workout));
        object.put(Constants.workoutName, workoutName);
        object.put(Constants.workoutId, createdWorkoutObjectId);

        object.save();
    }

    private void deleteAssociatedStrengthComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteAssociatedCardioComponents() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteAssociatedExerciseDiaryEntries() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.exerciseType, getString(R.string.workout));
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteCurrentWorkout() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkout);
        ParseObject object = query.get(createdWorkoutObjectId);
        object.delete();

        deleteAssociatedStrengthComponents();
        deleteAssociatedCardioComponents();
        deleteAssociatedExerciseDiaryEntries();
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        startActivity(i);
        finish();
    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class WorkoutFragmentsAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        public WorkoutFragmentsAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                        fragment = CreateWorkoutBasicFragment.newInstance(dayOffset);
                    }
                    else {
                        fragment = CreateWorkoutBasicFragment.newInstance(dayOffset, workoutInfo, workoutDaysList, mode);
                    }
                    break;
                case 1:
                    if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                        fragment = CreateWorkoutComponentsFragment.newInstance(dayOffset);
                    }
                    else {
                        fragment = CreateWorkoutComponentsFragment.newInstance(dayOffset,
                                workoutInfo.getWorkoutName(),  mode, cardioComponents, strengthComponents);
                    }
                    break;
                default:
                    fragment = null;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.basic);
                case 1:
                    return getString(R.string.components);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }
    }

    //-----------------------------------------
    // ASYNCTASK
    //-----------------------------------------

    private class SaveCreatedWorkout extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(mode != CreateOrEditMealActivity.MODE.MODE_EDIT) {
                showMainLayout(false);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                switch (mode) {
                    case MODE_CREATE:
                        saveCreatedWorkout();
                        break;
                    case MODE_EDIT:
                        editCurrentWorkout();
                        break;
                    case MODE_VIEW:
                        if(!isPartOfDiary) {
                            addCurrentWorkoutToExerciseDiary();
                        }
                        else {
                            // Edit workout here
                            editWorkoutInExerciseDiary();
                        }
                        break;
                }
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                    Toast.makeText(CreateOrEditWorkoutActivity.this, getString(R.string.saved_workout_successfully), Toast.LENGTH_SHORT).show();
                    setResult(RESULT_OK);
                    finish();
                }
                else if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                    Utilities.showSnackBar(getString(R.string.edited_workout_successfully), viewPager);
                    flipViewMode();
                }
                else if(mode == CreateOrEditMealActivity.MODE.MODE_VIEW) {
                    returnToDiaryPage();
                }
            }
            else {
                Toast.makeText(CreateOrEditWorkoutActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new SaveCreatedWorkout().execute();
                }
            }
        }
    }

    private class LoadCreatedWorkout extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadCreatedWorkout();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setUpViewPager();
                showMainLayout(true);
                /*
                flipCache = retrievedMealName;
                */
            }
            else {
                Toast.makeText(CreateOrEditWorkoutActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new LoadCreatedWorkout().execute();
                }
            }
        }
    }

    private class DeleteCurrentWorkoutObject extends AsyncTask<Void, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deleteCurrentWorkout();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(CreateOrEditWorkoutActivity.this, getString(R.string.workout_deleted_successfully), Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
            else {
                Toast.makeText(CreateOrEditWorkoutActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
