package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.adapters.MealComponentsAdapter;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.dialog_fragments.CarbsDetailedInfoDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.FatBreakdownDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.VitaminsAndMineralsDialogFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.fragments.ViewCreatedFoodFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.utilities.NoScrollLayoutManager;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.Ingredients;
import com.vidhyasagar.fitcentive.wrappers.ServingInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import github.nisrulz.recyclerviewhelper.RVHItemDividerDecoration;
import github.nisrulz.recyclerviewhelper.RVHItemTouchHelperCallback;

import static com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment.CASE_CARBS;
import static com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment.CASE_FATS;
import static com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment.CASE_PROTEINS;

public class CreateOrEditMealActivity extends AppCompatActivity {

    public static int NO_OF_TABS_NEEDED_TO_ADD_FOOD = 5;
    public static int RESULT_FOOD = -1;
    public static int RESULT_RECIPE = -2;

    public static int RETURN_FROM_FOOD_SELECT = 7;
    public static int RETURN_FROM_EDIT_FOOD = 8;
    public static int RETURN_FROM_EDIT_RECIPE = 9;
    public static int RETURNING_TO_MEAL_FRAGMENT_AFTER_CREATE = 10;
    public static int RETURNING_TO_MEAL_FRAGMENT_AFTER_EDIT = 11;

    public static String IS_PART_OF_MEAL = "IS_PART_OF_MEAL";
    public static String IS_COMING_FROM_DIARY = "IS_COMING_FROM_DIARY";

    public static enum MODE {MODE_VIEW, MODE_CREATE, MODE_EDIT};

    int dayOffset, newDayOffset;
    DiaryPageFragment.ENTRY_TYPE type, newType;

    MODE viewMode;
    // This is there because we dont want to save to our foodDiary if we are already coming from there
    boolean isComingFromDiary;
    String mealObjectId;
    String retrievedMealName;

    int RETRY_COUNT = 0;
    String flipCache;

    double totalFatsCarbsAndProteins;
    double totalMealFats;
    double totalMealCarbs;
    double totalMealProteins;
    double totalMealCalories;

    double totalMealPolyUnsaturatedFat;
    double totalMealMonoUnsaturatedFat;
    double totalMealTransFat;
    double totalMealSaturatedFat;
    double totalMealCholestrol;
    double totalMealSugar;
    double totalMealFiber;
    double totalMealSodium;
    double totalMealPotassium;
    double totalMealCalcium;
    double totalMealIron;
    double totalMealVitamin_a;
    double totalMealVitamin_c;

    Toolbar toolbar;
    ProgressBar progressBar;
    ScrollView scrollView;
    TextView carbs, fats, proteins, calories;
    TextView viewVitamins, totalCaloriesTextView;
    TextInputEditText mealName;
    FloatingActionButton addFoodButton;
    PieChart pieChart;

    RecyclerView mealComponentsRecycler;
    NoScrollLayoutManager llm;
    MealComponentsAdapter adapter;
    ItemTouchHelper.Callback callback;
    ItemTouchHelper helper;
    ArrayList<Object> mealComponentsList;

    FatSecretFoods foods;
    FatSecretRecipes recipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_meal);

        // This method creates a new object if there isnt one already
        retrieveArguments();
        bindViews();
        setUpToolBar();
        setUpAddFoodButton();
        setUpMealComponentsRecycler();
        setUpPieChart();
        setUpClickableTextView();

        if(viewMode == MODE.MODE_VIEW || viewMode == MODE.MODE_EDIT) {
            new LoadMealObject().execute();
        }

        if(viewMode == MODE.MODE_VIEW) {
            // TODO: FIX BUG HERE
            addFoodButton.hide();
            mealName.setEnabled(false);
            adapter.setDisabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        if(viewMode == MODE.MODE_EDIT) {
            flipViewMode();
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_meal_activity, menu);

        switch(viewMode) {
            case MODE_CREATE:
                menu.findItem(R.id.action_delete).setVisible(false);
                menu.findItem(R.id.action_edit).setVisible(false);
                break;
            case MODE_VIEW:
                menu.findItem(R.id.action_delete).setVisible(false);
                break;
            case MODE_EDIT:
                menu.findItem(R.id.action_edit).setVisible(false);
                break;
        }

        MenuItem typeItem = menu.findItem(R.id.action_change_type);
        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        typeItem.setTitle(String.format(getString(R.string.entry_type), Utilities.getEntryTypeString(newType, this)));
        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(newDayOffset, this)));

        if (viewMode == MODE.MODE_CREATE) {
            typeItem.setVisible(false);
            dateItem.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(viewMode == MODE.MODE_EDIT) {
                    flipViewMode();
                }
                else {
                    onBackPressed();
                }
                return true;
            case R.id.action_save:
                if(isValidForSave()) {
                    new SaveCurrentMeal().execute();
                }
                return true;
            case R.id.action_delete:
                createDeleteConfirmationDialog();
                return true;
            case R.id.action_edit:
                flipViewMode();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            case R.id.action_change_type:
                showTypeDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_FOOD && requestCode == RETURN_FROM_FOOD_SELECT) {
            long foodId = data.getExtras().getLong(Constants.foodId);
            double numberOfServings = data.getExtras().getDouble(Constants.foodNumberOfServings);
            long servingId = data.getExtras().getLong(Constants.foodServingIds);
            new FetchFoodInfo(foodId, servingId, numberOfServings).execute();
        }
        else if (resultCode == RESULT_RECIPE && requestCode == RETURN_FROM_FOOD_SELECT) {
            long recipeId = data.getExtras().getLong(Constants.recipeId);
            int selectedServingOption = data.getExtras().getInt(Constants.recipeServingOption);
            double numberOfServings = data.getExtras().getDouble(Constants.recipeNumberOfServings);
            new FetchRecipeInfo(recipeId, selectedServingOption, numberOfServings).execute();
        }
        else if(resultCode == RESULT_OK && requestCode == RETURN_FROM_EDIT_FOOD) {
            double numberOfServings = data.getExtras().getDouble(Constants.numberOfServings);
            long servingId = data.getExtras().getLong(Constants.servingId);
            int dataSetPosition = adapter.getCachePosition();
            long foodId = data.getExtras().getLong(Constants.foodId);
            new FetchFoodInfo(foodId, servingId, numberOfServings, dataSetPosition).execute();
        }
        else if(resultCode == RESULT_OK && requestCode == RETURN_FROM_EDIT_RECIPE) {
            double numberOfServings = data.getExtras().getDouble(Constants.recipeNumberOfServings);
            int recipeServingOption = data.getExtras().getInt(Constants.recipeServingOption);
            int dataSetPosition = adapter.getCachePosition();
            long recipeId = data.getExtras().getLong(Constants.recipeId);
            new FetchRecipeInfo(recipeId, recipeServingOption, numberOfServings, dataSetPosition).execute();
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), toolbar);
        }
        else {
            if(mMonth == month) {
                newDayOffset = day - mDay;
            }
            else {
                dayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, newDayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        invalidateOptionsMenu();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTypeDialog() {
        CharSequence[] array = getResources().getStringArray(R.array.entry_types);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.select_entry_type))
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        invalidateOptionsMenu();
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(array, Utilities.getCheckedItem(type), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                newType = DiaryPageFragment.ENTRY_TYPE.BREAKFAST;
                                break;
                            case 1:
                                newType = DiaryPageFragment.ENTRY_TYPE.LUNCH;
                                break;
                            case 2:
                                newType = DiaryPageFragment.ENTRY_TYPE.DINNER;
                                break;
                            case 3:
                                newType = DiaryPageFragment.ENTRY_TYPE.SNACKS;
                                break;
                        }
                        invalidateOptionsMenu();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setUpInteractiveSwipeBasedOnMode() {
        if(viewMode == MODE.MODE_VIEW) {
            callback = new RVHItemTouchHelperCallback(adapter, false, false, false);
            helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(mealComponentsRecycler);
        }
        else {
            callback = new RVHItemTouchHelperCallback(adapter, true, false, true);
            helper = new ItemTouchHelper(callback);
            helper.attachToRecyclerView(mealComponentsRecycler);
        }
    }

    private void setUpMealComponentsRecycler() {
        mealComponentsList = new ArrayList<>();
        llm = new NoScrollLayoutManager(this);
        llm.setScrollEnabled(false);
        mealComponentsRecycler.setLayoutManager(llm);

        adapter = new MealComponentsAdapter(mealComponentsList, this, mealComponentsRecycler, type, totalCaloriesTextView, dayOffset);
        mealComponentsRecycler.setAdapter(adapter);

        setUpInteractiveSwipeBasedOnMode();

        // Set the divider in the recyclerview
        mealComponentsRecycler.addItemDecoration(new RVHItemDividerDecoration(this, LinearLayoutManager.VERTICAL));

        mealComponentsRecycler.setVisibility(View.GONE);
    }

    private boolean isValidForSave() {
        if(mealName.getText().toString().trim().isEmpty()) {
            Snackbar snackbar =  Snackbar.make(mealComponentsRecycler,
                    getString(R.string.name_your_meal), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
            return false;
        }

        if(mealComponentsList.isEmpty()) {
            Snackbar snackbar =  Snackbar.make(mealComponentsRecycler,
                    getString(R.string.add_atleast_one_food), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
            return false;
        }
        return true;
    }



    //----------------------------------------------------------------------------------------------------------------------------
    // TODO : Refactor to DRY up code
    //----------------------------------------------------------------------------------------------------------------------------

    private void extractData(JSONArray servingArray, long foodId, String foodName, String brandName,
                             double numberOfServings, long servingId, int dataSetPosition) throws Exception {
        int size = servingArray.length();
        for(int i = 0; i < size; i++) {
            JSONObject servingObject = servingArray.getJSONObject(i);
            long tempServingId = servingObject.optLong(Constants.servingId, -1);
            if(servingId == tempServingId) {
                extractData(servingObject, foodId, foodName, brandName, numberOfServings, dataSetPosition);
            }
        }
    }

    private void extractData(JSONObject servingObject, long foodId, String foodName, String brandName,
                             double numberOfServings, int dataSetPosition) throws Exception {

        long servingId = servingObject.optLong(Constants.servingId, -1);
        String servingUrl = servingObject.optString(Constants.servingUrl, Constants.noUrlFound);
        String servingDescription = servingObject.optString(Constants.servingDescription, Constants.unknown);
        double metricServingAmount = servingObject.optDouble(Constants.metricServingAmount, -1);
        String metricServingUnit = servingObject.optString(Constants.metricServingUnit, Constants.unknown);
        String measurementDescription = servingObject.optString(Constants.measurementDescription, Constants.unknown);
        double numberOfUnits = servingObject.optDouble(Constants.numberOfUnits, -1);
        DetailedFoodResultsInfo newInfo = new DetailedFoodResultsInfo(foodId, servingId, servingUrl, servingDescription,
                metricServingAmount, metricServingUnit, measurementDescription, numberOfUnits, this);

        // Setting up the heavy stuff
        newInfo.setBrandName(brandName);
        newInfo.setFoodName(foodName);
        newInfo.setNumberServings(numberOfServings);
        newInfo.setCalories(servingObject.optDouble(Constants.calories, -1));
        newInfo.setCarbs(servingObject.optDouble(Constants.carbs, -1));
        newInfo.setProtein(servingObject.optDouble(Constants.protein, -1));
        newInfo.setFat(servingObject.optDouble(Constants.fat, -1));
        newInfo.setSaturatedFat(servingObject.optDouble(Constants.saturatedFat, -1));
        newInfo.setPolyUnsaturatedFat(servingObject.optDouble(Constants.polyUnsaturatedFat, -1));
        newInfo.setMonoUnsaturatedFat(servingObject.optDouble(Constants.monoUnsaturatedFat, -1));
        newInfo.setTransFat(servingObject.optDouble(Constants.transFat, -1));
        newInfo.setCholestrol(servingObject.optDouble(Constants.cholestrol, -1));
        newInfo.setSodium(servingObject.optDouble(Constants.sodium, -1));
        newInfo.setPotassium(servingObject.optDouble(Constants.potassium, -1));
        newInfo.setFiber(servingObject.optDouble(Constants.fiber, -1));
        newInfo.setSugar(servingObject.optDouble(Constants.sugar, -1));
        newInfo.setVitamin_a(servingObject.optDouble(Constants.vitamin_a, -1));
        newInfo.setVitamin_c(servingObject.optDouble(Constants.vitamin_c, -1));
        newInfo.setCalcium(servingObject.optDouble(Constants.calcium, -1));
        newInfo.setIron(servingObject.optDouble(Constants.iron, -1));
        newInfo.setBonusFact();
        newInfo.setFavorite(false);

        if(dataSetPosition == -1) {
            mealComponentsList.add(newInfo);
        }
        else {
            mealComponentsList.set(dataSetPosition, newInfo);
        }
    }

    private void fetchFoodInfo(long foodId, long servingId, double numberOfServings, int dataSetPosition) throws Exception {
        JSONObject foodItem = foods.getFood(foodId);
        String foodName = foodItem.optString(Constants.foodName, Constants.unknown);
        String brandName = foodItem.optString(Constants.brandName, Constants.unknown);

        JSONObject servingInfo = foodItem.optJSONObject(Constants.servings);
        if(servingInfo != null) {
            JSONObject servingObject = null;
            JSONArray servingArray = null;
            Object tempObject = servingInfo.get(Constants.serving);
            if(tempObject != null) {
                if(tempObject instanceof JSONObject) {
                    servingObject = (JSONObject) tempObject;
                }
                else {
                    servingArray = (JSONArray) tempObject;
                }
            }

            if(servingArray != null) {
                extractData(servingArray, foodId, foodName, brandName, numberOfServings, servingId, dataSetPosition);
            }
            else {
                extractData(servingObject, foodId, foodName, brandName, numberOfServings, dataSetPosition);
            }
        }
    }

    private ArrayList<Ingredients> fetchIngredientsData(JSONArray array) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            Ingredients temp = new Ingredients();
            temp.setFoodId(object.optLong(Constants.foodId, -1));
            temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
            temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
            temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
            temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
            temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
            temp.setServingId(object.optLong(Constants.servingId, -1));
            ingredientsList.add(temp);
        }
        return ingredientsList;
    }

    private ArrayList<Ingredients> fetchIngredientsData(JSONObject object) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        Ingredients temp = new Ingredients();
        temp.setFoodId(object.optLong(Constants.foodId, -1));
        temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
        temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
        temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
        temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
        temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
        temp.setServingId(object.optLong(Constants.servingId, -1));

        ingredientsList.add(temp);
        return ingredientsList;
    }

    private void fetchIngredients(JSONObject ingredients, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(ingredients == null) {
            return;
        }
        ArrayList<Ingredients> ingredientsList;
        Object temp = ingredients.get(Constants.ingredient);
        if(temp instanceof JSONObject) {
            ingredientsList = fetchIngredientsData((JSONObject) temp);
        }
        else {
            ingredientsList = fetchIngredientsData((JSONArray) temp);
        }
        recipeInfo.setIngredientsList(ingredientsList);
    }

    private ArrayList<String> fetchDirectionsData(JSONArray array) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
            directionsList.add(directionDescription);
        }
        return directionsList;
    }

    private ArrayList<String> fetchDirectionsData(JSONObject object) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
        directionsList.add(directionDescription);
        return directionsList;
    }

    private void fetchDirections(JSONObject direction, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(direction == null) {
            return;
        }
        ArrayList<String> directionsList;
        Object temp = direction.get(Constants.direction);
        if(temp instanceof JSONObject) {
            // There is only one direction here
            directionsList = fetchDirectionsData((JSONObject) temp);
        }
        else {
            // There are multiple directions here, extract it from the JSONArray
            directionsList = fetchDirectionsData((JSONArray) temp);
        }
        recipeInfo.setDirections(directionsList);
    }

    private ArrayList<ServingInfo> extractServingData(JSONArray array) throws JSONException {
        ArrayList<ServingInfo> list = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject result = array.getJSONObject(i);
            ServingInfo newServingInfo = new ServingInfo();
            newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
            newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
            newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
            newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
            newServingInfo.setFat(result.optDouble(Constants.fat, -1));
            newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
            newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
            newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
            newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
            newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
            newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
            newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
            newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
            newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
            newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
            newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
            newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
            newServingInfo.setIron(result.optDouble(Constants.iron, -1));
            list.add(newServingInfo);
        }
        return list;
    }

    private ArrayList<ServingInfo> extractServingData(JSONObject result) {
        ArrayList<ServingInfo> list = new ArrayList<>();
        ServingInfo newServingInfo = new ServingInfo();
        newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
        newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
        newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
        newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
        newServingInfo.setFat(result.optDouble(Constants.fat, -1));
        newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
        newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
        newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
        newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
        newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
        newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
        newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
        newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
        newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
        newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
        newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
        newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
        newServingInfo.setIron(result.optDouble(Constants.iron, -1));
        list.add(newServingInfo);
        return list;
    }

    private void fetchServingSizeInfo(JSONObject servings, DetailedRecipeResultsInfo recipeInfo) throws JSONException{
        if(servings == null) {
            return;
        }

        ArrayList<ServingInfo> servingsList;
        Object temp = servings.get(Constants.serving);
        if(temp instanceof JSONObject) {
            servingsList = extractServingData((JSONObject) temp);
        }
        else {
            servingsList = extractServingData((JSONArray) temp);
        }

        recipeInfo.setServingsList(servingsList);
    }

    private void fetchRecipeInfo(long recipeId, int servingOption, double numberOfServings, int dataSetPosition) throws Exception {
        JSONObject result = recipes.getRecipe(recipeId);
        DetailedRecipeResultsInfo recipeInfo = new DetailedRecipeResultsInfo();
        if(result != null) {
            recipeInfo.setRecipeId(result.optLong(Constants.recipeId, -1));
            recipeInfo.setRecipeName(result.optString(Constants.recipeName, Constants.unknown));
            recipeInfo.setRecipeUrl(result.optString(Constants.recipeUrl, Constants.unknown));
            recipeInfo.setRecipeDescription(result.optString(Constants.recipeDescription, Constants.unknown));
            recipeInfo.setNumberOfServings(result.optDouble(Constants.numberOfServings, -1));
            recipeInfo.setPrepTime(result.optInt(Constants.prepTime, -1));
            recipeInfo.setCookingTime(result.optInt(Constants.cookingTime, -1));
            recipeInfo.setRating(result.optInt(Constants.rating, -1));
            recipeInfo.setRecipeType(result.optString(Constants.recipeType, Constants.unknown));
            recipeInfo.setRecipeImageUrl(result.optString(Constants.recipeImage, Constants.unknown));
            recipeInfo.setChosenNumberOfServings(numberOfServings);
            recipeInfo.setFavorite(false);
            recipeInfo.setSelectedServingOption(servingOption);
            fetchServingSizeInfo(result.optJSONObject(Constants.servingSizes), recipeInfo);
            // Directions and ingredients array lists. The method sets it on the object directly as recipeInfo is GLOBAL
            // NOT taking into account category type and similar stuff because tbh it's useless
            fetchDirections(result.optJSONObject(Constants.directions), recipeInfo);
            fetchIngredients(result.optJSONObject(Constants.ingredients), recipeInfo);

            recipeInfo.setBonusFact();

            if(dataSetPosition == -1) {
                mealComponentsList.add(recipeInfo);
            }
            else {
                mealComponentsList.set(dataSetPosition, recipeInfo);
            }
        }
    }


    //----------------------------------------------------------------------------------------------------------------------------
    // TODO : Refactor to DRY up code
    //----------------------------------------------------------------------------------------------------------------------------

    private void loadMealObject() throws Exception{
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedMeal);
        ParseObject result = query.get(this.mealObjectId);

        this.retrievedMealName = result.getString(Constants.mealName);

        List<Integer> foodIds = result.getList(Constants.foodIds);
        List<Integer> foodServingIds = result.getList(Constants.foodServingIds);
        List<Number> foodNumberOfServings = result.getList(Constants.foodNumberOfServings);
        List<Integer> recipeIds = result.getList(Constants.recipeIds);
        List<Integer> recipeServingOptions = result.getList(Constants.recipeServingOptions);
        List<Number> recipeNumberOfServings = result.getList(Constants.recipeNumberOfServings);

        // TODO : Assert over here that the sizes of all arrays match

        int size = foodIds.size();

        for(int i = 0; i < size; i++) {
            long foodId = foodIds.get(i);
            long foodServingId = foodServingIds.get(i);
            try {
                double foodNumberOfServing = (Double) foodNumberOfServings.get(i);
                fetchFoodInfo(foodId, foodServingId, foodNumberOfServing, -1);
            }
            catch(ClassCastException e) {
                int foodNumberOfServing = (Integer) foodNumberOfServings.get(i);
                fetchFoodInfo(foodId, foodServingId, foodNumberOfServing, -1);
            }
        }


        int recipeSize = recipeIds.size();

        for(int i = 0; i < recipeSize; i++) {
            long recipeId = recipeIds.get(i);
            int recipeServingOption = recipeServingOptions.get(i);
            try {
                double recipeNumberOfServing = (Double) recipeNumberOfServings.get(i);
                fetchRecipeInfo(recipeId, recipeServingOption, recipeNumberOfServing, -1);
            }
            catch (ClassCastException e) {
                int recipeNumberOfServing = (Integer) recipeNumberOfServings.get(i);
                fetchRecipeInfo(recipeId, recipeServingOption, recipeNumberOfServing, -1);
            }
        }
    }

    private void deleteCurrentMealObject() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedMeal);
        query.get(this.mealObjectId).delete();

        ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query1.whereEqualTo(Constants.entryType, getString(R.string.meal));
        query1.whereEqualTo(Constants.mealObjectId, mealObjectId);

        List<ParseObject> result = query1.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void createDeleteConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(CreateOrEditMealActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.delete_meal))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new DeleteCurrentMealObject().execute();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.delete_meal_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setUpAddFoodButton() {
        addFoodButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(viewMode == MODE.MODE_VIEW) {
                    Snackbar snackbar = Snackbar.make(v,
                            getString(R.string.cannot_edit_meal), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                }
                else {
                    startSelectFoodActivity();
                }
            }
        });
    }

    private void startSelectFoodActivity() {
        Intent i = new Intent(CreateOrEditMealActivity.this, SearchFoodActivity.class);
        i.putExtra(Constants.type, type);
        i.putExtra(Constants.returnDate, dayOffset);
        i.putExtra(SearchFoodActivity.NO_OF_TABS_STRING, NO_OF_TABS_NEEDED_TO_ADD_FOOD);
        startActivityForResult(i, RETURN_FROM_FOOD_SELECT);
    }


    private void retrieveArguments() {
        this.dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        this.type = (DiaryPageFragment.ENTRY_TYPE)
                getIntent().getExtras().getSerializable(SearchFoodResultsFragment.ENTRY_TYPE_STRING);
        this.viewMode = (MODE) getIntent().getExtras().getSerializable(ExpandedRecipeActivity.MODE_TYPE);
        this.isComingFromDiary = getIntent().getExtras().getBoolean(CreateOrEditMealActivity.IS_COMING_FROM_DIARY, false);
        if(viewMode != MODE.MODE_CREATE) {
            this.mealObjectId = getIntent().getExtras().getString(Constants.objectId, Constants.unknown);
        }

        newType = type;
        newDayOffset = dayOffset;
        invalidateOptionsMenu();
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mealName = (TextInputEditText) findViewById(R.id.meal_name_edittext);
        carbs = (TextView) findViewById(R.id.carbs_textview);
        proteins = (TextView) findViewById(R.id.proteins_textview);
        fats = (TextView) findViewById(R.id.fats_textview);
        calories = (TextView) findViewById(R.id.calories_textview);
        mealComponentsRecycler = (RecyclerView) findViewById(R.id.meal_components_recyler);
        addFoodButton = (FloatingActionButton) findViewById(R.id.add_food_button);
        viewVitamins = (TextView)  findViewById(R.id.view_vitamins_textview);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
        totalCaloriesTextView = (TextView) findViewById(R.id.meal_components_total_calories);
        pieChart = (PieChart) findViewById(R.id.pie_chart);
        foods = new FatSecretFoods();
        recipes = new FatSecretRecipes();
    }

    private void setUpToolBar() {
        switch(viewMode) {
            case MODE_CREATE:
                toolbar.setTitle(getString(R.string.create_meal));
                break;
            case MODE_EDIT:
                toolbar.setTitle(getString(R.string.edit_meal));
                break;
            case MODE_VIEW:
                toolbar.setTitle(getString(R.string.view_meal));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void flipViewMode() {

        if(viewMode == MODE.MODE_EDIT) {
            viewMode = MODE.MODE_VIEW;
            mealName.setEnabled(false);
            mealName.setText(flipCache);
            adapter.setDisabled(true);
            addFoodButton.hide();
        }
        else {
            viewMode = MODE.MODE_EDIT;
            mealName.setEnabled(true);
            mealName.setSelection(mealName.getText().length());
            flipCache = mealName.getText().toString().trim();
            adapter.setDisabled(false);
            addFoodButton.show();
        }

        setUpInteractiveSwipeBasedOnMode();
        invalidateOptionsMenu();
        setUpToolBar();
    }

    private void editCurrentMeal() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedMeal);
        ParseObject currentMealObject = query.get(this.mealObjectId);

        List<Long> foodIds = new ArrayList<>();
        List<Long> foodServingIds = new ArrayList<>();
        List<Double> foodNumberOfServings = new ArrayList<>();
        List<Long> recipeIds = new ArrayList<>();
        List<Double> recipeNumberOfServings = new ArrayList<>();
        List<Integer> recipeServingOptions = new ArrayList<>();

        for(Object object : mealComponentsList) {
            if(object instanceof DetailedFoodResultsInfo) {
                DetailedFoodResultsInfo info = (DetailedFoodResultsInfo) object;
                foodIds.add(info.getFoodId());
                foodServingIds.add(info.getServingId());
                foodNumberOfServings.add(info.getNumberOfServings());
            }
            else {
                DetailedRecipeResultsInfo info = (DetailedRecipeResultsInfo) object;
                recipeIds.add(info.getRecipeId());
                recipeServingOptions.add(info.getSelectedServingOption());
                recipeNumberOfServings.add(info.getChosenNumberOfServings());
            }
        }

        currentMealObject.put(Constants.foodIds, foodIds);
        currentMealObject.put(Constants.foodServingIds, foodServingIds);
        currentMealObject.put(Constants.foodNumberOfServings, foodNumberOfServings);

        currentMealObject.put(Constants.recipeIds, recipeIds);
        currentMealObject.put(Constants.recipeServingOptions, recipeServingOptions);
        currentMealObject.put(Constants.recipeNumberOfServings, recipeNumberOfServings);

        currentMealObject.put(Constants.mealName, mealName.getText().toString().trim());

        currentMealObject.save();

        flipCache = mealName.getText().toString().trim();

    }

    private void saveCurrentMeal() throws Exception {
        ParseObject newMeal = new ParseObject(Constants.CreatedMeal);
        newMeal.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newMeal.put(Constants.mealName, mealName.getText().toString().trim());

        // Now that we have the formalities out of the way, we proceed to input the actual stuff
        List<Long> foodIds = new ArrayList<>();
        List<Long> foodServingIds = new ArrayList<>();
        List<Double> foodNumberOfServings = new ArrayList<>();
        List<Long> recipeIds = new ArrayList<>();
        List<Double> recipeNumberOfServings = new ArrayList<>();
        List<Integer> recipeServingOptions = new ArrayList<>();

        for(Object object : mealComponentsList) {
            if(object instanceof DetailedFoodResultsInfo) {
                DetailedFoodResultsInfo info = (DetailedFoodResultsInfo) object;
                foodIds.add(info.getFoodId());
                foodServingIds.add(info.getServingId());
                foodNumberOfServings.add(info.getNumberOfServings());
            }
            else {
                DetailedRecipeResultsInfo info = (DetailedRecipeResultsInfo) object;
                recipeIds.add(info.getRecipeId());
                recipeServingOptions.add(info.getSelectedServingOption());
                recipeNumberOfServings.add(info.getChosenNumberOfServings());
            }
        }

        newMeal.put(Constants.foodIds, foodIds);
        newMeal.put(Constants.foodServingIds, foodServingIds);
        newMeal.put(Constants.foodNumberOfServings, foodNumberOfServings);

        newMeal.put(Constants.recipeIds, recipeIds);
        newMeal.put(Constants.recipeServingOptions, recipeServingOptions);
        newMeal.put(Constants.recipeNumberOfServings, recipeNumberOfServings);

        newMeal.save();

    }

    private void setUpPieChart() {
        pieChart.setBackgroundColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription(getString(R.string.nutritional_breakdown));
        pieChart.setDescriptionColor(ContextCompat.getColor(this, R.color.transp_black));
        pieChart.setDescriptionTextSize(11f);
        pieChart.setHoleRadius(5f);
        pieChart.setTransparentCircleRadius(1f);
        pieChart.setDrawSliceText(false);
        pieChart.setCenterTextSize(13f);

        Legend l = pieChart.getLegend();
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_blue_light, android.R.color.holo_red_light, android.R.color.holo_green_light};

        ArrayList<Entry> breakdown = new ArrayList<Entry>();

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, this);

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));

        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.animateXY(750, 750);

        pieChart.invalidate(); // refresh

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                // display msg when value selected
                if (e == null)
                    return;
                else {
                    showDetailedBreakDown(e.getXIndex());
                }
            }

            @Override
            public void onNothingSelected() {
                Log.i("applog", "Nothing is selected");
            }
        });

        if(mealComponentsList.isEmpty()) {
            pieChart.setVisibility(View.GONE);
        }
    }

    private void calculateTotal() {
        totalMealCalories = 0;
        totalMealCarbs = 0;
        totalMealFats = 0;
        totalMealProteins = 0;

        totalMealPolyUnsaturatedFat = 0;
        totalMealMonoUnsaturatedFat = 0;
        totalMealTransFat = 0;
        totalMealSaturatedFat = 0;
        totalMealCholestrol = 0;
        totalMealSugar = 0;
        totalMealFiber = 0;
        totalMealSodium = 0;
        totalMealPotassium = 0;
        totalMealCalcium = 0;
        totalMealIron = 0;
        totalMealVitamin_a = 0;
        totalMealVitamin_c = 0;

        for(Object object : mealComponentsList) {
            if(object instanceof DetailedFoodResultsInfo) {
                DetailedFoodResultsInfo info = (DetailedFoodResultsInfo) object;
                totalMealCalories += info.getCalories() * info.getNumberOfServings();
                totalMealFats += info.getFat() * info.getNumberOfServings();
                totalMealProteins += info.getProtein() * info.getNumberOfServings();
                totalMealCarbs += info.getCarbs() * info.getNumberOfServings();

                totalMealPolyUnsaturatedFat+= info.getPolyUnsaturatedFat() != -1 ? info.getPolyUnsaturatedFat() * info.getNumberOfServings() : 0;
                totalMealMonoUnsaturatedFat+= info.getMonoUnsaturatedFat() != -1 ? info.getMonoUnsaturatedFat() * info.getNumberOfServings() : 0;
                totalMealTransFat+= info.getTransFat() != -1 ? info.getTransFat() * info.getNumberOfServings() : 0;
                totalMealSaturatedFat+= info.getSaturatedFat() != -1 ? info.getSaturatedFat() * info.getNumberOfServings() : 0;

                totalMealCholestrol+= info.getCholestrol() != -1 ? info.getCholestrol()  * info.getNumberOfServings() : 0;
                totalMealSugar+= info.getSugar() != -1 ? info.getSugar() * info.getNumberOfServings() : 0;
                totalMealFiber+= info.getFiber() != -1 ? info.getFiber() * info.getNumberOfServings() : 0;
                totalMealSodium+= info.getSodium() != -1 ? info.getSodium() * info.getNumberOfServings() : 0;
                totalMealPotassium+= info.getPotassium() != -1 ? info.getPotassium() * info.getNumberOfServings() : 0;
                totalMealCalcium+= info.getCalcium() != -1 ? info.getCalcium() * info.getNumberOfServings() : 0;
                totalMealIron+= info.getIron() != -1 ? info.getIron() * info.getNumberOfServings() : 0;
                totalMealVitamin_a+= info.getVitamin_a() != -1 ? info.getVitamin_a() * info.getNumberOfServings() : 0;
                totalMealVitamin_c+= info.getVitamin_c() != -1 ? info.getVitamin_c() * info.getNumberOfServings() : 0;

            }
            else {
                DetailedRecipeResultsInfo info = (DetailedRecipeResultsInfo) object;
                ServingInfo servingInfo = info.getServingsList().get(info.getSelectedServingOption());
                totalMealCalories += servingInfo.getCalories() * info.getChosenNumberOfServings();
                totalMealFats += servingInfo.getFat() * info.getChosenNumberOfServings();
                totalMealProteins += servingInfo.getProtein() * info.getChosenNumberOfServings();
                totalMealCarbs += servingInfo.getCarbs() * info.getChosenNumberOfServings();

                totalMealPolyUnsaturatedFat+= servingInfo.getPolyUnsaturatedFat() != -1 ? servingInfo.getPolyUnsaturatedFat() * info.getChosenNumberOfServings() : 0;
                totalMealMonoUnsaturatedFat+= servingInfo.getMonoUnsaturatedFat() != -1 ? servingInfo.getMonoUnsaturatedFat() * info.getChosenNumberOfServings() : 0;
                totalMealTransFat+= servingInfo.getTransFat() != -1 ? servingInfo.getTransFat() * info.getChosenNumberOfServings() : 0;
                totalMealSaturatedFat+= servingInfo.getSaturatedFat() != -1 ? servingInfo.getSaturatedFat() * info.getChosenNumberOfServings() : 0;

                totalMealCholestrol+= servingInfo.getCholestrol() != -1 ? servingInfo.getCholestrol() * info.getChosenNumberOfServings() : 0;
                totalMealSugar+= servingInfo.getSugar() != -1 ? servingInfo.getSugar() * info.getChosenNumberOfServings() : 0;
                totalMealFiber+= servingInfo.getFiber() != -1 ? servingInfo.getFiber() * info.getChosenNumberOfServings() : 0;
                totalMealSodium+= servingInfo.getSodium() != -1 ? servingInfo.getSodium() * info.getChosenNumberOfServings() : 0;
                totalMealPotassium+= servingInfo.getPotassium() != -1 ? servingInfo.getPotassium() * info.getChosenNumberOfServings() : 0;
                totalMealCalcium+= servingInfo.getCalcium() != -1 ? servingInfo.getCalcium() * info.getChosenNumberOfServings() : 0;
                totalMealIron+= servingInfo.getIron() != -1 ? servingInfo.getIron() * info.getChosenNumberOfServings() : 0;
                totalMealVitamin_a+= servingInfo.getVitamin_a() != -1 ? servingInfo.getVitamin_a() * info.getChosenNumberOfServings() : 0;
                totalMealVitamin_c+= servingInfo.getVitamin_c() != -1 ? servingInfo.getVitamin_c() * info.getChosenNumberOfServings() : 0;
            }
        }

        totalFatsCarbsAndProteins = totalMealProteins + totalMealCarbs + totalMealFats;
    }

    public void updatePieChartData() {
        calculateTotal();

        setViewVisibility(pieChart, true, true);

        final int[] colors = {android.R.color.holo_blue_light, android.R.color.holo_red_light, android.R.color.holo_green_light};

        ArrayList<Entry> breakdown = new ArrayList<Entry>();

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, this);

        Entry carbs = new Entry( (float) (totalMealCarbs / totalFatsCarbsAndProteins) * 100, CASE_CARBS); // 0 == Carbs
        breakdown.add(carbs);
        Entry fats = new Entry( (float) (totalMealFats / totalFatsCarbsAndProteins) * 100, CASE_FATS); // 0 == Carbs
        breakdown.add(fats);
        Entry proteins = new Entry( (float) (totalMealProteins / totalFatsCarbsAndProteins) * 100, CASE_PROTEINS); // 0 == Carbs
        breakdown.add(proteins);

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));

        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.animateXY(750, 750);

        pieChart.invalidate(); // refresh

        updateHeaderMacroCount();
    }

    private void updateHeaderMacroCount() {
        carbs.setText(String.format(getString(R.string.two_decimal_places), totalMealCarbs));
        proteins.setText(String.format(getString(R.string.two_decimal_places), totalMealProteins));
        fats.setText(String.format(getString(R.string.two_decimal_places), totalMealFats));
        calories.setText(String.format(getString(R.string.two_decimal_places), totalMealCalories));
    }

    private void showDetailedBreakDown(int type) {
        switch (type) {
            case CASE_CARBS: // Carbs
                showDetailedCarbsBreakdown();
                break;
            case CASE_FATS: // Fats
                showDetailedFatsBreakdown();
                break;
            case CASE_PROTEINS: // Protein
                showDetailedProteinBreakdown();
                break;
            default: return;
        }
    }

    private void showDetailedCarbsBreakdown() {
        FragmentManager fm = getSupportFragmentManager();
        // Here we use 1 because the number of servings is already accounted for in totalMealxxxxx
        CarbsDetailedInfoDialogFragment carbsFragment =
                CarbsDetailedInfoDialogFragment.newInstance(totalMealCholestrol, totalMealSugar, totalMealFiber, 1);

        if(carbsFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            carbsFragment.show(fm, CarbsDetailedInfoDialogFragment.TAG);
        }
    }

    private void showDetailedFatsBreakdown() {
        FragmentManager fm = getSupportFragmentManager();
        FatBreakdownDialogFragment fatFragment =
                FatBreakdownDialogFragment.newInstance(totalMealPolyUnsaturatedFat, totalMealMonoUnsaturatedFat,
                        totalMealTransFat, totalMealSaturatedFat, 1);
        if(fatFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            fatFragment.show(fm, FatBreakdownDialogFragment.TAG);
        }
    }

    private void showDetailedProteinBreakdown() {
        Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.protein_data_text), Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void showDetailedVitaminsAndMineralsBreakdown() {
        FragmentManager fm = getSupportFragmentManager();
        VitaminsAndMineralsDialogFragment vitFragment = VitaminsAndMineralsDialogFragment.newInstance(
                totalMealSodium, totalMealPotassium, totalMealCalcium, totalMealIron, totalMealVitamin_a, totalMealVitamin_c, 1);

        if(vitFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            vitFragment.show(fm, VitaminsAndMineralsDialogFragment.TAG);
        }
    }

    private void setUpClickableTextView() {
        viewVitamins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedVitaminsAndMineralsBreakdown();
            }
        });
    }

    private void showMainLayout(boolean showMainLayout) {
        setViewVisibility(scrollView, showMainLayout, true);
        setViewVisibility(addFoodButton, showMainLayout, true);
        setViewVisibility(progressBar, !showMainLayout, true);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }
    }

    private String getMealTypeString(DiaryPageFragment.ENTRY_TYPE type) {
        switch (type) {
            case BREAKFAST: return getString(R.string.breakfast);
            case LUNCH: return getString(R.string.lunch);
            case DINNER: return getString(R.string.dinner);
            case SNACKS: return getString(R.string.snacks);
            case WATER: return getString(R.string.water);
            case EXCERCISE: return getString(R.string.excercise);
            default: return null;
        }
    }

    private void editDiaryEntryForMeal() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.mealType, getMealTypeString(type));
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        query.whereEqualTo(Constants.mealObjectId, this.mealObjectId);
        query.whereEqualTo(Constants.entryType, getString(R.string.meal));

        ParseObject object = query.getFirst();
        object.put(Constants.mealType, getMealTypeString(newType));
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));

        object.save();
    }

    private void addToFoodDiary() throws Exception {
        ParseObject newMeal = new ParseObject(Constants.FoodDiary);
        newMeal.put(Constants.entryType, getString(R.string.meal));
        newMeal.put(Constants.mealObjectId, this.mealObjectId);
        newMeal.put(Constants.isPermanent, false);
        newMeal.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        newMeal.put(Constants.mealType, getMealTypeString(newType));
        newMeal.put(Constants.username, ParseUser.getCurrentUser().getUsername());

        // numberServings, isFavorite and recipeServingOption are NOT used by this
        newMeal.save();
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(CreateOrEditMealActivity.this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.newDayOffset);
        startActivity(i);
        finish();
    }

    //------------------------
    // ASYNCTASK(S)
    //------------------------

    private class LoadMealObject extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadMealObject();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                mealName.setText(retrievedMealName);
                mealName.setSelection(mealName.getText().length());
                adapter.notifyDataSetChanged();
                adapter.updateTextViewHeader();
                mealComponentsRecycler.setVisibility(View.VISIBLE);
                flipCache = retrievedMealName;
                updatePieChartData();
            }
            else {
                Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new LoadMealObject().execute();
                }
            }
        }
    }

    private class FetchFoodInfo extends AsyncTask<Void, Void, Boolean> {

        long foodId, servingId;
        double numberOfServings;
        int dataSetPosition;

        public FetchFoodInfo(long foodId, long servingId, double numberOfServings) {
            this.foodId = foodId;
            this.servingId = servingId;
            this.numberOfServings = numberOfServings;
            this.dataSetPosition = -1;
        }

        public FetchFoodInfo(long foodId, long servingId, double numberOfServings, int dataSetPosition) {
            this.foodId = foodId;
            this.servingId = servingId;
            this.numberOfServings = numberOfServings;
            this.dataSetPosition = dataSetPosition;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchFoodInfo(foodId, servingId, numberOfServings, dataSetPosition);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                adapter.notifyDataSetChanged();
                adapter.updateTextViewHeader();
                mealComponentsRecycler.setVisibility(View.VISIBLE);
                updatePieChartData();
            }
            else {
                Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class FetchRecipeInfo extends AsyncTask<Void, Void, Boolean> {

        long recipeId;
        int selectedServingOption;
        double numberOfServings;
        int dataSetPosition;

        public FetchRecipeInfo(long recipeId, int selectedServingOption, double numberOfServings) {
            this.recipeId = recipeId;
            this.selectedServingOption = selectedServingOption;
            this.numberOfServings = numberOfServings;
            this.dataSetPosition = -1;
        }

        public FetchRecipeInfo(long recipeId, int selectedServingOption, double numberOfServings, int dataSetPosition) {
            this.recipeId = recipeId;
            this.selectedServingOption = selectedServingOption;
            this.numberOfServings = numberOfServings;
            this.dataSetPosition = dataSetPosition;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchRecipeInfo(recipeId, selectedServingOption, numberOfServings, dataSetPosition);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                adapter.notifyDataSetChanged();
                adapter.updateTextViewHeader();
                mealComponentsRecycler.setVisibility(View.VISIBLE);
                updatePieChartData();
            }
            else {
                Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SaveCurrentMeal extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(viewMode != MODE.MODE_EDIT) {
                showMainLayout(false);
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(viewMode == MODE.MODE_EDIT) {
                    editCurrentMeal();
                }
                else if(viewMode == MODE.MODE_CREATE) {
                    saveCurrentMeal();
                }
                else if(viewMode == MODE.MODE_VIEW && !isComingFromDiary) {
                    addToFoodDiary();
                }
                else if(viewMode == MODE.MODE_VIEW && isComingFromDiary) {
                    editDiaryEntryForMeal();
                }
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                if(viewMode == MODE.MODE_EDIT) {
                    Snackbar snackbar = Snackbar.make(mealComponentsRecycler,
                            getString(R.string.edited_meal_successfully), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                    flipViewMode();
                }
                else if(viewMode == MODE.MODE_CREATE){
                    Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.saved_meal_successfully), Toast.LENGTH_SHORT).show();
                    setResult(RESULT_OK);
                    finish();
                }
                else if(viewMode == MODE.MODE_VIEW) {
                    returnToDiaryPage();
                }

            }
            else {
                Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeleteCurrentMealObject extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deleteCurrentMealObject();
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                Snackbar snackbar = Snackbar.make(mealComponentsRecycler,
                        getString(R.string.meal_deleted_successfully), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                setResult(RESULT_OK);
                finish();
            }
            else {
                Toast.makeText(CreateOrEditMealActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
