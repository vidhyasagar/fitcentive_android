package com.vidhyasagar.fitcentive.activities;


import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.edit_profile_fragments.GoalsFragment;
import com.vidhyasagar.fitcentive.edit_profile_fragments.PersonalInfoFragment;

public class EditProfileActivity extends AppCompatActivity {

    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    public static int NO_OF_TABS = 2 ;

    String username;
    boolean isEditable;
    Bundle fragmentArgs;

    FragmentManager manager;
    PagerAdapter adapter;

    public void getExtrasFromIntent() {
        // This cannot be null
        username = getIntent().getExtras().getString("username");
        isEditable = getIntent().getExtras().getBoolean("isEditable");
        invalidateOptionsMenu();
    }

    public void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void setUpViewPager() {

        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new PagerAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }

    public void bindViews() {
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
    }

    public void saveUserInfo() {
        Fragment fragment1 = manager.findFragmentByTag(getString(R.string.android_hack) + R.id.pager + ":" + 0);
        Fragment fragment2 = manager.findFragmentByTag(getString(R.string.android_hack) + R.id.pager + ":" + 1);
        if(fragment1 != null) {
            if(((PersonalInfoFragment) fragment1).isDataCleanToSave()) {
                ((PersonalInfoFragment) fragment1).saveData();
            }
            else {
                viewPager.arrowScroll(View.FOCUS_LEFT);
            }
        }
        if(fragment2 != null) {
            // Similar stuff
            Log.i("savelog", "frag2 not null");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        bindViews();
        setUpToolBar();
        setUpViewPager();
        getExtrasFromIntent();

        fragmentArgs = new Bundle();
        fragmentArgs.putString("username", this.username);
        fragmentArgs.putBoolean("isEditable", this.isEditable);


        if(!ParseUser.getCurrentUser().getUsername().equals(username)) {
            getSupportActionBar().setTitle(getString(R.string.view_profile));
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_profile, menu);

        if(!isEditable) {
            menu.getItem(0).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                saveUserInfo();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }


    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    fragment = new PersonalInfoFragment();
                    break;
                case 1:
                    fragment =  new GoalsFragment();
                    break;
                default:
                    fragment = null;
                    break;
            }
            // This should never fail
            fragment.setArguments(fragmentArgs);
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.personal_info);
                case 1:
                    return getString(R.string.goals);
            }
            // This should never be reached
            return null;
        }
    }

}
