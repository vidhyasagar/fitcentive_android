package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.adapters.CommentsAdapter;
import com.vidhyasagar.fitcentive.adapters.NewsFeedAdapter;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.NoScrollLayoutManager;
import com.vidhyasagar.fitcentive.wrappers.CommentInfo;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// TODO : Handle UI gracefully when object does not exist
public class SinglePostActivity extends AppCompatActivity {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    EditText commentEditText;
    CardView cardView;
    TextView postUser, postTime, postCaption, postComments, postLikers;
    ImageView postImage;
    ImageButton likeButton, commentButton;
    ProgressBar progressBar;
    Button postButton;
    ScrollView scrollView;

    String postId;
    NewsFeedInfo newPost = null;
    int RETRY_COUNT = 0;

    ArrayList<CommentInfo> comments;
    NoScrollLayoutManager llm;
    CommentsAdapter rvAdapter;


    private void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void bindViews() {
//        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        recyclerView = (RecyclerView) findViewById(R.id.comments_recycler);
        commentEditText = (EditText) findViewById(R.id.comment_edittext);
        cardView = (CardView) findViewById(R.id.card_view);
        postUser = (TextView) findViewById(R.id.post_user);
        postTime = (TextView) findViewById(R.id.post_time);
        postCaption = (TextView) findViewById(R.id.post_caption);
        postImage = (ImageView) findViewById(R.id.post_photo);
        likeButton = (ImageButton) findViewById(R.id.like_button);
        commentButton = (ImageButton) findViewById(R.id.comment_button);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        postComments = (TextView) findViewById(R.id.numberCommentsTextView);
        postLikers = (TextView) findViewById(R.id.post_likers);
        postButton = (Button) findViewById(R.id.post_button);
        scrollView = (ScrollView) findViewById(R.id.scrollview);
    }

    private void fetchObjectInfo() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Post);
        // TODO: Handle no results gracefully
        query.whereEqualTo(Constants.objectId, postId);
        List<ParseObject> result = query.find();
        ParseObject post;
        if(result.isEmpty()) {
            return;
        }
        else {
            post = result.get(0);
        }
        String username = post.getString(Constants.username);
        String caption = post.getString(Constants.caption);
        int numberComments = post.getInt(Constants.numberComments);
        int numberLikes = post.getInt(Constants.numberLikes);
        Date date = post.getCreatedAt();
        ParseFile image = post.getParseFile(Constants.image);
        List<String> likersList = post.getList(Constants.likers);

        if(image != null) {
            byte[] byteArray = image.getData();
            Bitmap postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, postBitmap, postId);
        }
        else {
            newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, null, postId);
        }
        if(likersList == null) {
            newPost.setPostLikeStatus(false);
        }
        else {
            // If currentusername is in the list of likers for the post, then set it to true
            if(NewsFeedAdapter.checkLikeStatus(ParseUser.getCurrentUser().getUsername(), likersList)) {
                newPost.setPostLikeStatus(true);
            }
            else {
                newPost.setPostLikeStatus(false);
            }
        }

        if(likersList == null || likersList.size() == 0) {
            newPost.setFirstLiker(null);
        }
        else if(likersList.size() != 0) {
            // ParseQuery to extract user first name and last name from given user_name
            ParseQuery<ParseUser> query2 = ParseUser.getQuery();
            query2.whereEqualTo(Constants.username, likersList.get(0));

            // Since usernames are unique, this must return only ONE object
            List<ParseUser> foundUsers = query2.find();
            String likerName = foundUsers.get(0).getString(Constants.firstname) +
                    " " + foundUsers.get(0).getString(Constants.lastname);
            newPost.setFirstLiker(likerName);
        }

        if(NewsFeedAdapter.checkCommentStatus(ParseUser.getCurrentUser().getUsername(), postId)) {
            newPost.setPostCommentStatus(true);
        }
        else {
            newPost.setPostCommentStatus(false);
        }

    }

    private void fetchCommentInfo() throws Exception {
        comments.clear();
        String comment, commentId;
        String username;
        Date date;
        ParseFile imageFile;
        byte[] byteArray;
        Bitmap postBitmap;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Comment);
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();

        query.whereEqualTo(Constants.postId, postId);
        query.orderByAscending(Constants.createdAt);
        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            CommentInfo commentInfo;
            comment = object.getString(Constants.comment);
            username = object.getString(Constants.username);
            date = object.getCreatedAt();
            commentId = object.getObjectId();

            userQuery.whereEqualTo(Constants.username, username);
            ParseUser user = userQuery.getFirst();

            imageFile = user.getParseFile(Constants.profilePicture);
            if(imageFile != null) {
                byteArray = imageFile.getData();
                postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                commentInfo = new CommentInfo(postBitmap, username, comment, date, true, commentId);
            }
            else {
                commentInfo = new CommentInfo(BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo),
                        username, comment, date, true, commentId);
            }

            comments.add(commentInfo);
        }

    }

    private void setUpRecycler() {
        comments = new ArrayList<>();
        llm = new NoScrollLayoutManager(SinglePostActivity.this);
        llm.setScrollEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(llm);
        rvAdapter = new CommentsAdapter(comments, SinglePostActivity.this, newPost);
        recyclerView.setAdapter(rvAdapter);
    }

    public void updatePostUi(boolean isLiked) {
        if(isLiked) {
            if(newPost.getNumberLikes() == 0) {
                postLikers.setText(getString(R.string.you_like_this));
            }
            else if (newPost.getNumberLikes() == 1) {
                postLikers.setText(getString(R.string.you_and_one_like_this));
            }
            else {
                postLikers.setText(String.format(getString(R.string.you_and_x_others_like_this), String.valueOf(newPost.getNumberLikes())));
            }
            newPost.addNumberLikes(1);
        }
        else {
            if(newPost.getNumberLikes() == 1) {
               postLikers.setText(getString(R.string.no_one_likes_this));
            }
            else if(newPost.getNumberLikes() == 2) {
                postLikers.setText(String.format(getString(R.string.x_likes_this), newPost.getFirstLiker()));
            }
            else if(newPost.getNumberLikes() == 3) {
                postLikers.setText(String.format(getString(R.string.x_and_one_like_this), newPost.getFirstLiker()));
            }
            else {
               postLikers.setText(String.format(getString(R.string.x_and_x_others_like_this),
                        newPost.getFirstLiker(),
                        String.valueOf(newPost.getNumberLikes() - 2)));
            }

            newPost.addNumberLikes(-1);
        }
    }

    public void updateImageLikeStatus(final boolean isLiked, final View v) {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Post);
        query.whereEqualTo(Constants.objectId, postId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                    Toast.makeText(SinglePostActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                } else {
                    if (isLiked) {
                        objects.get(0).put(Constants.numberLikes, objects.get(0).getInt(Constants.numberLikes) + 1);
                    } else {
                        objects.get(0).put(Constants.numberLikes, objects.get(0).getInt(Constants.numberLikes) - 1);
                    }

                    // Adding liker username to likers list, if isLiked
                    List<String> likersArray = objects.get(0).getList(Constants.likers);
                    if (likersArray == null) {
                        likersArray = new ArrayList<>();
                    }

                    if (isLiked) {
                        boolean shouldAdd = true;
                        for (String liker : likersArray) {
                            if (liker.equals(ParseUser.getCurrentUser().getUsername())) {
                                shouldAdd = false;
                                break;
                            }
                        }
                        if (shouldAdd) {
                            likersArray.add(ParseUser.getCurrentUser().getUsername());
                            objects.get(0).put(Constants.likers, likersArray);
                        }
                    } else {
                        // Must remove user from list of likers now
                        List<String> newLikersArray = new ArrayList<String>();
                        for (String liker : likersArray) {
                            if (!liker.equals(ParseUser.getCurrentUser().getUsername())) {
                                newLikersArray.add(liker);
                            }
                        }
                        objects.get(0).put(Constants.likers, newLikersArray);
                    }


                    objects.get(0).saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                if (isLiked) {
                                    Snackbar snackbar = Snackbar.make(v, getString(R.string.liked), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                                    Toast.makeText(mContext, mContext.getString(R.string.liked), Toast.LENGTH_SHORT).show();
                                } else {
                                    Snackbar snackbar = Snackbar.make(v, getString(R.string.unliked), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                                    Toast.makeText(mContext, mContext.getString(R.string.unliked), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(SinglePostActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        // Adding notification object to corresponding postObject
        if (!newPost.getUserName().equals(ParseUser.getCurrentUser().getUsername()) && isLiked) {
            ParseObject newObject = new ParseObject("Notification");
            newObject.put("type", "like");
            newObject.put("postId", newPost.getObjectId());
            newObject.put("receiverUsername", newPost.getUserName());
            newObject.put("providerUsername", ParseUser.getCurrentUser().getUsername());
            newObject.put("isActive", true);
            if (ParseUser.getCurrentUser().getParseFile("profilePicture") != null) {
                newObject.put("image", ParseUser.getCurrentUser().getParseFile("profilePicture"));
            }
            newObject.saveInBackground();

            // Push notification cloud code call
            Map<String, String> params = new HashMap<>();
            params.put("receiver", newPost.getUserName());
            params.put("provider", ParseUser.getCurrentUser().getString("firstname")
                    + " " + ParseUser.getCurrentUser().getString("lastname"));
            ParseCloud.callFunctionInBackground("sendLikePush", params);

        }
    }

    private void setUpPostData() {
        Bitmap postBitmap = newPost.getPostImage();

        if (postBitmap != null) {
            postImage.setVisibility(View.VISIBLE);
            postImage.setImageBitmap(postBitmap);
        } else {
            postImage.setVisibility(View.GONE);
        }
        postCaption.setText(String.format(getString(R.string.quotes), newPost.getPostCaption()));
        postTime.setText(newPost.getPostDate());
        postUser.setText(newPost.getUserName());
        postComments.setText(String.valueOf(newPost.getNumberComments()));

        if (newPost.getPostLikeStatus()) {
            likeButton.setColorFilter(ContextCompat.getColor(this, R.color.app_theme));
            if (newPost.getNumberLikes() == 1) {
               postLikers.setText(getString(R.string.you_like_this));
            } else if (newPost.getNumberLikes() == 2) {
                postLikers.setText(getString(R.string.you_and_one_like_this));
            } else {
                postLikers.setText(String.format(getString(R.string.you_and_x_others_like_this), String.valueOf(newPost.getNumberLikes() - 1)));
            }
        } else {
            likeButton.setColorFilter(ContextCompat.getColor(this, R.color.transp_black));
            if (newPost.getNumberLikes() == 0) {
                postLikers.setText(getString(R.string.no_one_likes_this));
            } else if (newPost.getNumberLikes() == 1) {
                postLikers.setText(String.format(getString(R.string.x_likes_this), newPost.getFirstLiker()));
            } else if (newPost.getNumberLikes() == 2) {
                postLikers.setText(String.format(getString(R.string.x_and_one_like_this), newPost.getFirstLiker()));
            } else {
                postLikers.setText(String.format(getString(R.string.x_and_x_others_like_this),
                        newPost.getFirstLiker(),
                        String.valueOf(newPost.getNumberLikes() - 1)));
            }
        }

        if(newPost.getPostCommentStatus()) {
            commentButton.setColorFilter(ContextCompat.getColor(this, R.color.app_theme));
            postComments.setTextColor(ContextCompat.getColor(this, R.color.app_theme));
        }
        else {
            commentButton.setColorFilter(ContextCompat.getColor(this, R.color.transp_black));
            postComments.setTextColor(ContextCompat.getColor(this, R.color.transp_black));
        }

        // Like listener
       likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!newPost.getPostLikeStatus()) {
                    newPost.setPostLikeStatus(true);
                    likeButton.setColorFilter(ContextCompat.getColor(SinglePostActivity.this, R.color.app_theme));
                    updateImageLikeStatus(true, likeButton);
                    updatePostUi(true);
                } else {
                    newPost.setPostLikeStatus(false);
                    likeButton.setColorFilter(ContextCompat.getColor(SinglePostActivity.this, R.color.transp_black));
                    updateImageLikeStatus(false, likeButton);
                    updatePostUi(false);
                }
            }
        });

        // Clicking username to go to that user profile
        postUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SinglePostActivity.this, ProfileActivity.class);
                i.putExtra("username", newPost.getUserName());
                startActivity(i);
            }
        });

//        setUpCardViewLongClickListener(holder, info, position);
    }

    public void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(R.string.view_post);
    }

    public void saveComment() throws Exception {
        ParseObject commentObject = new ParseObject(Constants.Comment);
        commentObject.put(Constants.postId, postId);
        commentObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        commentObject.put(Constants.comment, commentEditText.getText().toString().trim());
        commentObject.save();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Post);
        query.whereEqualTo(Constants.objectId, postId);
        ParseObject post = query.getFirst();
        int numComments = post.getInt(Constants.numberComments);
        numComments++;
        post.put(Constants.numberComments, numComments);
        post.save();

        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        ParseUser foundUser = userParseQuery.getFirst();
        ParseFile imageFile = foundUser.getParseFile(Constants.profilePicture);
        byte[] byteArray = imageFile.getData();
        Bitmap picture = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        CommentInfo newComment = new CommentInfo(picture, ParseUser.getCurrentUser().getUsername(),
                commentEditText.getText().toString().trim(), new Date(), false, commentObject.getObjectId());
        comments.add(newComment);

        newPost.addNumberComments(1);
        newPost.setPostCommentStatus(true);

        // Adding notification object to account for this comment
        // Adding notification object to corresponding postObject
        if(!newPost.getUserName().equals(ParseUser.getCurrentUser().getUsername())) {
            ParseObject newObject = new ParseObject(Constants.Notification);
            newObject.put(Constants.type, getString(R.string.comment_request));
            newObject.put(Constants.postId, newPost.getObjectId());
            newObject.put(Constants.receiverUsername, newPost.getUserName());
            newObject.put(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
            newObject.put(Constants.isActive, true);
            if (ParseUser.getCurrentUser().getParseFile(Constants.profilePicture) != null) {
                newObject.put(Constants.image, ParseUser.getCurrentUser().getParseFile(Constants.profilePicture));
            }
            newObject.save();

            // Push notification cloud code call
            Map<String, String> params = new HashMap<>();
            params.put(getString(R.string.receiver), newPost.getUserName());
            params.put(getString(R.string.provider), ParseUser.getCurrentUser().getString(Constants.firstname)
                    + " " + ParseUser.getCurrentUser().getString(Constants.lastname));
            ParseCloud.callFunctionInBackground(getString(R.string.commentPush), params);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post);

        bindViews();
        setUpToolBar();
        setUpRecycler();

        Intent callingIntent = getIntent();
        postId = callingIntent.getStringExtra("postId");

//        swipeRefreshLayout.setColorSchemeResources(R.color.app_theme);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.app_theme),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        // Listeners
//        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                new FetchPostData(false).execute();
//            }
//        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(commentEditText.getText().toString().trim())) {
                    new SaveComment().execute();
                }
                else {
                    Toast.makeText(SinglePostActivity.this, getString(R.string.error_empty_comment), Toast.LENGTH_SHORT).show();
                }
            }
        });

        commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.smoothScrollTo(0, postButton.getTop());
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        new FetchPostData(true).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //------------------------------------------
    // ASYNCTASK
    //------------------------------------------

    private class FetchPostData extends AsyncTask<Void, Void, Boolean> {

        boolean showProgressBar;

        public FetchPostData(boolean showProgressBar) {
            this.showProgressBar = showProgressBar;
        }

        @Override
        protected void onPreExecute() {
            if(showProgressBar) {
                setViewVisibility(progressBar, true, true);
                setViewVisibility(cardView, false, true);
                setViewVisibility(commentEditText, false, true);
                setViewVisibility(postButton, false, true);
            }
            setViewVisibility(recyclerView, false, true);

        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(showProgressBar) {
                    fetchObjectInfo();
                }
                fetchCommentInfo();
                return true;
            }
            catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(recyclerView, true, true);
                setViewVisibility(cardView, true, true);
                setViewVisibility(postButton, true, true);
                setViewVisibility(commentEditText, true, true);
                setUpPostData();
                rvAdapter.setInfo(newPost);
                rvAdapter.notifyDataSetChanged();
//                swipeRefreshLayout.setRefreshing(false);
            }
            else {
                Toast.makeText(SinglePostActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < 6) {
                    new FetchPostData(true).execute();
                }
            }
        }
    }

    private class SaveComment extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                saveComment();
                return true;
            } catch (Exception e ){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar snackbar =  Snackbar.make(recyclerView, getString(R.string.comment_post_successful), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                postComments.setText(String.valueOf(newPost.getNumberComments()));
                commentButton.setColorFilter(ContextCompat.getColor(SinglePostActivity.this, R.color.app_theme));

                commentEditText.getText().clear();
                rvAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(comments.size()-1);
            }
            else {
                Toast.makeText(SinglePostActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
