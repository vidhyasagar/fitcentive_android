package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;
import com.squareup.otto.Bus;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretProfile;
import com.vidhyasagar.fitcentive.dialog_fragments.UpdatePostDialogFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.fragments.UserResultsFragment;
import com.vidhyasagar.fitcentive.main_activity_fragments.DiaryFragment;
import com.vidhyasagar.fitcentive.main_activity_fragments.FollowerListFragment;
import com.vidhyasagar.fitcentive.main_activity_fragments.NewsFeedFragment;
import com.vidhyasagar.fitcentive.main_activity_fragments.NotificationsFragment;
import com.vidhyasagar.fitcentive.main_activity_fragments.NutritionFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.EventBus;
import com.vidhyasagar.fitcentive.utilities.MySearchSuggestion;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

// This class does the main stuff after user logs in


public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private LinearLayout linearLayout;
    private LinearLayout userDetailslinearLayout;

    FrameLayout frameLayout;
    FloatingSearchView searchView;
    Bitmap userAvatarImage;
    TextView usernameTextView, userEmailTextView, userRealNameTextView;
    ImageView avatarImageView;
    ProgressBar progressBar;
    String username;
    String email;
    String firstname, lastname;
    ParseFile profilePictureFile;
    InputMethodManager imm;
    FatSecretProfile foodProfile;
    BottomBar bottomBar;

    String startMethod = null;

    public void showMainLayout(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            userDetailslinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            userDetailslinearLayout.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    userDetailslinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
            avatarImageView.setVisibility(show ? View.VISIBLE : View.GONE);
            avatarImageView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    avatarImageView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
            progressBar.setVisibility(show ? View.GONE : View.VISIBLE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
        else {
            userDetailslinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
            avatarImageView.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void startProfileActivity() {
        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(i);
    }

    private void displayImage() {
        if (profilePictureFile != null) {
            try {
                byte[] data = profilePictureFile.getData();
                userAvatarImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                if (userAvatarImage != null) {
                    avatarImageView.setImageBitmap(userAvatarImage);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            avatarImageView.setImageResource(R.drawable.ic_avatar);
        }
    }

    public void getHeaderUserDetails() throws Exception {
        username = ParseUser.getCurrentUser().getUsername();
        email = ParseUser.getCurrentUser().getEmail();
        try {
            firstname = ParseUser.getCurrentUser().fetch().getString(Constants.firstname);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            lastname = ParseUser.getCurrentUser().fetch().getString(Constants.lastname);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setUserAvatar() throws Exception{
        profilePictureFile =  ParseUser.getCurrentUser().getParseFile(Constants.profilePicture);
    }

    public void bindViews() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        linearLayout = (LinearLayout) navigationView.getHeaderView(0);
        userEmailTextView = (TextView) linearLayout.findViewById(R.id.user_email);
        usernameTextView = (TextView) linearLayout.findViewById(R.id.user_name);
        userRealNameTextView = (TextView) linearLayout.findViewById(R.id.user_real_name);
        avatarImageView = (ImageView) linearLayout.findViewById(R.id.profile_avatar);
        progressBar = (ProgressBar) linearLayout.findViewById(R.id.progress_bar);
        userDetailslinearLayout = (LinearLayout) linearLayout.findViewById(R.id.user_text_details_layout);
        searchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        frameLayout = (FrameLayout) findViewById(R.id.search_suggestions_frame);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        foodProfile = new FatSecretProfile();
    }

    // TODO : Set up search bar like in profileActivity
    public void  setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void setUpParseInstallation() {
        // Push notifications creating Installation Object
        ParseInstallation installation =  ParseInstallation.getCurrentInstallation();
        installation.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        installation.saveInBackground();
    }

    //TODO : Remove local method and call static method
    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void showSearchResultsFragment(UserResultsFragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_suggestions_frame, fragment).commit();
        if(frameLayout.getVisibility() != View.VISIBLE) {
            setViewVisibility(frameLayout, true, true);
            frameLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_down_top));
        }
    }

    private void initSearchBar() {

        searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                setViewVisibility(searchView, false, true);
                searchView.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_up_bottom));
                setViewVisibility(frameLayout, false, true);
                frameLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_up_bottom));
            }
        });

        searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                // Fetch from User either (FirstName + LastName) [or] (Username)
                // Depending on what the user inputs
                // This fetch and swap must be done in the background thread to not mess up UI thread
                // But first, we must hide the frameLayout if it is indeed shown
                setViewVisibility(frameLayout, false, true);

                if(!TextUtils.isEmpty(newQuery.trim())){
                    if(!ProfileActivity.containsWhitespace(newQuery.trim())) {
                        new FetchSuggestions().execute(newQuery.trim());
                    }
                    else {
                        String[] names = newQuery.split("\\s+");
                        new FetchSuggestions().execute(names[0], names[1]);
                    }
                }
            }
        });

        // When a user hits the search button, the search results are loaded onto searchResultsFrame
        // This is a fragment that displays results in a clickable listview
        // Upon Click, the fragment launches a new activity, which is basically the profile of the other selected user
        // Fragment runs it's own AsyncTask based on search query to generate suggestions
        // This fetch is NOT handled by parent activity
        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                Bundle bundle = new Bundle();
                bundle.putString("query", searchSuggestion.getBody().trim());
                bundle.putBoolean("isMainActivity", true);
                UserResultsFragment searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment(searchResultsFragment);
            }

            @Override
            public void onSearchAction(String currentQuery) {
                Bundle bundle = new Bundle();
                bundle.putString("query", currentQuery.trim());
                bundle.putBoolean("isMainActivity", true);
                UserResultsFragment searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment(searchResultsFragment);
            }
        });

        searchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                View v = MainActivity.this.getCurrentFocus();
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isMainActivity", true);
                bundle.putString("query", searchView.getQuery().trim());
                UserResultsFragment searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment(searchResultsFragment);

                if(frameLayout.getVisibility() != View.VISIBLE) {
                    setViewVisibility(frameLayout, true, true);
                    frameLayout.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_down_top));
                }
            }
        });
    }

    public void setFrameLayoutWidth(int height) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth, height);
        int[] attrs = new int[] {android.R.attr.actionBarSize};
        TypedArray ta = this.obtainStyledAttributes(attrs);
        int actionBarSize = ta.getDimensionPixelSize(0,0);
        ta.recycle();
        params.setMargins(0,actionBarSize,0,0);
        frameLayout.setLayoutParams(params);
    }

    private void startSearch() {
        setViewVisibility(searchView, true, true);
        searchView.startAnimation(AnimationUtils.loadAnimation(MainActivity.this, R.anim.slide_down_top));
    }

    private void setMenuCounter(@IdRes int itemId, int count) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        view.setText(count > 0 ? String.valueOf(count) : null);
        view.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.app_theme));
    }

    private void createFatSecretUserIfNotExisting() {
        // Check to see if user exists already
        // If not, create one and add to Database
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Fatsecret);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        try {
            ParseObject object = query.getFirst();
            Constants.userOauthToken = object.getString(Constants.userToken);
            Constants.userOauthSecret = object.getString(Constants.userSecret);
        } catch (ParseException e) {
            // Exception thrown will be user not found
            e.printStackTrace();
            // Create user now
            try {
                JSONObject profile = foodProfile.createProfile();
                ParseObject newObject = new ParseObject(Constants.Fatsecret);
                newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
                newObject.put(Constants.userSecret, profile.getString(Constants.auth_secret));
                newObject.put(Constants.userToken, profile.getString(Constants.auth_token));
                newObject.save();
                // Init these for future references elsewhere
                Constants.userOauthSecret = profile.getString(Constants.auth_secret);
                Constants.userOauthToken = profile.getString(Constants.auth_token);
            }
            catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setUpBottomBar() {
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_user) {
                    startProfileActivity();
                }
                if (tabId == R.id.tab_home) {
                    Fragment fragment1 = new NewsFeedFragment();
                    Bundle args1 = new Bundle();
                    args1.putBoolean("isNewsfeed", true);
                    fragment1.setArguments(args1);
                    updateDisplay(fragment1, NewsFeedFragment.TAG);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        new InitFatSecretData().execute();
        new SetUpUserData().execute();
        new GetNotificationsCount().execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpParseInstallation();
        setUpToolBar();
        bindViews();
        initSearchBar();
        setUpBottomBar();

        // Make newsfeed fragment the default fragment shown at start

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            startMethod = extras.getString(Constants.START_METHOD, Constants.NORMAL_START);
        }
        else {
            startMethod = Constants.NORMAL_START;
        }

        // TODO: Refactor this to streamline finishing activities and returning to last selected screen

        if(startMethod.equals(Constants.NORMAL_START)) {
            navigationView.setCheckedItem(R.id.navigation_item_home);
            Fragment fragment0 = new NewsFeedFragment();
            Bundle args0 = new Bundle();
            args0.putBoolean("isNewsfeed", true);
            fragment0.setArguments(args0);
            updateDisplay(fragment0, NewsFeedFragment.TAG);
        }
        else if (startMethod.equals(Constants.PUSH_START)){
            navigationView.setCheckedItem(R.id.navigation_item_notifications);
            Fragment fragment0 = new NotificationsFragment();
            updateDisplay(fragment0, NotificationsFragment.TAG);
        }
        else if (startMethod.equals(Constants.RETURN_FROM_SEARCH_FOOD)) {
            Bundle args0 = new Bundle();
            args0.putInt(Constants.DAY_OFFSET, extras.getInt(Constants.DAY_OFFSET));
            navigationView.setCheckedItem(R.id.navigation_item_diary);
            Fragment fragment0 = new DiaryFragment();
            fragment0.setArguments(args0);
            updateDisplay(fragment0, DiaryFragment.TAG);
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startProfileActivity();
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {

                    case R.id.navigation_item_home:
                        Fragment fragment1 = new NewsFeedFragment();
                        Bundle args1 = new Bundle();
                        args1.putBoolean("isNewsfeed", true);
                        fragment1.setArguments(args1);
                        updateDisplay(fragment1, NewsFeedFragment.TAG);
                        break;

                    case R.id.navigation_item_followers:
                        Fragment fragment2 = FollowerListFragment.newInstance();
                        updateDisplay(fragment2, NewsFeedFragment.TAG);
                        break;

                    case R.id.navigation_item_notifications:
                        updateDisplay(new NotificationsFragment(), NotificationsFragment.TAG);
                        break;

                    case R.id.navigation_item_diary:
                        updateDisplay(new DiaryFragment(), DiaryFragment.TAG);
                        break;

                    case R.id.navigation_item_nutrition:
                        updateDisplay(NutritionFragment.newInstance(0), NutritionFragment.TAG);
                        break;

                    case R.id.help_item:
                        Toast.makeText(MainActivity.this, "No help yet...", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.preferences_item:
                        Toast.makeText(MainActivity.this, "No preferences yet...", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.logout_item:
                        ParseUser.getCurrentUser().logOut();
                        Intent i = new Intent(MainActivity.this, LoginScreenActivity.class);
                        startActivity(i);
                        finish();
                        break;
                }
                return true;
            }
        });

    }

    private void createNavigatingAwayConfirmationDialog(final Fragment fragment, final String tag) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.unsaved_changes))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment, tag).commit();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        navigationView.setCheckedItem(R.id.navigation_item_diary);
                    }
                })
                .setMessage(getString(R.string.unsaved_diary_changes_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void updateDisplay(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        DiaryFragment diaryFragment = (DiaryFragment) fragmentManager.findFragmentByTag(DiaryFragment.TAG);
        if(diaryFragment != null && diaryFragment.isVisible()) {
            // Logic to see if it has unsaved changes and if so, then prompt confirmation dialog
            DiaryPageFragment diaryPageFragment = diaryFragment.getCurrentFragment();
            if(diaryPageFragment.hasUnsavedChanges()) {
                createNavigatingAwayConfirmationDialog(fragment, tag);
            }
            else {
                fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, tag).commit();
            }
        }
        else {
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment, tag).commit();
        }
    }

    private int getNotificationsCount() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Notification);
        query.whereEqualTo(Constants.receiverUsername, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.isActive, true);
        return query.count();
    }

    public void updateNotificationsCount() {
        new GetNotificationsCount().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_search:
                startSearch();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Fragment updatePostFragment = getSupportFragmentManager().findFragmentByTag(UpdatePostDialogFragment.TAG);
        Fragment nutritionFragment = getSupportFragmentManager().findFragmentByTag(NutritionFragment.TAG);

        if(updatePostFragment != null) {
            updatePostFragment.onActivityResult(requestCode, resultCode, data);
        }

        if(nutritionFragment != null) {
            nutritionFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    //-------------------------------
    // ASYNCTASK
    //-------------------------------

    private class SetUpUserData extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                getHeaderUserDetails();
                setUserAvatar();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                usernameTextView.setText(username);
                userEmailTextView.setText(email);
                userRealNameTextView.setText(firstname + " " + lastname);
                displayImage();
                showMainLayout(true);
            }
            else {
                Toast.makeText(MainActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                new SetUpUserData().execute();
            }
        }
    }

    public class GetNotificationsCount extends AsyncTask<Void, Void, Boolean> {

        int notificationsCount;

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                notificationsCount = getNotificationsCount();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setMenuCounter(R.id.navigation_item_notifications, notificationsCount);
            }
            else {
                Toast.makeText(MainActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    // TODO : Edge case, duplicates arise if multiple people have the same names
    private class FetchSuggestions extends AsyncTask<String, Void, Boolean> {

        List<MySearchSuggestion> suggestions = new ArrayList<>();
        int maxNumberOfSuggestions = 5;

        private void addToSuggestions(List<ParseUser> users) {
            for(ParseUser user : users) {
                maxNumberOfSuggestions--;
                if(maxNumberOfSuggestions >= 0) {
                    suggestions.add(new MySearchSuggestion(user.getString(Constants.firstname) + " " +
                            user.getString(Constants.lastname)));
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            searchView.swapSuggestions(suggestions);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if(params.length == 1) {
                String userInput = params[0];
                ParseQuery<ParseUser> userNameQuery = ParseUser.getQuery();
                ParseQuery<ParseUser> firstNameQuery = ParseUser.getQuery();
                ParseQuery<ParseUser> lastNameQuery = ParseUser.getQuery();
                List<ParseUser> usernameQueryResult;
                List<ParseUser> firstNameQueryResult;
                List<ParseUser> lastNameQueryResult;
                try {
                    if(userInput.charAt(0) == '@') {
                        userInput = userInput.substring(1);
                        userNameQuery.whereContains(Constants.username, userInput);
                        userNameQuery.setLimit(MainActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        usernameQueryResult = userNameQuery.find();
                        addToSuggestions(usernameQueryResult);
                        if(usernameQueryResult.size() == 0) {
                            suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                        }
                    }
                    else {
                        // Maybe the user has input a firstName. Try this check
                        userInput = userInput.substring(0, 1).toUpperCase() + userInput.substring(1);
                        firstNameQuery.whereContains(Constants.firstname, userInput);
                        firstNameQuery.setLimit(MainActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        firstNameQueryResult = firstNameQuery.find();
                        addToSuggestions(firstNameQueryResult);

                        // Maybe user has input a last name. Try this check (Final one)
                        lastNameQuery.whereContains(Constants.lastname, userInput);
                        lastNameQuery.setLimit(MainActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        lastNameQueryResult = lastNameQuery.find();
                        addToSuggestions(lastNameQueryResult);

                        if (lastNameQueryResult.size() == 0 && firstNameQueryResult.size() == 0) {
                            suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                        }
                    }

                    return true;

                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else {
                String uInputFirstName = params[0];
                String uInputLastName = params[1];
                uInputFirstName = uInputFirstName.substring(0, 1).toUpperCase() + uInputFirstName.substring(1);
                uInputLastName = uInputLastName.substring(0, 1).toUpperCase() + uInputLastName.substring(1);
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereContains(Constants.firstname, uInputFirstName);
                query.whereContains(Constants.lastname, uInputLastName);
                try {
                    List<ParseUser> result = query.find();
                    if(result.size() == 0) {
                        suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                    }
                    else {
                        addToSuggestions(result);
                    }
                    return true;
                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                }

            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                searchView.swapSuggestions(suggestions);
            }
            else {
                Toast.makeText(MainActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class InitFatSecretData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            createFatSecretUserIfNotExisting();
            return null;
        }
    }
}
