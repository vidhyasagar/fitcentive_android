package com.vidhyasagar.fitcentive.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.utilities.MultiSpinner;
import com.vidhyasagar.fitcentive.wrappers.FilterInfo;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;

// TODO: Refine this with how WGER.DE works with URL Filtering. Seems semi-stable now but could have kinks in the long run
public class ExerciseFilterActivity extends AppCompatActivity {

    public static final String FILTER_INFO = "FILTER_INFO";

    public static final int CATEGORY_OFFSET = 8;
    public static final int REGULAR_OFFSET = 1;

    ArrayList<Integer> categories, primaryMuscles, secondaryMuscles, equipment;

    Toolbar toolbar;
    MultiSpinner categorySpinner, equipmentSpinner, primarySpinner, secondarySpinner;
    Button filterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_filter);

        bindViews();
        setUpToolbar();
        setUpSpinners();
        setUpApplyFiltersButton();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filters, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_filter:
                saveFiltersAndReturn();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        categorySpinner = (MultiSpinner) findViewById(R.id.category_spinner);
        equipmentSpinner = (MultiSpinner) findViewById(R.id.equipment_spinner);
        primarySpinner = (MultiSpinner) findViewById(R.id.primary_spinner);
        secondarySpinner = (MultiSpinner) findViewById(R.id.secondary_spinner);
        filterButton = (Button) findViewById(R.id.apply_filters_button);
    }

    private void setUpApplyFiltersButton() {
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFiltersAndReturn();
            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.exercise_filters));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private ArrayList<Integer> getSelectedFrom(MultiSpinner spinner, boolean isCategory) {
        ArrayList<Integer> list = new ArrayList<>();
        boolean atLeastOneUnselected = false;
        boolean[] selected = spinner.getSelected();
        for(int i = 0; i < selected.length; i++) {
            if(selected[i]) {
                if(isCategory) {
                    list.add(i + CATEGORY_OFFSET);
                }
                else {
                    list.add(i + REGULAR_OFFSET);
                }
            }
            else {
                atLeastOneUnselected = true;
            }
        }
        if(atLeastOneUnselected) {
            return list;
        }
        else {
            return new ArrayList<Integer>();
        }
    }

    private void saveFiltersAndReturn() {
        FilterInfo info = new FilterInfo();


        info.setCategories(getSelectedFrom(categorySpinner, true));
        info.setEquipment(getSelectedFrom(equipmentSpinner, false));
        info.setPrimaryMuscles(getSelectedFrom(primarySpinner, false));
        info.setSecondaryMuscles(getSelectedFrom(secondarySpinner, false));

        Intent i = new Intent();
        i.putExtra(FILTER_INFO, info);

        setResult(RESULT_OK, i);
        finish();
    }

    private void setUpSpinners() {
        setUpCategorySpinner();
        setUpEquipmentSpinner();
        setUpPrimaryMusclesSpinner();
        setUpSecondaryMusclesSpinner();
    }

    private void setUpCategorySpinner() {
        String[]  categoriesArray = getResources().getStringArray(R.array.exercise_categories);
        List<String> choices = new ArrayList<>();
        for(String string : categoriesArray) {
            choices.add(string);
        }
        categories = new ArrayList<>();
        categorySpinner.setItems(choices, getString(R.string.all_exercise_categories), new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {

            }
        });
    }

    private void setUpEquipmentSpinner() {
        String[]  categoriesArray = getResources().getStringArray(R.array.exercise_equipment);
        List<String> choices = new ArrayList<>();
        for(String string : categoriesArray) {
            choices.add(string);
        }
        equipment = new ArrayList<>();
        equipmentSpinner.setItems(choices, getString(R.string.all_equipment), new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {

            }
        });

    }

    private void setUpPrimaryMusclesSpinner() {
        String[]  categoriesArray = getResources().getStringArray(R.array.exercise_muscles);
        List<String> choices = new ArrayList<>();
        for(String string : categoriesArray) {
            choices.add(string);
        }
        primaryMuscles = new ArrayList<>();
        primarySpinner.setItems(choices, getString(R.string.all_primary_muscles), new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {

            }
        });

    }

    private void setUpSecondaryMusclesSpinner() {
        String[]  categoriesArray = getResources().getStringArray(R.array.exercise_muscles);
        List<String> choices = new ArrayList<>();
        for(String string : categoriesArray) {
            choices.add(string);
        }
        secondaryMuscles = new ArrayList<>();
        secondarySpinner.setItems(choices, getString(R.string.all_secondary_muscles), new MultiSpinner.MultiSpinnerListener() {
            @Override
            public void onItemsSelected(boolean[] selected) {

            }
        });

    }


}
