package com.vidhyasagar.fitcentive.activities;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    InputMethodManager imm;
    RelativeLayout relativeLayout;
    AutoCompleteTextView userEmail;

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public void verifyAccount(View v) {
        if(TextUtils.isEmpty(userEmail.getText().toString())) {
            userEmail.setError(getString(R.string.error_field_required));
            userEmail.requestFocus();
        }
        else if(!isEmailValid(userEmail.getText().toString())) {
            userEmail.setError(getString(R.string.error_invalid_email));
            userEmail.requestFocus();
        }
        else {
            // In this case, the email is cleared by our email checking mechanisms
            Toast.makeText(ForgotPasswordActivity.this, getString(R.string.verify_email_sent), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        // Setting action bar theme
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(ForgotPasswordActivity.this, R.color.app_theme)));

        // Keyboard preferences
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        userEmail = (AutoCompleteTextView) findViewById(R.id.email);

        // To make sure keyboard is hidden during launch
        // Not really needed, but an extra line of code never hurt anyone
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // On touch listener to hide keyboard when user presses anywhere on screen
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });
    }
}
