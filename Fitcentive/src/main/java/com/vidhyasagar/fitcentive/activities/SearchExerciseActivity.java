package com.vidhyasagar.fitcentive.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_exercise_fragments.CardioFragment;
import com.vidhyasagar.fitcentive.search_exercise_fragments.CreatedExercisesFragment;
import com.vidhyasagar.fitcentive.search_exercise_fragments.ExerciseResultsFragment;
import com.vidhyasagar.fitcentive.search_exercise_fragments.WorkoutsFragment;

public class SearchExerciseActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    public static final int NO_OF_TABS_DEFAULT = 4;
    public static final int NO_OF_TABS_WORKOUT_MODE = 3;

    public static final int STRENGTH_TAB = 0;
    public static final int CARDIO_TAB = 1;
    public static final int CREATED_TAB = 2;
    public static final int WORKOUT_TAB = 3;

    public static String NO_OF_TABS_STRING = "NO_OF_TABS";

    boolean isReturningToWorkoutFragment;

    int dayOffset;
    int NO_OF_TABS;

    Toolbar toolbar;
    FragmentManager manager;
    DiaryPagerAdapter adapter;
    FloatingSearchView floatingSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_exercise);

        retrieveArguments();
        bindViews();
        setUpViewPager();
        setUpToolbar();
        setUpSearchBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_meals_fragment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_create_meal:
                if(viewPager.getCurrentItem() == WORKOUT_TAB) {
                    startCreateWorkoutActivity();
                }
                else {
                    startCreateExerciseActivity();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT) {
            setResult(resultCode, data);
            finish();
        }
    }

    private void searchOnExerciseResultsFragment(String query, int tab) {
        ExerciseResultsFragment fragment = (ExerciseResultsFragment) adapter.getFragment(tab);
        fragment.searchForExercise(query);
    }

    private void startCreateWorkoutActivity() {
        WorkoutsFragment fragment = (WorkoutsFragment) adapter.getFragment(WORKOUT_TAB);
        fragment.startCreateWorkoutActivity();
    }

    private void startCreateExerciseActivity() {
        Intent i = new Intent(this, CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_CREATE);
        if(viewPager.getCurrentItem() == CREATED_TAB) {
            boolean isCardio = ((CreatedExercisesFragment) adapter.getFragment(CREATED_TAB)).getIfCardio();
            i.putExtra(Constants.isCardio, isCardio);
        }
        else {
            if(viewPager.getCurrentItem() == CARDIO_TAB) {
                i.putExtra(Constants.isCardio, true);
            }
            else if(viewPager.getCurrentItem() == STRENGTH_TAB) {
                i.putExtra(Constants.isCardio, false);
            }
        }
        startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

    private void callToSearchOnRightFragment(String query, int tabNumber) {
        switch (tabNumber) {
            case STRENGTH_TAB:
                searchOnExerciseResultsFragment(query, tabNumber);
                break;
            case CARDIO_TAB:
                break;
            case CREATED_TAB:
                searchOnCreatedExercisesFragment(query, tabNumber);
                break;
            case WORKOUT_TAB:
                break;

        }
    }

    private void searchOnCreatedExercisesFragment(String query, int tabNumber) {
        CreatedExercisesFragment fragment = (CreatedExercisesFragment) adapter.getFragment(tabNumber);
        fragment.searchForExerciseAndCardio(query);
    }

    private void setUpSearchBar() {
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                callToSearchOnRightFragment(newQuery.trim(), viewPager.getCurrentItem());
            }
        });

        floatingSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                View v = SearchExerciseActivity.this.getCurrentFocus();
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);

            }
        });
    }


    public void setUpViewPager() {

        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new DiaryPagerAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                // Change search hint here
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void setUpToolbar() {
        // Setting toolbar
        // Skipping as title set by manifest
//        toolbar.setTitle();
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    private void retrieveArguments() {
        Intent i = getIntent();
        dayOffset = i.getExtras().getInt(Constants.DAY_OFFSET);
        NO_OF_TABS = i.getExtras().getInt(NO_OF_TABS_STRING, NO_OF_TABS_DEFAULT);

        if(NO_OF_TABS == NO_OF_TABS_DEFAULT) {
            isReturningToWorkoutFragment = false;
        }
        else {
            isReturningToWorkoutFragment = true;
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        floatingSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
    }


    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class DiaryPagerAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public DiaryPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position){
                case 0:
                    fragment = ExerciseResultsFragment.newInstance(dayOffset, isReturningToWorkoutFragment);
                    break;
                case 1:
                    fragment = CardioFragment.newInstance(dayOffset, isReturningToWorkoutFragment);
                    break;
                case 2:
                    fragment = CreatedExercisesFragment.newInstance(dayOffset, isReturningToWorkoutFragment);
                    break;
                case 3:
                    fragment = WorkoutsFragment.newInstance(dayOffset);
                    break;
                default:
                    fragment = null;
                    break;
            }
            // This should never fail
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.strength);
                case 1:
                    return getString(R.string.cardio);
                case 2:
                    return getString(R.string.created);
                case 3:
                    return getString(R.string.workouts);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }

    }

}
