package com.vidhyasagar.fitcentive.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.parse.FindCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.adapters.NewsFeedAdapter;
import com.vidhyasagar.fitcentive.dialog_fragments.UpdatePostDialogFragment;
import com.vidhyasagar.fitcentive.fragments.UserResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.AppBarStateChangeListener;
import com.vidhyasagar.fitcentive.utilities.MySearchSuggestion;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileActivity extends AppCompatActivity {

    static final String TAG = "ProfileActivity";
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int SELECT_FILE = 2;
    public static final int REQUEST_WRITE_PERMISSION = 3;
    public static final int RETRY_LIMIT = 6;
    private static final String COLLAPSED = "COLLAPSED";
    private static final String EXPANDED = "EXPANDED";
    private static final String START = "START";

    int RETRY_COUNT = 0;
    boolean MENU_SHOWN = false;
    boolean IS_INIT_COMPLETE = false;
    boolean IS_PROFILE_PICTURE = true;
    boolean IS_CAMERA_USED = true;
    boolean IS_POST_PHOTO = false;
    boolean IS_CURRENT_USER_PROFILE = true;
    boolean IS_REQUESTED_TO_FOLLOW = false;
    boolean IS_FOLLOWING = false;
    boolean NO_POSTS_TO_DISPLAY = false;
    boolean IS_USER_CLEARED_TO_VIEW = false;

    private String previousState = "START";
    private Uri selectedImageUri;

    ImageView coverImage;
    ImageView postImage;
    Button addImageButton;
    ImageView postImageDeleteButton;
    TextView userNameBig;
    TextView userNameSmall;
    TextView spotters, spottees;
    TextView noPostsTextView;
    EditText postText;
    TextView userTagline;
    CircleImageView profileImage;
    FloatingActionButton editButton, postButton, spotButton, viewButton, refreshButton;
    AppBarLayout appBarLayout;
    Bitmap selectedProfilePicture;
    Bitmap selectedCoverPicture;
    Bitmap selectedPostPicture;
    Toolbar toolbar;
    ProgressBar progressBar, postsProgressBar;
    RecyclerView recyclerView;
    LinearLayoutManager lm;
    NewsFeedAdapter rvAdapter;
    ArrayList<NewsFeedInfo> news;
    FloatingSearchView searchBar;
    FrameLayout searchResultsFrame;
    InputMethodManager imm;
    CardView emptyCardView;
    SwipeRefreshLayout swipeRefreshLayout;

    // Intent recognization variables
    Intent callingIntent;
    String intentUserName;

    // Variables for storing retrieved values in init asynctask
    ParseFile profilePictureFile, coverPictureFile;
    String titleText, tagLine;
    int noSpotters, noSpottees;
    byte[] profilePictureData, coverPictureData;

    Fragment searchResultsFragment = null;


    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public void showMainLayout(final boolean show) {

        setViewVisibility(appBarLayout, show, true);
        setViewVisibility(recyclerView, show, true);
        setViewVisibility(postsProgressBar, show, true);
        setViewVisibility(toolbar, show, true);
        setViewVisibility(refreshButton, show, false);
        if(IS_CURRENT_USER_PROFILE) {
            setViewVisibility(editButton, show, false);
            setViewVisibility(postButton, show, false);
        }
        else {
            setViewVisibility(spotButton, show, false);
            setViewVisibility(viewButton, show, false);
        }
        setViewVisibility(profileImage, show, true);
        setViewVisibility(progressBar, !show, true);
    }

    public boolean isUserProfileClearedToView() throws Exception {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereContains(Constants.username, intentUserName);
        ParseUser user = query.getFirst();
        if(user.getBoolean(Constants.isPublic)) {
            return true;
        }
        else {
            ParseQuery<ParseObject> followQuery = new ParseQuery<>(Constants.Follow);
            followQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            List<ParseObject> follow = followQuery.find();
            // Checking to see if intentUserName is in currentUser's following list
            // Following list is determined by user, they can choose to not let someone follow them
            if(follow.isEmpty() || follow.size() != 1) {
                return false;
            }
            List<String> followingList = follow.get(0).getList(Constants.following);
            if(followingList == null) {
                return false;
            }
            for(String followingUser : followingList) {
                if(followingUser.equals(intentUserName)) {
                    return true;
                }
            }
            return false;
        }
    }

    // TODO : Apply time zone conversions based on location. Also fix dirty hack for the 1Hour offset
    public void populateNews() throws ParseException, Exception {
        // Name, caption, comments, likes, date, image
        // If the user whom you are querying has a private profile, and current user is NOT following that user
        // then currentUser must NOT be able to see intentUserName's profile
        // Must now be checking currentUser's followingList and/or intentUserName's followerList
        news.clear();
        ParseQuery<ParseObject> postsQuery = new ParseQuery<>(Constants.Post);
        if(IS_CURRENT_USER_PROFILE) {
            IS_USER_CLEARED_TO_VIEW = true;
            postsQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        }
        else {
            if(!isUserProfileClearedToView()) {
                IS_USER_CLEARED_TO_VIEW = false;
                return;
            }
            else {
                IS_USER_CLEARED_TO_VIEW = true;
            }
            postsQuery.whereEqualTo(Constants.username, intentUserName);
        }
        postsQuery.orderByDescending(Constants.createdAt);
        postsQuery.setLimit(ProfileActivity.this.getResources().getInteger(R.integer.post_query_init_limit));
        List<ParseObject> objects = postsQuery.find();

        if(objects.size() == 0) {
            return;
        }

        for(ParseObject post : objects) {
            String objectId = post.getObjectId();
            String username = post.getString(Constants.username);
            String caption = post.getString(Constants.caption);
            int numberComments = post.getInt(Constants.numberComments);
            int numberLikes = post.getInt(Constants.numberLikes);
            Date date = post.getCreatedAt();
            ParseFile image = post.getParseFile(Constants.image);
            List<String> likersList = post.getList(Constants.likers);
            NewsFeedInfo newPost;
            if(image != null) {
                byte[] byteArray = image.getData();
                Bitmap postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, postBitmap, objectId);
            }
            else {
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, null, objectId);
            }
            if(likersList == null) {
                newPost.setPostLikeStatus(false);
            }
            else {
                // If currentusername is in the list of likers for the post, then set it to true
                if(NewsFeedAdapter.checkLikeStatus(ParseUser.getCurrentUser().getUsername(), likersList)) {
                    newPost.setPostLikeStatus(true);
                }
                else {
                    newPost.setPostLikeStatus(false);
                }
            }

            if(likersList == null || likersList.size() == 0) {
                newPost.setFirstLiker(null);
            }
            else if(likersList.size() != 0) {
                // ParseQuery to extract user first name and last name from given user_name
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(Constants.username, likersList.get(0));

                // Since usernames are unique, this must return only ONE object
                List<ParseUser> foundUsers = query.find();
                String likerName = foundUsers.get(0).getString(Constants.firstname) + " " +
                        foundUsers.get(0).getString(Constants.lastname);
                newPost.setFirstLiker(likerName);
            }

            if(NewsFeedAdapter.checkCommentStatus(ParseUser.getCurrentUser().getUsername(), objectId)) {
                newPost.setPostCommentStatus(true);
            }
            else {
                newPost.setPostCommentStatus(false);
            }


            news.add(newPost);
        }
    }

    public void requestToFollowUser() throws ParseException {
        // Must add a notification request to the Notification class
        ParseObject newObject = new ParseObject(Constants.Notification);
        newObject.put(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.receiverUsername, intentUserName);
        newObject.put(Constants.type, getString(R.string.follow_request));
        newObject.put(Constants.isActive, true);
        // No postId needed to follow user
        newObject.put(Constants.postId, "");
        if(ParseUser.getCurrentUser().getParseFile(Constants.profilePicture) != null) {
            newObject.put(Constants.image, ParseUser.getCurrentUser().getParseFile(Constants.profilePicture));
        }
        newObject.save();


        // Push notification cloud code call
        Map<String, String> params = new HashMap<>();
        params.put(getString(R.string.receiver), intentUserName);
        params.put(getString(R.string.provider), ParseUser.getCurrentUser().getString(Constants.firstname)
                + " " + ParseUser.getCurrentUser().getString(Constants.lastname));
        ParseCloud.callFunctionInBackground(getString(R.string.followPush), params);
    }

    public void removeFromFollowingList() throws ParseException {
        // If these function is called, you can be sure that the currentUser is already following intentUserName
        // Must remove currentUser from intentUserName's followers
        // Must also remove intentUserName from currentUser's following
        // Removing the notification that might have been posted when the currentUser
        // initially tried following the intentUserName user
        ParseQuery<ParseObject> notQuery = new ParseQuery<ParseObject>(Constants.Notification);
        notQuery.whereEqualTo(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
        notQuery.whereEqualTo(Constants.receiverUsername, intentUserName);
        notQuery.whereEqualTo(Constants.postId, "");
        notQuery.whereEqualTo(Constants.type, getString(R.string.follow_request));
        notQuery.whereEqualTo(Constants.isActive, true);
        List<ParseObject> notificationObject = notQuery.find();
        if(notificationObject.isEmpty()) {
            ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
            ParseQuery<ParseObject> query1 = new ParseQuery<>(Constants.Follow);
            query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            query1.whereEqualTo(Constants.username, intentUserName);
            ParseObject result = query.getFirst();
            ParseObject result1 = query1.getFirst();

            // Updating current user's following list
            if(result != null ) {
                // Size will always HAVE to be 1. If not, something is wrong
                List<String> followingList = result.getList(Constants.following);
                int numFollowing = result.getInt(Constants.numFollowing);
                numFollowing--;
                List<String> tempList = new ArrayList<>();
                for(String user : followingList) {
                    if(!user.equals(intentUserName)) {
                        tempList.add(user);
                    }
                }
                result.put(Constants.following, tempList);
                result.put(Constants.numFollowing, numFollowing);
                result.save();
            }

            // Updating intentUser's followers list
            if(result1 != null) {
                List<String> followersList = result1.getList(Constants.followers);
                int numFollowers = result1.getInt(Constants.numFollowers);
                numFollowers--;
                List<String> tempList = new ArrayList<>();
                for(String user : followersList) {
                    if(!user.equals(ParseUser.getCurrentUser().getUsername())) {
                        tempList.add(user);
                    }
                }
                result1.put(Constants.followers, tempList);
                result1.put(Constants.numFollowers, numFollowers);
                result1.save();
            }
        }
        else {
            notificationObject.get(0).delete();
        }

    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public Bitmap grabImage(ImageView imageView) {
        this.getContentResolver().notifyChange(selectedImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap = null;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImageUri);

        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Failed to load", e);
        }

        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        imageView.setImageBitmap(scaled);
        return scaled;


    }

    public Bitmap grabImage(CircleImageView imageView) {
        this.getContentResolver().notifyChange(selectedImageUri, null);
        ContentResolver cr = this.getContentResolver();
        Bitmap bitmap = null;
        try
        {
            bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImageUri);

        }
        catch (Exception e)
        {
            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Failed to load", e);
        }

        int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
        imageView.setImageBitmap(scaled);
        return scaled;

    }

    public File createTemporaryFile(String part, String ext) throws Exception {
        File tempDir = Environment.getExternalStorageDirectory();
        tempDir = new File(tempDir.getAbsolutePath()+"/.temp/");
        if(!tempDir.exists())
        {
            tempDir.mkdirs();
        }
        return File.createTempFile(part, ext, tempDir);
    }

    public void startPostCropPictureActivity() {
        CropImage.activity(selectedImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(getString(R.string.crop_image_activity_title))
                .setAspectRatio(1, 1)
                .setFixAspectRatio(false)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .start(ProfileActivity.this);
    }

    public void startProfileCropPictureActivity() {
        CropImage.activity(selectedImageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(getString(R.string.crop_image_activity_title))
                .setAspectRatio(IS_PROFILE_PICTURE ? 1 : 2, 1)
                .setFixAspectRatio(true)
                .setCropShape(IS_PROFILE_PICTURE ? CropImageView.CropShape.OVAL : CropImageView.CropShape.RECTANGLE)
                .start(ProfileActivity.this);

    }

    public void startEditProfileActivity(boolean isCurrentUser) {
        Intent i = new Intent(ProfileActivity.this, EditProfileActivity.class);
        if(isCurrentUser) {
            i.putExtra("username", ParseUser.getCurrentUser().getUsername());
            i.putExtra("isEditable", true);
        }
        else {
            i.putExtra("username", intentUserName);
            i.putExtra("isEditable", false);
        }
        startActivity(i);
    }

    private void onPostSelectedImageResult() {

        setViewVisibility(addImageButton, false, true);
        setViewVisibility(postImage, true, true);
        setViewVisibility(postImageDeleteButton, true, true);

        selectedPostPicture = grabImage(postImage);

    }

    private void onSelectedImageResult() {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        if(IS_PROFILE_PICTURE) {
            selectedProfilePicture = grabImage(profileImage);
            selectedProfilePicture.compress(Bitmap.CompressFormat.JPEG, getResources().getInteger(R.integer.bitmap_compression), bytes);
            selectedProfilePicture.compress(Bitmap.CompressFormat.PNG, getResources().getInteger(R.integer.bitmap_compression), stream);
        }
        else {
            selectedCoverPicture = grabImage(coverImage);
            selectedCoverPicture.compress(Bitmap.CompressFormat.JPEG, getResources().getInteger(R.integer.bitmap_compression), bytes);
            selectedCoverPicture.compress(Bitmap.CompressFormat.PNG, getResources().getInteger(R.integer.bitmap_compression), stream);
        }

        byte[] byte_data = stream.toByteArray();
        ParseFile file = new ParseFile(byte_data);
        new SavePicture().execute(file);

    }

    private void mayRequestWriteToStorage() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(ProfileActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getString(R.string.confirm_write_access_denial))
                        .setMessage(getString(R.string.permission_confirmation_write))
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ProfileActivity.this,
                                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
                            }
                        })
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(ProfileActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_PERMISSION);
            }
        }
        else {
            cameraIntent();
        }

    }

    private void mayRequestCamera() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(ProfileActivity.this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this,
                    Manifest.permission.CAMERA)) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle(getString(R.string.confirm_camera_access_denial))
                        .setMessage(getString(R.string.permission_confirmation_camera))
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(ProfileActivity.this,
                                        new String[]{Manifest.permission.CAMERA}, REQUEST_IMAGE_CAPTURE);
                            }
                        })
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                    builder.setIcon(getDrawable(R.drawable.ic_logo));
                }
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(ProfileActivity.this,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_IMAGE_CAPTURE);
            }
        }
        else {
            mayRequestWriteToStorage();
        }

    }

    private void galleryIntent() {
        IS_CAMERA_USED = false;
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), SELECT_FILE);
    }

    private void cameraIntent() {
        IS_CAMERA_USED = true;
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photo;
            try
            {
                // place where to store camera taken picture
                photo = this.createTemporaryFile("picture", ".jpg");
                photo.delete();
            }
            catch(Exception e)
            {
                Log.v(TAG, "Can't create file to take picture!");
                Toast.makeText(ProfileActivity.this, getString(R.string.need_write_permission), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                return;
            }
            selectedImageUri = Uri.fromFile(photo);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImageUri);
            //start camera intent
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
        else {
            Toast.makeText(ProfileActivity.this, getString(R.string.need_camera_for_action), Toast.LENGTH_SHORT).show();
        }

    }

    public void createImagePickingDialog() {
        final CharSequence[] items = getResources().getStringArray(R.array.image_picking_choices);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(IS_PROFILE_PICTURE ? getString(R.string.set_new_profile_picture) : getString(R.string.set_new_cover_photo))
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            mayRequestCamera();
                        }
                        else if(which == 1) {
                            galleryIntent();
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setPicture(View v) {
        if(v.getId() == R.id.profile_image) {
            IS_PROFILE_PICTURE = true;
        }
        else if(v.getId() == R.id.cover_photo){
            IS_PROFILE_PICTURE = false;
        }
        IS_POST_PHOTO = false;

        createImagePickingDialog();
    }

    public void setPostPicture(View v) {
        IS_POST_PHOTO = true;

        final CharSequence[] items = getResources().getStringArray(R.array.image_picking_choices);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getString(R.string.add_post_picture))
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            mayRequestCamera();
                        }
                        else if(which == 1) {
                            galleryIntent();
                        }
                        else {
                            dialog.dismiss();
                        }
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void displayImage(boolean isProfilePicture) {
        if(isProfilePicture) {
            if(profilePictureData != null) {
                selectedProfilePicture = BitmapFactory.decodeByteArray(profilePictureData, 0, profilePictureData.length);
                if (selectedProfilePicture != null) {
                    profileImage.setImageBitmap(selectedProfilePicture);
                } else {
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                profileImage.setImageResource(R.drawable.ic_avatar);
            }
        }
        else if(!isProfilePicture && coverPictureData != null){
            selectedCoverPicture = BitmapFactory.decodeByteArray(coverPictureData, 0, coverPictureData.length);
            if(selectedCoverPicture != null) {
                coverImage.setImageBitmap(selectedCoverPicture);
            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void initPictures() throws ParseException, Exception {
        if(IS_CURRENT_USER_PROFILE) {
            profilePictureFile = ParseUser.getCurrentUser().getParseFile(Constants.profilePicture);
            coverPictureFile = ParseUser.getCurrentUser().getParseFile(Constants.coverPicture);
        }
        else {
            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo(Constants.username, intentUserName);
            ParseUser result = query.getFirst();
            profilePictureFile = result.getParseFile(Constants.profilePicture);
            coverPictureFile = result.getParseFile(Constants.coverPicture);
        }

        if(profilePictureFile != null)
            profilePictureData = profilePictureFile.getData();

        if(coverPictureFile != null)
            coverPictureData = coverPictureFile.getData();

    }

    public void showTitleText(final boolean show) {
        setViewVisibility(userNameSmall, show, true);
        setViewVisibility(profileImage, !show, true);
    }

    public void initHeadingText() throws ParseException {
        if(IS_CURRENT_USER_PROFILE) {
            ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
            userParseQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            ParseUser currentUser = userParseQuery.getFirst();
            titleText = currentUser.getString(Constants.firstname) + " " +
                    currentUser.getString(Constants.lastname);
            tagLine = currentUser.getString(Constants.profileTagline);

            // Spotters are followers
            ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Follow);
            query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            List<ParseObject> result = query.find();
            if(result.size() == 0) {
                noSpottees = noSpotters = 0;
            }
            else {
                noSpottees = result.get(0).getInt(Constants.numFollowing);
                noSpotters = result.get(0).getInt(Constants.numFollowers);
            }
        }
        else {
            ParseQuery<ParseUser> query = ParseUser.getQuery();
            ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.Follow);
            query1.whereEqualTo(Constants.username, intentUserName);
            query.whereEqualTo(Constants.username, intentUserName);
            // This HAS to be of size 1 as usernames are unique
            List<ParseUser> result = query.find();
            List<ParseObject> result1 = query1.find();
            titleText = result.get(0).getString(Constants.firstname) + " " + result.get(0).getString(Constants.lastname);
            tagLine = result.get(0).getString(Constants.profileTagline);

            if(result1.size() == 0) {
                noSpottees = noSpotters = 0;
            }
            else {
                noSpottees = result1.get(0).getInt(Constants.numFollowing);
                noSpotters = result1.get(0).getInt(Constants.numFollowers);
            }

        }

        if(tagLine == null) {
            tagLine = getString(R.string.no_tagline);
        }
        else {
            tagLine = '"' + tagLine + '"';
        }

    }

    public void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        userNameBig = (TextView) findViewById(R.id.user_full_name_big);
        userNameSmall = (TextView) findViewById(R.id.user_full_name_small);
        spotters = (TextView) findViewById(R.id.no_of_spotters);
        spottees = (TextView) findViewById(R.id.no_of_spottees);
        coverImage = (ImageView) findViewById(R.id.cover_photo);
        editButton = (FloatingActionButton) findViewById(R.id.fab);
        postButton = (FloatingActionButton) findViewById(R.id.fab2);
        spotButton = (FloatingActionButton) findViewById(R.id.spot_button);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        userTagline = (TextView) findViewById(R.id.user_tag_line);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        postsProgressBar = (ProgressBar) findViewById(R.id.posts_progress_bar);
        searchBar = (FloatingSearchView) findViewById(R.id.floating_search_view);
        searchResultsFrame = (FrameLayout) findViewById(R.id.search_suggestions_frame);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        emptyCardView = (CardView) findViewById(R.id.card_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        viewButton = (FloatingActionButton) findViewById(R.id.view_button);
        noPostsTextView = (TextView) findViewById(R.id.no_posts_text_view);
        refreshButton = (FloatingActionButton) findViewById(R.id.refresh_button);
    }

    public void deletePostPicture(final View v) {

        setViewVisibility(v, false, true);
        setViewVisibility(postImage, false, true);
        setViewVisibility(addImageButton, true, true);

    }

    public void showPostDialog(final View v) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(ProfileActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.post_update))
                .setPositiveButton(getString(R.string.post), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_post_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) ProfileActivity.this.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_post_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();

        // Init the post_image ImageView which is set to View.GONE initially
        RelativeLayout rel = (RelativeLayout) dialog.findViewById(R.id.post_dialog_rel_layout);
        rel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });
        addImageButton = (Button) dialog.findViewById(R.id.add_photo_button);
        postImage = (ImageView) dialog.findViewById(R.id.post_image);
        postText = (EditText) dialog.findViewById(R.id.post_text);
        postImageDeleteButton = (ImageView) dialog.findViewById(R.id.post_image_delete_button);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(postText.getText().toString().trim())) {
                    // Perform saving of the post
                    new SavePost().execute(postText.getText().toString().trim());
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_empty_post), Toast.LENGTH_SHORT).show();
                }
            }
        });

        addImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPostPicture(v);
            }
        });

        postImageDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deletePostPicture(v);
            }
        });

    }

    public void setFrameLayoutWidth(int height) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        CoordinatorLayout.LayoutParams params = new CoordinatorLayout.LayoutParams(screenWidth, height);
        int[] attrs = new int[] {android.R.attr.actionBarSize};
        TypedArray ta = this.obtainStyledAttributes(attrs);
        int actionBarSize = ta.getDimensionPixelSize(0,0);
        ta.recycle();
        params.setMargins(0,actionBarSize,0,0);
        searchResultsFrame.setLayoutParams(params);
    }

    private void startSearch() {
        setViewVisibility(searchBar, true, true);
        searchBar.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.slide_down_top));
    }

    public static boolean hasLength(String str) {
        return (str != null && str.length() > 0);
    }

    public static boolean containsWhitespace(String str) {
        if (!hasLength(str)) {
            return false;
        }
        int strLen = str.length();
        for (int i = 0; i < strLen; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    private void showSearchResultsFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_suggestions_frame, searchResultsFragment).commit();
        if(searchResultsFrame.getVisibility() != View.VISIBLE) {
            setViewVisibility(searchResultsFrame, true, true);
            searchResultsFrame.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.slide_down_top));
        }
    }

    private void initSearchBar() {
        searchBar.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                setViewVisibility(searchBar, false, true);
                searchBar.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.slide_up_bottom));
                setViewVisibility(searchResultsFrame, false, true);
                searchResultsFrame.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.slide_up_bottom));
            }
        });

        searchBar.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                // Fetch from User either (FirstName + LastName) [or] (Username)
                // Depending on what the user inputs
                // This fetch and swap must be done in the background thread to not mess up UI thread
                // But first, we must hide the frameLayout if it is indeed shown
                setViewVisibility(searchResultsFrame, false, true);

                if(!TextUtils.isEmpty(newQuery.trim())){
                    if(!containsWhitespace(newQuery.trim())) {
                        new FetchSuggestions().execute(newQuery.trim());
                    }
                    else {
                        String[] names = newQuery.split("\\s+");
                        new FetchSuggestions().execute(names[0], names[1]);
                    }
                }
            }
        });

        // When a user hits the search button, the search results are loaded onto searchResultsFrame
        // This is a fragment that displays results in a clickable listview
        // Upon Click, the fragment launches a new activity, which is basically the profile of the other selected user
        // Fragment runs it's own AsyncTask based on search query to generate suggestions
        // This fetch is NOT handled by parent activity
        searchBar.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                Bundle bundle = new Bundle();
                bundle.putString("query", searchSuggestion.getBody().trim());
                searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment();
            }

            @Override
            public void onSearchAction(String currentQuery) {
                Bundle bundle = new Bundle();
                bundle.putString("query", currentQuery.trim());
                searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment();
            }
        });

        searchBar.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                View v = ProfileActivity.this.getCurrentFocus();
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                Bundle bundle = new Bundle();
                bundle.putString("query", searchBar.getQuery());
                searchResultsFragment = new UserResultsFragment();
                searchResultsFragment.setArguments(bundle);
                showSearchResultsFragment();

                if(searchResultsFrame.getVisibility() != View.VISIBLE) {
                    setViewVisibility(searchResultsFrame, true, true);
                    searchResultsFrame.startAnimation(AnimationUtils.loadAnimation(ProfileActivity.this, R.anim.slide_down_top));
                }
            }
        });
    }

    private void determineIfCurrentUserProfile() {
        if(intentUserName != null && !intentUserName.equals(ParseUser.getCurrentUser().getUsername())) {
            // There is a username in the calling intent
            // Init user info now based on the username
            // Requires the disabling of edit profile button(s), add post button
            // Also requires pictures to be non clickable
            IS_CURRENT_USER_PROFILE = false;
            editButton.setVisibility(View.GONE);
            postButton.setVisibility(View.GONE);
            spotButton.setVisibility(View.VISIBLE);
            profileImage.setClickable(false);
            coverImage.setClickable(false);

        }
        else {
            IS_CURRENT_USER_PROFILE = true;
        }
    }

    private void setUpRecycler() {
        news = new ArrayList<>();
        lm = new LinearLayoutManager(ProfileActivity.this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lm);
        rvAdapter = new NewsFeedAdapter(news, ProfileActivity.this, false);
        rvAdapter.setEmptyCardAndRecycler(recyclerView, emptyCardView);
        recyclerView.setAdapter(rvAdapter);
    }

    private void setUpFloatingActionButtons() {

        spotButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IS_FOLLOWING || IS_REQUESTED_TO_FOLLOW) {
                    spotButton.setImageResource(R.drawable.ic_spot);
                    IS_REQUESTED_TO_FOLLOW = false;
                    IS_FOLLOWING = false;
                    new RequestUserFollow().execute(false);
                }
                else {
                    spotButton.setImageResource(R.drawable.ic_request);
                    IS_REQUESTED_TO_FOLLOW = true;
                    // If this happens, currentUser != User being displayed over here FOR SURE
                    // This also means that the intentUserName variable is NOT NULL
                    // currentUser MUST now be following intentUserName
                    new RequestUserFollow().execute(true);
                }
            }
        });

        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startEditProfileActivity(true);
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IS_USER_CLEARED_TO_VIEW) {
                    startEditProfileActivity(false);
                }
                else {
                    Toast.makeText(ProfileActivity.this, getString(R.string.user_is_private), Toast.LENGTH_SHORT).show();
                }
            }
        });

        postButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPostDialog(v);
            }
        });

        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });
    }

    private void setUpAppBarBehaviour() {
        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if(state.toString().equals(COLLAPSED)) {
                    showTitleText(true);
                    if(!previousState.equals(COLLAPSED) && !previousState.equals(START)) {
                        invalidateOptionsMenu();
                    }
                    previousState = state.toString();
                }
                else if(state.toString().equals(EXPANDED)){
                    showTitleText(false);
                    if(!previousState.equals(EXPANDED) && !previousState.equals(START)) {
                        invalidateOptionsMenu();
                    }
                    previousState = state.toString();
                }
                else {
                    showTitleText(false);
                }

            }
        });
    }

    private void forceEnableSearchButton() {
        invalidateOptionsMenu();
    }

    private void setUpSwipeRefresh() {

        swipeRefreshLayout.setColorSchemeResources(R.color.app_theme);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchPosts(false).execute();
            }
        });
    }

    private void setUpToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    private void savePost(String caption, boolean isImageIncluded) throws ParseException, Exception {
        String username = ParseUser.getCurrentUser().getUsername();
        ParseObject newPost = new ParseObject(Constants.Post);
        newPost.put(Constants.username, username);
        newPost.put(Constants.caption, caption);
        newPost.put(Constants.numberComments, 0);
        newPost.put(Constants.numberLikes, 0);
        if(isImageIncluded) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            selectedPostPicture.compress(Bitmap.CompressFormat.JPEG, getResources().getInteger(R.integer.bitmap_compression), bytes);
            selectedPostPicture.compress(Bitmap.CompressFormat.PNG, getResources().getInteger(R.integer.bitmap_compression), stream);
            byte[] byte_data = stream.toByteArray();
            ParseFile image = new ParseFile(byte_data);
            newPost.put(Constants.image, image);
        }
        newPost.save();

    }

    private void checkIfUserIsBeingFollowed() throws ParseException, Exception {
        if(!IS_CURRENT_USER_PROFILE) {
            ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
            List<String> templist = new ArrayList<>();
            templist.add(intentUserName);
            query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            query.whereContainedIn(Constants.following, templist);
            List<ParseObject> result = query.find();
            if(result.size() == 0) {
                IS_FOLLOWING = false;
            }
            else {
                IS_FOLLOWING = true;
            }

            // Now checking if currentUser has requested to follow intentUsername
            ParseQuery<ParseObject> notQuery = new ParseQuery<ParseObject>(Constants.Notification);
            notQuery.whereEqualTo(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
            notQuery.whereEqualTo(Constants.receiverUsername, intentUserName);
            notQuery.whereEqualTo(Constants.postId, "");
            notQuery.whereEqualTo(Constants.type, getString(R.string.follow_request));
            notQuery.whereEqualTo(Constants.isActive, true);
            List<ParseObject> objects = notQuery.find();
            if(objects.isEmpty()) {
                IS_REQUESTED_TO_FOLLOW = false;
            }
            else {
                IS_REQUESTED_TO_FOLLOW = true;
            }
        }

    }

    private void updateImageForUser()  {
        final ParseFile image = ParseUser.getCurrentUser().getParseFile(Constants.profilePicture);
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Notification);
        query.whereEqualTo(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e == null) {
                    for (ParseObject object : objects) {
                        object.put(Constants.image, image);
                        object.saveInBackground();
                    }
                }
                else {
                    e.printStackTrace();

                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Quit immediately if network isn't available
        if(!LoginScreenActivity.isNetworkAvailable(ProfileActivity.this)) {
            Toast.makeText(ProfileActivity.this, getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        setContentView(R.layout.activity_profile);
        bindViews();

        setUpToolBar();

        initSearchBar();

        callingIntent = getIntent();
        intentUserName = callingIntent.getStringExtra("username");

        determineIfCurrentUserProfile();
        setUpRecycler();


//        OnResume takes care of this now
//        -----------------------------------
//        new InitUserInfo().execute();
//        new FetchPosts().execute();
//        -----------------------------------

        setUpFloatingActionButtons();
        setUpAppBarBehaviour();
        setUpSwipeRefresh();


    }

    @Override
    protected void onResume() {
        super.onResume();
        new InitUserInfo().execute();
        new FetchPosts(true).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);

        if(NO_POSTS_TO_DISPLAY) {
            menu.findItem(R.id.menu_search).setVisible(true);
            menu.findItem(R.id.action_edit).setVisible(false);
        }

        else {
            for(int i = 0; i < menu.size(); i++) {
                if(MENU_SHOWN) {
                    menu.getItem(i).setVisible(true);
                }
                else {
                    menu.getItem(i).setVisible(false);
                }
            }

            MENU_SHOWN = !MENU_SHOWN;

            if(intentUserName != null && !intentUserName.equals(ParseUser.getCurrentUser().getUsername())) {
                // Setting edit profile menu button to not be shown
                menu.findItem(R.id.action_edit).setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                startEditProfileActivity(true);
                return true;
            case R.id.menu_search:
                startSearch();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_IMAGE_CAPTURE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mayRequestWriteToStorage();
                }
                break;
            }
            case REQUEST_WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    cameraIntent();
                }
                break;
            }
            default: break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        UpdatePostDialogFragment fragment = (UpdatePostDialogFragment) getSupportFragmentManager().findFragmentByTag(UpdatePostDialogFragment.TAG);

        if(fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        else {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE || requestCode == REQUEST_IMAGE_CAPTURE) {
                    if (!IS_CAMERA_USED) {
                        selectedImageUri = data.getData();
                    }
                    if (IS_POST_PHOTO) {
                        startPostCropPictureActivity();
                    } else {
                        startProfileCropPictureActivity();
                    }
                } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    selectedImageUri = result.getUri();
                    if (IS_POST_PHOTO) {
                        onPostSelectedImageResult();
                    } else {
                        onSelectedImageResult();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                Exception error = result.getError();
                error.printStackTrace();
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        deleteCache(ProfileActivity.this);
    }

//    @Override
//    public void onBackPressed() {
//        Intent i;
//        if(IS_CURRENT_USER_PROFILE) {
////            super.onBackPressed();
//            i = new Intent(ProfileActivity.this, MainActivity.class);
//
//        }
//        else {
//            i = new Intent(ProfileActivity.this, ProfileActivity.class);
//        }
//        startActivity(i);
//        finish();
//    }

    // ---------------------------------------------
    // Tasks to do separate from the main UI thread
    // ---------------------------------------------

    private class InitUserInfo extends AsyncTask<Void, Void, Boolean> {

        public InitUserInfo() {}

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            IS_INIT_COMPLETE = false;
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                initHeadingText();
                initPictures();
                // Checking whether or not user is being followed by currentUser
                checkIfUserIsBeingFollowed();

                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                IS_INIT_COMPLETE = true;
                userNameBig.setText(titleText);
                userNameSmall.setText(titleText);
                userTagline.setText(tagLine);
                spottees.setText(String.format(getString(R.string.spots), noSpottees));
                spotters.setText(String.format(getString(R.string.spotted_by), noSpotters));
                displayImage(true);
                displayImage(false);
                showMainLayout(true);

                // Setting button based on follow status

                if(IS_FOLLOWING) {
                    spotButton.setImageResource(R.drawable.ic_spotting);
                }
                else {
                    if(IS_REQUESTED_TO_FOLLOW) {
                        spotButton.setImageResource(R.drawable.ic_request);
                    }
                    else {
                        spotButton.setImageResource(R.drawable.ic_spot);
                    }
                }

            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < RETRY_LIMIT) {
                    new InitUserInfo().execute();
                }
            }

        }
    }

    private class SavePicture extends AsyncTask<ParseFile, Void, Boolean> {

        @Override
        protected Boolean doInBackground(ParseFile... params) {
            ParseUser user = ParseUser.getCurrentUser();
            if(IS_PROFILE_PICTURE) {
                user.put(Constants.profilePicture, params[0]);
            }
            else {
                user.put(Constants.coverPicture, params[0]);
            }
            try {
                user.save();
                updateImageForUser();
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(IS_PROFILE_PICTURE) {
                    Snackbar snackbar =  Snackbar.make(profileImage, getString(R.string.profile_picture_update), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                    Toast.makeText(ProfileActivity.this, getString(R.string.profile_picture_update), Toast.LENGTH_SHORT).show();
                }
                else {
                    Snackbar snackbar =  Snackbar.make(coverImage, getString(R.string.cover_photo_update), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    View view = snackbar.getView();
                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                    Toast.makeText(ProfileActivity.this, getString(R.string.cover_photo_update), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
//                finish();
            }

        }
    }

    private class SavePost extends AsyncTask<String, Void, Boolean> {

        private boolean isImageIncluded = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(addImageButton.getVisibility() != View.VISIBLE) {
                isImageIncluded = true;
            }
        }

        @Override
        protected Boolean doInBackground(String... params) {

            String caption = params[0];

            try {
                savePost(caption, isImageIncluded);
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar snackbar =  Snackbar.make(postButton, getString(R.string.post_post_successful), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                Toast.makeText(ProfileActivity.this, getString(R.string.post_post_successful), Toast.LENGTH_SHORT).show();
                new FetchPosts(true).execute();
            }
            else {
//                Snackbar.make(postButton, getString(R.string.error_unexpected), Snackbar.LENGTH_LONG).show();
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class FetchPosts extends AsyncTask<Void, Void, Boolean> {

        boolean shouldShowProgressBar;

        public FetchPosts(boolean shouldShowProgressBar) {
            this.shouldShowProgressBar = shouldShowProgressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(IS_INIT_COMPLETE && shouldShowProgressBar) {
                setViewVisibility(postsProgressBar, true, true);
            }
            setViewVisibility(recyclerView, false, true);
            setViewVisibility(emptyCardView, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                populateNews();
                // Adding to adapters following list. Since this is user profile, only his posts need to be shown
                ArrayList<String> temp = new ArrayList<>();
                if(IS_CURRENT_USER_PROFILE) {
                    temp.add(ParseUser.getCurrentUser().getUsername());
                }
                else {
                    temp.add(intentUserName);
                }
                rvAdapter.setFollowingList(temp);
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            setViewVisibility(postsProgressBar, false, true);
            if(aBoolean) {
                rvAdapter.setPostsCount(ProfileActivity.this.getResources().getInteger(R.integer.post_query_init_limit));
                rvAdapter.notifyDataSetChanged();
                if(!news.isEmpty()) {
                    NO_POSTS_TO_DISPLAY = false;
                    setViewVisibility(emptyCardView, false, true);
                    setViewVisibility(recyclerView, true, true);
                }
                else {
                    NO_POSTS_TO_DISPLAY = true;
                    forceEnableSearchButton();
                    setViewVisibility(recyclerView, false, true);
                    setViewVisibility(emptyCardView, true, true);
                    if(IS_USER_CLEARED_TO_VIEW) {
                        noPostsTextView.setText(getString(R.string.no_posts_at_all));
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < RETRY_LIMIT) {
                    new FetchPosts(true).execute();
                }
            }
        }
    }

    // TODO : Edge case, duplicates arise if multiple people have the same names
    private class FetchSuggestions extends AsyncTask<String, Void, Boolean> {

        List<MySearchSuggestion> suggestions = new ArrayList<>();
        int maxNumberOfSuggestions = 5;

        private void addToSuggestions(List<ParseUser> users) {
            for(ParseUser user : users) {
                maxNumberOfSuggestions--;
                if(maxNumberOfSuggestions >= 0) {
                    suggestions.add(new MySearchSuggestion(user.getString(Constants.firstname) +
                            " " + user.getString(Constants.lastname)));
                }
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            searchBar.swapSuggestions(suggestions);
        }

        @Override
        protected Boolean doInBackground(String... params) {
            if(params.length == 1) {
                String userInput = params[0];
                ParseQuery<ParseUser> userNameQuery = ParseUser.getQuery();
                ParseQuery<ParseUser> firstNameQuery = ParseUser.getQuery();
                ParseQuery<ParseUser> lastNameQuery = ParseUser.getQuery();
                List<ParseUser> usernameQueryResult;
                List<ParseUser> firstNameQueryResult;
                List<ParseUser> lastNameQueryResult;
                try {
                    if(userInput.charAt(0) == '@') {
                        userInput = userInput.substring(1);
                        userNameQuery.whereContains(Constants.username, userInput);
                        userNameQuery.setLimit(ProfileActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        usernameQueryResult = userNameQuery.find();
                        addToSuggestions(usernameQueryResult);
                        if(usernameQueryResult.size() == 0) {
                            suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                        }
                    }
                    else {
                        // Maybe the user has input a firstName. Try this check
                        userInput = userInput.substring(0, 1).toUpperCase() + userInput.substring(1);
                        firstNameQuery.whereContains(Constants.firstname, userInput);
                        firstNameQuery.setLimit(ProfileActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        firstNameQueryResult = firstNameQuery.find();
                        addToSuggestions(firstNameQueryResult);

                        // Maybe user has input a last name. Try this check (Final one)
                        lastNameQuery.whereContains(Constants.lastname, userInput);
                        lastNameQuery.setLimit(ProfileActivity.this.getResources().getInteger(R.integer.suggestions_limit));
                        lastNameQueryResult = lastNameQuery.find();
                        addToSuggestions(lastNameQueryResult);

                        if (lastNameQueryResult.size() == 0 && firstNameQueryResult.size() == 0) {
                            suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                        }
                    }

                    return true;

                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else {
                String uInputFirstName = params[0];
                String uInputLastName = params[1];
                uInputFirstName = uInputFirstName.substring(0, 1).toUpperCase() + uInputFirstName.substring(1);
                uInputLastName = uInputLastName.substring(0, 1).toUpperCase() + uInputLastName.substring(1);
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereContains(Constants.firstname, uInputFirstName);
                query.whereContains(Constants.lastname, uInputLastName);
                try {
                    List<ParseUser> result = query.find();
                    if(result.size() == 0) {
                        suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                    }
                    else {
                        addToSuggestions(result);
                    }
                    return true;
                } catch (ParseException e) {
                    e.printStackTrace();
                    return false;
                }

            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                searchBar.swapSuggestions(suggestions);
            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class RequestUserFollow extends AsyncTask<Boolean, Void, Boolean> {

        boolean isBeingFollowed;

        @Override
        protected Boolean doInBackground(Boolean... params) {
            // find user object in "Follow", if not found, create one "now"
            // CURRENT USER follows intentUserName
            try {
                isBeingFollowed = params[0];
                if(isBeingFollowed) {
                    requestToFollowUser();
                }
                else {
                    removeFromFollowingList();
                }

                // RE-checking clearances based on recent change to follow status
                if(!isUserProfileClearedToView()) {
                    IS_USER_CLEARED_TO_VIEW = false;
                }
                else {
                    IS_USER_CLEARED_TO_VIEW = true;
                }
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(isBeingFollowed) {
                    Snackbar.make(spotButton, getString(R.string.follow_request_success), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new RequestUserFollow().execute(!isBeingFollowed);
                                }
                            }).show();
                    IS_REQUESTED_TO_FOLLOW = true;
//                    noSpotters++;

//                    Toast.makeText(ProfileActivity.this, getString(R.string.follow_success), Toast.LENGTH_SHORT).show();
                }
                else {
                    Snackbar.make(spotButton, getString(R.string.unfollow_success), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new RequestUserFollow().execute(!isBeingFollowed);
                                }
                            }).show();
                    IS_REQUESTED_TO_FOLLOW = false;
                    if(noSpotters != 0) {
                        noSpotters--;
                    }
//                    Toast.makeText(ProfileActivity.this, getString(R.string.unfollow_success), Toast.LENGTH_SHORT).show();
                }

                spotters.setText(String.format(getString(R.string.spotted_by), noSpotters));
                if(IS_REQUESTED_TO_FOLLOW) {
                    spotButton.setImageResource(R.drawable.ic_request);
                }
                else {
                    spotButton.setImageResource(R.drawable.ic_spot);
                }

            }
            else {
                Toast.makeText(ProfileActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
