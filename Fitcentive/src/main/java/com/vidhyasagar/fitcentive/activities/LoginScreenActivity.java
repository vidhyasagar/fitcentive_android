package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.parse.Constants;


import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginScreenActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {
    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private AutoCompleteTextView mUsernameView;
    private TextInputLayout emailLayout;
    private TextInputLayout usernameLayout;

    private EditText mPasswordView;
    private View mProgressView;
    private LinearLayout mLoginFormView;
    private TextView forgotPassword;
    private Button createNewAccount;
    private RelativeLayout relativeLayout;
    private InputMethodManager imm;
    private Switch aSwitch;
    private TextView loginChoice, emailChoice, usernameChoice;
    Button mEmailSignInButton;

    private boolean isLoginViaUsername = false;

    private void startMainActivity() {
        Intent i = new Intent(LoginScreenActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    private void bindViews() {
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);
        loginChoice = (TextView) findViewById(R.id.login_choice);
        emailChoice = (TextView) findViewById(R.id.emailTextView);
        usernameChoice = (TextView) findViewById(R.id.usernameTextView);
        aSwitch = (Switch) findViewById(R.id.switch1);
        mPasswordView = (EditText) findViewById(R.id.password);
        mLoginFormView = (LinearLayout) findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        forgotPassword = (TextView) findViewById(R.id.forgot_password);
        createNewAccount = (Button) findViewById(R.id.create_new_account_button);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        emailLayout = (TextInputLayout) findViewById(R.id.email_layout);
        usernameLayout = (TextInputLayout) findViewById(R.id.username_layout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // If a user has already signed in, then just move on directly to the main screen
        if(ParseUser.getCurrentUser() != null && isNetworkAvailable(LoginScreenActivity.this)) {
            startMainActivity();
            finish();
            return;
        }

        bindViews();
        populateAutoComplete();

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        // To make sure keyboard is hidden during launch
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // On touch listener to hide keyboard when user presses anywhere on screen
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        mEmailView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // Perform action on Enter key press
                    mEmailView.clearFocus();
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });

        mUsernameView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_UP) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER))
                {
                    // Perform action on Enter key press
                    mUsernameView.clearFocus();
                    mPasswordView.requestFocus();
                    return true;
                }
                return false;
            }
        });

        aSwitch.setChecked(false);
        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    String temp = mEmailView.getText().toString();
                    emailLayout.setVisibility(View.GONE);
                    usernameLayout.setVisibility(View.VISIBLE);
                    mUsernameView.setText(temp);
                    mUsernameView.requestFocus();
                    isLoginViaUsername = true;
                }
                else {
                    String temp = mUsernameView.getText().toString();
                    usernameLayout.setVisibility(View.GONE);
                    emailLayout.setVisibility(View.VISIBLE);
                    mEmailView.setText(temp);
                    mEmailView.requestFocus();
                    isLoginViaUsername = false;
                }
            }
        });

    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    private Boolean checkIfEmailIsWrong(String email) {
        ParseQuery<ParseUser> queryUsers = ParseUser.getQuery();
        queryUsers.whereEqualTo(Constants.email, email);
        try{
            return (queryUsers.count() != 0) ? false : true;
        } catch(ParseException e) {
            return false;
        }

    }

    private Boolean checkIfUsernameIsWrong(String username) {
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo(Constants.username, username);
        try{
            return (query.count() != 0) ? false : true;
        } catch(ParseException e) {
            return false;
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        if(!isNetworkAvailable(LoginScreenActivity.this)) {
            Toast.makeText(LoginScreenActivity.this, getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
            return;
        }

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString().trim();
        final String username = mUsernameView.getText().toString().trim();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }
        else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if(!isLoginViaUsername) {
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }
        }
        else {
            //Check for valid username
            if (TextUtils.isEmpty(username)) {
                mUsernameView.setError(getString(R.string.error_field_required));
                focusView = mUsernameView;
                cancel = true;
            }
            if (username.length() < 6) {
                mUsernameView.setError(getString(R.string.error_username_too_small));
                focusView = mUsernameView;
                cancel = true;
            }
        }


        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            if(!isLoginViaUsername) {
                attemptLoginViaEmail(email, password);
            }
            else {
                attemptLoginViaUsername(username, password);
            }

        }
    }

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void attemptLoginViaUsername(final String username, final String password) {
        ParseUser.logInInBackground(username, password, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException e) {
                AlertDialog.Builder builder;
                if (user == null) {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(LoginScreenActivity.this);
                        builder.setTitle(getString(R.string.error_incorrect_credentials))
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        showProgress(false);

                                    }
                                });
                    } else {
                        builder = new AlertDialog.Builder(LoginScreenActivity.this);
                        builder.setTitle(R.string.error_incorrect_credentials)
                                .setIcon(getDrawable(R.drawable.ic_logo))
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        showProgress(false);

                                    }
                                });
                    }
                    Boolean isUsernameWrong = checkIfUsernameIsWrong(username);
                    if (isUsernameWrong) {
                        builder.setMessage(getString(R.string.wrong_username_error_message));
                    } else {
                        builder.setMessage(getString(R.string.wrong_password_error_message));
                    }
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    // Login success
                    Toast.makeText(LoginScreenActivity.this, getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                    startMainActivity();
                }
            }
        });
    }

    // TODO: Refactor alertdialog here
    private void attemptLoginViaEmail(final String email, final String password) {
        ParseQuery<ParseUser> usernameQuery = ParseUser.getQuery();
        usernameQuery.whereEqualTo(Constants.email, email);
        usernameQuery.findInBackground(new FindCallback<ParseUser>() {
            @Override
            public void done(List<ParseUser> objects, ParseException e) {
                if(objects.size() != 0) {
                    String userName = objects.get(0).getUsername();
                    ParseUser.logInInBackground(userName, password, new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException e) {
                            AlertDialog.Builder builder;
                            if (user == null) {
                                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(LoginScreenActivity.this);
                                    builder.setTitle(getString(R.string.error_incorrect_credentials))
                                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    showProgress(false);

                                                }
                                            });
                                } else {
                                    builder = new AlertDialog.Builder(LoginScreenActivity.this);
                                    builder.setTitle(R.string.error_incorrect_credentials)
                                            .setIcon(getDrawable(R.drawable.ic_logo))
                                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    showProgress(false);

                                                }
                                            });
                                }
                                Boolean isEmailWrong = checkIfEmailIsWrong(email);
                                if (isEmailWrong) {
                                    builder.setMessage(getString(R.string.wrong_email_error_message));
                                } else {
                                    builder.setMessage(getString(R.string.wrong_password_error_message));
                                }
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            } else {
                                // Login success
                                Toast.makeText(LoginScreenActivity.this, getString(R.string.login_success), Toast.LENGTH_SHORT).show();
                                startMainActivity();
                            }
                        }
                    });
                }
                else {
                    AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(LoginScreenActivity.this);
                            builder.setTitle(getString(R.string.error_incorrect_credentials))
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            showProgress(false);

                                        }
                                    });
                        } else {
                            builder = new AlertDialog.Builder(LoginScreenActivity.this);
                            builder.setTitle(R.string.error_incorrect_credentials)
                                    .setIcon(getDrawable(R.drawable.ic_logo))
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            showProgress(false);

                                        }
                                    });
                        }
                        builder.setMessage(getString(R.string.wrong_email_error_message));
                        AlertDialog dialog = builder.create();
                        dialog.show();
                }
            }
        });
    }

    private boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 6;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
            forgotPassword.setVisibility(show ? View.GONE : View.VISIBLE);
            createNewAccount.setVisibility(show? View.GONE : View.VISIBLE);
            loginChoice.setVisibility(show? View.GONE : View.VISIBLE);
            emailChoice.setVisibility(show? View.GONE : View.VISIBLE);
            usernameChoice.setVisibility(show? View.GONE : View.VISIBLE);
            aSwitch.setVisibility(show? View.GONE : View.VISIBLE);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            forgotPassword.setVisibility(show ? View.GONE : View.VISIBLE);
            createNewAccount.setVisibility(show? View.GONE : View.VISIBLE);
            loginChoice.setVisibility(show? View.GONE : View.VISIBLE);
            emailChoice.setVisibility(show? View.GONE : View.VISIBLE);
            usernameChoice.setVisibility(show? View.GONE : View.VISIBLE);
            aSwitch.setVisibility(show? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginScreenActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    public void createNewAccount(View v) {
        if(isNetworkAvailable(LoginScreenActivity.this)) {
            Intent i = new Intent(LoginScreenActivity.this, CreateAccountActivity.class);
            startActivity(i);
        }
        else {
            Toast.makeText(LoginScreenActivity.this, getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
        }
    }

    public void forgotPassword(View v) {
        if(isNetworkAvailable(LoginScreenActivity.this)) {
            Intent i = new Intent(LoginScreenActivity.this, ForgotPasswordActivity.class);
            startActivity(i);
        }
        else {
            Toast.makeText(LoginScreenActivity.this, getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
        }
    }

}

