package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.gigamole.infinitecycleviewpager.HorizontalInfiniteCycleViewPager;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DetailedFoodSearchResultsFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.FoodResultsInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class ExpandedSearchFoodActivity extends AppCompatActivity {

    public static String RESULT_TYPE_STRING = "RESULT_TYPE";
    public static String ENTRY_TYPE_STRING = "ENTRY_TYPE";
    public static String CURRENT_ITEM_STRING = "CURRENT_ITEM";
    public static String RESULT_LIST = "RESULT_LIST";
    public static String FOOD_ID = "FOOD_ID";
    public static String IS_PART_OF_VIEWPAGER = "IS_PART_OF_VIEWPAGER";
    public static int MAX_RESULTS_FROM_API_CALL = 50;

    int INFINITE_PAGE_COUNT = 5000000;
    int CURRENT_ITEM = -1;
    int RETRY_COUNT = 0;
    int dayOffset;
    long chosenServingId;
    double chosenNumberServings;

    boolean isReturningToCreateMealActivity;

    SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType;
    DiaryPageFragment.ENTRY_TYPE entryType;
    ArrayList<FoodResultsInfo> resultList;

    Toolbar toolbar;
    FloatingSearchView floatingSearchView;
    HorizontalInfiniteCycleViewPager viewPager;
    InfiniteAdapter adapter;
    FragmentManager fragmentManager;
    TextView servingSize;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_search_food);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        setUpViewPager();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expanded_search_food, menu);

        MenuItem typeItem = menu.findItem(R.id.action_change_type);
        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        typeItem.setTitle(String.format(getString(R.string.entry_type), Utilities.getEntryTypeString(entryType, this)));
        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(dayOffset, this)));

        if(isReturningToCreateMealActivity) {
            typeItem.setVisible(false);
            dateItem.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_food:
                addCurrentFood();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            case R.id.action_change_type:
                showTypeDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), viewPager);
        }
        else {
            if(mMonth == month) {
                dayOffset = day - mDay;
            }
            else {
                dayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, dayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        invalidateOptionsMenu();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTypeDialog() {
        CharSequence[] array = getResources().getStringArray(R.array.entry_types);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.select_entry_type))
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        invalidateOptionsMenu();
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(array, Utilities.getCheckedItem(entryType), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                entryType = DiaryPageFragment.ENTRY_TYPE.BREAKFAST;
                                break;
                            case 1:
                                entryType = DiaryPageFragment.ENTRY_TYPE.LUNCH;
                                break;
                            case 2:
                                entryType = DiaryPageFragment.ENTRY_TYPE.DINNER;
                                break;
                            case 3:
                                entryType = DiaryPageFragment.ENTRY_TYPE.SNACKS;
                                break;
                        }
                        invalidateOptionsMenu();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void setChosenServingId(long id) {
        this.chosenServingId = id;
    }

    public void setChosenNumberServings(double no) {
        this.chosenNumberServings = no;
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(ExpandedSearchFoodActivity.this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        startActivity(i);
        finish();
    }

    private void returnToMealActivity(long foodId) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.foodId, foodId);
        resultIntent.putExtra(Constants.foodNumberOfServings, this.chosenNumberServings);
        resultIntent.putExtra(Constants.foodServingIds, this.chosenServingId);
        setResult(CreateOrEditMealActivity.RESULT_FOOD, resultIntent);
        finish();
    }

    public void addCurrentFood() {
        DetailedFoodSearchResultsFragment fragment = adapter.getFragment(CURRENT_ITEM);
        fragment.sendDataToParentActivity(true);
        new AddFoodToDatabaseTemporarily(resultList.get(CURRENT_ITEM).getFoodId()).execute();
    }

    private void retrieveArguments() {
        this.dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        this.entryType = (DiaryPageFragment.ENTRY_TYPE) getIntent().getExtras().getSerializable(ENTRY_TYPE_STRING);
        this.resultType = (SearchFoodResultsFragment.RESULT_TYPE_ENUM) getIntent().getExtras().getSerializable(RESULT_TYPE_STRING);
        this.resultList = getIntent().getExtras().getParcelableArrayList(RESULT_LIST);
        this.CURRENT_ITEM = getIntent().getExtras().getInt(CURRENT_ITEM_STRING);
        this.isReturningToCreateMealActivity = getIntent().getExtras().getBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL);
    }

    private void bindViews() {
        viewPager = (HorizontalInfiniteCycleViewPager) findViewById(R.id.viewpager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        floatingSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        servingSize = (TextView) findViewById(R.id.number_of_servings);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        fragmentManager = getSupportFragmentManager();
    }

    public void setUpViewPager() {
        adapter = new InfiniteAdapter(fragmentManager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(CURRENT_ITEM);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                CURRENT_ITEM = position - INFINITE_PAGE_COUNT;
                if(CURRENT_ITEM > MAX_RESULTS_FROM_API_CALL - 1) {
                    CURRENT_ITEM = CURRENT_ITEM - MAX_RESULTS_FROM_API_CALL;
                }
                if(CURRENT_ITEM < 0) {
                    CURRENT_ITEM = MAX_RESULTS_FROM_API_CALL + CURRENT_ITEM;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setUpToolbar() {
        // Setting toolbar
        switch (resultType) {
            case TYPE_ALL:
                toolbar.setTitle(getString(R.string.all_foods));
                break;
            case TYPE_RECENT:
                toolbar.setTitle(getString(R.string.recent_foods));
                break;
            case TYPE_FREQUENT:
                toolbar.setTitle(getString(R.string.frequent_foods));
                break;
            case TYPE_FAVORITES:
                toolbar.setTitle(getString(R.string.favorite_foods));
                break;
            case TYPE_RECIPES:
                toolbar.setTitle(getString(R.string.created_recipes));
                break;
            case TYPE_MEALS:
                toolbar.setTitle(getString(R.string.created_meals));
                break;
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }

    private boolean isPresentInArray(JSONArray array, long recipeId) {
        int size = array.length();
        try {
            for (int i = 0; i < size; i++) {
                JSONObject object = array.getJSONObject(i);
                if(recipeId == object.optLong(Constants.foodId, -1)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isPresentInFavorites(JSONObject object, long foodId) {
        try {
            Object temp = object.opt(Constants.food);
            if(temp instanceof JSONObject) {
                if(foodId ==  ((JSONObject) temp).optLong(Constants.foodId, -1)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return isPresentInArray((JSONArray) temp, foodId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean getWhetherFavoriteOrNot(long foodId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, getString(R.string.food));
        query.whereEqualTo(Constants.foodIdNumber, foodId);

        try {
            ParseObject object = query.getFirst();
            return object.getBoolean(Constants.isFavorite);
        } catch (ParseException e) {
            FatSecretFoods foods = new FatSecretFoods();
            JSONObject object = foods.getFavoriteFoods();
            return isPresentInFavorites(object, foodId);
        }
    }

    private void addFoodToDatabaseTemporarily(long foodId) throws Exception {
        ParseObject newObject = new ParseObject(Constants.FoodDiary);
        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.entryType, getString(R.string.food));
        newObject.put(Constants.foodIdNumber, foodId);
        switch (entryType) {
            case BREAKFAST:
                newObject.put(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                newObject.put(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                newObject.put(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                newObject.put(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                newObject.put(Constants.mealType, getString(R.string.water));
                break;
        }
        newObject.put(Constants.servingIdField, this.chosenServingId);
        newObject.put(Constants.numberServings, this.chosenNumberServings);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        // Must also put in the isFavorite field here
        // Rules : If another food with same foodId exists in the database, set it to that
        //         If not, set it to false
        newObject.put(Constants.isFavorite, getWhetherFavoriteOrNot(foodId));
        newObject.save();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private void showMainLayout(boolean show) {
        setViewVisibility(progressBar, !show, true);
        setViewVisibility(viewPager, show, true);
    }

    public void goPreviousViewpagerPage() {
        viewPager.setCurrentItem(CURRENT_ITEM - 1);
    }

    public void goNextViewpagerPage() {
        viewPager.setCurrentItem(CURRENT_ITEM + 1);
    }

    //-------------------------------
    // ADAPTER(S)
    //-------------------------------

    public class InfiniteAdapter extends FragmentStatePagerAdapter {

        HashMap<Integer, DetailedFoodSearchResultsFragment> mPageReferenceMap;

        public InfiniteAdapter(FragmentManager fm) {
            super(fm);
            mPageReferenceMap = new HashMap<>();
        }

        public DetailedFoodSearchResultsFragment getFragment(int key) {
            return mPageReferenceMap.get(key);
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public Fragment getItem(int position) {
            DetailedFoodSearchResultsFragment fragment = DetailedFoodSearchResultsFragment.newInstance(resultList.get(position).getFoodId(), true);
            mPageReferenceMap.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            mPageReferenceMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            DetailedFoodSearchResultsFragment fragment = (DetailedFoodSearchResultsFragment) super.instantiateItem(container,
                    position);
            mPageReferenceMap.put(position, fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return resultList.size();
        }
    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class AddFoodToDatabaseTemporarily extends AsyncTask<Void, Void, Boolean> {

        private long foodId;

        public AddFoodToDatabaseTemporarily(long foodId) {
            this.foodId = foodId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isReturningToCreateMealActivity) {
                    addFoodToDatabaseTemporarily(foodId);
                }
                return true;
            } catch (Exception e ){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                if(!isReturningToCreateMealActivity) {
                    returnToDiaryPage();
                }
                else {
                    returnToMealActivity(foodId);
                }
            }
            else {
                Toast.makeText(ExpandedSearchFoodActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new AddFoodToDatabaseTemporarily(foodId).execute();
                }
            }
        }
    }

}
