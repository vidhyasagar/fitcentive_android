package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.create_food_fragments.FoodBasicInfoFragment;
import com.vidhyasagar.fitcentive.create_food_fragments.FoodNutritionFragment;
import com.vidhyasagar.fitcentive.parse.Constants;

import java.util.List;

public class CreateOrEditFoodActivity extends AppCompatActivity {

    public static int CREATE_FOOD = 19;
    public static int NO_OF_TABS = 2 ;

    boolean isEditMode;
    String createdFoodObjectId;

    String foodName, foodDescription, foodBrand, foodServingSize, foodServingSizeUnit;
    double calories, carbs, protein, fat;
    double saturatedFat;
    double polyUnsaturatedFat;
    double monoUnsaturatedFat;
    double transFat;
    double cholestrol;
    double sodium;
    double potassium;
    double fiber;
    double sugar;
    double vitamin_a;
    double vitamin_c;
    double calcium;
    double iron;

    FragmentManager manager;
    CreateFoodAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_food);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        hideKeyboard();

        if(isEditMode) {
            new LoadCreatedFood().execute();
        }
        else {
            setUpViewPager();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_food, menu);

        if(isEditMode) {
            menu.findItem(R.id.action_delete).setVisible(true);
        }
        else {
            menu.findItem(R.id.action_delete).setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                validateAndSaveCreatedFood();
                return true;
            case R.id.action_delete:
                createDeleteConfirmationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createDeleteConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(CreateOrEditFoodActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.delete_created_food))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new DeleteCurrentCreatedFoodObject().execute();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.delete_created_food_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewPager.getWindowToken(), 0);
    }

    private void retrieveArguments() {
        isEditMode = getIntent().getExtras().getBoolean(ExpandedRecipeActivity.IS_EDITING, false);
        if(isEditMode) {
            createdFoodObjectId = getIntent().getExtras().getString(Constants.objectId);
            invalidateOptionsMenu();
        }
    }

    private void handleExistingErrors() {
        if(!((FoodBasicInfoFragment)adapter.getFragment(0)).validateData()) {
            viewPager.setCurrentItem(0);
            FoodBasicInfoFragment.BASIC_ERROR_TYPE type = ((FoodBasicInfoFragment)adapter.getFragment(0)).getErrorType();
            String errorMessage = null;
            switch (type) {
                case TYPE_NAME:
                    errorMessage = getString(R.string.food_error_name);
                    break;
                case TYPE_SERVING_SIZE:
                    errorMessage = getString(R.string.food_error_serving);
                    break;
                case TYPE_SERVING_UNIT:
                    errorMessage = getString(R.string.food_error_unit);
                    break;
                case TYPE_DESCIPTION:
                    errorMessage = getString(R.string.food_error_description);
                default:
                    break;
            }
            ((FoodBasicInfoFragment)adapter.getFragment(0)).makeSnackbarMessage(errorMessage);
        }
        else {
            viewPager.setCurrentItem(1);
            FoodNutritionFragment.NUTRITION_ERROR_TYPE type = ((FoodNutritionFragment)adapter.getFragment(1)).getErrorType();
            String errorMessage = null;
            switch (type) {
                case TYPE_CALORIES:
                    errorMessage = getString(R.string.food_error_calories);
                    break;
                case TYPE_CARBS:
                    errorMessage = getString(R.string.food_error_carbs);
                    break;
                case TYPE_PROTEINS:
                    errorMessage = getString(R.string.food_error_proteins);
                    break;
                case TYPE_FATS:
                    errorMessage = getString(R.string.food_error_fats);
                default:
                    break;
            }
            ((FoodNutritionFragment)adapter.getFragment(1)).makeSnackbarMessage(errorMessage);
        }
    }

    private void deleteCurrentCreatedFood() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedFood);
        query.get(createdFoodObjectId).delete();

        ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query1.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query1.whereEqualTo(Constants.entryType, getString(R.string.created_food));
        query1.whereEqualTo(Constants.createdFoodObjectId, createdFoodObjectId);

        List<ParseObject> result = query1.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void validateAndSaveCreatedFood() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewPager.getWindowToken(), 0);

        if(((FoodBasicInfoFragment)adapter.getFragment(0)).validateData() &&
                ((FoodNutritionFragment)adapter.getFragment(1)).validateData()) {
            ((FoodBasicInfoFragment)adapter.getFragment(0)).sendDataToParent();
            ((FoodNutritionFragment)adapter.getFragment(1)).sendDataToParent();
            new SaveCreatedFood().execute();
        }
        else {
            handleExistingErrors();
        }
    }

    public void sendAcrossData(String foodName, String foodBrand, String foodDescription, String foodServingSize, String foodServingSizeUnit) {
        this.foodBrand = foodBrand;
        this.foodDescription = foodDescription;
        this.foodName = foodName;
        this.foodServingSize = foodServingSize;
        this.foodServingSizeUnit = foodServingSizeUnit;
    }

    public void sendAcrossBasicData(double calories, double fat, double protein, double carbs) {
        this.calories = calories;
        this.fat = fat;
        this.protein = protein;
        this.carbs = carbs;
    }

    public void sendAcrossDetailedData(double saturatedFat, double polyUnsaturatedFat, double monoUnsaturatedFat, double transFat,
                                       double cholestrol, double potassium, double sodium, double fiber, double sugar, double vitamin_a,
                                       double vitamin_c, double calcium, double iron) {

        this.saturatedFat = saturatedFat;
        this.polyUnsaturatedFat = polyUnsaturatedFat;
        this.monoUnsaturatedFat = monoUnsaturatedFat;
        this.transFat = transFat;
        this.potassium = potassium;
        this.sodium = sodium;
        this.calcium = calcium;
        this.iron = iron;
        this.sugar = sugar;
        this.fiber = fiber;
        this.cholestrol = cholestrol;
        this.vitamin_a = vitamin_a;
        this.vitamin_c = vitamin_c;
    }


    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
    }

    private void setUpToolbar() {
        if(isEditMode) {
            toolbar.setTitle(getString(R.string.edit_created_food));
        }
        else {
            toolbar.setTitle(getString(R.string.create_food));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void updateCreatedFood() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedFood);
        ParseObject food = query.get(createdFoodObjectId);

        food.put(Constants.foodNameField, this.foodName);
        food.put(Constants.foodBrand, this.foodBrand);
        food.put(Constants.foodDescriptionField, this.foodDescription);
        food.put(Constants.servingSizeField, Double.valueOf(this.foodServingSize));
        food.put(Constants.servingSizeUnit, this.foodServingSizeUnit);

        food.put(Constants.caloriesField, this.calories);
        food.put(Constants.fatsField, this.fat);
        food.put(Constants.proteinsField, this.protein);
        food.put(Constants.carbsField, this.carbs);

        food.put(Constants.polyFats, this.polyUnsaturatedFat);
        food.put(Constants.monoFats, this.monoUnsaturatedFat);
        food.put(Constants.satFats, this.saturatedFat);
        food.put(Constants.transFats, this.transFat);

        food.put(Constants.cholesterolField, this.cholestrol);
        food.put(Constants.sugar, this.sugar);
        food.put(Constants.fiber, this.fiber);
        food.put(Constants.sodium, this.sodium);
        food.put(Constants.potassium, this.potassium);
        food.put(Constants.calcium, this.calcium);
        food.put(Constants.iron, this.iron);
        food.put(Constants.vitaminA, this.vitamin_a);
        food.put(Constants.vitaminC, this.vitamin_c);

        food.save();
    }

    private void saveCreatedFood() throws Exception {
        ParseObject newFood = new ParseObject(Constants.CreatedFood);
        newFood.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newFood.put(Constants.foodNameField, this.foodName);
        newFood.put(Constants.foodBrand, this.foodBrand);
        newFood.put(Constants.foodDescriptionField, this.foodDescription);
        newFood.put(Constants.servingSizeField, Double.valueOf(this.foodServingSize));
        newFood.put(Constants.servingSizeUnit, this.foodServingSizeUnit);

        newFood.put(Constants.caloriesField, this.calories);
        newFood.put(Constants.fatsField, this.fat);
        newFood.put(Constants.proteinsField, this.protein);
        newFood.put(Constants.carbsField, this.carbs);

        newFood.put(Constants.polyFats, this.polyUnsaturatedFat);
        newFood.put(Constants.monoFats, this.monoUnsaturatedFat);
        newFood.put(Constants.satFats, this.saturatedFat);
        newFood.put(Constants.transFats, this.transFat);

        newFood.put(Constants.cholesterolField, this.cholestrol);
        newFood.put(Constants.sugar, this.sugar);
        newFood.put(Constants.fiber, this.fiber);
        newFood.put(Constants.sodium, this.sodium);
        newFood.put(Constants.potassium, this.potassium);
        newFood.put(Constants.calcium, this.calcium);
        newFood.put(Constants.iron, this.iron);
        newFood.put(Constants.vitaminA, this.vitamin_a);
        newFood.put(Constants.vitaminC, this.vitamin_c);

        newFood.save();

    }

    private void setUpViewPager() {
        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new CreateFoodAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }


    public void goToNextFragment() {
        viewPager.setCurrentItem(1);
    }

    public void goToPreviousFragment() {
        viewPager.setCurrentItem(0);
    }

    private void loadCreatedFoodInfo() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedFood);
        ParseObject food = query.get(createdFoodObjectId);

        foodName = food.getString(Constants.foodNameField);
        foodDescription = food.getString(Constants.foodDescriptionField);
        foodBrand = food.getString(Constants.foodBrand);
        foodServingSize = String.valueOf(food.getDouble(Constants.servingSizeField));
        foodServingSizeUnit = food.getString(Constants.servingSizeUnit);

        calories = food.getDouble(Constants.caloriesField);
        fat = food.getDouble(Constants.fatsField);
        protein = food.getDouble(Constants.proteinsField);
        carbs = food.getDouble(Constants.carbsField);

        polyUnsaturatedFat = food.getDouble(Constants.polyFats);
        monoUnsaturatedFat = food.getDouble(Constants.monoFats);
        transFat = food.getDouble(Constants.transFats);
        saturatedFat = food.getDouble(Constants.satFats);

        cholestrol = food.getDouble(Constants.cholesterolField);
        sugar = food.getDouble(Constants.sugar);
        fiber = food.getDouble(Constants.fiber);
        sodium = food.getDouble(Constants.sodium);
        potassium = food.getDouble(Constants.potassium);
        calcium = food.getDouble(Constants.calcium);
        iron = food.getDouble(Constants.iron);
        vitamin_a = food.getDouble(Constants.vitaminA);
        vitamin_c = food.getDouble(Constants.vitaminC);
    }

    private void showMainLayout(boolean show) {
        setViewVisibility(tabLayout, show, true);
        setViewVisibility(viewPager, show, true);
        setViewVisibility(progressBar, !show, true);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class CreateFoodAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        public CreateFoodAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    if(isEditMode) {
                        fragment = FoodBasicInfoFragment.newInstance(foodName, foodBrand, foodDescription,
                                                                    foodServingSize, foodServingSizeUnit);
                    }
                    else {
                        fragment = FoodBasicInfoFragment.newInstance();
                    }
                    break;
                case 1:
                    if(isEditMode) {
                        fragment = FoodNutritionFragment.newInstance(String.valueOf(calories), String.valueOf(protein),
                                String.valueOf(fat), String.valueOf(carbs), String.valueOf(polyUnsaturatedFat),
                                String.valueOf(monoUnsaturatedFat), String.valueOf(saturatedFat), String.valueOf(transFat),
                                String.valueOf(cholestrol), String.valueOf(sugar), String.valueOf(fiber),
                                String.valueOf(sodium), String.valueOf(potassium), String.valueOf(calcium),
                                String.valueOf(iron), String.valueOf(vitamin_a), String.valueOf(vitamin_c));
                    }
                    else {
                        fragment = FoodNutritionFragment.newInstance();
                    }
                    break;
                default:
                    fragment = null;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.basic);
                case 1:
                    return getString(R.string.nutrition);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }
    }

    private class SaveCreatedFood extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(isEditMode) {
                    updateCreatedFood();
                }
                else {
                    saveCreatedFood();
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(isEditMode) {
                    Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.updated_food_successfully), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.saved_food_successfully), Toast.LENGTH_SHORT).show();
                }
                setResult(RESULT_OK);
                finish();
            }
            else {
                Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LoadCreatedFood extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadCreatedFoodInfo();
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                setUpViewPager();
            }
            else {
                Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeleteCurrentCreatedFoodObject extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deleteCurrentCreatedFood();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.food_deleted_successfully), Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }
            else {
                Toast.makeText(CreateOrEditFoodActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }

    }
}
