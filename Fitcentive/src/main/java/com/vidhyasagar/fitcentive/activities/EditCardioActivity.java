package com.vidhyasagar.fitcentive.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_exercise_fragments.CardioFragment;
import com.vidhyasagar.fitcentive.search_exercise_fragments.ExerciseResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;

public class EditCardioActivity extends AppCompatActivity {

    public static final String createdCardioExerciseName = "CREATED_CARDIO_NAME";

    boolean isFillUpModeFromCreatedCardios;

    CardioInfo info;
    int dayOffset;

    boolean isCreated;
    boolean isReturningToWorkoutFragment;

    String createdCardioName;
    String createdCardioObjectId;
    double createdCardioMet;

    Toolbar toolbar;
    FrameLayout frameLayout;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_cardio);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        setUpEditFragment();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_edit_cardio_activity, menu);

        if(!isCreated) {
            MenuItem item = menu.findItem(R.id.action_edit);
            item.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        if(item.getItemId() == R.id.action_edit) {
            startEditCreatedCardioExerciseActivity();
        }
        return super.onOptionsItemSelected(item);
    }

    private void retrieveArguments() {
        info = getIntent().getExtras().getParcelable(ExpandedExerciseActivity.INFO);
        dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        isReturningToWorkoutFragment = getIntent().getExtras().getBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, false);
        // THIS BOOLEAN MEANS ITS A CREATED EXERCISE THAT IS GONNA BE EDITED, AKA A DIARY ENTRY OF A CREATED CARDIO EXERCISE
        isCreated = getIntent().getExtras().getBoolean(Constants.isCreated);
        createdCardioName = getIntent().getExtras().getString(createdCardioExerciseName, Constants.unknown);
        createdCardioObjectId = getIntent().getExtras().getString(CardioFragment.CREATED_CARDIO_OBJECT_ID, Constants.unknown);
        createdCardioMet = getIntent().getExtras().getDouble(CardioFragment.CREATED_CARDIO_MET, -1);
        if(createdCardioName.equals(Constants.unknown)) {
            isFillUpModeFromCreatedCardios = false;
        }
        else {
            isFillUpModeFromCreatedCardios = true;
            // This means info is null
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
        fragmentManager = getSupportFragmentManager();
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(isFillUpModeFromCreatedCardios) {
            getSupportActionBar().setTitle(getString(R.string.cardio));
        }
        else {
            getSupportActionBar().setTitle(getString(R.string.edit_cardio_entry));
        }
    }

    private void setUpEditFragment() {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(isFillUpModeFromCreatedCardios) {
            fragmentTransaction.replace(R.id.frame_layout,
                    CardioFragment.newInstance(dayOffset, createdCardioName, createdCardioMet, true,
                            createdCardioObjectId, isReturningToWorkoutFragment), CardioFragment.TAG).commit();
        }
        else {
            fragmentTransaction.replace(R.id.frame_layout, CardioFragment.newInstance(dayOffset, info, isCreated, isReturningToWorkoutFragment), CardioFragment.TAG).commit();
        }
    }

    private void startEditCreatedCardioExerciseActivity() {
        Intent i = new Intent(this, CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_EDIT);
        i.putExtra(Constants.isCardio, true);
        if(isFillUpModeFromCreatedCardios) {
            i.putExtra(Constants.createdExerciseObjectId, createdCardioObjectId);
        }
        else {
            i.putExtra(Constants.createdExerciseObjectId, info.getCreatedCardioObjectId());
        }
        startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

}
