package com.vidhyasagar.fitcentive.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.create_exercise_fragments.BasicCreatedExerciseFragment;
import com.vidhyasagar.fitcentive.create_exercise_fragments.DetailedCreatedExerciseFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.FitnessFormulae;
import com.vidhyasagar.fitcentive.utilities.Utilities;

import java.util.ArrayList;
import java.util.List;

public class CreateOrEditExerciseActivity extends AppCompatActivity {

    public static final int RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING = 23;

    private static final int NO_OF_TABS = 2;
    public static final int CATEGORY_OFFSET = 8;
    public static final int NORMAL_OFFSET = 1;

    CreateOrEditMealActivity.MODE mode;
    int dayOffset;
    boolean isCardio;
    String createdExerciseObjectId;

    FragmentManager manager;
    CreatedExerciseAdapter adapter;

    ProgressBar progressBar;
    TabLayout tabLayout;
    ViewPager viewPager;
    Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_or_edit_exercise);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        setUpViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_exercise_activity, menu);

        if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false);
        }
        else {
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                validateAndSave();
                return true;
            case R.id.action_delete:
                createDeleteConfirmationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createDeleteConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(CreateOrEditExerciseActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.delete_created_exercise))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new DeleteCurrentExerciseObject().execute();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.delete_exercise_confirmation));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void retrieveArguments() {
        dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        mode = (CreateOrEditMealActivity.MODE) getIntent().getExtras().getSerializable(ExpandedRecipeActivity.MODE_TYPE);
        isCardio = getIntent().getExtras().getBoolean(Constants.isCardio);
        if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
            createdExerciseObjectId = getIntent().getExtras().getString(Constants.createdExerciseObjectId);
        }
        invalidateOptionsMenu();
    }

    private void bindViews() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
    }


    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        switch (mode) {
            case MODE_CREATE:
                getSupportActionBar().setTitle(getString(R.string.create_new_exercise));
                break;
            case MODE_EDIT:
                getSupportActionBar().setTitle(getString(R.string.edit_created_exercise));
                break;
            case MODE_VIEW:
                getSupportActionBar().setTitle(getString(R.string.view_created_exercise));
        }
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(viewPager.getWindowToken(), 0);
    }

    private void setUpViewPager() {
        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new CreatedExerciseAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    public void goToNextFragment() {
        viewPager.setCurrentItem(1);
    }

    public void goToPreviousFragment() {
        viewPager.setCurrentItem(0);
    }

    public void updateDetailsFragmentName(String name) {
        DetailedCreatedExerciseFragment fragment = (DetailedCreatedExerciseFragment) adapter.getFragment(1);
        fragment.updateNameOnScreen(name);
    }

    public void setStrengthLayout(boolean isStrength) {
        DetailedCreatedExerciseFragment fragment = (DetailedCreatedExerciseFragment) adapter.getFragment(1);
        fragment.setLayout(isStrength);
    }

    private int applyCategoryOffset(int num) {
        return num + CATEGORY_OFFSET;
    }

    private ArrayList<Integer> applyOffset(ArrayList<Integer> numbers) {
        ArrayList<Integer> augmentedList = new ArrayList<>();
        for(Integer num : numbers) {
            augmentedList.add(num + NORMAL_OFFSET);
        }
        return augmentedList;
    }

    private void validateAndSave() {
        hideKeyboard();
        if(((BasicCreatedExerciseFragment) adapter.getFragment(0)).isValid() && ((DetailedCreatedExerciseFragment) adapter.getFragment(1)).isValid()) {
            // Proceed to saving
            boolean isStrength = ((BasicCreatedExerciseFragment) adapter.getFragment(0)).getIsStrength();

            if(isStrength) {
                int selectedCategory = applyCategoryOffset(((DetailedCreatedExerciseFragment) adapter.getFragment(1)).getSelectedCategory());
                ArrayList<Integer> selectedEquipment = applyOffset(((DetailedCreatedExerciseFragment) adapter.getFragment(1)).getSelectedEquipment());
                ArrayList<Integer> selectedPrimaryMuscles = applyOffset(((DetailedCreatedExerciseFragment) adapter.getFragment(1)).getSelectedPrimaryMuscles());
                ArrayList<Integer> selectedSecondaryMuscles = applyOffset(((DetailedCreatedExerciseFragment) adapter.getFragment(1)).getSelectedSecondaryMuscles());
                String exerciseName = ((BasicCreatedExerciseFragment) adapter.getFragment(0)).getExerciseName();
                String exerciseDescription = ((BasicCreatedExerciseFragment) adapter.getFragment(0)).getExerciseDescription();
                // Now you have all data, all that is left to do is to actually save the created workout
                new SaveCreatedExercise(exerciseName, exerciseDescription, selectedCategory, selectedEquipment, selectedPrimaryMuscles, selectedSecondaryMuscles).execute();
            }
            else {
                double metValue = ((DetailedCreatedExerciseFragment) adapter.getFragment(1)).getMetValue();
                String exerciseName = ((BasicCreatedExerciseFragment) adapter.getFragment(0)).getExerciseName();
                new SaveCreatedExercise(exerciseName, metValue).execute();
            }
        }
        else {
            if(!((BasicCreatedExerciseFragment) adapter.getFragment(0)).isValid()) {
                viewPager.setCurrentItem(0);
                ((BasicCreatedExerciseFragment) adapter.getFragment(0)).handleErrors();
            }
            else {
                viewPager.setCurrentItem(1);
                ((DetailedCreatedExerciseFragment) adapter.getFragment(1)).handleErrors();
            }
        }
    }

    // TODO: Weight must be pipelined in from USERPROFILE
    private double getCaloriesBurned(double metValue, double minutes) {
        double weightInPounds = 180;
        double calories = FitnessFormulae.getCaloriesBurned(weightInPounds, metValue, minutes);
        return (Math.round(calories * 100.0) / 100.0);
    }

    private void editCreatedCardioExercise(String exerciseName, double metValue) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);

        object.put(Constants.exerciseName, exerciseName);
        object.put(Constants.metValue, metValue);
        object.save();

        // ALSO MUST CHANGE EXISTING NAME IN ALL EXERCISE DIARY ENTRIES AND UPDATE CALORIES BURNED VALUE TO MATCH NEW MET-VALUE
        ParseQuery<ParseObject> fixQuery = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        fixQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        fixQuery.whereEqualTo(Constants.isCreated, true);
        fixQuery.whereEqualTo(Constants.exerciseType, getString(R.string.cardio_type));
        fixQuery.whereEqualTo(Constants.createdExerciseObjectId, createdExerciseObjectId);

        List<ParseObject> result = fixQuery.find();

        for(ParseObject res : result) {
            res.put(Constants.exerciseName, exerciseName);
            res.put(Constants.caloriesBurned, getCaloriesBurned(metValue, res.getDouble(Constants.minutesPerformed)));
            res.save();
        }


        // ALSO MUST CHANGE NAME IN ALL WORKOUT CARDIO COMPONENTS
        ParseQuery<ParseObject> workoutFixQuery = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        workoutFixQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        workoutFixQuery.whereEqualTo(Constants.isCreated, true);
        workoutFixQuery.whereEqualTo(Constants.createdExerciseObjectId, createdExerciseObjectId);

        List<ParseObject> cardioResult = workoutFixQuery.find();

        for(ParseObject res : cardioResult) {
            res.put(Constants.exerciseName, exerciseName);
            res.save();
        }
    }

    private void saveCreatedCardioExercise(String exerciseName, double metValue) throws Exception {
        ParseObject object = new ParseObject(Constants.CreatedExercise);
        object.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        object.put(Constants.exerciseName, exerciseName);
        object.put(Constants.metValue, metValue);
        object.put(Constants.isCardio, true);
        object.save();
    }

    private void editCreatedStrengthExercise(String exerciseName, String exerciseDescription, int selectedCategory,
                                             ArrayList<Integer> selectedEquipment, ArrayList<Integer> selectedPrimaryMuscles,
                                             ArrayList<Integer> selectedSecondaryMuscles) throws Exception {

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);

        object.put(Constants.exerciseName, exerciseName);
        object.put(Constants.exerciseDescription, exerciseDescription);
        object.put(Constants.category, selectedCategory);
        if(selectedEquipment.isEmpty()) {
            selectedEquipment.add(-1);
        }
        if(selectedPrimaryMuscles.isEmpty()) {
            selectedPrimaryMuscles.add(-1);
        }
        if(selectedSecondaryMuscles.isEmpty()) {
            selectedSecondaryMuscles.add(-1);
        }
        object.put(Constants.equipmentList, selectedEquipment);
        object.put(Constants.primaryMusclesList, selectedPrimaryMuscles);
        object.put(Constants.secondaryMusclesList, selectedSecondaryMuscles);
        object.save();


        // ALSO MUST CHANGE EXISTING NAME IN ALL EXERCISE DIARY ENTRIES AND UPDATE CALORIES BURNED VALUE TO MATCH NEW MET-VALUE
        ParseQuery<ParseObject> fixQuery = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        fixQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        fixQuery.whereEqualTo(Constants.isCreated, true);
        fixQuery.whereEqualTo(Constants.exerciseType, getString(R.string.weights_type));
        fixQuery.whereEqualTo(Constants.createdExerciseObjectId, createdExerciseObjectId);

        List<ParseObject> result = fixQuery.find();

        for(ParseObject res : result) {
            res.put(Constants.exerciseName, exerciseName);
            res.save();
        }

        // ALSO MUST CHANGE NAME IN ALL WORKOUT STRENGHT COMPONENTS
        ParseQuery<ParseObject> workoutFixQuery = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        workoutFixQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        workoutFixQuery.whereEqualTo(Constants.isCreated, true);
        workoutFixQuery.whereEqualTo(Constants.createdExerciseObjectId, createdExerciseObjectId);

        List<ParseObject> strengthResult = workoutFixQuery.find();

        for(ParseObject res : strengthResult) {
            res.put(Constants.exerciseName, exerciseName);
            res.save();
        }
    }

    private void saveCreatedStrengthExercise(String exerciseName, String exerciseDescription, int selectedCategory,
                                             ArrayList<Integer> selectedEquipment, ArrayList<Integer> selectedPrimaryMuscles,
                                             ArrayList<Integer> selectedSecondaryMuscles) throws Exception {

        ParseObject object = new ParseObject(Constants.CreatedExercise);
        object.put(Constants.username, ParseUser.getCurrentUser().getUsername());

        object.put(Constants.exerciseName, exerciseName);
        object.put(Constants.exerciseDescription, exerciseDescription);
        object.put(Constants.category, selectedCategory);
        if(selectedEquipment.isEmpty()) {
            selectedEquipment.add(-1);
        }
        if(selectedPrimaryMuscles.isEmpty()) {
            selectedPrimaryMuscles.add(-1);
        }
        if(selectedSecondaryMuscles.isEmpty()) {
            selectedSecondaryMuscles.add(-1);
        }
        object.put(Constants.equipmentList, selectedEquipment);
        object.put(Constants.primaryMusclesList, selectedPrimaryMuscles);
        object.put(Constants.secondaryMusclesList, selectedSecondaryMuscles);
        object.put(Constants.isCardio, false);
        object.save();
    }

    private void showMainLayout(boolean show) {
        Utilities.setViewVisibility(tabLayout, show, true, this);
        Utilities.setViewVisibility(viewPager, show, true, this);
        Utilities.setViewVisibility(progressBar, !show, true, this);
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        startActivity(i);
        finish();
    }

    private void deleteCurrentExerciseObject() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        ParseObject object = query.get(createdExerciseObjectId);
        object.delete();

        // Now proceeding to delete all diary entries which have THIS createdExercise in it
        ParseQuery<ParseObject> deleteQuery = new ParseQuery<>(Constants.ExerciseDiary);
        deleteQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        deleteQuery.whereEqualTo(Constants.isCreated, true);
        deleteQuery.whereEqualTo(Constants.createdExerciseObjectId, createdExerciseObjectId);

        List<ParseObject> objects = deleteQuery.find();

        for(ParseObject temp : objects) {
            temp.delete();
        }
    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class CreatedExerciseAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        public CreatedExerciseAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                        fragment = BasicCreatedExerciseFragment.newInstance(isCardio);
                    }
                    else if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                        fragment = BasicCreatedExerciseFragment.newInstance(isCardio, createdExerciseObjectId);
                    }
                    else {
                        // This statement is there in case we decide to make future changes to existing design
                        fragment = null;
                    }
                    break;
                case 1:
                    if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                        fragment = DetailedCreatedExerciseFragment.newInstance(isCardio);
                    }
                    else if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                        fragment = DetailedCreatedExerciseFragment.newInstance(isCardio, createdExerciseObjectId);
                    }
                    else {
                        fragment = null;
                    }
                    break;
                default:
                    fragment = null;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.basic_info);
                case 1:
                    return getString(R.string.detailed_info);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }
    }

    //----------------------------------
    // ASYNCTASK(S)
    //----------------------------------

    private class SaveCreatedExercise extends AsyncTask<Void, Void, Boolean> {

        String exName, exDesc;
        int selectedCategory;
        double metValue;
        boolean isCardio;
        ArrayList<Integer> selectedEquipment, selectedPrimaryMuscles, selectedSecondaryMuscles;

        public SaveCreatedExercise(String exName, String exDesc, int selectedCategory,
                                   ArrayList<Integer> selectedEquipment, ArrayList<Integer> selectedPrimaryMuscles,
                                   ArrayList<Integer> selectedSecondaryMuscles) {
            this.exName = exName;
            this.exDesc = exDesc;
            this.selectedCategory = selectedCategory;
            this.selectedEquipment = selectedEquipment;
            this.selectedPrimaryMuscles = selectedPrimaryMuscles;
            this.selectedSecondaryMuscles = selectedSecondaryMuscles;
            isCardio = false;
        }

        public SaveCreatedExercise(String exName, double metValue) {
            this.exName = exName;
            this.metValue = metValue;
            isCardio = true;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(mode == CreateOrEditMealActivity.MODE.MODE_CREATE) {
                    if (isCardio) {
                        saveCreatedCardioExercise(exName, metValue);
                    } else {
                        saveCreatedStrengthExercise(exName, exDesc, selectedCategory, selectedEquipment, selectedPrimaryMuscles, selectedSecondaryMuscles);
                    }
                }
                else if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                    if(isCardio) {
                        editCreatedCardioExercise(exName, metValue);
                    }
                    else {
                        editCreatedStrengthExercise(exName, exDesc, selectedCategory, selectedEquipment, selectedPrimaryMuscles, selectedSecondaryMuscles);
                    }
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                    returnToDiaryPage();
                }
                else {
                    setResult(RESULT_OK);
                    finish();
                }
            }
            else {
                Toast.makeText(CreateOrEditExerciseActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeleteCurrentExerciseObject extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deleteCurrentExerciseObject();
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(CreateOrEditExerciseActivity.this, getString(R.string.exercise_deleted_successfully), Toast.LENGTH_SHORT).show();
                if(mode == CreateOrEditMealActivity.MODE.MODE_EDIT) {
                    returnToDiaryPage();
                }
                else {
                    setResult(RESULT_OK);
                    finish();
                }
            }
            else {
                Toast.makeText(CreateOrEditExerciseActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
