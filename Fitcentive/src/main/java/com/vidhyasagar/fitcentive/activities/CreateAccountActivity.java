package com.vidhyasagar.fitcentive.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.create_account_fragments.BirthdayFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.GenderFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.NameFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.PasswordFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.EmailFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.TermsFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.UsernameFragment;
import com.vidhyasagar.fitcentive.create_account_fragments.WelcomeFragment;
import com.vidhyasagar.fitcentive.parse.Constants;


// TODO : Make sure lack of internet does not set off an inifinite loop and thus, a crash
public class CreateAccountActivity extends AppCompatActivity {

    int CURRENT_FRAGMENT = 0;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    FrameLayout frameLayout;
    RelativeLayout relativeLayout;
    InputMethodManager imm;
    ProgressBar progressBar;

    String email;
    String username;
    String password;
    String firstName;
    String lastName;
    int birthday_day, birthday_month, birthday_year;
    boolean isMale;

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setUsername(String newUsername) {
        this.username = newUsername;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }

    public void setName(String newFirstName, String newLastName) {
        this.firstName = newFirstName;
        this.lastName = newLastName;
    }

    public void setBirthday(int day, int month, int year) {
        this.birthday_day = day;
        this.birthday_month = month;
        this.birthday_year = year;
    }

    public void setGender(boolean gender) {
        this.isMale = gender;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        relativeLayout = (RelativeLayout) findViewById(R.id.create_account_relative_layout);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, new WelcomeFragment()).commit();

        // Setting action bar theme
        android.support.v7.app.ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(CreateAccountActivity.this, R.color.app_theme)));
    }

    public void signUpNewUser() {
        ParseUser user = new ParseUser();
        user.setUsername(this.username);
        user.setPassword(this.password);
        user.setEmail(this.email);
        user.put(Constants.birthdayDay, this.birthday_day);
        user.put(Constants.birthdayMonth, this.birthday_month);
        user.put(Constants.birthdayYear, this.birthday_year);
        user.put(Constants.isMale, this.isMale);
        user.put(Constants.firstname, this.firstName);
        user.put(Constants.lastname, this.lastName);
        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                } else {
                    Toast.makeText(CreateAccountActivity.this, getString(R.string.sign_up_success), Toast.LENGTH_SHORT).show();
                }
            }
        });
        Intent i = new Intent(CreateAccountActivity.this, LoginScreenActivity.class);
        startActivity(i);
        finish();
    }

    public void replaceNextFragment(View v) {
        switch (CURRENT_FRAGMENT) {
            case 0:
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                    fragmentTransaction.replace(R.id.frameLayout, new UsernameFragment(), UsernameFragment.TAG).commit();
                    CURRENT_FRAGMENT++;
                    break;
            case 1:
                    UsernameFragment usernameFragment = (UsernameFragment) fragmentManager.findFragmentByTag(UsernameFragment.TAG);
                    if(usernameFragment.isUsernameValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new EmailFragment(), EmailFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;
            case 2:
                    EmailFragment emailFragment = (EmailFragment) fragmentManager.findFragmentByTag(EmailFragment.TAG);
                    if(emailFragment.isEmailValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new NameFragment(), NameFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;
            case 3:
                    NameFragment nameFragment = (NameFragment) fragmentManager.findFragmentByTag(NameFragment.TAG);
                    if(nameFragment.isNameValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new BirthdayFragment(), BirthdayFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;
            case 4:
                    BirthdayFragment birthdayFragment = (BirthdayFragment) fragmentManager.findFragmentByTag(BirthdayFragment.TAG);
                    if(birthdayFragment.isBirthdayValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new GenderFragment(), GenderFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;
            case 5:
                    GenderFragment genderFragment = (GenderFragment) fragmentManager.findFragmentByTag(GenderFragment.TAG);
                    if(genderFragment.isGenderValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new PasswordFragment(), PasswordFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;
            case 6:
                    PasswordFragment passwordFragment = (PasswordFragment) fragmentManager.findFragmentByTag(PasswordFragment.TAG);
                    if(passwordFragment.isPasswordValid()) {
                        fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
                        fragmentTransaction.replace(R.id.frameLayout, new TermsFragment(), TermsFragment.TAG).commit();
                        CURRENT_FRAGMENT++;
                    }
                    break;

            default:
                    // TODO : Add logic to call parse cloud code to send credentials across and facilitate email verification
                    if(isNetworkAvailable()) {
                        signUpNewUser();
                    }
                    else {
                        Toast.makeText(CreateAccountActivity.this, getString(R.string.error_no_network_connection), Toast.LENGTH_SHORT).show();
                    }
                    break;
        }
    }

    public void createConfirmationDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(CreateAccountActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.cancel_account_creation))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        CreateAccountActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setMessage(getString(R.string.cancel_account_creation_desc));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                createConfirmationDialog();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        createConfirmationDialog();
    }
}

