package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.Double2;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;

public class AddWaterActivity extends AppCompatActivity {

    public static String CUPS = "CUPS";

    int dayOffset;
    String objectId;
    double retrievedWaterAmount;
    boolean isEditMode;

    Toolbar toolbar;
    EditText cupsOfWater;
    Button addToDiaryButton;
    Button plus1Cup, plus2Cup, plus4Cup, plus8Cup;
    RelativeLayout mainLayout;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_water);

        retrieveArguments();
        bindViews();
        setUpToolBar();
        setUpPlusButtons();
        setUpAddToDiaryButton();
        setUpMainLayoutKeyboardHide();
        loadDataIfNeeded();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_expanded_search_food, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_food:
                validateAndReturn();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpMainLayoutKeyboardHide(){
        mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(v, AddWaterActivity.this);
            }
        });
    }

    private void loadDataIfNeeded() {
        if(isEditMode) {
            new LoadObject().execute();
        }
    }

    private void retrieveArguments() {
        this.dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        this.objectId = getIntent().getExtras().getString(Constants.objectId, Constants.unknown);

        if(objectId.equals(Constants.unknown)) {
            isEditMode = false;
        }
        else {
            isEditMode = true;
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cupsOfWater = (EditText) findViewById(R.id.water_edittext);
        addToDiaryButton = (Button) findViewById(R.id.add_water_button);
        plus1Cup = (Button) findViewById(R.id.plus_one);
        plus2Cup = (Button) findViewById(R.id.plus_two);
        plus4Cup = (Button) findViewById(R.id.plus_four);
        plus8Cup = (Button) findViewById(R.id.plus_eight);
        mainLayout = (RelativeLayout) findViewById(R.id.water_relative_layout);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void setUpToolBar() {
        toolbar.setTitle(getString(R.string.add_water));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpPlusButtons() {
        plus1Cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToEditText(1);
            }
        });
        plus2Cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToEditText(2);
            }
        });
        plus4Cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToEditText(4);
            }
        });
        plus8Cup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToEditText(8);
            }
        });
    }

    private void addToEditText(int toAdd) {
        if(cupsOfWater.getText().toString().isEmpty()) {
            cupsOfWater.setText(String.valueOf(toAdd));
        }
        else {
            cupsOfWater.setText(String.valueOf(Double.valueOf(cupsOfWater.getText().toString()) + toAdd));
        }

        cupsOfWater.setSelection(cupsOfWater.getText().length());
    }

    private void setUpAddToDiaryButton() {
        addToDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateAndReturn();
            }
        });
        if(isEditMode) {
            addToDiaryButton.setText(getString(R.string.save));
        }
        else {
            addToDiaryButton.setText(getString(R.string.add_to_diary));
        }
    }

    private void validateAndReturn() {
        if(cupsOfWater.getText().toString().trim().isEmpty() || Double.valueOf(cupsOfWater.getText().toString()) == 0) {
            Snackbar snackbar = Snackbar.make(addToDiaryButton,
                    getString(R.string.add_valid_amount), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            new AddWaterToDatabaseTemporarily().execute();
        }
    }

    private void editWaterEntry() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.WaterDiary);
        ParseObject object = query.get(objectId);

        object.put(Constants.cupsOfWater, Double.valueOf(cupsOfWater.getText().toString()));

        object.save();
    }

    private void addToWaterDatabaseTemporarily() throws Exception {
        ParseObject water = new ParseObject(Constants.WaterDiary);
        water.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        water.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        water.put(Constants.cupsOfWater, Double.valueOf(cupsOfWater.getText().toString()));
        water.put(Constants.isPermanent, false);

        water.save();
    }

    private void loadObject() throws Exception{
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.WaterDiary);
        ParseObject object = query.get(objectId);
        retrievedWaterAmount = object.getDouble(Constants.cupsOfWater);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }
    }

    //-----------------------------------
    // ASYNCTASK(S)           
    //-----------------------------------

    private class AddWaterToDatabaseTemporarily extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(isEditMode) {
                    editWaterEntry();
                }
                else {
                    addToWaterDatabaseTemporarily();
                }
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar snackbar = Snackbar.make(addToDiaryButton,
                        getString(R.string.added_to_diary), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);
                finish();
            }
            else {
                Toast.makeText(AddWaterActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class LoadObject extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(mainLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadObject();
                return true;
            }
            catch(Exception e){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean){
                setViewVisibility(mainLayout, true, true);
                setViewVisibility(progressBar, false, true);
                cupsOfWater.setText(String.valueOf(retrievedWaterAmount));
                cupsOfWater.setSelection(cupsOfWater.getText().length());
            }
            else {
                Toast.makeText(AddWaterActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
