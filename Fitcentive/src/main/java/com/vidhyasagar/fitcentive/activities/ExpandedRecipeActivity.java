package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipeNutritionFragment;
import com.vidhyasagar.fitcentive.detailed_recipe_fragments.RecipePreparationFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.Ingredients;
import com.vidhyasagar.fitcentive.wrappers.ServingInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ExpandedRecipeActivity extends AppCompatActivity {

    public static String RECIPE_OBJECT_STRING = "RECIPE_OBJECT_STRING";
    public static String MODE_TYPE = "MODE_TYPE";
    public static String IS_EDITING = "IS_EDITING";

    TabLayout tabLayout;
    ViewPager viewPager;
    public static int NO_OF_TABS = 2 ;

    long recipeId;
    int RETRY_COUNT = 0;
    int dayOffset;
    double selectNumberServings;
    String mealType;

    boolean isEditMode;
    DiaryPageFragment.ENTRY_TYPE entryType;

    boolean isReturningToCreateMealActivity;
    boolean isPartOfCreatedMeal;

    FatSecretRecipes recipes;
    DetailedRecipeResultsInfo recipeInfo;

    FragmentManager manager;
    RecipeAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_recipe);

        retrieveArguments();
        bindViews();
        setUpToolbar();

//        if(recipeInfo == null) {
            new FetchRecipeInfo().execute();
//        }
//        else {
//            setUpViewPager();
//        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
             case android.R.id.home:
                 onBackPressed();
                 return true;
             default:
                 return super.onOptionsItemSelected(item);
        }
    }

    private void retrieveArguments() {
        this.recipeId = getIntent().getExtras().getLong(Constants.recipeId, -1);
//        if(recipeId == -1 ) {
//            this.recipeInfo = getIntent().getExtras().getParcelable(RECIPE_OBJECT_STRING);
//        }
        this.dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        this.mealType = getIntent().getExtras().getString(Constants.mealType);
        this.selectNumberServings = getIntent().getExtras().getDouble(Constants.numberServings, 1);
        this.isEditMode = getIntent().getExtras().getBoolean(IS_EDITING, false);
        this.isReturningToCreateMealActivity = getIntent().getExtras().getBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL);
        this.entryType = (DiaryPageFragment.ENTRY_TYPE) getIntent().getExtras().getSerializable(Constants.type);
        this.isPartOfCreatedMeal = getIntent().getExtras().getBoolean(CreateOrEditMealActivity.IS_PART_OF_MEAL, false);
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        recipes = new FatSecretRecipes();
        recipeInfo = new DetailedRecipeResultsInfo();
    }

    private void setUpToolbar() {
        if(!isEditMode) {
            toolbar.setTitle(getString(R.string.recipe_info));
        }
        else {
            toolbar.setTitle(getString(R.string.edit_recipe_entry));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpViewPager() {
        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new RecipeAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }


    //------------------------
    //TODO : DRY this code up


    private ArrayList<Ingredients> fetchIngredientsData(JSONArray array) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            Ingredients temp = new Ingredients();
            temp.setFoodId(object.optLong(Constants.foodId, -1));
            temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
            temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
            temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
            temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
            temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
            temp.setServingId(object.optLong(Constants.servingId, -1));
            ingredientsList.add(temp);
        }
        return ingredientsList;
    }

    private ArrayList<Ingredients> fetchIngredientsData(JSONObject object) throws JSONException {
        ArrayList<Ingredients> ingredientsList = new ArrayList<>();
        Ingredients temp = new Ingredients();
        temp.setFoodId(object.optLong(Constants.foodId, -1));
        temp.setFoodName(object.optString(Constants.foodName, Constants.unknown));
        temp.setIngredientDescription(object.optString(Constants.ingredientDescription, Constants.unknown));
        temp.setIngredientUrl(object.optString(Constants.ingredientUrl, Constants.unknown));
        temp.setMeasurementDescription(object.optString(Constants.measurementDescription, Constants.unknown));
        temp.setNumberOfUnits(object.optDouble(Constants.numberOfUnits, -1));
        temp.setServingId(object.optLong(Constants.servingId, -1));

        ingredientsList.add(temp);
        return ingredientsList;
    }

    private void fetchIngredients(JSONObject ingredients) throws JSONException{
        if(ingredients == null) {
            return;
        }
        ArrayList<Ingredients> ingredientsList;
        Object temp = ingredients.get(Constants.ingredient);
        if(temp instanceof JSONObject) {
            ingredientsList = fetchIngredientsData((JSONObject) temp);
        }
        else {
            ingredientsList = fetchIngredientsData((JSONArray) temp);
        }
        recipeInfo.setIngredientsList(ingredientsList);
    }

    private ArrayList<String> fetchDirectionsData(JSONArray array) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject object = array.getJSONObject(i);
            String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
            directionsList.add(directionDescription);
        }
        return directionsList;
    }

    private ArrayList<String> fetchDirectionsData(JSONObject object) throws JSONException {
        ArrayList<String> directionsList = new ArrayList<>();
        String directionDescription = object.optString(Constants.directionDescription, Constants.unknown);
        directionsList.add(directionDescription);
        return directionsList;
    }

    private void fetchDirections(JSONObject direction) throws JSONException{
        if(direction == null) {
            return;
        }
        ArrayList<String> directionsList;
        Object temp = direction.get(Constants.direction);
        if(temp instanceof JSONObject) {
            // There is only one direction here
            directionsList = fetchDirectionsData((JSONObject) temp);
        }
        else {
            // There are multiple directions here, extract it from the JSONArray
            directionsList = fetchDirectionsData((JSONArray) temp);
        }
        recipeInfo.setDirections(directionsList);
    }

    private ArrayList<ServingInfo> extractServingData(JSONArray array) throws JSONException {
        ArrayList<ServingInfo> list = new ArrayList<>();
        int size = array.length();
        for(int i = 0; i < size; i++) {
            JSONObject result = array.getJSONObject(i);
            ServingInfo newServingInfo = new ServingInfo();
            newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
            newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
            newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
            newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
            newServingInfo.setFat(result.optDouble(Constants.fat, -1));
            newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
            newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
            newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
            newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
            newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
            newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
            newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
            newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
            newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
            newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
            newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
            newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
            newServingInfo.setIron(result.optDouble(Constants.iron, -1));
            list.add(newServingInfo);
        }
        return list;
    }

    private ArrayList<ServingInfo> extractServingData(JSONObject result) {
        ArrayList<ServingInfo> list = new ArrayList<>();
        ServingInfo newServingInfo = new ServingInfo();
        newServingInfo.setServingSize(result.optString(Constants.servingSize, Constants.unknown));
        newServingInfo.setCalories(result.optDouble(Constants.calories, -1));
        newServingInfo.setCarbs(result.optDouble(Constants.carbs, -1));
        newServingInfo.setProtein(result.optDouble(Constants.protein, -1));
        newServingInfo.setFat(result.optDouble(Constants.fat, -1));
        newServingInfo.setSaturatedFat(result.optDouble(Constants.saturatedFat, -1));
        newServingInfo.setPolyUnsaturatedFat(result.optDouble(Constants.polyUnsaturatedFat, -1));
        newServingInfo.setMonoUnsaturatedFat(result.optDouble(Constants.monoUnsaturatedFat, -1));
        newServingInfo.setTransFat(result.optDouble(Constants.transFat, -1));
        newServingInfo.setCholestrol(result.optDouble(Constants.cholestrol, -1));
        newServingInfo.setSodium(result.optDouble(Constants.sodium, -1));
        newServingInfo.setPotassium(result.optDouble(Constants.potassium, -1));
        newServingInfo.setFiber(result.optDouble(Constants.fiber, -1));
        newServingInfo.setSugar(result.optDouble(Constants.sugar, -1));
        newServingInfo.setVitamin_a(result.optDouble(Constants.vitamin_a, -1));
        newServingInfo.setVitamin_c(result.optDouble(Constants.vitamin_c, -1));
        newServingInfo.setCalcium(result.optDouble(Constants.calcium, -1));
        newServingInfo.setIron(result.optDouble(Constants.iron, -1));
        list.add(newServingInfo);
        return list;
    }

    private void fetchServingSizeInfo(JSONObject servings) throws JSONException{
        if(servings == null) {
            return;
        }

        ArrayList<ServingInfo> servingsList;
        Object temp = servings.get(Constants.serving);
        if(temp instanceof JSONObject) {
            servingsList = extractServingData((JSONObject) temp);
        }
        else {
            servingsList = extractServingData((JSONArray) temp);
        }

        recipeInfo.setServingsList(servingsList);
    }

    private void fetchDetailedRecipeInfo() throws JSONException {
        JSONObject result = recipes.getRecipe(recipeId);
        if(result != null) {
            recipeInfo.setRecipeId(result.optLong(Constants.recipeId, -1));
            recipeInfo.setRecipeName(result.optString(Constants.recipeName, Constants.unknown));
            recipeInfo.setRecipeUrl(result.optString(Constants.recipeUrl, Constants.unknown));
            recipeInfo.setRecipeDescription(result.optString(Constants.recipeDescription, Constants.unknown));
            recipeInfo.setNumberOfServings(result.optDouble(Constants.numberOfServings, -1));
            recipeInfo.setPrepTime(result.optInt(Constants.prepTime, -1));
            recipeInfo.setCookingTime(result.optInt(Constants.cookingTime, -1));
            recipeInfo.setRating(result.optInt(Constants.rating, -1));
            recipeInfo.setRecipeType(result.optString(Constants.recipeType, Constants.unknown));
            recipeInfo.setRecipeImageUrl(result.optString(Constants.recipeImage, Constants.unknown));
            fetchServingSizeInfo(result.optJSONObject(Constants.servingSizes));
            // Directions and ingredients array lists. The method sets it on the object directly as recipeInfo is GLOBAL
            // NOT taking into account category type and similar stuff because tbh it's useless
            fetchDirections(result.optJSONObject(Constants.directions));
            fetchIngredients(result.optJSONObject(Constants.ingredients));
        }
    }


    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class RecipeAdapter extends FragmentPagerAdapter {

        public RecipeAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    fragment = RecipeNutritionFragment.newInstance(recipeInfo, dayOffset, mealType, selectNumberServings, isEditMode,
                                                isReturningToCreateMealActivity, entryType, isPartOfCreatedMeal);
                    break;
                case 1:
                    fragment = RecipePreparationFragment.newInstance(recipeInfo);
                    break;
                default:
                    fragment = null;
                    break;
            }
            // This should never fail
//            fragment.setArguments(fragmentArgs);
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.nutrition);
                case 1:
                    return getString(R.string.preparation);
            }
            // This should never be reached
            return null;
        }
    }


    //------------------------------------
    // ASYNCTASK(S)
    //------------------------------------

    private class FetchRecipeInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(tabLayout, false, true);
            setViewVisibility(viewPager, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchDetailedRecipeInfo();
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(tabLayout, true, true);
                setViewVisibility(viewPager, true, true);
                setUpViewPager();
            }
            else {
                Toast.makeText(ExpandedRecipeActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new FetchRecipeInfo().execute();
                }
            }
        }
    }
}
