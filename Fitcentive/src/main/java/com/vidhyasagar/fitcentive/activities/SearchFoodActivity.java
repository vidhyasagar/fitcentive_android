package com.vidhyasagar.fitcentive.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.CreatedFoodResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.MealResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.RecipeResultsFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.MySearchSuggestion;
import com.vidhyasagar.fitcentive.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchFoodActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;

    public static final String IS_FROM_BARCODE = "IS_FROM_BARCODE";

    public static final int BARCODE_RESULT = 41;

    public static int NO_OF_TABS_DEFAULT = 7;

    public static final int MAX_RESULTS_AUTOCOMPLETE = 5;

    public static int ALL_FOOD_SEARCH_TAB = 0;

    public static int SEARCH_FOOD_RESULT_FRAGMENT_LIMIT = 4;
    public static int SEARCH_CREATED_FOOD_RESULT_FRAGMENT_LIMIT = 5;
    public static int SEARCH_MEAL_RESULT_FRAGMENT_LIMIT = 6;
    public static int SEARCH_RECIPE_RESULT_FRAGMENT_LIMIT = 7;

    public static String NO_OF_TABS_STRING = "NO_OF_TABS";

    DiaryPageFragment.ENTRY_TYPE type;
    int returnDate;
    int NO_OF_TABS;

    boolean isReturningToCreateMealActivity;

    Toolbar toolbar;
    FragmentManager manager;
    SearchFoodFragmentsAdapter adapter;
    FloatingSearchView floatingSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_food);

        retrieveArguments();
        bindViews();
        setUpViewPager();
        setUpToolbar();
        setUpSearchBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_food, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_barcode:
                startBarcodeActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == CreateOrEditMealActivity.RESULT_FOOD || resultCode == CreateOrEditMealActivity.RESULT_RECIPE) {
            if(requestCode == CreateOrEditMealActivity.RETURN_FROM_FOOD_SELECT) {
                setResult(resultCode, data);
                finish();
            }
        }
        if(resultCode == RESULT_OK && requestCode == BARCODE_RESULT) {
            String format = data.getStringExtra(BarcodeActivity.FORMAT);
            String rawData = data.getStringExtra(BarcodeActivity.RAW_DATA);
            String gtin13number = null;
            if(format.equals(Constants.upc_a)) {
                gtin13number = Utilities.convertFromUPC_A(rawData);
            }
            else if (format.equals(Constants.upc_e)) {
                gtin13number = Utilities.convertFromUPC_E(rawData);
            }
            else if(format.equals(Constants.ean_8)) {
                gtin13number = Utilities.convertFromEAN_8(rawData);
            }
            else if(format.equals(Constants.ean_13)) {
                gtin13number = rawData;
            }

            searchForFoodByBarcode(gtin13number);
        }
    }

    private void searchForFoodByBarcode(String barcode) {
        if(barcode == null) {
            return;
        }
        new SearchForFoodByBarcode(barcode).execute();

    }


    private void startBarcodeActivity() {
        Intent i = new Intent(this, BarcodeActivity.class);
        startActivityForResult(i, BARCODE_RESULT);
    }

    private void callRightFragmentToSearchForFood(String query, boolean isOkForAll) {
        if(viewPager.getCurrentItem() < SEARCH_FOOD_RESULT_FRAGMENT_LIMIT) {
            SearchFoodResultsFragment fragment = (SearchFoodResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
            if ((fragment.getResultType() == SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_ALL && isOkForAll) ||
                    fragment.getResultType() != SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_ALL) {
                fragment.searchForFood(query);
            }

        }
        else if(viewPager.getCurrentItem() < SEARCH_MEAL_RESULT_FRAGMENT_LIMIT) {
            if(isReturningToCreateMealActivity) {
                RecipeResultsFragment fragment = (RecipeResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
                fragment.searchForRecipe(query, true);
            }
        }
        else if(viewPager.getCurrentItem() < SEARCH_RECIPE_RESULT_FRAGMENT_LIMIT && isOkForAll) {
            RecipeResultsFragment fragment = (RecipeResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
            fragment.searchForRecipe(query, true);
        }
    }

    private void callRecipeFragmentToSearchForRecipeViaTextChanged(String query) {
        RecipeResultsFragment fragment = (RecipeResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForRecipe(query, false);
    }

    private void callMealFragmentToSearchForMeal(String query) {
        MealResultsFragment fragment = (MealResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForMeal(query);
    }

    private void callFoodFragmentToSearch(String query) {
        CreatedFoodResultsFragment fragment = (CreatedFoodResultsFragment) adapter.getFragment(viewPager.getCurrentItem());
        fragment.searchForFood(query);
    }

    private void setUpSearchBar() {
        floatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                if(viewPager.getCurrentItem() == SEARCH_RECIPE_RESULT_FRAGMENT_LIMIT - 1) {
                    callRecipeFragmentToSearchForRecipeViaTextChanged(newQuery.trim());
                }
                else if(viewPager.getCurrentItem() == SEARCH_CREATED_FOOD_RESULT_FRAGMENT_LIMIT - 1) {
                    callFoodFragmentToSearch(newQuery.trim());
                }
                else if(viewPager.getCurrentItem() == SEARCH_MEAL_RESULT_FRAGMENT_LIMIT - 1) {
                    if(isReturningToCreateMealActivity) {
                        callRecipeFragmentToSearchForRecipeViaTextChanged(newQuery.trim());
                    }
                    else {
                        callMealFragmentToSearchForMeal(newQuery.trim());
                    }
                }
                else {
                    callRightFragmentToSearchForFood(newQuery.trim(), false);
                }

                // This needs some refactoring, hard to debug
                // Buuuuttt moving on, this is the autocomplete functionality
                if(viewPager.getCurrentItem() == ALL_FOOD_SEARCH_TAB) {
                    if(!TextUtils.isEmpty(newQuery.trim())){
                        new FetchSuggestions(newQuery.trim()).execute();
                    }
                }
            }
        });

        floatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {
                callRightFragmentToSearchForFood(searchSuggestion.getBody().trim(), true);
            }

            @Override
            public void onSearchAction(String currentQuery) {
                callRightFragmentToSearchForFood(currentQuery.trim(), true);
            }
        });

        floatingSearchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {
                View v = SearchFoodActivity.this.getCurrentFocus();
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
                callRightFragmentToSearchForFood(floatingSearchView.getQuery().trim(), true);

            }
        });

    }

    public void setUpViewPager() {

        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new SearchFoodFragmentsAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == SEARCH_RECIPE_RESULT_FRAGMENT_LIMIT - 1) {
                    floatingSearchView.setSearchHint(getString(R.string.search_recipes_hint));
                }
                else if(position == SEARCH_MEAL_RESULT_FRAGMENT_LIMIT - 1) {
                    floatingSearchView.setSearchHint(getString(R.string.search_meals_hint));
                }
                else {
                    floatingSearchView.setSearchHint(getString(R.string.search_foods_hint));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        viewPager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utilities.hideKeyboard(v, SearchFoodActivity.this);
            }
        });

    }

    private void setUpToolbar() {
        // Setting toolbar
        switch (type) {
            case BREAKFAST:
                toolbar.setTitle(getString(R.string.breakfast));
                break;
            case LUNCH:
                toolbar.setTitle(getString(R.string.lunch));
                break;
            case DINNER:
                toolbar.setTitle(getString(R.string.dinner));
                break;
            case SNACKS:
                toolbar.setTitle(getString(R.string.snacks));
                break;
            case WATER:
                toolbar.setTitle(getString(R.string.water));
                break;
            case EXCERCISE:
                toolbar.setTitle(getString(R.string.excercise));
                break;
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(isReturningToCreateMealActivity) {
            actionBar.setTitle(getString(R.string.add_food));
        }
    }

    private void retrieveArguments() {
        Intent i = getIntent();
        type = (DiaryPageFragment.ENTRY_TYPE) i.getExtras().getSerializable(Constants.type);
        returnDate = i.getExtras().getInt(Constants.returnDate);
        NO_OF_TABS = i.getExtras().getInt(NO_OF_TABS_STRING, NO_OF_TABS_DEFAULT);

        if(NO_OF_TABS == NO_OF_TABS_DEFAULT) {
            isReturningToCreateMealActivity = false;
        }
        else {
            isReturningToCreateMealActivity = true;
        }
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.pager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        floatingSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
    }

    private void fetchSuggestions(String exp, List<MySearchSuggestion> suggestions) throws Exception {
        FatSecretFoods foods = new FatSecretFoods();
        JSONObject result = foods.autoComplete(exp, MAX_RESULTS_AUTOCOMPLETE);
        if(result != null) {
            Object object = result.get(Constants.suggestion);
            if(object instanceof JSONArray) {
                JSONArray tempArray = (JSONArray) object;
                int size = tempArray.length();
                for(int i = 0; i < size; i++) {
                    suggestions.add(new MySearchSuggestion(tempArray.getString(i)));
                }
            }
            else if(object instanceof String) {
                String sug = (String) object;
                suggestions.add(new MySearchSuggestion(sug));
            }
        }
    }

    private void startViewFoodActivity(long foodId) {
        Intent i = new Intent(this, EditFoodEntryActivity.class);
        i.putExtra(ExpandedSearchFoodActivity.FOOD_ID, foodId);
        i.putExtra(Constants.DAY_OFFSET, returnDate);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, type);
        i.putExtra(IS_FROM_BARCODE, true);
        startActivity(i);
    }

    private void createNoResultsFoundDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(SearchFoodActivity.this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.no_results_found))
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .setMessage(getString(R.string.no_barcode_results_found_prompt));
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class SearchFoodFragmentsAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public SearchFoodFragmentsAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }


        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position){
                case 0:
                    fragment =  SearchFoodResultsFragment.newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_ALL, type, returnDate,
                            isReturningToCreateMealActivity);
                    break;
                case 1:
                    fragment =  SearchFoodResultsFragment.newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_FAVORITES, type, returnDate,
                            isReturningToCreateMealActivity);
                    break;
                case 2:
                    fragment =  SearchFoodResultsFragment.newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_RECENT, type, returnDate,
                            isReturningToCreateMealActivity);
                    break;
                case 3:
                    fragment =  SearchFoodResultsFragment.newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_FREQUENT, type, returnDate,
                            isReturningToCreateMealActivity);
                    break;
                case 4:
                    fragment = getCount() == NO_OF_TABS_DEFAULT ?
                            CreatedFoodResultsFragment.newInstance(SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_FOODS, type, returnDate) :
                            RecipeResultsFragment.newInstance(returnDate, type, isReturningToCreateMealActivity) ;
                    break;
                case 5:
                    fragment =  getCount() == NO_OF_TABS_DEFAULT ?  MealResultsFragment.newInstance(returnDate, type) :
                                RecipeResultsFragment.newInstance(returnDate, type, isReturningToCreateMealActivity) ;
                    break;
                case 6:
                    fragment =  RecipeResultsFragment.newInstance(returnDate, type, isReturningToCreateMealActivity);
                    break;
                default:
                    fragment = null;
                    break;
            }
            // This should never fail
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.all);
                    case 1:
                        return getString(R.string.favorites);
                    case 2:
                        return getString(R.string.recent);
                    case 3:
                        return getString(R.string.frequent);
                    case 4:
                        return getCount() == NO_OF_TABS_DEFAULT ? getString(R.string.foods) : getString(R.string.recipes);
                    case 5:
                        return  getCount() == NO_OF_TABS_DEFAULT ? getString(R.string.meals) : getString(R.string.recipes);
                    case 6:
                        return getString(R.string.recipes);
                }
                // This should never be reached
                return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }

    }

    //------------------------------------------------------
    // TASK THAT FETCHES AUTCOMPLETE RESULTS FROM FATSECRET
    //------------------------------------------------------

    private class FetchSuggestions extends AsyncTask<Void, Void, Boolean> {

        String expression;
        List<MySearchSuggestion> suggestions;

        public FetchSuggestions(String exp) {
            this.expression = exp;
            suggestions = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            floatingSearchView.swapSuggestions(suggestions);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchSuggestions(expression, suggestions);
                if(suggestions.isEmpty()) {
                    suggestions.add(new MySearchSuggestion(getString(R.string.no_results_found)));
                }
                return true;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                floatingSearchView.swapSuggestions(suggestions);
            }
            else {
                Toast.makeText(SearchFoodActivity.this, getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SearchForFoodByBarcode extends AsyncTask<Void, Void, Long> {

        String barcode;

        public SearchForFoodByBarcode(String barcode) {
            this.barcode = barcode;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Long doInBackground(Void... params) {
            FatSecretFoods foods = new FatSecretFoods();
            return foods.getFoodIdFromBarcode(barcode);
        }

        @Override
        protected void onPostExecute(Long aLong) {
            if(aLong == 0) {
                // This means that no results were found
                createNoResultsFoundDialog();
            }
            else {
                startViewFoodActivity(aLong);
            }
            super.onPostExecute(aLong);
        }
    }

}
