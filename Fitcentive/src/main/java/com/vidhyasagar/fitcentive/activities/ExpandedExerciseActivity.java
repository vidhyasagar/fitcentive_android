package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.wger.ExerciseRequest;
import com.vidhyasagar.fitcentive.detailed_exercise_fragments.ExerciseDetailsFragment;
import com.vidhyasagar.fitcentive.detailed_exercise_fragments.ExerciseSpecificsFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_exercise_fragments.ExerciseResultsFragment;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.Calendar;

public class ExpandedExerciseActivity extends AppCompatActivity {

    public static final String INFO = "INFO";
    public static final String IS_EDITING = "IS_EDITING";

    public static int NO_OF_TABS = 2 ;

    int RETRY_COUNT = 0;
    ExerciseInfo info;
    int dayOffset;

    boolean isEditMode;
    boolean isCreated;
    boolean isReturningToCreateWorkoutActivity;

    ExerciseRequest exerciseRequest;

    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentManager manager;
    ExerciseAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;

    int numberOfSets;
    double repsPerSet;
    double weightPerSet = -1;
    String weightUnit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expanded_exercise);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        setUpViewPager();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_expanded_exercise_activity, menu);

        MenuItem item = menu.findItem(R.id.action_edit);
        if(isCreated) {
            item.setVisible(true);
        }
        else {
            item.setVisible(false);
        }

        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(dayOffset, this)));

        if(isReturningToCreateWorkoutActivity) {
            dateItem.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                saveCurrentExerciseToDiary();
                return true;
            case R.id.action_edit:
                startEditExerciseActivity();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), viewPager);
        }
        else {
            if(mMonth == month) {
                dayOffset = day - mDay;
            }
            else {
                dayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, dayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        invalidateOptionsMenu();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void retrieveArguments() {
        this.info = getIntent().getParcelableExtra(INFO);
        this.dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        this.isEditMode = getIntent().getExtras().getBoolean(IS_EDITING);
        this.isCreated = getIntent().getExtras().getBoolean(Constants.isCreated);
        this.isReturningToCreateWorkoutActivity = getIntent().getExtras().getBoolean(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, false);
        invalidateOptionsMenu();
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        exerciseRequest = new ExerciseRequest();
    }

    private void setUpToolbar() {
        if(!isEditMode) {
            toolbar.setTitle(getString(R.string.exercise_info));
        }
        else {
            toolbar.setTitle(getString(R.string.edit_exercise_entry));
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpViewPager() {
        // Fragment manager to add fragment in viewpager we will pass object of Fragment manager to adpater class.
        manager = getSupportFragmentManager();
        //object of PagerAdapter passing fragment manager object as a parameter of constructor of PagerAdapter class.
        adapter = new ExerciseAdapter(manager);
        //set Adapter to view pager
        viewPager.setAdapter(adapter);
        //set tablayout with viewpager
        tabLayout.setupWithViewPager(viewPager);
        // adding functionality to tab and viewpager to manage each other when a page is changed or when a tab is selected
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    public void goToNextFragment() {
        viewPager.setCurrentItem(1);
    }

    private void startEditExerciseActivity() {
        Intent i = new Intent(this, CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_EDIT);
        i.putExtra(Constants.isCardio, false);
        i.putExtra(Constants.createdExerciseObjectId, info.getCreatedExerciseObjectId());
        startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

    private void saveCurrentExerciseToDiary() {
        if(((ExerciseSpecificsFragment) adapter.getFragment(0)).isDataValidForSave()) {
            ((ExerciseSpecificsFragment) adapter.getFragment(0)).sendDataToParent();
            new AddExerciseToDiaryTemporarily().execute();
        }
        else {
            viewPager.setCurrentItem(0);
        }
    }

    public void setRequiredFields(int numberOfSets, double repsPerSet) {
        this.numberOfSets = numberOfSets;
        this.repsPerSet = repsPerSet;
    }

    public void setOptionalFields(double weightPerSet, String unit) {
        this.weightPerSet = weightPerSet;
        this.weightUnit = unit;
    }

    private void editCurrentExerciseEntry() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.ExerciseDiary);
        ParseObject object = query.get(info.getObjectId());

        object.put(Constants.numberOfSets, this.numberOfSets);
        object.put(Constants.repsPerSet, this.repsPerSet);
        if(weightUnit != null) {
            object.put(Constants.weightUnit, weightUnit);
        }
        else {
            object.put(Constants.weightUnit, Constants.unknown);
        }

        object.put(Constants.weightPerSet, weightPerSet);
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        object.save();
    }

    private void saveCreatedStrengthExerciseToDiary() throws Exception {
        ParseObject newObject = new ParseObject(Constants.ExerciseDiary);

        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.exerciseId, info.getId());
        newObject.put(Constants.exerciseName, info.getName());
        newObject.put(Constants.exerciseType, getString(R.string.weights_type));

        newObject.put(Constants.numberOfSets, this.numberOfSets);
        newObject.put(Constants.repsPerSet, this.repsPerSet);
        if(weightUnit != null) {
            newObject.put(Constants.weightUnit, weightUnit);
        }
        else {
            newObject.put(Constants.weightUnit, Constants.unknown);
        }

        newObject.put(Constants.weightPerSet, weightPerSet);

        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        newObject.put(Constants.isCreated, true);

        // Again, this getObjectId() returns the createdExerciseObjectId and NOT the objectId pertaining to the exerciseDiary entry
        newObject.put(Constants.createdExerciseObjectId, info.getCreatedExerciseObjectId());

        newObject.save();
    }

    private void saveToDiary() throws Exception {
        ParseObject newObject = new ParseObject(Constants.ExerciseDiary);

        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.exerciseId, info.getId());
        newObject.put(Constants.exerciseName, info.getName());
        newObject.put(Constants.exerciseType, getString(R.string.weights_type));

        newObject.put(Constants.numberOfSets, this.numberOfSets);
        newObject.put(Constants.repsPerSet, this.repsPerSet);
        if(weightUnit != null) {
            newObject.put(Constants.weightUnit, weightUnit);
        }
        else {
            newObject.put(Constants.weightUnit, Constants.unknown);
        }

        newObject.put(Constants.weightPerSet, weightPerSet);

        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        newObject.put(Constants.isCreated, false);

        newObject.save();
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.dayOffset);
        startActivity(i);
        finish();
    }

    private void returnToWorkoutComponentsFragment() {
        Intent i = new Intent();
        info.setNumberOfSets(this.numberOfSets);
        info.setRepsPerSet(repsPerSet);
        info.setWeightUnit(weightUnit != null ? weightUnit : Constants.unknown);
        info.setWeightPerSet(weightPerSet);
        info.setCreated(isCreated);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        if(isCreated) {
            setResult(CreateOrEditWorkoutActivity.RESULT_CREATED_STRENGTH, i);
        }
        else {
            setResult(CreateOrEditWorkoutActivity.RESULT_STRENGTH, i);
        }
        finish();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    // ---------------------------------------
    // ViewPager/TabLayout adapter
    // ---------------------------------------

    public class ExerciseAdapter extends FragmentPagerAdapter {

        SparseArray<Fragment> fragmentMap;

        public Fragment getFragment(int key) {
            return fragmentMap.get(key);
        }

        public ExerciseAdapter(FragmentManager fm) {
            super(fm);
            fragmentMap = new SparseArray<>();
        }

        @Override
        public Fragment getItem(int position) {

            Fragment fragment;
            switch (position){
                case 0:
                    fragment = ExerciseSpecificsFragment.newInstance(dayOffset, info, isEditMode);
                    break;
                case 1:
                    fragment = ExerciseDetailsFragment.newInstance(dayOffset, info);
                    break;
                default:
                    fragment = null;
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NO_OF_TABS;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return getString(R.string.specifics);
                case 1:
                    return getString(R.string.details);
            }
            // This should never be reached
            return null;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            fragmentMap.remove(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            fragmentMap.put(position, fragment);
            return fragment;
        }
    }


    //-----------------------------------------------
    // ASYNCTASK(S)
    //-----------------------------------------------

    private class AddExerciseToDiaryTemporarily extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(tabLayout, false, true);
            setViewVisibility(viewPager, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isReturningToCreateWorkoutActivity) {
                    if (isEditMode) {
                        editCurrentExerciseEntry();
                    } else {
                        if (isCreated) {
                            saveCreatedStrengthExerciseToDiary();
                        } else {
                            saveToDiary();
                        }
                    }
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(isReturningToCreateWorkoutActivity) {
                    returnToWorkoutComponentsFragment();
                }
                else {
                    returnToDiaryPage();
                }
            }
            else {
                Toast.makeText(ExpandedExerciseActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new AddExerciseToDiaryTemporarily().execute();
                }
            }
        }

    }

}
