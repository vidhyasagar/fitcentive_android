package com.vidhyasagar.fitcentive.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretDiary;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DetailedFoodSearchResultsFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.fragments.ViewCreatedFoodFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;

public class EditFoodEntryActivity extends AppCompatActivity {

    public static String IS_CREATED_FOOD = "IS_CREATED_FOOD";
    public static String IS_FROM_DIARY = "IS_FROM_DIARY";

    long foodId;
    long servingId;
    double numberOfServings;

    long chosenServingId;
    double chosenNumberServings;

    int RETRY_COUNT = 0;
    int dayOffset, newDayOffset;

    boolean isPartOfCreatedMeal;

    // For created foods only
    boolean isCreatedFood;
    boolean isCreatedFoodEditMode;
    boolean isFromDiary;
    boolean isFromBarcode;
    String createdFoodObjectId;

    DiaryPageFragment.ENTRY_TYPE entryType;
    DiaryPageFragment.ENTRY_TYPE newEntryType;

    FrameLayout frameLayout;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_food_entry);

        retrieveArguments();
        bindViews();
        setUpToolbar();
        setUpEditFoodFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_expanded_search_food, menu);

        MenuItem typeItem = menu.findItem(R.id.action_change_type);
        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        typeItem.setTitle(String.format(getString(R.string.entry_type), Utilities.getEntryTypeString(newEntryType, this)));
        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(newDayOffset, this)));

        if(isPartOfCreatedMeal) {
            typeItem.setVisible(false);
            dateItem.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_add_food:
                addCurrentFood();
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            case R.id.action_change_type:
                showTypeDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), toolbar);
        }
        else {
            if(mMonth == month) {
                newDayOffset = day - mDay;
            }
            else {
                newDayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, newDayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        invalidateOptionsMenu();
                        ViewCreatedFoodFragment fragment = (ViewCreatedFoodFragment) fragmentManager.findFragmentByTag(ViewCreatedFoodFragment.TAG);
                        if(fragment != null) {
                            fragment.setNewDayOffset(newDayOffset);
                        }
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTypeDialog() {
        CharSequence[] array = getResources().getStringArray(R.array.entry_types);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.select_entry_type))
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        invalidateOptionsMenu();
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(array, Utilities.getCheckedItem(entryType), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ViewCreatedFoodFragment fragment = (ViewCreatedFoodFragment) fragmentManager.findFragmentByTag(ViewCreatedFoodFragment.TAG);
                        switch (which) {
                            case 0:
                                newEntryType = DiaryPageFragment.ENTRY_TYPE.BREAKFAST;
                                break;
                            case 1:
                                newEntryType = DiaryPageFragment.ENTRY_TYPE.LUNCH;
                                break;
                            case 2:
                                newEntryType = DiaryPageFragment.ENTRY_TYPE.DINNER;
                                break;
                            case 3:
                                newEntryType = DiaryPageFragment.ENTRY_TYPE.SNACKS;
                                break;
                        }

                        if(fragment != null) {
                            fragment.setNewEntryType(newEntryType);
                        }
                        invalidateOptionsMenu();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void addCurrentFood() {
        if(isCreatedFood) {
            ViewCreatedFoodFragment fragment = (ViewCreatedFoodFragment) fragmentManager.findFragmentByTag(ViewCreatedFoodFragment.TAG);
            fragment.saveCurrentFoodToDiary();
        }
        else {
            DetailedFoodSearchResultsFragment fragment = (DetailedFoodSearchResultsFragment) fragmentManager.findFragmentByTag(DetailedFoodSearchResultsFragment.TAG);
            fragment.sendDataToParentActivity(false);
            new EditFoodEntryInDatabase(foodId).execute();
        }
    }

    public void setChosenServingId(long id) {
        this.chosenServingId = id;
    }

    public void setChosenNumberServings(double no) {
        this.chosenNumberServings = no;
    }

    private void setUpToolbar() {
        // Setting toolbar
        if(isCreatedFood) {
            if(isFromDiary) {
                toolbar.setTitle(getString(R.string.edit_food_entry));
            }
            else {
                toolbar.setTitle(getString(R.string.view_created_food));
            }
        }
        else {
            if(isFromBarcode) {
                toolbar.setTitle(getString(R.string.view_food));
            }
            else {
                toolbar.setTitle(getString(R.string.edit_food_entry));
            }
        }
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

    }


    private void retrieveArguments() {
        foodId = getIntent().getExtras().getLong(ExpandedSearchFoodActivity.FOOD_ID, -1);
        servingId = getIntent().getExtras().getLong(Constants.servingId, -1);

        isPartOfCreatedMeal = getIntent().getExtras().getBoolean(CreateOrEditMealActivity.IS_PART_OF_MEAL, false);
        isFromBarcode = getIntent().getExtras().getBoolean(SearchFoodActivity.IS_FROM_BARCODE, false);

        numberOfServings = getIntent().getExtras().getDouble(Constants.numberServings, 1);
        dayOffset = getIntent().getExtras().getInt(Constants.DAY_OFFSET);
        entryType = (DiaryPageFragment.ENTRY_TYPE) getIntent().getExtras().get(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING);
        isCreatedFood = getIntent().getExtras().getBoolean(IS_CREATED_FOOD, false);

        if(isCreatedFood) {
            isFromDiary = getIntent().getExtras().getBoolean(IS_FROM_DIARY, false);
            createdFoodObjectId = getIntent().getExtras().getString(Constants.objectId);
            isCreatedFoodEditMode = getIntent().getExtras().getBoolean(ExpandedRecipeActivity.IS_EDITING, false);
        }

        newEntryType = entryType;
        newDayOffset = dayOffset;
        invalidateOptionsMenu();
    }

    private void bindViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        frameLayout = (FrameLayout) findViewById(R.id.frame_container);
        fragmentManager = getSupportFragmentManager();
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    private void setUpEditFoodFragment() {
        fragmentTransaction  = fragmentManager.beginTransaction();
        if(isCreatedFood) {
            fragmentTransaction.replace(R.id.frame_container,
                    ViewCreatedFoodFragment.newInstance(createdFoodObjectId, numberOfServings,
                            isCreatedFoodEditMode, dayOffset, entryType),
                    ViewCreatedFoodFragment.TAG).commit();
        }
        else {
            fragmentTransaction.replace(R.id.frame_container,
                    DetailedFoodSearchResultsFragment.newInstance(foodId, false, servingId, numberOfServings, isFromBarcode),
                    DetailedFoodSearchResultsFragment.TAG).commit();
        }
    }

    private void showMainLayout(boolean show) {
        setViewVisibility(progressBar, !show, true);
        setViewVisibility(frameLayout, show, true);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    private boolean isPresentInArray(JSONArray array, long recipeId) {
        int size = array.length();
        try {
            for (int i = 0; i < size; i++) {
                JSONObject object = array.getJSONObject(i);
                if(recipeId == object.optLong(Constants.foodId, -1)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isPresentInFavorites(JSONObject object, long foodId) {
        try {
            Object temp = object.opt(Constants.food);
            if(temp instanceof JSONObject) {
                if(foodId ==  ((JSONObject) temp).optLong(Constants.foodId, -1)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return isPresentInArray((JSONArray) temp, foodId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean getWhetherFavoriteOrNot(long foodId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, getString(R.string.food));
        query.whereEqualTo(Constants.foodIdNumber, foodId);

        try {
            ParseObject object = query.getFirst();
            return object.getBoolean(Constants.isFavorite);
        } catch (ParseException e) {
            FatSecretFoods foods = new FatSecretFoods();
            JSONObject object = foods.getFavoriteFoods();
            return isPresentInFavorites(object, foodId);
        }
    }

    private void addToFoodDiary(long foodId) throws Exception {
        ParseObject newObject = new ParseObject(Constants.FoodDiary);
        newObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.isPermanent, false);
        newObject.put(Constants.entryType, getString(R.string.food));
        newObject.put(Constants.foodIdNumber, foodId);
        switch (newEntryType) {
            case BREAKFAST:
                newObject.put(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                newObject.put(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                newObject.put(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                newObject.put(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                newObject.put(Constants.mealType, getString(R.string.water));
                break;
        }
        newObject.put(Constants.servingIdField, this.chosenServingId);
        newObject.put(Constants.numberServings, this.chosenNumberServings);
        newObject.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        // Must also put in the isFavorite field here
        // Rules : If another food with same foodId exists in the database, set it to that
        //         If not, set it to false
        newObject.put(Constants.isFavorite, getWhetherFavoriteOrNot(foodId));
        newObject.save();
    }

    // TODO: Make edits and deleted by objectId, and not by re-querying
    private void editFoodEntry(long foodId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.numberServings, numberOfServings);
        query.whereEqualTo(Constants.servingIdField, servingId);
        query.whereEqualTo(Constants.entryType, getString(R.string.food));
        query.whereEqualTo(Constants.foodIdNumber, foodId);
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        switch (entryType) {
            case BREAKFAST:
                query.whereEqualTo(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                query.whereEqualTo(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                query.whereEqualTo(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                query.whereEqualTo(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                query.whereEqualTo(Constants.mealType, getString(R.string.water));
                break;
            default:
                break;
        }

        ParseObject object = query.getFirst();
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        object.put(Constants.servingIdField, chosenServingId);
        object.put(Constants.numberServings, chosenNumberServings);
        // New addition
        switch (newEntryType) {
            case BREAKFAST:
                object.put(Constants.mealType, getString(R.string.breakfast));
                break;
            case LUNCH:
                object.put(Constants.mealType, getString(R.string.lunch));
                break;
            case DINNER:
                object.put(Constants.mealType, getString(R.string.dinner));
                break;
            case SNACKS:
                object.put(Constants.mealType, getString(R.string.snacks));
                break;
            case WATER:
                object.put(Constants.mealType, getString(R.string.water));
                break;
        }
        long foodEntryId = object.getLong(Constants.foodEntryIdField);
        object.save();

        FatSecretDiary diary = new FatSecretDiary();
        diary.editFoodEntry(foodEntryId, Constants.placeholderName, chosenServingId, numberOfServings, null);

    }

    private void returnToDiaryPage() {
        Intent i = new Intent(EditFoodEntryActivity.this, MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.newDayOffset);
        startActivity(i);
        finish();
    }

    private void returnToMealPage(long foodId) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.servingId, this.chosenServingId);
        resultIntent.putExtra(Constants.foodId, foodId);
        resultIntent.putExtra(Constants.numberOfServings, this.chosenNumberServings);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    //-------------------
    // ASYNCTASK(S)
    //-------------------


    private class EditFoodEntryInDatabase extends AsyncTask<Void, Void, Boolean> {

        private long foodId;

        public EditFoodEntryInDatabase(long foodId) {
            this.foodId = foodId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showMainLayout(false);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isPartOfCreatedMeal) {
                    if(isFromBarcode) {
                        addToFoodDiary(foodId);
                    }
                    else {
                        editFoodEntry(foodId);
                    }
                }
                return true;
            } catch (Exception e){
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                showMainLayout(true);
                if(isPartOfCreatedMeal) {
                    returnToMealPage(foodId);
                }
                else {
                    returnToDiaryPage();
                }
            }
            else {
                Toast.makeText(EditFoodEntryActivity.this, getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new EditFoodEntryInDatabase(foodId).execute();
                }
            }
        }
    }
}
