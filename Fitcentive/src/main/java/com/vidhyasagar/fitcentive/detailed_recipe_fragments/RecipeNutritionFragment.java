package com.vidhyasagar.fitcentive.detailed_recipe_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.activities.MainActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.dialog_fragments.CarbsDetailedInfoDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.FatBreakdownDialogFragment;
import com.vidhyasagar.fitcentive.dialog_fragments.VitaminsAndMineralsDialogFragment;
import com.vidhyasagar.fitcentive.fragments.DetailedFoodSearchResultsFragment;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.fragments.ViewCreatedFoodFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeNutritionFragment extends Fragment {

    private DetailedRecipeResultsInfo recipeInfo;

    public static final int CASE_CARBS = 0;
    public static final int CASE_FATS = 1;
    public static final int CASE_PROTEINS = 2;


    int dayOffset, newDayOffset;
    String mealType, newMealType;

    int RETRY_COUNT = 0;

    TextView recipeName, recipeServingDescription;
    PieChart pieChart;
    TextView carbs, proteins, fats, calories;
    TextView numberOfServings;
    TextView vitaminsAndMinerals;
    Button addToDiaryButton;
    LinearLayout linearLayout;
    LinearLayout carbsLayout, fatsLayout, proteinsLayout;
    LinearLayout parentLayout;
    ProgressBar progressBar;

    EditText servingSizeEditText;
    Spinner dialogServingSizeSpinner;

    int CURRENT_SERVING_OPTION;
    double totalFatCarbsAndProteins;
    double selectedNumberOfServings;
    boolean isEditMode;
    DiaryPageFragment.ENTRY_TYPE entryType;

    boolean isReturningToCreateMealActivity;
    boolean isPartOfCreatedMeal;

    public static RecipeNutritionFragment newInstance(DetailedRecipeResultsInfo info, int dayOffset,
                                                      String mealType, double selectedNumberOfServings,
                                                      boolean isEditMode, boolean isReturningToCreateMealActivity,
                                                      DiaryPageFragment.ENTRY_TYPE entryType, boolean isPartOfCreatedMeal) {
        RecipeNutritionFragment fragment = new RecipeNutritionFragment();
        Bundle args = new Bundle();
        args.putParcelable(RecipePreparationFragment.RECIPE_INFO_STRING, info);
        args.putInt(SearchFoodResultsFragment.DAY_OFFSET_STRING, dayOffset);
        args.putString(Constants.mealType, mealType);
        args.putDouble(Constants.numberServings, selectedNumberOfServings);
        args.putBoolean(ExpandedRecipeActivity.IS_EDITING, isEditMode);
        args.putBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL, isReturningToCreateMealActivity);
        args.putBoolean(CreateOrEditMealActivity.IS_PART_OF_MEAL, isPartOfCreatedMeal);
        args.putSerializable(Constants.type, entryType);
        fragment.setArguments(args);
        return fragment;
    }

    public RecipeNutritionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_recipe_nutrition, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);
        setUpServingMenu();
        setUpOnScreenData();
        setUpPieChart();
        setUpClickableTextView();
        setUpClickableMacros();
        setUpAddButton();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_expanded_search_food, menu);
        MenuItem typeItem = menu.findItem(R.id.action_change_type);
        MenuItem dateItem = menu.findItem(R.id.action_change_date);

        typeItem.setTitle(String.format(getString(R.string.entry_type), Utilities.getEntryTypeString(entryType, getContext())));
        dateItem.setTitle(String.format(getString(R.string.entry_date), Utilities.getDateString(newDayOffset, getContext())));

        if(isReturningToCreateMealActivity || isPartOfCreatedMeal) {
            typeItem.setVisible(false);
            dateItem.setVisible(false);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_food:
                if(isEditMode) {
                    new EditCurrentRecipe().execute();
                }
                else {
                    new AddCurrentRecipe().execute();
                }
                return true;
            case R.id.action_change_date:
                showDateDialog();
                return true;
            case R.id.action_change_type:
                showTypeDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Parameters are selected values of day month and year
    private void updateDayOffset(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
//        c.add(Calendar.DATE, dayOffset);
        // Current year, date and month
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        if(year != mYear || Math.abs(mMonth - month) > 8) {
            Utilities.showSnackBar(getString(R.string.date_change_too_large), addToDiaryButton);
        }
        else {
            if(mMonth == month) {
                newDayOffset = day - mDay;
            }
            else {
                newDayOffset = Utilities.computeDayOffset(mDay, mMonth, day, month, year);
            }
        }
    }

    private void showDateDialog() {
        // Get Current Date from the display on screen
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, newDayOffset);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        updateDayOffset(year, monthOfYear, dayOfMonth);
                        getActivity().invalidateOptionsMenu();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    private void showTypeDialog() {
        CharSequence[] array = getResources().getStringArray(R.array.entry_types);
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.select_entry_type))
                .setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().invalidateOptionsMenu();
                        dialog.dismiss();
                    }
                })
                .setSingleChoiceItems(array, Utilities.getCheckedItem(entryType), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                entryType = DiaryPageFragment.ENTRY_TYPE.BREAKFAST;
                                newMealType = getString(R.string.breakfast);
                                break;
                            case 1:
                                entryType = DiaryPageFragment.ENTRY_TYPE.LUNCH;
                                newMealType = getString(R.string.lunch);
                                break;
                            case 2:
                                entryType = DiaryPageFragment.ENTRY_TYPE.DINNER;
                                newMealType = getString(R.string.dinner);
                                break;
                            case 3:
                                entryType = DiaryPageFragment.ENTRY_TYPE.SNACKS;
                                newMealType = getString(R.string.snacks);
                                break;
                        }
                        getActivity().invalidateOptionsMenu();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setUpAddButton() {
        if(isEditMode) {
            addToDiaryButton.setText(getString(R.string.save));
        }

        addToDiaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEditMode) {
                    new EditCurrentRecipe().execute();
                }
                else {
                    new AddCurrentRecipe().execute();
                }
            }
        });
    }

    private boolean isPresentInArray(JSONArray array, long recipeId) {
        int size = array.length();
        try {
            for (int i = 0; i < size; i++) {
                JSONObject object = array.getJSONObject(i);
                if(recipeId == object.optLong(Constants.recipeId, -1)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isPresentInFavorites(JSONObject object, long recipeId) {
        try {
            Object temp = object.opt(Constants.recipe);
            if(temp instanceof JSONObject) {
                if(recipeId ==  ((JSONObject) temp).optLong(Constants.recipeId, -1)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return isPresentInArray((JSONArray) temp, recipeId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean getWhetherFavoriteOrNot(long recipeId) {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.recipeIdField, recipeId);
        query.whereEqualTo(Constants.entryType, getString(R.string.recipe));

        try {
            ParseObject object = query.getFirst();
            return object.getBoolean(Constants.isFavorite);
        } catch (ParseException e) {
            // This means the object doesnt exist in the db.
            // We have to check if it exists in Fatsecret or not
            FatSecretRecipes recipes = new FatSecretRecipes();
            JSONObject object = recipes.getFavoriteRecipes();
            return isPresentInFavorites(object, recipeId);
        }
    }

    private void addCurrentRecipe() throws Exception {
        ParseObject newRecipe = new ParseObject(Constants.FoodDiary);
        newRecipe.put(Constants.entryType, getString(R.string.recipe));
        newRecipe.put(Constants.recipeIdField, recipeInfo.getRecipeId());
        newRecipe.put(Constants.isPermanent, false);
        newRecipe.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        newRecipe.put(Constants.mealType, this.newMealType);
        newRecipe.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        newRecipe.put(Constants.numberServings, Double.valueOf(numberOfServings.getText().toString()));
        newRecipe.put(Constants.isFavorite, getWhetherFavoriteOrNot(recipeInfo.getRecipeId()));
        newRecipe.put(Constants.recipeServingOption, CURRENT_SERVING_OPTION);
        newRecipe.save();

    }

    private void editCurrentRecipe() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.numberServings, selectedNumberOfServings);
        query.whereEqualTo(Constants.entryType, getString(R.string.recipe));
        query.whereEqualTo(Constants.recipeIdField, recipeInfo.getRecipeId());
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        query.whereEqualTo(Constants.mealType, mealType);

        ParseObject object = query.getFirst();
        object.put(Constants.numberServings, Double.valueOf(numberOfServings.getText().toString()));
        object.put(Constants.recipeServingOption, CURRENT_SERVING_OPTION);
        object.put(Constants.mealType, this.newMealType);
        object.put(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(newDayOffset));
        object.save();

    }

    private void setUpClickableMacros() {
        carbsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(CASE_CARBS);
            }
        });
        fatsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(CASE_FATS);
            }
        });
        proteinsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedBreakDown(CASE_PROTEINS);
            }
        });
    }

    private void setUpOnScreenData() {
        recipeName.setText(recipeInfo.getRecipeName());
        recipeServingDescription.setText(recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getServingSize());
        carbs.setText(String.format(getString(R.string.two_decimal_places), recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCarbs()
                * Double.valueOf(numberOfServings.getText().toString())));
        proteins.setText(String.format(getString(R.string.two_decimal_places), recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getProtein()
                * Double.valueOf(numberOfServings.getText().toString())));
        fats.setText(String.format(getString(R.string.two_decimal_places), recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getFat()
                * Double.valueOf(numberOfServings.getText().toString())));
        calories.setText(String.format(getString(R.string.two_decimal_places), recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCalories()
                * Double.valueOf(numberOfServings.getText().toString())));
    }

    private void bindViews(View v) {
        recipeName = (TextView) v.findViewById(R.id.recipe_name);
        recipeServingDescription = (TextView) v.findViewById(R.id.recipe_serving_size);
        pieChart = (PieChart) v.findViewById(R.id.pie_chart);
        carbs = (TextView) v.findViewById(R.id.carbs_textview);
        proteins = (TextView) v.findViewById(R.id.proteins_textview);
        fats = (TextView) v.findViewById(R.id.fats_textview);
        calories = (TextView) v.findViewById(R.id.calories_textview);
        numberOfServings = (TextView) v.findViewById(R.id.number_of_servings);
        addToDiaryButton = (Button) v.findViewById(R.id.add_to_diary_button);
        vitaminsAndMinerals = (TextView) v.findViewById(R.id.view_vitamins_textview);
        linearLayout = (LinearLayout) v.findViewById(R.id.serving_layout);
        carbsLayout = (LinearLayout) v.findViewById(R.id.carbs_layout);
        proteinsLayout = (LinearLayout) v.findViewById(R.id.proteins_layout);
        fatsLayout = (LinearLayout) v.findViewById(R.id.fats_layout);
        parentLayout = (LinearLayout) v.findViewById(R.id.parent_linear_layout);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
    }

    private void retrieveArguments() {
        this.recipeInfo = getArguments().getParcelable(RecipePreparationFragment.RECIPE_INFO_STRING);
        this.CURRENT_SERVING_OPTION = getArguments().getInt(ExpandedSearchFoodActivity.CURRENT_ITEM_STRING, 0);
        this.dayOffset = getArguments().getInt(SearchFoodResultsFragment.DAY_OFFSET_STRING);
        this.mealType = getArguments().getString(Constants.mealType);
        this.selectedNumberOfServings = getArguments().getDouble(Constants.numberServings);
        this.isEditMode = getArguments().getBoolean(ExpandedRecipeActivity.IS_EDITING);
        this.isReturningToCreateMealActivity = getArguments().getBoolean(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL);
        this.entryType = (DiaryPageFragment.ENTRY_TYPE) getArguments().getSerializable(Constants.type);
        this.isPartOfCreatedMeal = getArguments().getBoolean(CreateOrEditMealActivity.IS_PART_OF_MEAL);

        newMealType = mealType;
        newDayOffset = dayOffset;
        getActivity().invalidateOptionsMenu();
    }

    private void calculateTotal() {
        totalFatCarbsAndProteins =  recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCarbs() +
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getFat() +
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getProtein();
    }

    private void setUpPieChart() {
        calculateTotal();
        pieChart.setBackgroundColor(Color.TRANSPARENT);
        pieChart.setUsePercentValues(true);
        pieChart.setDescription(getString(R.string.nutritional_breakdown));
        pieChart.setDescriptionColor(ContextCompat.getColor(getContext(), R.color.transp_black));
        pieChart.setDescriptionTextSize(11f);
        pieChart.setHoleRadius(5f);
        pieChart.setTransparentCircleRadius(1f);
        pieChart.setDrawSliceText(false);
//        pieChart.setCenterText(getString(R.string.nutritional_breakdown));
        pieChart.setCenterTextSize(13f);

        Legend l = pieChart.getLegend();
//        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setFormSize(10f); // set the size of the legend forms/shapes
        l.setEnabled(true);
        l.setForm(Legend.LegendForm.SQUARE); // set what type of form/shape should be used
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
        l.setTextSize(12f);
        l.setTextColor(Color.BLACK);
        l.setXEntrySpace(5f); // set the space between the legend entries on the x-axis
        l.setYEntrySpace(5f); // set the space between the legend entries on the y-axis
        l.setWordWrapEnabled(true);

        final int[] colors = {android.R.color.holo_blue_light, android.R.color.holo_red_light, android.R.color.holo_green_light};
//        l.setCustom(colors, categories);


        ArrayList<Entry> breakdown = new ArrayList<Entry>();

        Entry carbs = new Entry( (float) (recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCarbs() / totalFatCarbsAndProteins) * 100, CASE_CARBS); // 0 == Carbs
        breakdown.add(carbs);
        Entry fats = new Entry( (float) (recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getFat() / totalFatCarbsAndProteins) * 100, CASE_FATS); // 0 == Carbs
        breakdown.add(fats);
        Entry proteins = new Entry( (float) (recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getProtein() / totalFatCarbsAndProteins) * 100, CASE_PROTEINS); // 0 == Carbs
        breakdown.add(proteins);

        PieDataSet setBreakdown = new PieDataSet(breakdown, "");
//        setBreakdown.setHighlightEnabled(true);
        setBreakdown.setAxisDependency(YAxis.AxisDependency.LEFT);
        setBreakdown.setSliceSpace(5f);
        setBreakdown.setColors(colors, getContext());

        ArrayList<String> xVals = new ArrayList<String>();
        xVals.add(getString(R.string.carbs));
        xVals.add(getString(R.string.fats));
        xVals.add(getString(R.string.proteins));
//
        PieData data = new PieData(xVals, setBreakdown);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(10f);
        pieChart.setData(data);
        pieChart.setHighlightPerTapEnabled(true);
        pieChart.animateXY(750, 750);

        pieChart.invalidate(); // refresh

        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                // display msg when value selected
                if (e == null)
                    return;
                else {
                    showDetailedBreakDown(e.getXIndex());
                }
            }

            @Override
            public void onNothingSelected() {
                Log.i("applog", "Nothing is selected");
            }
        });
    }

    private void showDetailedBreakDown(int type) {
        switch (type) {
            case CASE_CARBS: // Carbs
                showDetailedCarbsBreakdown();
                break;
            case CASE_FATS: // Fats
                showDetailedFatsBreakdown();
                break;
            case CASE_PROTEINS: // Protein
                showDetailedProteinBreakdown();
                break;
            default: return;
        }
    }

    private void showDetailedCarbsBreakdown() {
        FragmentManager fm = getFragmentManager();
        CarbsDetailedInfoDialogFragment carbsFragment = CarbsDetailedInfoDialogFragment.newInstance(
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCholestrol(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getSugar(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getFiber(), Double.valueOf(numberOfServings.getText().toString()));

        if(carbsFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            carbsFragment.show(fm, CarbsDetailedInfoDialogFragment.TAG);
        }
    }

    private void showDetailedFatsBreakdown() {
        FragmentManager fm = getFragmentManager();
        FatBreakdownDialogFragment fatFragment =
                FatBreakdownDialogFragment.newInstance(recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getPolyUnsaturatedFat(),
                        recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getMonoUnsaturatedFat(),
                        recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getTransFat(),
                        recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getSaturatedFat(), Double.valueOf(numberOfServings.getText().toString()));
        if(fatFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            fatFragment.show(fm, FatBreakdownDialogFragment.TAG);
        }
    }

    private void showDetailedProteinBreakdown() {
        Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.protein_data_text), Snackbar.LENGTH_LONG);
        snackbar.show();
        View view = snackbar.getView();
        TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        txtv.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void showDetailedVitaminsAndMineralsBreakdown() {
        FragmentManager fm = getFragmentManager();
        VitaminsAndMineralsDialogFragment vitFragment = VitaminsAndMineralsDialogFragment.newInstance(
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getSodium(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getPotassium(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getCalcium(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getIron(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getVitamin_a(),
                recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getVitamin_c(), Double.valueOf(numberOfServings.getText().toString()));

        if(vitFragment == null) {
            Snackbar snackbar =  Snackbar.make(pieChart, getString(R.string.no_data_available), Snackbar.LENGTH_LONG);
            snackbar.show();
            View view = snackbar.getView();
            TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
            txtv.setGravity(Gravity.CENTER_HORIZONTAL);
        }
        else {
            vitFragment.show(fm, VitaminsAndMineralsDialogFragment.TAG);
        }
    }

    private void setUpClickableTextView() {
        vitaminsAndMinerals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailedVitaminsAndMineralsBreakdown();
            }
        });
    }

    private void setUpServingMenu() {
        numberOfServings.setText(String.valueOf(selectedNumberOfServings));
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createServingDialog();
            }
        });
    }

    public void createServingDialog() {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogCustom);
        builder.setTitle(getString(R.string.serving_size))
                .setPositiveButton(getString(R.string.save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(getContext().getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_serving_size_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_serving_size_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();


        servingSizeEditText = (EditText) dialog.findViewById(R.id.number_of_servings);
        dialogServingSizeSpinner = (Spinner) dialog.findViewById(R.id.serving_size);

        servingSizeEditText.setText(numberOfServings.getText().toString());
        servingSizeEditText.setSelection(servingSizeEditText.getText().length());

        setUpSpinner();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(servingSizeEditText.getText()) && Double.valueOf(servingSizeEditText.getText().toString()) != 0) {
                    updateNumberOfServings();
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(getContext(), getString(R.string.error_invalid_number_servings), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setUpSpinner() {

        List<String> spinnerArray =  new ArrayList<>();
        int size = recipeInfo.getServingsList().size();
        for(int i = 0; i < size; i++) {
            spinnerArray.add(recipeInfo.getServingsList().get(i).getServingSize());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                getContext(), android.R.layout.simple_spinner_item, spinnerArray);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dialogServingSizeSpinner.setAdapter(adapter);

        dialogServingSizeSpinner.setSelection(CURRENT_SERVING_OPTION);

        dialogServingSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CURRENT_SERVING_OPTION = position;
//                calculateTotal();
                recipeServingDescription.setText(recipeInfo.getServingsList().get(CURRENT_SERVING_OPTION).getServingSize());
//                refreshPieChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void updateNumberOfServings() {
        numberOfServings.setText(servingSizeEditText.getText().toString());
//        Commented out because redundant, activity calls it before saving anyway
//        sendDataToParentActivity();
        fats.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(recipeInfo.getServingsList()
                .get(CURRENT_SERVING_OPTION).getFat() * Double.valueOf(numberOfServings.getText().toString()))));
        carbs.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(recipeInfo.getServingsList()
                .get(CURRENT_SERVING_OPTION).getCarbs() * Double.valueOf(numberOfServings.getText().toString()))));
        proteins.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(recipeInfo.getServingsList()
                .get(CURRENT_SERVING_OPTION).getProtein() * Double.valueOf(numberOfServings.getText().toString()))));
        calories.setText(String.valueOf(DetailedFoodSearchResultsFragment.roundUp(recipeInfo.getServingsList()
                .get(CURRENT_SERVING_OPTION).getCalories() * Double.valueOf(numberOfServings.getText().toString()))));
    }

    private void returnToDiaryPage() {
        Intent i = new Intent(getActivity(), MainActivity.class);
        i.putExtra(Constants.START_METHOD, Constants.RETURN_FROM_SEARCH_FOOD);
        i.putExtra(Constants.DAY_OFFSET, this.newDayOffset);
        getActivity().startActivity(i);
        getActivity().finish();
    }

    private void returnToMealActivity() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.recipeId, recipeInfo.getRecipeId());
        resultIntent.putExtra(Constants.recipeServingOption, CURRENT_SERVING_OPTION);
        resultIntent.putExtra(Constants.recipeNumberOfServings, Double.valueOf(numberOfServings.getText().toString()));
        getActivity().setResult(CreateOrEditMealActivity.RESULT_RECIPE, resultIntent);
        getActivity().finish();
    }

    private void returnToCreatedMeal() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(Constants.recipeId, recipeInfo.getRecipeId());
        resultIntent.putExtra(Constants.recipeServingOption, CURRENT_SERVING_OPTION);
        resultIntent.putExtra(Constants.recipeNumberOfServings, Double.valueOf(numberOfServings.getText().toString()));
        getActivity().setResult(RESULT_OK, resultIntent);
        getActivity().finish();
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    //----------------------------
    //
    //

    private class AddCurrentRecipe extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(parentLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isReturningToCreateMealActivity) {
                    addCurrentRecipe();
                }
                return true;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(parentLayout, true, true);
                if(!isReturningToCreateMealActivity) {
                    returnToDiaryPage();
                }
                else {
                    returnToMealActivity();
                }
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new AddCurrentRecipe().execute();
                }
            }
        }
    }

    private class EditCurrentRecipe extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(parentLayout, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(!isPartOfCreatedMeal) {
                    editCurrentRecipe();
                }
                return true;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(parentLayout, true, true);
                if(isPartOfCreatedMeal) {
                    returnToCreatedMeal();
                }
                else {
                    returnToDiaryPage();
                }
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new EditCurrentRecipe().execute();
                }
            }
        }
    }

}
