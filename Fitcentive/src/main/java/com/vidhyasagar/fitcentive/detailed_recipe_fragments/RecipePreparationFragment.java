package com.vidhyasagar.fitcentive.detailed_recipe_fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.thefinestartist.finestwebview.FinestWebView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.Ingredients;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecipePreparationFragment extends Fragment {

    public static String RECIPE_INFO_STRING = "RECIPE_INFO_STRING";

    DetailedRecipeResultsInfo recipeInfo;
    Drawable recipeImageDrawable = null;

    View lineBreak;
    ProgressBar progressBar;
    ScrollView mainScrollView;
    ImageView recipeImage;
    TextView recipeName, recipeDescription, recipeNumberServings;
    TextView recipeCookingTime, recipePrepTime;
    ExpandableHeightListView ingredients, directions;
    Button returnToTop;

    public static RecipePreparationFragment newInstance(DetailedRecipeResultsInfo info) {
        RecipePreparationFragment fragment = new RecipePreparationFragment();
        Bundle args = new Bundle();
        args.putParcelable(RECIPE_INFO_STRING, info);
        fragment.setArguments(args);
        return fragment;
    }

    public RecipePreparationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_recipe_preparation, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        retrieveArguments();
        bindViews(view);

        if(!recipeInfo.getRecipeImageUrl().equals(Constants.unknown)) {
            new FetchRecipeImage().execute();
        }
        else {
            setUpOnScreenData();
        }

        setUpIngredients();
        setUpDirections();
        setUpReturnToTopButton();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_recipe_preparation, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_in_browser:
                openUpWebView(recipeInfo.getRecipeUrl());
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void openUpWebView(String url) {
        new FinestWebView.Builder(getContext())
                .progressBarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .swipeRefreshColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .toolbarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .menuColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .menuTextColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .statusBarColor(ContextCompat.getColor(getContext(), R.color.app_theme))
                .titleColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .urlColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .iconDefaultColor(ContextCompat.getColor(getContext(), android.R.color.white))
                .show(url);
    }

    private void loadImageFromWebOperations(String url) throws Exception{
        InputStream is = (InputStream) new URL(url).getContent();
        recipeImageDrawable = Drawable.createFromStream(is, "src name");

    }

    private void retrieveArguments() {
        recipeInfo = getArguments().getParcelable(RECIPE_INFO_STRING);
    }

    private void bindViews(View v) {
        mainScrollView = (ScrollView) v.findViewById(R.id.main_scrollview);
        recipeImage = (ImageView) v.findViewById(R.id.recipe_image);
        recipeName = (TextView) v.findViewById(R.id.recipe_name);
        recipeDescription = (TextView) v.findViewById(R.id.recipe_description);
        recipeNumberServings = (TextView) v.findViewById(R.id.number_of_servings);
        recipeCookingTime = (TextView) v.findViewById(R.id.cooking_time);
        recipePrepTime = (TextView) v.findViewById(R.id.prep_time);
        progressBar = (ProgressBar) v.findViewById(R.id.progress_bar);
        lineBreak = v.findViewById(R.id.line_break_3);
        ingredients = (ExpandableHeightListView) v.findViewById(R.id.ingredients_listview);
        directions = (ExpandableHeightListView) v.findViewById(R.id.directions_listview);
        returnToTop = (Button) v.findViewById(R.id.return_to_top_button);
    }

    private void setUpReturnToTopButton() {
        returnToTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.fullScroll(View.FOCUS_UP);
            }
        });
    }

    private void setUpOnScreenData() {
        recipeName.setText(recipeInfo.getRecipeName());
        recipeDescription.setText(recipeInfo.getRecipeDescription());
        if(recipeInfo.getCookingTime() != -1) {
            recipeCookingTime.setText(String.format(getString(R.string.x_mins), recipeInfo.getCookingTime()));
        }
        else {
            recipeCookingTime.setText(Constants.unknown);

        }
        if(recipeInfo.getPrepTime() != -1 ) {
            recipePrepTime.setText(String.format(getString(R.string.x_mins), recipeInfo.getPrepTime()));
        }
        else {
            recipePrepTime.setText(Constants.unknown);
        }
        if(recipeInfo.getNumberOfServings() != -1) {
            recipeNumberServings.setText(String.valueOf(recipeInfo.getNumberOfServings()));
        }
        else {
            recipeNumberServings.setText(Constants.unknown);
        }

        if(recipeImageDrawable != null) {
            recipeImage.setBackground(recipeImageDrawable);
        }
        else {
            recipeImage.setVisibility(View.GONE);
            lineBreak.setVisibility(View.GONE);
        }
    }

    private ArrayList<String> modifyIngredientsList(ArrayList<Ingredients> ingredientsList) {
        ArrayList<String> list = new ArrayList<>();
        for(Ingredients ingredient : ingredientsList) {
            list.add(ingredient.getIngredientDescription());
        }
        return list;
    }

    private ArrayList<String> modifyDirections(ArrayList<String> directionsList) {
        ArrayList<String> list = new ArrayList<>();
        int size = directionsList.size();
        for(int i = 0; i < size; i++) {
            list.add((i + 1) + ". " +  directionsList.get(i));
        }
        return list;
    }

    private void setUpIngredients() {
        ArrayList<String> modifiedIngredients = modifyIngredientsList(recipeInfo.getIngredientsList());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.layout_recipe_listview_textview, modifiedIngredients);
        ingredients.setAdapter(adapter);
        ingredients.setExpanded(true);
    }

    private void setUpDirections() {
        ArrayList<String> modifiedDirections = modifyDirections(recipeInfo.getDirections());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.layout_recipe_listview_textview, modifiedDirections);
        directions.setAdapter(adapter);
        directions.setExpanded(true);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    //----------------------------
    // ASYNCTASK(S)
    //----------------------------

    private class FetchRecipeImage extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            setViewVisibility(mainScrollView, false, true);
            setViewVisibility(progressBar, true, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                loadImageFromWebOperations(recipeInfo.getRecipeImageUrl());
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                setViewVisibility(progressBar, false, true);
                setViewVisibility(mainScrollView, true, true);
                setUpOnScreenData();
            }
            else {
                Toast.makeText(getContext(), getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }

    }

}
