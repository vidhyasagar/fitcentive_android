package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thefinestartist.finestwebview.FinestWebView;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.RecipeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vharihar on 2016-11-22.
 */

public class RecipeResultsAdapter extends  RecyclerView.Adapter<RecipeResultsAdapter.RecipeResultHolder>  {

    private ArrayList<RecipeInfo> recipeList;
    private Context mContext;
    private int dayOffset;
    private String searchText;

    private boolean isReturningToCreateMealActivity;

    private int CURRENT_PAGE_NUMBER = 1;
    private int RETRY_COUNT = 0;
    private FatSecretRecipes recipes;
    private DiaryPageFragment.ENTRY_TYPE entryType;

    private boolean HAVE_RESULTS_EXHAUSTED = false;


    public static class RecipeResultHolder extends RecyclerView.ViewHolder {

        TextView recipeName, recipeDescription;
        ImageView webButton;
        ImageView favoriteButton;
        CardView cardView;

        public RecipeResultHolder(View itemView) {
            super(itemView);

            recipeName = (TextView) itemView.findViewById(R.id.recipe_name);
            recipeDescription = (TextView) itemView.findViewById(R.id.recipe_description);
            webButton = (ImageView) itemView.findViewById(R.id.web_button);
            cardView = (CardView) itemView.findViewById(R.id.recipe_results_card_view);
            favoriteButton = (ImageView)itemView.findViewById(R.id.favorite_button);
         }
    }

    public RecipeResultsAdapter(ArrayList<RecipeInfo> recipeList, Context mContext, int dayOffset, DiaryPageFragment.ENTRY_TYPE entryType,
                                boolean isReturningToCreateMealActivity) {
        this.recipeList = recipeList;
        this.mContext = mContext;
        this.dayOffset = dayOffset;
        this.entryType = entryType;
        this.isReturningToCreateMealActivity = isReturningToCreateMealActivity;
        recipes = new FatSecretRecipes();
    }

    @Override
    public RecipeResultsAdapter.RecipeResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_recipe_result, parent, false);
        return new RecipeResultHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecipeResultsAdapter.RecipeResultHolder holder, int position) {
        final RecipeInfo info = recipeList.get(position);

        holder.recipeName.setText(info.getRecipeName());
        holder.recipeDescription.setText(info.getRecipeDescription());

        holder.webButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openUpWebView(info.getRecipeUrl());
            }
        });

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExpandedRecipeActivity(info.getRecipeId());
            }
        });


        if(info.isFavorite()) {
            holder.favoriteButton.setVisibility(View.VISIBLE);
        } else {
            holder.favoriteButton.setVisibility(View.GONE);
        }

        if(position == recipeList.size() - 10) {
            if(!HAVE_RESULTS_EXHAUSTED) {
                new SearchRecipes(searchText).execute();
            }
        }
    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    public void setSearchText(String text) {
        this.searchText = text;
        // Setting this to false because with a new searchText, results may differ
        HAVE_RESULTS_EXHAUSTED = false;
    }

    public void setCurrentPage(int page) {
        this.CURRENT_PAGE_NUMBER = page;
    }

    private String getMealTypeString(DiaryPageFragment.ENTRY_TYPE type) {
        switch (type) {
            case BREAKFAST: return mContext.getString(R.string.breakfast);
            case LUNCH: return mContext.getString(R.string.lunch);
            case DINNER: return mContext.getString(R.string.dinner);
            case EXCERCISE: return mContext.getString(R.string.excercise);
            case WATER: return mContext.getString(R.string.water);
            case SNACKS: return mContext.getString(R.string.snacks);
            default: return null;
        }
    }

    public void replaceDataSet(ArrayList<RecipeInfo> newSet) {
        this.recipeList = newSet;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.recipeList.isEmpty();
    }

    private void startExpandedRecipeActivity(long recipeId){
        Intent i = new Intent(mContext, ExpandedRecipeActivity.class);
        i.putExtra(Constants.recipeId, recipeId);
        i.putExtra(SearchFoodResultsFragment.DAY_OFFSET_STRING, this.dayOffset);
        i.putExtra(Constants.mealType, getMealTypeString(entryType));
        i.putExtra(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL, isReturningToCreateMealActivity);
        i.putExtra(Constants.type, this.entryType);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURN_FROM_FOOD_SELECT);
    }

    private void openUpWebView(String url) {
        new FinestWebView.Builder(mContext)
                .progressBarColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .swipeRefreshColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .toolbarColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .menuColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .menuTextColor(ContextCompat.getColor(mContext, android.R.color.white))
                .statusBarColor(ContextCompat.getColor(mContext, R.color.app_theme))
                .titleColor(ContextCompat.getColor(mContext, android.R.color.white))
                .urlColor(ContextCompat.getColor(mContext, android.R.color.white))
                .iconDefaultColor(ContextCompat.getColor(mContext, android.R.color.white))
                .show(url);
    }

    private void fatSecretRecipeSearch(String searchText) throws JSONException {
        JSONObject result = recipes.searchRecipes(searchText, CURRENT_PAGE_NUMBER, null);
        if(result != null) {
            JSONArray resultArray = result.optJSONArray(Constants.recipe);
            if(resultArray != null) {
                CURRENT_PAGE_NUMBER++;
                int size = resultArray.length();
                for (int i = 0; i < size; i++) {
                    JSONObject recipeItem = resultArray.getJSONObject(i);
                    long recipeId = recipeItem.optLong(Constants.recipeId, -1);
                    String recipeDescription = recipeItem.optString(Constants.recipeDescription, Constants.unknown);
                    String recipeImage = recipeItem.optString(Constants.recipeImage, null);
                    String recipeName = recipeItem.optString(Constants.recipeName, Constants.unknown);
                    String recipeUrl = recipeItem.optString(Constants.recipeUrl, null);
                    RecipeInfo newFood = new RecipeInfo(recipeId, recipeName, recipeDescription, recipeUrl, recipeImage);
                    newFood.setFavorite(false);
                    recipeList.add(newFood);
                }
            }
            else {
                // This means theres no value for "recipe", re-searching is useless because it's just gonna clog up the UI thread
                HAVE_RESULTS_EXHAUSTED = true;
            }
        }
    }

    //--------------------------
    // ASYNCTASK(S)
    //--------------------------

    private class SearchRecipes extends AsyncTask<Void, Void, Boolean> {

        private String searchText;

        public SearchRecipes(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fatSecretRecipeSearch(searchText);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                notifyDataSetChanged();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new SearchRecipes(this.searchText).execute();
                }
            }
        }
    }
}
