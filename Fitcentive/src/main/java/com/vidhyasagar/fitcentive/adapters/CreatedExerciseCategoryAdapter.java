package com.vidhyasagar.fitcentive.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-14.
 */

public class CreatedExerciseCategoryAdapter extends ArrayAdapter<String>{

    ArrayList<String> items;
    Context context;

    int selectedPosition = -1;

    public CreatedExerciseCategoryAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.items = objects;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_exercise_meta_data, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        CheckBox box = (CheckBox) convertView.findViewById(R.id.checkBox1);

        name.setText(items.get(position));

        if(position == selectedPosition) {
            box.setOnCheckedChangeListener(null);
            box.setChecked(true);
        }
        else {
            box.setOnCheckedChangeListener(null);
            box.setChecked(false);
        }

        box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    selectedPosition = position;
                }
                else {
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public int getSelectedPosition() {
        return this.selectedPosition;
    }

    public void setPositionAsChecked(int position) {
        selectedPosition = position - CreateOrEditExerciseActivity.CATEGORY_OFFSET;
        notifyDataSetChanged();
    }
}
