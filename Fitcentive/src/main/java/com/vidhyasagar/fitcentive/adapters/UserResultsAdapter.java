package com.vidhyasagar.fitcentive.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.wrappers.UserResultsInfo;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vharihar on 2016-10-04.
 */

// TODO : Centre text when no results are found using LayoutParams
public class UserResultsAdapter extends RecyclerView.Adapter<UserResultsAdapter.UserResultsHolder>  {

    private ArrayList<UserResultsInfo> userData;
    private Context context;

    public static class UserResultsHolder extends RecyclerView.ViewHolder {

        protected CardView uCardView;
        protected TextView uName;
        protected TextView uUserName;
        protected CircleImageView uImage;
        protected Context mContext;

        public UserResultsHolder(View itemView, final Context mContext) {
            super(itemView);

            uUserName = (TextView) itemView.findViewById(R.id.user_username_or_location);
            uName = (TextView) itemView.findViewById(R.id.user_full_name);
            uImage = (CircleImageView) itemView.findViewById(R.id.user_profile_picture);
            uCardView = (CardView) itemView.findViewById(R.id.user_results_card_view);
            this.mContext = mContext;

        }

    }

    public UserResultsAdapter(ArrayList<UserResultsInfo> data, Context context) {
        this.userData = data;
        this.context = context;
    }

    @Override
    public UserResultsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_user_results_list, parent, false);
        return new UserResultsHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(final UserResultsHolder holder, int position) {
        final UserResultsInfo cData = userData.get(position);
        if(cData != null) {
            holder.uImage.setImageBitmap(cData.getUserProfilePicture());
            holder.uName.setText(cData.getUserFullName());
            holder.uUserName.setText(cData.getUserUserName());
            holder.uImage.setVisibility(View.VISIBLE);
            holder.uUserName.setVisibility(View.VISIBLE);

            holder.uCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.uCardView.setBackgroundColor(ContextCompat.getColor(context, R.color.lighter_gray));
                    Intent i = new Intent(context, ProfileActivity.class);
                    i.putExtra("username", cData.getUserUserName());
                    context.startActivity(i);
                }
            });
        }
        else {
            holder.uName.setText(context.getString(R.string.no_results_found));
            holder.uUserName.setVisibility(View.GONE);
            holder.uImage.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return this.userData.size();
    }



}
