package com.vidhyasagar.fitcentive.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.activities.SinglePostActivity;
import com.vidhyasagar.fitcentive.dialog_fragments.UpdatePostDialogFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.CommentInfo;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vharihar on 2016-09-28.
 */

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.NewsFeedHolder> {

    // News Feed Holder that holds references to UI elements following the ViewHolder Pattern
    public static class NewsFeedHolder extends RecyclerView.ViewHolder {

        protected ImageView pImage;
        protected TextView pComments;
        protected TextView pCaption;
        protected TextView pUser;
        protected TextView pDate;
        protected TextView pLikers;
        protected ImageButton commentButton;
        protected ImageButton likeButton;
        protected CardView pCardView;

        protected boolean isLiked = false;

        public NewsFeedHolder(View itemView) {
            super(itemView);

            pImage = (ImageView) itemView.findViewById(R.id.post_photo);
            pCaption = (TextView) itemView.findViewById(R.id.post_caption);
            pDate = (TextView) itemView.findViewById(R.id.post_time);
            pUser = (TextView) itemView.findViewById(R.id.post_user);
            pLikers = (TextView) itemView.findViewById(R.id.post_likers);
            commentButton = (ImageButton) itemView.findViewById(R.id.comment_button);
            likeButton = (ImageButton) itemView.findViewById(R.id.like_button);
            pComments = (TextView) itemView.findViewById(R.id.numberComentsTextView);
            pCardView = (CardView) itemView.findViewById(R.id.card_view);
        }
    }

    // Members for the main newsfeed elements
    private ArrayList<NewsFeedInfo> newsFeedList;
    private ArrayList<String> following;
    private Context mContext;
    private int POSTS_COUNT;
    private boolean canClickUserName;

    RecyclerView rView;
    CardView eCView;

    // Members for associated comment elements
    private ArrayList<CommentInfo> commentList;
    RecyclerView recyclerView;
    LinearLayoutManager llm;
    CommentsAdapter rvAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;
    EditText commentEditText;
    RelativeLayout commentLayout;
    InputMethodManager imm;

    // --------------------------------------------------------------

    public NewsFeedAdapter(ArrayList<NewsFeedInfo> list, Context mContext, boolean canClickUserName){
        this.newsFeedList = list;
        this.mContext = mContext;
        this.POSTS_COUNT  = mContext.getResources().getInteger(R.integer.post_query_init_limit);
        this.canClickUserName = canClickUserName;
        imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void setEmptyCardAndRecycler(RecyclerView rView, CardView eCView) {
        this.rView = rView;
        this.eCView = eCView;
    }

    @Override
    public NewsFeedAdapter.NewsFeedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_newsfeed_card, parent, false);
        return new NewsFeedHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsFeedAdapter.NewsFeedHolder holder, int position) {
        if(!newsFeedList.isEmpty()) {
            final NewsFeedInfo info = newsFeedList.get(position);
            Bitmap postImage = info.getPostImage();

            if (postImage != null) {
                holder.pImage.setVisibility(View.VISIBLE);
                holder.pImage.setImageBitmap(info.getPostImage());
            } else {
                holder.pImage.setVisibility(View.GONE);
            }
            holder.pCaption.setText(String.format(mContext.getString(R.string.quotes), info.getPostCaption()));
            holder.pDate.setText(info.getPostDate());
            holder.pUser.setText(info.getUserName());
            holder.pComments.setText(String.valueOf(info.getNumberComments()));

            if (info.getPostLikeStatus()) {
                holder.likeButton.setColorFilter(ContextCompat.getColor(mContext, R.color.app_theme));
                holder.isLiked = true;
                if (info.getNumberLikes() == 1) {
                    holder.pLikers.setText(mContext.getString(R.string.you_like_this));
                } else if (info.getNumberLikes() == 2) {
                    holder.pLikers.setText(mContext.getString(R.string.you_and_one_like_this));
                } else {
                    holder.pLikers.setText(String.format(mContext.getString(R.string.you_and_x_others_like_this), String.valueOf(info.getNumberLikes() - 1)));
                }
            } else {
                holder.likeButton.setColorFilter(ContextCompat.getColor(mContext, R.color.transp_black));
                if (info.getNumberLikes() == 0) {
                    holder.pLikers.setText(mContext.getString(R.string.no_one_likes_this));
                } else if (info.getNumberLikes() == 1) {
                    holder.pLikers.setText(String.format(mContext.getString(R.string.x_likes_this), info.getFirstLiker()));
                } else if (info.getNumberLikes() == 2) {
                    holder.pLikers.setText(String.format(mContext.getString(R.string.x_and_one_like_this), info.getFirstLiker()));
                } else {
                    holder.pLikers.setText(String.format(mContext.getString(R.string.x_and_x_others_like_this),
                            info.getFirstLiker(),
                            String.valueOf(info.getNumberLikes() - 1)));
                }
            }

            if(info.getPostCommentStatus()) {
                holder.commentButton.setColorFilter(ContextCompat.getColor(mContext, R.color.app_theme));
                holder.pComments.setTextColor(ContextCompat.getColor(mContext, R.color.app_theme));
            }
            else {
                holder.commentButton.setColorFilter(ContextCompat.getColor(mContext, R.color.transp_black));
                holder.pComments.setTextColor(ContextCompat.getColor(mContext, R.color.transp_black));
            }

            // Like listener
            holder.likeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!holder.isLiked) {
                        holder.isLiked = true;
                        holder.likeButton.setColorFilter(ContextCompat.getColor(mContext, R.color.app_theme));
                        updateImageLikeStatus(holder.getAdapterPosition(), true, holder.likeButton);
                        updatePostUi(info, holder, true);
                    } else {
                        holder.isLiked = false;
                        holder.likeButton.setColorFilter(ContextCompat.getColor(mContext, R.color.transp_black));
                        updateImageLikeStatus(holder.getAdapterPosition(), false, holder.likeButton);
                        updatePostUi(info, holder, false);
                    }
                }
            });

            // Clicking username to go to that user profile
            if(this.canClickUserName) {
                holder.pUser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, ProfileActivity.class);
                        i.putExtra("username", info.getUserName());
                        mContext.startActivity(i);
                    }
                });
            }

            setUpCommentButtonListener(holder, info, position);
            setUpCardViewLongClickListener(holder, info, position);
            setUpCardViewShortClickListener(info.getObjectId(), holder.pCardView);
        }

        if(POSTS_COUNT - position == 4) {
            // 3rd last position, fetch more posts as the user is nearing the end
            new FetchMoreInfo().execute();
        }
    }

    @Override
    public int getItemCount() {
        return this.newsFeedList.size();
    }

    // --------------------------------------------------------------

    static public boolean checkLikeStatus(String username, List<String> likersList) {
        for(String liker : likersList) {
            if(liker.equals(username)) {
                return true;
            }
        }
        return false;
    }

    static public boolean checkCommentStatus(String username, String postId) {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Comment);
        query.whereEqualTo(Constants.username, username);
        query.whereEqualTo(Constants.postId, postId);
        try {
            ParseObject object = query.getFirst();
            if(object != null) {
                return true;
            } else {
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    // --------------------------------------------------------------

    public void updateElementAt(int position, NewsFeedInfo info) {
        newsFeedList.set(position, info);
        this.notifyDataSetChanged();
    }

    public Context getmContext() {
        return this.mContext;
    }

    public void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.comment_label), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.caption_copied), Toast.LENGTH_SHORT).show();
    }

    public void deletePost(String objectId, int position) throws Exception {
        // Deleting post
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Post);
        query.whereEqualTo(Constants.objectId, objectId);
        ParseObject object = query.getFirst();
        object.delete();

        // Deleting associated comments
        ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.Comment);
        query.whereEqualTo(Constants.postId, objectId);
        List<ParseObject> result = query.find();
        for(ParseObject comment : result) {
            comment.delete();
        }

        newsFeedList.remove(position);
    }

    public void removePost(final NewsFeedInfo info, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
        builder.setTitle(mContext.getString(R.string.confirm_post_deletion))
                .setMessage(mContext.getString(R.string.post_deletion_message))
                .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(mContext.getDrawable(R.drawable.ic_logo));
        }

        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

        // Perform deletion on click of positive button
        // Perform saving on click of positive button
        dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Perform deletion of the post
                new DeletePost(info.getObjectId(), position).execute();
                dialog.dismiss();
            }
        });
    }

    public void editPost(final NewsFeedInfo info, final int position) {
        FragmentManager fm = ((FragmentActivity)mContext).getSupportFragmentManager();
        UpdatePostDialogFragment fragment = new UpdatePostDialogFragment();
        fragment.setData(info, position, this);
        fragment.show(fm, UpdatePostDialogFragment.TAG);
    }

    public void setUpCardViewLongClickListener(NewsFeedHolder holder, final NewsFeedInfo info, final int position) {
        // Long Click listener
        holder.pCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence [] choices;
                if(info.getUserName().equals(ParseUser.getCurrentUser().getUsername())) {
                    choices = mContext.getResources().getStringArray(R.array.my_comment_choices);
                }
                else {
                    choices = mContext.getResources().getStringArray(R.array.comment_choices);
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

                builder.setItems(choices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if(info.getUserName().equals(ParseUser.getCurrentUser().getUsername())) {
                            switch (item) {
                                case 0: copyToClipBoard(info.getPostCaption());
                                    break;
                                case 1: editPost(info, position);
                                    break;
                                case 2: removePost(info, position);
                                    break;
                                default: break;
                            }
                        }
                        else {
                            switch (item) {
                                case 0: copyToClipBoard(info.getPostCaption());
                                    break;
                                default: break;
                            }
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.show();
                return true;
            }
        });
    }

    public void setUpCardViewShortClickListener(final String objectId, CardView cardView) {
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, SinglePostActivity.class);
                i.putExtra("postId", objectId);
                mContext.startActivity(i);
            }
        });
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = mContext.getResources().getInteger(android.R.integer.config_shortAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public void updateImageLikeStatus(int position, final boolean isLiked, final View v) {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Post);
        query.whereEqualTo(Constants.objectId, newsFeedList.get(position).getObjectId());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e != null) {
                    e.printStackTrace();
                    Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                }
                else {
                    if(isLiked) {
                        objects.get(0).put(Constants.numberLikes, objects.get(0).getInt(Constants.numberLikes) + 1);
                    }
                    else {
                        objects.get(0).put(Constants.numberLikes, objects.get(0).getInt(Constants.numberLikes) - 1);
                    }

                    // Adding liker username to likers list, if isLiked
                    List<String> likersArray =  objects.get(0).getList(Constants.likers);
                    if(likersArray == null) {
                        likersArray = new ArrayList<>();
                    }

                    if(isLiked) {
                        boolean shouldAdd = true;
                        for (String liker : likersArray) {
                            if (liker.equals(ParseUser.getCurrentUser().getUsername())) {
                                shouldAdd = false;
                                break;
                            }
                        }
                        if (shouldAdd) {
                            likersArray.add(ParseUser.getCurrentUser().getUsername());
                            objects.get(0).put(Constants.likers, likersArray);
                        }
                    }

                    else {
                        // Must remove user from list of likers now
                        List<String> newLikersArray = new ArrayList<String>();
                        for(String liker : likersArray) {
                            if(!liker.equals(ParseUser.getCurrentUser().getUsername())) {
                                newLikersArray.add(liker);
                            }
                        }
                        objects.get(0).put(Constants.likers, newLikersArray);
                    }


                    objects.get(0).saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e == null) {
                                if(isLiked) {
                                    Snackbar snackbar =  Snackbar.make(v, mContext.getString(R.string.liked), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                                    Toast.makeText(mContext, mContext.getString(R.string.liked), Toast.LENGTH_SHORT).show();
                                }
                                else {
                                    Snackbar snackbar =  Snackbar.make(v, mContext.getString(R.string.unliked), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                    View view = snackbar.getView();
                                    TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                                    txtv.setGravity(Gravity.CENTER_HORIZONTAL);
//                                    Toast.makeText(mContext, mContext.getString(R.string.unliked), Toast.LENGTH_LONG).show();
                                }
                            }
                            else {
                                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        // Adding notification object to corresponding postObject
        if(!newsFeedList.get(position).getUserName().equals(ParseUser.getCurrentUser().getUsername()) && isLiked) {
            ParseObject newObject = new ParseObject(Constants.Notification);
            newObject.put(Constants.type, mContext.getString(R.string.like_request));
            newObject.put(Constants.postId, newsFeedList.get(position).getObjectId());
            newObject.put(Constants.receiverUsername, newsFeedList.get(position).getUserName());
            newObject.put(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
            newObject.put(Constants.isActive, true);
            if (ParseUser.getCurrentUser().getParseFile(Constants.profilePicture) != null) {
                newObject.put(Constants.image, ParseUser.getCurrentUser().getParseFile(Constants.profilePicture));
            }
            newObject.saveInBackground();

            // Push notification cloud code call
            Map<String, String> params = new HashMap<>();
            params.put(mContext.getString(R.string.receiver), newsFeedList.get(position).getUserName());
            params.put(mContext.getString(R.string.provider), ParseUser.getCurrentUser().getString(Constants.firstname)
                                    + " " + ParseUser.getCurrentUser().getString(Constants.lastname));
            ParseCloud.callFunctionInBackground(mContext.getString(R.string.likePush), params);
        }
    }

    public void updatePostUi(NewsFeedInfo info, NewsFeedAdapter.NewsFeedHolder holder , boolean isLiked) {
        if(isLiked) {
            if(info.getNumberLikes() == 0) {
                holder.pLikers.setText(mContext.getString(R.string.you_like_this));
            }
            else if (info.getNumberLikes() == 1) {
                holder.pLikers.setText(mContext.getString(R.string.you_and_one_like_this));
            }
            else {
                holder.pLikers.setText(String.format(mContext.getString(R.string.you_and_x_others_like_this), String.valueOf(info.getNumberLikes())));
            }
            info.addNumberLikes(1);
        }
        else {
            if(info.getNumberLikes() == 1) {
                holder.pLikers.setText(mContext.getString(R.string.no_one_likes_this));
            }
            else if(info.getNumberLikes() == 2) {
                holder.pLikers.setText(String.format(mContext.getString(R.string.x_likes_this), info.getFirstLiker()));
            }
            else if(info.getNumberLikes() == 3) {
                holder.pLikers.setText(String.format(mContext.getString(R.string.x_and_one_like_this), info.getFirstLiker()));
            }
            else {
                holder.pLikers.setText(String.format(mContext.getString(R.string.x_and_x_others_like_this),
                        info.getFirstLiker(),
                        String.valueOf(info.getNumberLikes() - 2)));
            }

            info.addNumberLikes(-1);
        }
    }

    public void createCommentDialog(final NewsFeedInfo info, final int position) {

        // Building dialog to show
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
        builder.setTitle(mContext.getString(R.string.add_or_view_comments))
                .setPositiveButton(mContext.getString(R.string.post), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(mContext.getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_comment_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_comment_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.show();

        // Initializing views from dialog
        commentList = new ArrayList<>();
        llm =  new LinearLayoutManager(dialog.getContext());
        llm.setStackFromEnd(true);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.comments_recycler);
        swipeRefreshLayout = (SwipeRefreshLayout) dialog.findViewById(R.id.swipeContainer);
        commentEditText = (EditText) dialog.findViewById(R.id.comment_edittext);
        commentLayout = (RelativeLayout) dialog.findViewById(R.id.comment_dialog_rel_layout);
        progressBar = (ProgressBar) dialog.findViewById(R.id.progress_bar);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(llm);
        rvAdapter = new CommentsAdapter(commentList, dialog.getContext(), this, info, position);
        recyclerView.setAdapter(rvAdapter);

        // Setting colors for swipe refresh as well as for the progressbar
        swipeRefreshLayout.setColorSchemeResources(R.color.app_theme);
        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.app_theme),
                android.graphics.PorterDuff.Mode.MULTIPLY);

        // Listeners
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchComments(info.getObjectId(), false).execute();
            }
        });
        commentLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                return true;
            }
        });

        // Override post button on the created dialog
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(commentEditText.getText().toString().trim())) {
                    // Perform saving of the post
                    new SaveComment(info.getObjectId(), commentEditText.getText().toString().trim(), info).execute();
                }
                else {
                    Toast.makeText(mContext, mContext.getString(R.string.error_empty_comment), Toast.LENGTH_SHORT).show();
                }
            }
        });


        new FetchComments(info.getObjectId(), true).execute();

    }

    public void setUpCommentButtonListener(NewsFeedHolder holder, final NewsFeedInfo info, final int position) {
        holder.commentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createCommentDialog(info, position);
            }
        });
    }

    public void setFollowingList(ArrayList<String> following) {
        this.following = following;
    }

    public void setPostsCount(int count) {
        this.POSTS_COUNT = count;
    }

    public void populateNews() throws ParseException, Exception {
        // Name, caption, comments, likes, date, image
        ParseQuery<ParseObject> postsQuery = new ParseQuery<>(Constants.Post);
        // Post query check where username is list of usernames of people who current user follows
        if(following == null) {
            return;
        }
        postsQuery.whereContainedIn(Constants.username, following);
        postsQuery.orderByDescending(Constants.createdAt);
        postsQuery.setSkip(POSTS_COUNT);
        postsQuery.setLimit(mContext.getResources().getInteger(R.integer.post_query_init_limit));
        POSTS_COUNT += mContext.getResources().getInteger(R.integer.post_query_init_limit);
        List<ParseObject> objects = postsQuery.find();

        if(objects.size() == 0) {
            return;
        }

        for(ParseObject post : objects) {
            String objectId = post.getObjectId();
            String username = post.getString(Constants.username);
            String caption = post.getString(Constants.caption);
            int numberComments = post.getInt(Constants.numberComments);
            int numberLikes = post.getInt(Constants.numberLikes);
            Date date = post.getCreatedAt();
            ParseFile image = post.getParseFile(Constants.image);
            List<String> likersList = post.getList(Constants.likers);
            NewsFeedInfo newPost;
            if(image != null) {
                byte[] byteArray = image.getData();
                Bitmap postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, postBitmap, objectId);
            }
            else {
                newPost = new NewsFeedInfo(username, caption, numberComments, numberLikes, date, null, objectId);
            }

            if(likersList == null) {
                newPost.setPostLikeStatus(false);
            }
            else {
                // If currentusername is in the list of likers for the post, then set it to true
                if(checkLikeStatus(ParseUser.getCurrentUser().getUsername(), likersList)) {
                    newPost.setPostLikeStatus(true);
                }
                else {
                    newPost.setPostLikeStatus(false);
                }
            }
            if(likersList == null || likersList.size() == 0) {
                newPost.setFirstLiker(null);
            }
            else if(likersList.size() != 0) {
                // ParseQuery to extract user first name and last name from given user_name
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo(Constants.username, likersList.get(0));

                // Since usernames are unique, this must return only ONE object
                List<ParseUser> foundUsers = query.find();
                String likerName = foundUsers.get(0).getString(Constants.firstname) + " " +
                        foundUsers.get(0).getString(Constants.lastname);
                newPost.setFirstLiker(likerName);
            }

            if(checkCommentStatus(ParseUser.getCurrentUser().getUsername(), objectId)) {
                newPost.setPostCommentStatus(true);
            }
            else {
                newPost.setPostCommentStatus(false);
            }

            this.newsFeedList.add(newPost);
        }
    }

    public void fetchCommentsForPost(String postId) throws ParseException, Exception {
        commentList.clear();
        String comment, commentId;
        String username;
        Date date;
        ParseFile imageFile;
        byte[] byteArray;
        Bitmap postBitmap;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Comment);
        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();

        query.whereEqualTo(Constants.postId, postId);
        query.orderByAscending(Constants.createdAt);
        List<ParseObject> result = query.find();

        for(ParseObject object : result) {
            CommentInfo commentInfo;
            comment = object.getString(Constants.comment);
            username = object.getString(Constants.username);
            date = object.getCreatedAt();
            commentId = object.getObjectId();

            userQuery.whereEqualTo(Constants.username, username);
            ParseUser user = userQuery.getFirst();

            imageFile = user.getParseFile(Constants.profilePicture);
            if(imageFile != null) {
                byteArray = imageFile.getData();
                postBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
                commentInfo = new CommentInfo(postBitmap, username, comment, date, true, commentId);
            }
            else {
                commentInfo = new CommentInfo(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo),
                        username, comment, date, true, commentId);
            }

            commentList.add(commentInfo);
        }

    }

    public void saveComment(String postId, String comment, NewsFeedInfo info) throws ParseException, Exception {
        ParseObject commentObject = new ParseObject(Constants.Comment);
        commentObject.put(Constants.postId, postId);
        commentObject.put(Constants.username, ParseUser.getCurrentUser().getUsername());
        commentObject.put(Constants.comment, comment);
        commentObject.save();

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Post);
        query.whereEqualTo(Constants.objectId, postId);
        ParseObject post = query.getFirst();
        int numComments = post.getInt(Constants.numberComments);
        numComments++;
        post.put(Constants.numberComments, numComments);
        post.save();

        ParseQuery<ParseUser> userParseQuery = ParseUser.getQuery();
        userParseQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        ParseUser foundUser = userParseQuery.getFirst();
        ParseFile imageFile = foundUser.getParseFile(Constants.profilePicture);
        byte[] byteArray = imageFile.getData();
        Bitmap picture = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        CommentInfo newComment = new CommentInfo(picture, ParseUser.getCurrentUser().getUsername(), comment, new Date(), false, commentObject.getObjectId());
        commentList.add(newComment);

        info.addNumberComments(1);
        info.setPostCommentStatus(true);

        // Adding notification object to account for this comment
        // Adding notification object to corresponding postObject
        if(!info.getUserName().equals(ParseUser.getCurrentUser().getUsername())) {
            ParseObject newObject = new ParseObject(Constants.Notification);
            newObject.put(Constants.type, mContext.getString(R.string.comment_request));
            newObject.put(Constants.postId, info.getObjectId());
            newObject.put(Constants.receiverUsername, info.getUserName());
            newObject.put(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
            newObject.put(Constants.isActive, true);
            if (ParseUser.getCurrentUser().getParseFile(Constants.profilePicture) != null) {
                newObject.put(Constants.image, ParseUser.getCurrentUser().getParseFile(Constants.profilePicture));
            }
            newObject.save();

            // Push notification cloud code call
            Map<String, String> params = new HashMap<>();
            params.put(mContext.getString(R.string.receiver), info.getUserName());
            params.put(mContext.getString(R.string.provider), ParseUser.getCurrentUser().getString(Constants.firstname)
                                    + " " + ParseUser.getCurrentUser().getString(Constants.lastname));
            ParseCloud.callFunctionInBackground(mContext.getString(R.string.commentPush), params);
        }
    }

    //----------------------------------------
    // TASKS TO DO OFF THE MAIN UI THREAD
    //----------------------------------------

    private class FetchMoreInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                populateNews();
                return true;
            } catch(ParseException e) {
                e.printStackTrace();
                return false;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                notifyDataSetChanged();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class FetchComments extends AsyncTask<Void, Void, Boolean> {

        String postId;
        boolean showProgressBar;

        public FetchComments(String objectId, boolean showProgressBar) {
            this.postId = objectId;
            this.showProgressBar = showProgressBar;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(showProgressBar) {
                setViewVisibility(progressBar, true, true);
            }
            setViewVisibility(recyclerView, false, true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchCommentsForPost(postId);
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
            catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(showProgressBar) {
                    setViewVisibility(progressBar, false, true);
                }
                rvAdapter.notifyDataSetChanged();
                setViewVisibility(recyclerView, true, true);
                swipeRefreshLayout.setRefreshing(false);
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class SaveComment extends AsyncTask<Void, Void, Boolean> {

        private String postId;
        private String comment;
        private NewsFeedInfo info;

        public SaveComment(String postId, String comment, NewsFeedInfo info) {
            this.comment = comment;
            this.postId = postId;
            this.info = info;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                saveComment(postId, comment, info);
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e ){
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Snackbar snackbar =  Snackbar.make(recyclerView, mContext.getString(R.string.comment_post_successful), Snackbar.LENGTH_LONG);
                snackbar.show();
                View view = snackbar.getView();
                TextView txtv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
                txtv.setGravity(Gravity.CENTER_HORIZONTAL);

                commentEditText.getText().clear();
                rvAdapter.notifyDataSetChanged();
                recyclerView.scrollToPosition(commentList.size()-1);
                notifyDataSetChanged();
//                new FetchComments(postId, true).execute();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeletePost extends AsyncTask<Void, Void, Boolean> {

        int position;
        String objectId;

        public DeletePost(String objectId, int position) {
            this.objectId = objectId;
            this.position = position;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deletePost(objectId, position);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.post_deleted), Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
                if (!newsFeedList.isEmpty()) {
                    setViewVisibility(eCView, false, true);
                    setViewVisibility(rView, true, true);
                } else {
                    setViewVisibility(rView, false, true);
                    setViewVisibility(eCView, true, true);
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
