package com.vidhyasagar.fitcentive.adapters;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.CommentInfo;
import com.vidhyasagar.fitcentive.wrappers.NewsFeedInfo;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vharihar on 2016-10-12.
 */
public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentHolder> {

    private ArrayList<CommentInfo> comments;
    private Context mContext;
    NewsFeedAdapter newsFeedAdapter;
    NewsFeedInfo info;
    int infoPosition;
    InputMethodManager imm;

    boolean IS_COMMENT_DIFFERENT = false;
    boolean IS_SINGLE_POST = false;

    public static class CommentHolder extends RecyclerView.ViewHolder{

        protected CircleImageView userImage;
        protected TextView commentTextView;
        protected TextView userName;
        protected TextView commentDate;
        protected CardView commentCardView;

        public CommentHolder(View item) {
            super(item);

            userImage = (CircleImageView) item.findViewById(R.id.user_profile_picture);
            commentTextView = (TextView) item.findViewById(R.id.post_comment);
            userName = (TextView) item.findViewById(R.id.user_full_name);
            commentDate = (TextView) item.findViewById(R.id.comment_date);
            commentCardView = (CardView) item.findViewById(R.id.comment_card_view);

        }
    }

    public CommentsAdapter(ArrayList<CommentInfo> comments, Context mContext, NewsFeedAdapter newsFeedAdapter, NewsFeedInfo info, int position) {
        this.comments = comments;
        this.mContext = mContext;
        this.newsFeedAdapter = newsFeedAdapter;
        this.info = info;
        this.infoPosition = position;
        imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        IS_SINGLE_POST = false;
    }

    public CommentsAdapter(ArrayList<CommentInfo> comments, Context mContext, NewsFeedInfo info) {
        this.comments = comments;
        this.mContext = mContext;
        this.info = info;
        IS_SINGLE_POST = true;
        imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public CommentsAdapter.CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_comment_card, parent, false);
        return new CommentHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.CommentHolder holder, final int position) {
        final CommentInfo commentInfo = comments.get(position);

        holder.commentDate.setText(commentInfo.getCommentDate());
        holder.commentTextView.setText(commentInfo.getComment());
        holder.userName.setText(commentInfo.getCommentUsername());
        holder.userImage.setImageBitmap(commentInfo.getUserPhoto());

        holder.commentCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        // Long Click listener
        holder.commentCardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final CharSequence [] choices;
                if(commentInfo.getCommentUsername().equals(ParseUser.getCurrentUser().getUsername())) {
                    choices = mContext.getResources().getStringArray(R.array.my_comment_choices);
                }
                else {
                    choices = mContext.getResources().getStringArray(R.array.comment_choices);
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

                builder.setItems(choices, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if(commentInfo.getCommentUsername().equals(ParseUser.getCurrentUser().getUsername())) {
                            switch (item) {
                                case 0: copyToClipBoard(commentInfo.getComment());
                                        break;
                                case 1: editComment(commentInfo, position);
                                        break;
                                case 2: removeComment(commentInfo, position);
                                        break;
                                default: break;
                            }
                        }
                        else {
                            switch (item) {
                                case 0: copyToClipBoard(commentInfo.getComment());
                                    break;
                                default: break;
                            }
                        }
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public void setInfo(NewsFeedInfo info) {
        this.info = info;
    }

    public void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.comment_label), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.comment_copied), Toast.LENGTH_SHORT).show();
    }

    public void editComment(final CommentInfo commentInfo, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
        builder.setTitle(mContext.getString(R.string.edit_comment))
                .setPositiveButton(mContext.getString(R.string.update), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(mContext.getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_edit_comment_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_edit_comment_dialog, null));
        }

        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

        final EditText commentEditText = (EditText) dialog.findViewById(R.id.comment_edittext);
        commentEditText.setText(commentInfo.getComment());
        commentEditText.setSelection(commentInfo.getComment().length());

        // Perform saving on click of positive button
        dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(commentEditText.getText().toString().trim())) {
                    // Perform saving of the post
                    new UpdateComment(commentEditText.getText().toString().trim(), commentInfo.getCommentId(), position).execute();
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(mContext, mContext.getString(R.string.error_empty_comment), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void removeComment(final CommentInfo commentInfo, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
        builder.setTitle(mContext.getString(R.string.confirm_comment_deletion))
                .setMessage(mContext.getString(R.string.comment_deletion_message))
                .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                })
                .setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(mContext.getDrawable(R.drawable.ic_logo));
        }

        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

        // Perform deletion on click of positive button
        // Perform saving on click of positive button
        dialog.getButton(android.app.AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Perform deletion of the post
                new DeleteComment(commentInfo.getCommentId(), position).execute();
                dialog.dismiss();
                }
        });
    }

    public void updateComment(String comment, String commentId, int position) throws ParseException, Exception {

        if(!comment.equals(comments.get(position).getComment())) {
            ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Comment);
            query.whereEqualTo(Constants.objectId, commentId);
            ParseObject foundComment = query.getFirst();
            foundComment.put(Constants.comment, comment);
            foundComment.save();
            comments.get(position).setComment(comment);
            IS_COMMENT_DIFFERENT = true;
        }
        else {
            IS_COMMENT_DIFFERENT = false;
        }
    }

    public void deleteComment(String commentId, int position) throws ParseException, Exception {
        String postId;

        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Comment);
        query.whereEqualTo(Constants.objectId, commentId);
        ParseObject object = query.getFirst();
        postId = object.getString(Constants.postId);
        object.delete();

        ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.Post);
        query1.whereEqualTo(Constants.objectId, postId);
        ParseObject post = query1.getFirst();
        int numComments = post.getInt(Constants.numberComments);
        numComments--;
        post.put(Constants.numberComments, numComments);
        post.save();

        comments.remove(position);

        info.addNumberComments(-1);
        info.setPostCommentStatus(NewsFeedAdapter.checkCommentStatus(ParseUser.getCurrentUser().getUsername(), info.getObjectId()));

    }

    //-----------------------------------------
    // ASYNCTASKS
    //-----------------------------------------

    private class UpdateComment extends AsyncTask<Void, Void, Boolean> {

        private String comment;
        private String postId;
        private int position;

        public UpdateComment(String commment, String postId, int position) {
            this.comment = commment;
            this.postId = postId;
            this.position = position;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                updateComment(comment, postId, position);
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(IS_COMMENT_DIFFERENT) {
                    Toast.makeText(mContext, mContext.getString(R.string.comment_updated), Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                }
                else {
                    Toast.makeText(mContext, mContext.getString(R.string.no_changes), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeleteComment extends AsyncTask<Void, Void, Boolean> {

        int position;
        String commentId;

        public DeleteComment(String commentId, int position) {
            this.commentId = commentId;
            this.position = position;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try{
                deleteComment(commentId, position);
                return true;
            } catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.comment_deleted), Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
                if(!IS_SINGLE_POST) {
                    newsFeedAdapter.updateElementAt(infoPosition, info);
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
