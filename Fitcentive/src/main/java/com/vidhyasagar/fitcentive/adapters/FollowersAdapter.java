package com.vidhyasagar.fitcentive.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.FollowerInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vharihar on 2016-12-19.
 */

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.FollowerHolder> {

    public static class FollowerHolder extends RecyclerView.ViewHolder {

        protected CircleImageView profilePicture;
        protected TextView userFullName, username;
        protected CardView cardView;
        protected Button followButton;
        protected ImageView deleteButton;

        protected boolean isFollowingCurrentUser;

        public FollowerHolder(View itemView) {
            super(itemView);
            profilePicture = (CircleImageView) itemView.findViewById(R.id.user_profile_picture);
            cardView = (CardView) itemView.findViewById(R.id.follower_results_card_view);
            userFullName = (TextView) itemView.findViewById(R.id.user_full_name);
            username = (TextView) itemView.findViewById(R.id.user_username_or_location);
            followButton = (Button) itemView.findViewById(R.id.follow_status_button);
            deleteButton = (ImageView) itemView.findViewById(R.id.delete_button);
        }
    }

    ArrayList<FollowerInfo> followerInfo;
    Context context;

    public FollowersAdapter(ArrayList<FollowerInfo> info, Context context) {
        this.followerInfo = info;
        this.context = context;
    }

    @Override
    public FollowerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_follower_results_list, parent, false);
        return new FollowerHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowerHolder holder, final int position) {
        final FollowerInfo info = followerInfo.get(position);
        if(info != null) {
            holder.profilePicture.setImageBitmap(info.getUserPicture());
            holder.userFullName.setText(info.getUserFullName());
            holder.username.setText(info.getUsername());

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ProfileActivity.class);
                    i.putExtra("username", info.getUsername());
                    context.startActivity(i);
                }
            });

            holder.isFollowingCurrentUser = info.isFollowingCurrentUser();

            if(holder.isFollowingCurrentUser) {
                holder.followButton.setText(context.getString(R.string.unfollow));
                holder.followButton.setBackgroundColor(ContextCompat.getColor(context, R.color.app_theme));
                holder.followButton.setTextColor(ContextCompat.getColor(context, android.R.color.white));
            }
            else {
                holder.followButton.setText(context.getString(R.string.follow));
            }
            holder.followButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new RequestUserFollow(info, holder.followButton, holder.isFollowingCurrentUser).execute();
                    if(holder.isFollowingCurrentUser) {
                        holder.followButton.setText(context.getString(R.string.follow));
                        holder.followButton.setBackgroundColor(ContextCompat.getColor(context, R.color.app_theme));
                        holder.followButton.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                    }
                    else {
                        holder.followButton.setText(context.getString(R.string.requested));
                        holder.followButton.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                        holder.followButton.setTextColor(ContextCompat.getColor(context, R.color.transp_black));
                    }

                    holder.isFollowingCurrentUser = !holder.isFollowingCurrentUser;
                }
            });

            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createDeleteConfirmationDialog(info, v, position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return followerInfo.size();
    }

    public void replaceDataSet(ArrayList<FollowerInfo> set) {
        this.followerInfo = set;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.followerInfo.isEmpty();
    }


    private void createDeleteConfirmationDialog(final FollowerInfo info, final View v, final int position) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context, R.style.AlertDialogCustom);
        builder.setTitle(context.getString(R.string.remove_follower))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new RemoveUserFromFollowersList(info, v, position).execute();
                    }
                })
                .setNegativeButton(context.getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(context.getDrawable(R.drawable.ic_logo));
            builder.setView(R.layout.layout_remove_follower_dialog);
        }

        else {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService (Context.LAYOUT_INFLATER_SERVICE);
            builder.setView(inflater.inflate(R.layout.layout_remove_follower_dialog, null));
        }

        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            builder.setIcon(context.getDrawable(R.drawable.ic_logo));
        }

        AlertDialog dialog = builder.create();
        dialog.show();

        TextView infoText = (TextView) dialog.findViewById(R.id.info_text);
        CircleImageView imageView = (CircleImageView) dialog.findViewById(R.id.user_image);

        imageView.setImageBitmap(info.getUserPicture());
        infoText.setText(String.format(context.getString(R.string.remove_follower_confirmation), info.getUserFullName()));
    }


    public void requestToFollowUser(FollowerInfo info) throws ParseException {
        // Must add a notification request to the Notification class
        ParseObject newObject = new ParseObject(Constants.Notification);
        newObject.put(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
        newObject.put(Constants.receiverUsername, info.getUsername());
        newObject.put(Constants.type, context.getString(R.string.follow_request));
        newObject.put(Constants.isActive, true);
        // No postId needed to follow user
        newObject.put(Constants.postId, "");
        if(ParseUser.getCurrentUser().getParseFile(Constants.profilePicture) != null) {
            newObject.put(Constants.image, ParseUser.getCurrentUser().getParseFile(Constants.profilePicture));
        }
        newObject.save();


        // Push notification cloud code call
        Map<String, String> params = new HashMap<>();
        params.put(context.getString(R.string.receiver), info.getUsername());
        params.put(context.getString(R.string.provider), ParseUser.getCurrentUser().getString(Constants.firstname)
                + " " + ParseUser.getCurrentUser().getString(Constants.lastname));
        ParseCloud.callFunctionInBackground(context.getString(R.string.followPush), params);
    }

    public void removeFromFollowingList(FollowerInfo info) throws ParseException {
        // If these function is called, you can be sure that the currentUser is already following intentUserName
        // Must remove currentUser from intentUserName's followers
        // Must also remove intentUserName from currentUser's following
        // Removing the notification that might have been posted when the currentUser
        // initially tried following the intentUserName user
        ParseQuery<ParseObject> notQuery = new ParseQuery<ParseObject>(Constants.Notification);
        notQuery.whereEqualTo(Constants.providerUsername, ParseUser.getCurrentUser().getUsername());
        notQuery.whereEqualTo(Constants.receiverUsername, info.getUsername());
        notQuery.whereEqualTo(Constants.postId, "");
        notQuery.whereEqualTo(Constants.type, context.getString(R.string.follow_request));
        notQuery.whereEqualTo(Constants.isActive, true);
        List<ParseObject> notificationObject = notQuery.find();
        if(notificationObject.isEmpty()) {
            ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
            ParseQuery<ParseObject> query1 = new ParseQuery<>(Constants.Follow);
            query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
            query1.whereEqualTo(Constants.username, info.getUsername());
            ParseObject result = query.getFirst();
            ParseObject result1 = query1.getFirst();

            // Updating current user's following list
            if(result != null ) {
                // Size will always HAVE to be 1. If not, something is wrong
                List<String> followingList = result.getList(Constants.following);
                int numFollowing = result.getInt(Constants.numFollowing);
                numFollowing--;
                List<String> tempList = new ArrayList<>();
                for(String user : followingList) {
                    if(!user.equals(info.getUsername())) {
                        tempList.add(user);
                    }
                }
                result.put(Constants.following, tempList);
                result.put(Constants.numFollowing, numFollowing);
                result.save();
            }

            // Updating intentUser's followers list
            if(result1 != null) {
                List<String> followersList = result1.getList(Constants.followers);
                int numFollowers = result1.getInt(Constants.numFollowers);
                numFollowers--;
                List<String> tempList = new ArrayList<>();
                for(String user : followersList) {
                    if(!user.equals(ParseUser.getCurrentUser().getUsername())) {
                        tempList.add(user);
                    }
                }
                result1.put(Constants.followers, tempList);
                result1.put(Constants.numFollowers, numFollowers);
                result1.save();
            }
        }
        else {
            notificationObject.get(0).delete();
        }

    }

    private void removeFromFollowersList(FollowerInfo info) throws ParseException {
        // Must remove from info USER's following list the CURRENT USER
        // Must remove from CURRENT_USER's followers list the info USER
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        ParseQuery<ParseObject> query1 = new ParseQuery<>(Constants.Follow);
        query1.whereEqualTo(Constants.username, info.getUsername());
        ParseObject result = query.getFirst();
        ParseObject result1 = query1.getFirst();

        // Updating current user's followers list
        if(result != null ) {
            // Size will always HAVE to be 1. If not, something is wrong
            List<String> followersList = result.getList(Constants.followers);
            int numFollowers = result.getInt(Constants.numFollowers);
            numFollowers--;
            List<String> tempList = new ArrayList<>();
            for(String user : followersList) {
                if(!user.equals(info.getUsername())) {
                    tempList.add(user);
                }
            }
            result.put(Constants.followers, tempList);
            result.put(Constants.numFollowers, numFollowers);
            result.save();
        }

        // Updating info User's followers list
        if(result1 != null) {
            List<String> followingList = result1.getList(Constants.following);
            int numFollowing = result1.getInt(Constants.numFollowing);
            numFollowing--;
            List<String> tempList = new ArrayList<>();
            for(String user : followingList) {
                if(!user.equals(ParseUser.getCurrentUser().getUsername())) {
                    tempList.add(user);
                }
            }
            result1.put(Constants.following, tempList);
            result1.put(Constants.numFollowing, numFollowing);
            result1.save();
        }
    }


    //----------------------------------------------------------
    // ASYNCTASK(S)
    //----------------------------------------------------------


    private class RequestUserFollow extends AsyncTask<Boolean, Void, Boolean> {

        FollowerInfo info;
        Button button;
        boolean isFollowingCurrentUser;

        public RequestUserFollow(FollowerInfo info, Button button, boolean isFollowingCurrentUser) {
            this.info = info;
            this.button = button;
            this.isFollowingCurrentUser = isFollowingCurrentUser;
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            // find user object in "Follow", if not found, create one "now"
            // CURRENT USER follows intentUserName
            try {
                if(isFollowingCurrentUser) {
                    removeFromFollowingList(info);
                }
                else {
                    requestToFollowUser(info);
                }

                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(isFollowingCurrentUser) {
                    Utilities.showSnackBar(context.getString(R.string.unfollow_success), button);
                }
                else {
                    Utilities.showSnackBar(context.getString(R.string.follow_request_success), button);
                }

            }
            else {
                Toast.makeText(context, context.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class RemoveUserFromFollowersList extends AsyncTask<Boolean, Void, Boolean> {

        FollowerInfo info;
        int pos;
        View view;

        public RemoveUserFromFollowersList(FollowerInfo info, View view, int position) {
            this.info = info;
            this.view = view;
            this.pos = position;
        }

        @Override
        protected Boolean doInBackground(Boolean... params) {
            // find user object in "Follow", if not found, create one "now"
            // CURRENT USER follows intentUserName
            try {
                removeFromFollowersList(info);
                return true;
            }
            catch (ParseException e) {
                e.printStackTrace();
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Utilities.showSnackBar(context.getString(R.string.removed_follower), view);
                followerInfo.remove(pos);
                notifyItemRemoved(pos);
                notifyItemRangeChanged(pos, getItemCount());

            }
            else {
                Toast.makeText(context, context.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
