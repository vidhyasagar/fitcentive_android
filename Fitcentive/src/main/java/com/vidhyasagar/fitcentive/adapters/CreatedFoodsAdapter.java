package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditFoodActivity;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.fragments.ViewCreatedFoodFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.FoodResultsInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-02.
 */

public class CreatedFoodsAdapter extends RecyclerView.Adapter<CreatedFoodsAdapter.CreatedFoodResultHolder> {

    public static int NAME_CHAR_LIMIT = 28;

    private ArrayList<FoodResultsInfo> createdFoodsList;
    private Context mContext;
    private SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType;
    private DiaryPageFragment.ENTRY_TYPE entryType;
    private int dayOffset;


    public static class CreatedFoodResultHolder extends RecyclerView.ViewHolder {

        protected TextView createdFoodName;
        protected TextView createdFoodBrand;
        protected TextView createdFoodDescription;
        protected TextView createdFoodServingSize;
        protected TextView createdFoodCalories;
        protected CardView cardView;

        public CreatedFoodResultHolder(View view) {
            super(view);

            createdFoodName = (TextView) view.findViewById(R.id.food_name);
            createdFoodBrand = (TextView) view.findViewById(R.id.food_brand);
            createdFoodDescription = (TextView) view.findViewById(R.id.food_description);
            createdFoodServingSize = (TextView) view.findViewById(R.id.food_serving_size);
            createdFoodCalories = (TextView) view.findViewById(R.id.food_calories);
            cardView = (CardView) view.findViewById(R.id.user_results_card_view);
        }

    }

    public CreatedFoodsAdapter(ArrayList<FoodResultsInfo> createdFoodsList, Context mContext,
                               SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType,
                               DiaryPageFragment.ENTRY_TYPE entryType, int dayOffset) {
        this.createdFoodsList = createdFoodsList;
        this.mContext = mContext;
        this.resultType = resultType;
        this.entryType = entryType;
        this.dayOffset = dayOffset;
    }


    @Override
    public CreatedFoodResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_created_food_result, parent, false);
        return new CreatedFoodResultHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(CreatedFoodResultHolder holder, final int position) {
        final FoodResultsInfo info = createdFoodsList.get(position);

        if(info.getBrandName().isEmpty()) {
            holder.createdFoodBrand.setVisibility(View.GONE);
        }
        else {
            holder.createdFoodBrand.setVisibility(View.VISIBLE);
            holder.createdFoodBrand.setText(info.getBrandName());
        }

        holder.createdFoodName.setText(info.getFoodName());
        holder.createdFoodDescription.setText(info.getFoodDescription());
        holder.createdFoodServingSize.setText(info.getServingSize());
        holder.createdFoodCalories.setText(String.format(mContext.getString(R.string.x_kcals), info.getCalories()));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startViewCreatedFoodActivity(info.getObjectId());
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bringUpLongClickOptionsMenu(info, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return createdFoodsList.size();
    }

    private void bringUpLongClickOptionsMenu(final FoodResultsInfo info, final int position) {
        final CharSequence [] choices;
        choices = mContext.getResources().getStringArray(R.array.created_food_list_choices);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: copyToClipBoard(info.getFoodName());
                        break;
                    case 1: startEditCreatedFoodActivity(info.getObjectId());
                        break;
                    case 2: deleteCreatedFood(info.getObjectId(), position);
                        break;
                    default: break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    // Deletion done in background here instead of using Asynctask
    private void deleteCreatedFood(final String objectId, int position) {
        // Delete from database, delete from current data set, notifyDataSetChanged
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedFood);
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                object.deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null) {
                            e.printStackTrace();
                        }
                        else {
                            Toast.makeText(mContext, mContext.getString(R.string.food_deleted_successfully), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                // Now proceeding to delete all diary entries that have this created food in it
                ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.FoodDiary);
                query1.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
                query1.whereEqualTo(Constants.entryType, mContext.getString(R.string.created_food));
                query1.whereEqualTo(Constants.createdFoodObjectId, objectId);

                query1.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if(e == null) {
                            for(ParseObject toDelete : objects) {
                                toDelete.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e != null) {
                                            e.printStackTrace();
                                        }

                                    }
                                });
                            }
                        }
                        else {
                            e.printStackTrace();
                        }
                    }
                });

            }
        });

        createdFoodsList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    private void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.food_name), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.food_name_copied), Toast.LENGTH_SHORT).show();
    }

    private void startEditCreatedFoodActivity(String objectId) {
        Intent i = new Intent(mContext, CreateOrEditFoodActivity.class);
        i.putExtra(Constants.objectId, objectId);
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, true);
        ((Activity) mContext).startActivityForResult(i, ViewCreatedFoodFragment.RETURN_TO_VIEW_CREATED_FOOD);
    }

    private void startViewCreatedFoodActivity(String objectId) {
        Intent i = new Intent(mContext, EditFoodEntryActivity.class);
        i.putExtra(Constants.DAY_OFFSET , dayOffset);
        i.putExtra(Constants.objectId, objectId);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, entryType);
        i.putExtra(EditFoodEntryActivity.IS_CREATED_FOOD, true);
        mContext.startActivity(i);
    }

    public void replaceDataSet(ArrayList<FoodResultsInfo> newDataSet) {
        this.createdFoodsList = newDataSet;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.createdFoodsList.isEmpty();
    }


}