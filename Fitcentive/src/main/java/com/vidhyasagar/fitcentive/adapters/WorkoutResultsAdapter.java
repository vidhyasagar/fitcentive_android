package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;

import java.util.ArrayList;
import java.util.List;

import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_EDIT;
import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_VIEW;

/**
 * Created by vharihar on 2016-12-16.
 */


public class WorkoutResultsAdapter extends RecyclerView.Adapter<WorkoutResultsAdapter.WorkoutResultHolder> {

    public static int NAME_CHAR_LIMIT = 28;
    public static int TRUNCATE_LIMIT = 40;

    private ArrayList<CreatedWorkoutInfo> workoutList;
    private Context mContext;
    private int dayOffset;


    public static class WorkoutResultHolder extends RecyclerView.ViewHolder {

        protected TextView workoutName;
        protected ExpandableHeightListView workoutComponentsListView;
        protected TextView workoutCalories;
        protected CardView cardView;

        public WorkoutResultHolder(View view) {
            super(view);

            workoutName = (TextView) view.findViewById(R.id.meal_name);
            workoutComponentsListView = (ExpandableHeightListView) view.findViewById(R.id.meal_components_listview);
            workoutCalories = (TextView) view.findViewById(R.id.food_calories);
            cardView = (CardView) view.findViewById(R.id.user_results_card_view);
        }

    }

    public WorkoutResultsAdapter(ArrayList<CreatedWorkoutInfo> workoutList, Context mContext, int dayOffset) {
        this.workoutList = workoutList;
        this.mContext = mContext;
        this.dayOffset = dayOffset;
    }


    @Override
    public WorkoutResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_meal_result, parent, false);
        return new WorkoutResultHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(WorkoutResultHolder holder, final int position) {
        final CreatedWorkoutInfo info = workoutList.get(position);

        holder.workoutName.setText(truncateString(info.getWorkoutName(), TRUNCATE_LIMIT));
        holder.workoutCalories.setText(String.format(mContext.getString(R.string.x_kcals), info.getTotalNumberOfCalories()));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.layout_meal_listview_textview, info.getExerciseNames());
        holder.workoutComponentsListView.setAdapter(adapter);
        holder.workoutComponentsListView.setExpanded(true);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditWorkoutActivity(info.getWorkoutObjectId(), true);
            }
        });

        holder.workoutComponentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startEditWorkoutActivity(info.getWorkoutObjectId(), true);
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bringUpLongClickOptionsMenu(info, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return workoutList.size();
    }

    private void bringUpLongClickOptionsMenu(final CreatedWorkoutInfo info, final int position) {
        final CharSequence [] choices;
        choices = mContext.getResources().getStringArray(R.array.workout_list_choices);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: copyToClipBoard(info.getWorkoutName());
                        break;
                    case 1: startEditWorkoutActivity(info.getWorkoutObjectId(), false);
                        break;
                    case 2: deleteWorkout(info, position);
                        break;
                    default: break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    // Deletion done in background here instead of using Asynctask
    private void deleteWorkout(final CreatedWorkoutInfo info, int position) {
        // Delete from database, delete from current data set, notifyDataSetChanged

        new DeleteCurrentWorkoutObject(info.getWorkoutObjectId()).execute();

        workoutList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    private void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.workout_name), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.workout_name_copied), Toast.LENGTH_SHORT).show();
    }

    private void startEditWorkoutActivity(String objectId, boolean isViewMode) {
        Intent i = new Intent(mContext, CreateOrEditWorkoutActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(Constants.workoutId, objectId);
        if(isViewMode) {
            i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_VIEW);
        }
        else {
            i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_EDIT);
        }
        ((Activity) mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_FRAGMENT_AFTER_EDIT);
    }


    public static String truncateString(String foodName) {
        if(foodName.length() <= NAME_CHAR_LIMIT) {
            return foodName;
        }
        else {
            return foodName.substring(0, NAME_CHAR_LIMIT) + "...";
        }
    }

    public static String truncateString(String foodName, int limit) {
        if(foodName.length() <= limit) {
            return foodName;
        }
        else {
            return foodName.substring(0, limit) + "...";
        }
    }

    public void replaceDataSet(ArrayList<CreatedWorkoutInfo> newDataSet) {
        this.workoutList = newDataSet;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.workoutList.isEmpty();
    }

    private void deleteAssociatedStrengthComponents(String createdWorkoutObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutStrengthComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteAssociatedCardioComponents(String createdWorkoutObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkoutCardioComponent);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteAssociatedExerciseDiaryEntries(String createdWorkoutObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.exerciseType, mContext.getString(R.string.workout));
        query.whereEqualTo(Constants.workoutId, createdWorkoutObjectId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.delete();
        }
    }

    private void deleteCurrentWorkout(String createdWorkoutObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.CreatedWorkout);
        ParseObject object = query.get(createdWorkoutObjectId);
        object.delete();

        deleteAssociatedStrengthComponents(createdWorkoutObjectId);
        deleteAssociatedCardioComponents(createdWorkoutObjectId);
        deleteAssociatedExerciseDiaryEntries(createdWorkoutObjectId);
    }

    //------------------------------------
    // ASYNCTASK
    //------------------------------------

    private class DeleteCurrentWorkoutObject extends AsyncTask<Void, Void, Boolean> {

        String createdWorkoutObjectId;

        public DeleteCurrentWorkoutObject(String createdWorkoutObjectId) {
            this.createdWorkoutObjectId = createdWorkoutObjectId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deleteCurrentWorkout(createdWorkoutObjectId);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.workout_deleted_successfully), Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

}