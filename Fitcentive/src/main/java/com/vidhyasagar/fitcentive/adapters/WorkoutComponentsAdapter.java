package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.EditCardioActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.create_workout_fragments.CreateWorkoutComponentsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_exercise_fragments.ExerciseResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import java.util.ArrayList;
import java.util.Collections;

import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;

/**
 * Created by vharihar on 2016-12-16.
 */

/**
 * Created by vharihar on 2016-10-27.
 */
public class WorkoutComponentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RVHAdapter {

    public static int RECIPE_DESC_MARGIN_END = 20;
    public static int TRUNCATE_LIMIT = 25;

    private ArrayList<Object> items;
    private Context mContext;
    private RecyclerView parentRecycler;

    private Object cache;
    private TextView headerTextView;

    int dayOffset;
    int cachePosition;
    boolean isDisabled;

    CreateWorkoutComponentsFragment fragment;


    public static class ItemViewHolder extends RecyclerView.ViewHolder implements RVHViewHolder {

        TextView foodName, servingSize, foodBrand, calorieCount, bonusFact;
        ImageView favoriteButton, cookingIcon;
        RelativeLayout parentLayout;

        public ItemViewHolder(View itemView) {
            super(itemView);
            foodName = (TextView) itemView.findViewById(R.id.food_name);
            servingSize = (TextView) itemView.findViewById(R.id.serving_size);
            foodBrand = (TextView) itemView.findViewById(R.id.food_brand);
            calorieCount = (TextView) itemView.findViewById(R.id.calorie_count);
            bonusFact = (TextView) itemView.findViewById(R.id.bonus_fact);
            favoriteButton = (ImageView) itemView.findViewById(R.id.favorite_button);
            cookingIcon = (ImageView) itemView.findViewById(R.id.cooking_icon);
            parentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_relative_layout);
        }

        @Override
        public void onItemSelected(int actionstate) {

        }

        @Override
        public void onItemClear() {

        }
    }



    public WorkoutComponentsAdapter(ArrayList<Object> items, Context mContext, RecyclerView parentRecycler, TextView headerTextView, int offset, CreateWorkoutComponentsFragment fragment) {
        this.items = items;
        this.mContext = mContext;
        this.parentRecycler = parentRecycler;
        this.headerTextView = headerTextView;
        this.dayOffset = offset;
        this.fragment = fragment;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(!isDisabled) {
            swap(fromPosition, toPosition);
        }
        return false;
    }

    @Override
    public void onItemDismiss(int position, int direction) {
        if(!isDisabled) {
            remove(position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diary_entry_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(items.get(position) instanceof ExerciseInfo) {
            ExerciseInfo info = (ExerciseInfo) items.get(position);
            initStrengthView((ItemViewHolder) holder, info, position);
        }
        else {
            CardioInfo info = (CardioInfo) items.get(position);
            initCardioView((ItemViewHolder) holder, info, position);
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public int getCachePosition() {
        return cachePosition;
    }

    public void updateTextViewHeader() {
        double totalCalories = 0;

        for(Object object : items) {
            if(object instanceof CardioInfo) {
                CardioInfo newInfo = (CardioInfo) object;
                totalCalories += newInfo.getCaloriesBurned();
            }
        }

        if(totalCalories == 0) {
            headerTextView.setVisibility(View.GONE);
        }
        else {
            Double d = totalCalories;
            headerTextView.setVisibility(View.VISIBLE);
            headerTextView.setText(String.format(mContext.getString(R.string.x_kcals), d.intValue()));
        }
    }

    private void addOrRemoveItem(boolean toAdd, int position) {
        if(toAdd) {
            items.add(position, cache);
            notifyItemInserted(position);
        }
        else {
            items.remove(position);
            notifyItemRemoved(position);
        }

        if(items.isEmpty()) {
            parentRecycler.setVisibility(View.GONE);
        }
        else {
            parentRecycler.setVisibility(View.VISIBLE);
        }

        updateTextViewHeader();
    }

    private void remove(final int position) {
        cache = items.get(position);
        Snackbar.make(parentRecycler, mContext.getString(R.string.item_deleted), Snackbar.LENGTH_LONG)
                .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addOrRemoveItem(true, position);
                    }
                })

                .show();
        addOrRemoveItem(false, position);
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(items, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }


    private void setWeightsOnExerciseIfApplicable(TextView bonusFact, ExerciseInfo info) {
        if(info.getWeightPerSet() != -1 && !info.getWeightUnit().equals(Constants.unknown)) {
            bonusFact.setText(String.format(mContext.getString(R.string.x_units), info.getWeightPerSet(), info.getWeightUnit()));
            bonusFact.setVisibility(View.VISIBLE);
        }
        else {
            bonusFact.setVisibility(View.GONE);
        }
    }

    private void initStrengthView(final ItemViewHolder holder, final ExerciseInfo info, final int position) {
        holder.cookingIcon.setVisibility(View.GONE);
        holder.calorieCount.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);

        holder.foodBrand.setVisibility(View.VISIBLE);

        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getName(), TRUNCATE_LIMIT));

        holder.foodBrand.setText(String.format(mContext.getString(R.string.x_sets), info.getNumberOfSets()));

        holder.servingSize.setText(String.format(mContext.getString(R.string.x_reps_per_set), info.getRepsPerSet()));
        holder.servingSize.setTextColor(ContextCompat.getColor(mContext, R.color.transp_black));
        setWeightsOnExerciseIfApplicable(holder.bonusFact, info);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDisabled) {
                    fragment.setPositionToUpdate(position);
                    if(info.isCreated()) {
                        startCreatedEditExerciseActivity(info);
                    }
                    else {
                        startEditExerciseEntryActivity(info);
                    }
                }
            }
        });
    }

    private void initCardioView(final ItemViewHolder holder, final CardioInfo info, final int position) {
        holder.cookingIcon.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);
        holder.bonusFact.setVisibility(View.GONE);

        holder.foodBrand.setVisibility(View.GONE);
        holder.calorieCount.setVisibility(View.VISIBLE);

        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getName(), TRUNCATE_LIMIT));
        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), Double.valueOf(info.getCaloriesBurned()).intValue()));

        holder.servingSize.setText(String.format(mContext.getString(R.string.x_mins), (int) ((Math.round(info.getMinutesPerformed())))));
        holder.servingSize.setTextColor(ContextCompat.getColor(mContext, R.color.app_theme));


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDisabled) {
                    fragment.setPositionToUpdate(position);
                    if(info.isCreated()) {
                        startEditCreatedCardioEntryActivity(info);
                    }
                    else {
                        startEditCardioEntryActivity(info);
                    }
                }
            }
        });
    }

    private void startEditExerciseEntryActivity(ExerciseInfo info) {
        Intent i = new Intent(mContext, ExpandedExerciseActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedExerciseActivity.IS_EDITING, true);
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, true);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }

    private void startCreatedEditExerciseActivity(ExerciseInfo info) {
        Intent i = new Intent(mContext, ExpandedExerciseActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.isCreated, true);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedExerciseActivity.IS_EDITING, true);
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, true);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }

    private void startEditCreatedCardioEntryActivity(CardioInfo info) {
        Intent i = new Intent(mContext, EditCardioActivity.class);
        i.putExtra(Constants.isCreated, true);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, true);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }

    private void startEditCardioEntryActivity(CardioInfo info) {
        Intent i = new Intent(mContext, EditCardioActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, true);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
    }


}
