package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.MealInfo;

import java.util.ArrayList;
import java.util.List;

import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_EDIT;
import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_VIEW;

/**
 * Created by vharihar on 2016-11-29.
 */
public class MealResultsAdapter extends RecyclerView.Adapter<MealResultsAdapter.MealResultHolder> {

    public static int NAME_CHAR_LIMIT = 28;

    private ArrayList<MealInfo> mealList;
    private Context mContext;
    private SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType;
    private DiaryPageFragment.ENTRY_TYPE entryType;
    private int dayOffset;


    public static class MealResultHolder extends RecyclerView.ViewHolder {

        protected TextView mealName;
        protected ExpandableHeightListView mealComponentsListView;
        protected TextView foodCalories;
        protected CardView cardView;

        public MealResultHolder(View view) {
            super(view);

            mealName = (TextView) view.findViewById(R.id.meal_name);
            mealComponentsListView = (ExpandableHeightListView) view.findViewById(R.id.meal_components_listview);
            foodCalories = (TextView) view.findViewById(R.id.food_calories);
            cardView = (CardView) view.findViewById(R.id.user_results_card_view);
        }

    }

    public MealResultsAdapter(ArrayList<MealInfo> mealList, Context mContext,
                              SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType,
                              DiaryPageFragment.ENTRY_TYPE entryType, int dayOffset) {
        this.mealList = mealList;
        this.mContext = mContext;
        this.resultType = resultType;
        this.entryType = entryType;
        this.dayOffset = dayOffset;
    }


    @Override
    public MealResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_meal_result, parent, false);
        return new MealResultHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(MealResultHolder holder, final int position) {
        final MealInfo info = mealList.get(position);

        holder.mealName.setText(truncateString(info.getMealName()));
        holder.foodCalories.setText(String.format(mContext.getString(R.string.x_kcals), info.getTotalNumberOfCalories()));

        ArrayList<String> mealComponentsList = getAugmentedList(info.getFoodAndRecipeNames(), info.getServingSizes());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext, R.layout.layout_meal_listview_textview, mealComponentsList);
        holder.mealComponentsListView.setAdapter(adapter);
        holder.mealComponentsListView.setExpanded(true);
        holder.mealComponentsListView.setClickable(false);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditMealActivity(info.getMealObjectId(), true);
            }
        });

        holder.mealComponentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startEditMealActivity(info.getMealObjectId(), true);
            }
        });

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                bringUpLongClickOptionsMenu(info, position);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mealList.size();
    }

    private void bringUpLongClickOptionsMenu(final MealInfo info, final int position) {
        final CharSequence [] choices;
            choices = mContext.getResources().getStringArray(R.array.meal_list_choices);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: copyToClipBoard(info.getMealName());
                        break;
                    case 1: startEditMealActivity(info.getMealObjectId(), false);
                        break;
                    case 2: deleteMeal(info, position);
                        break;
                    default: break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    // Deletion done in background here instead of using Asynctask
    private void deleteMeal(final MealInfo info, int position) {
        // Delete from database, delete from current data set, notifyDataSetChanged
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedMeal);
        query.getInBackground(info.getMealObjectId(), new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                object.deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null) {
                            e.printStackTrace();
                        }
                        else {
                            Toast.makeText(mContext, mContext.getString(R.string.meal_deleted_successfully), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                ParseQuery<ParseObject> query1 = new ParseQuery<ParseObject>(Constants.FoodDiary);
                query1.whereEqualTo(Constants.entryType, mContext.getString(R.string.meal));
                query1.whereEqualTo(Constants.mealObjectId, info.getMealObjectId());

                query1.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if(e == null) {
                            for(ParseObject temp : objects) {
                                temp.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e != null) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        mealList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    private void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.meal_name), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.meal_name_copied), Toast.LENGTH_SHORT).show();
    }

    private void startEditMealActivity(String objectId, boolean isViewMode) {
        Intent i = new Intent(mContext, CreateOrEditMealActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(SearchFoodResultsFragment.ENTRY_TYPE_STRING, entryType);
        if(isViewMode) {
            i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_VIEW);
        }
        else {
            i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_EDIT);
        }
        i.putExtra(Constants.objectId, objectId);
        ((Activity) mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_EDIT);
    }

    private ArrayList<String> getAugmentedList(List<String> foodAndRecipeNames, List<String> servingSizes) {
        ArrayList<String> augmentedList = new ArrayList<>();
        int size = foodAndRecipeNames.size();
        for(int i = 0; i < size; i++) {
            augmentedList.add(foodAndRecipeNames.get(i) + ", " + servingSizes.get(i));
        }
        return augmentedList;
    }

    public static String truncateString(String foodName) {
        if(foodName.length() <= NAME_CHAR_LIMIT) {
            return foodName;
        }
        else {
            return foodName.substring(0, NAME_CHAR_LIMIT) + "...";
        }
    }

    public static String truncateString(String foodName, int limit) {
        if(foodName.length() <= limit) {
            return foodName;
        }
        else {
            return foodName.substring(0, limit) + "...";
        }
    }

    public void replaceDataSet(ArrayList<MealInfo> newDataSet) {
        this.mealList = newDataSet;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.mealList.isEmpty();
    }


}
