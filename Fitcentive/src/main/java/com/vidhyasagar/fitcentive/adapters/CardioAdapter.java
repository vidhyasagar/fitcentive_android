package com.vidhyasagar.fitcentive.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-12.
 */

public class CardioAdapter extends ArrayAdapter<String> {

    Context mContext;
    ArrayList<String> cardioNames;

    public CardioAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.cardioNames = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_search_exercise_result, null);
        }

        TextView cardioName = (TextView) convertView.findViewById(R.id.exercise_name);

        cardioName.setText(cardioNames.get(position));

        return convertView;
    }

    @Override
    public int getCount() {
        return cardioNames.size();
    }

    public void replaceDataSet(ArrayList<String> set) {
        this.cardioNames = set;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.cardioNames.isEmpty();
    }

    public ArrayList<String> getDataSet() {
        return cardioNames;
    }
}
