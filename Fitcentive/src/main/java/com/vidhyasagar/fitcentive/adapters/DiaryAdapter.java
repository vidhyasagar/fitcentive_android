package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.AddWaterActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.EditCardioActivity;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretDiary;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretRecipes;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.wrappers.CardioInfo;
import com.vidhyasagar.fitcentive.wrappers.CreatedWorkoutInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;
import com.vidhyasagar.fitcentive.wrappers.MealInfo;
import com.vidhyasagar.fitcentive.wrappers.WaterInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;

import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_EDIT;
import static com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity.MODE.MODE_VIEW;

/**
 * Created by vharihar on 2016-10-27.
 */
public class DiaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RVHAdapter {

    public static int RECIPE_DESC_MARGIN_END = 20;
    public static int TRUNCATE_LIMIT = 25;

    private ArrayList<Object> items;
    private Context mContext;
    private RecyclerView parentRecycler;

    private Object cache;
    private DiaryPageFragment.ENTRY_TYPE type;
    private DiaryPageFragment fragment;
    private TextView headerTextView;

    int dayOffset;
    FatSecretFoods foods;
    FatSecretRecipes recipes;

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements RVHViewHolder{

        TextView foodName, servingSize, foodBrand, calorieCount, bonusFact;
        ImageView favoriteButton, cookingIcon;
        RelativeLayout parentLayout;

        boolean isFavorited = false;

        public ItemViewHolder(View itemView) {
            super(itemView);
            foodName = (TextView) itemView.findViewById(R.id.food_name);
            servingSize = (TextView) itemView.findViewById(R.id.serving_size);
            foodBrand = (TextView) itemView.findViewById(R.id.food_brand);
            calorieCount = (TextView) itemView.findViewById(R.id.calorie_count);
            bonusFact = (TextView) itemView.findViewById(R.id.bonus_fact);
            favoriteButton = (ImageView) itemView.findViewById(R.id.favorite_button);
            cookingIcon = (ImageView) itemView.findViewById(R.id.cooking_icon);
            parentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_relative_layout);
        }

        @Override
        public void onItemSelected(int actionstate) {

        }

        @Override
        public void onItemClear() {

        }
    }



    public DiaryAdapter(ArrayList<Object> items, Context mContext, RecyclerView parentRecycler,
                        DiaryPageFragment.ENTRY_TYPE type, DiaryPageFragment fragment, TextView headerTextView, int offset) {
        this.items = items;
        this.mContext = mContext;
        this.parentRecycler = parentRecycler;
        this.type = type;
        this.fragment = fragment;
        this.headerTextView = headerTextView;
        this.dayOffset = offset;
        foods = new FatSecretFoods();
        recipes = new FatSecretRecipes();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        swap(fromPosition, toPosition);
        return false;
    }

    @Override
    public void onItemDismiss(int position, int direction) {
        remove(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diary_entry_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(items.get(position) instanceof DetailedFoodResultsInfo) {
            DetailedFoodResultsInfo info = (DetailedFoodResultsInfo) items.get(position);
            if(info.isCreatedFood()) {
                initCreatedFoodItemView((ItemViewHolder) holder, info);
            }
            else {
                initFoodItemView((ItemViewHolder) holder, info);
            }
        }
        else if(items.get(position) instanceof DetailedRecipeResultsInfo) {
            DetailedRecipeResultsInfo info = (DetailedRecipeResultsInfo) items.get(position);
            initRecipeView((ItemViewHolder) holder, info);
        }

        else if(items.get(position) instanceof MealInfo) {
            MealInfo info = (MealInfo) items.get(position);
            initMealView((ItemViewHolder) holder, info);
        }

        else if(items.get(position) instanceof WaterInfo) {
            WaterInfo info = (WaterInfo) items.get(position);
            initWaterView((ItemViewHolder) holder, info);
        }

        else if(items.get(position) instanceof ExerciseInfo) {
            ExerciseInfo info = (ExerciseInfo) items.get(position);
            initExerciseView((ItemViewHolder) holder, info);
        }
        else if(items.get(position) instanceof CardioInfo) {
            CardioInfo info = (CardioInfo) items.get(position);
            initCardioView((ItemViewHolder) holder, info);
        }
        else if(items.get(position) instanceof CreatedWorkoutInfo) {
            CreatedWorkoutInfo info = (CreatedWorkoutInfo) items.get(position);
            initWorkoutView((ItemViewHolder) holder, info);
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    private void remove(final int position) {
        cache = items.get(position);
        Snackbar.make(parentRecycler, mContext.getString(R.string.item_deleted), Snackbar.LENGTH_LONG)
                .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        items.add(position, cache);
                        notifyItemInserted(position);
                    }
                })
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        if(event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_SWIPE) {
                            if(cache instanceof DetailedFoodResultsInfo) {
                                DetailedFoodResultsInfo newCache = (DetailedFoodResultsInfo) cache;
                                if(newCache.isCreatedFood()) {
                                    new RemoveCreatedFoodFromDatabase(newCache.getObjectId(), newCache.getNumberOfServings(), type).execute();
                                }
                                else {
                                    new RemoveFromFoodDatabase(newCache.getFoodId(), newCache.getServingId(), newCache.getNumberOfServings(), type).execute();
                                }
                            }
                            else if (cache instanceof DetailedRecipeResultsInfo){
                                DetailedRecipeResultsInfo newCache = (DetailedRecipeResultsInfo) cache;
                                new RemoveFromRecipeDatabase(newCache.getRecipeId(), newCache.getChosenNumberOfServings(), type).execute();
                            }
                            else if (cache instanceof MealInfo) {
                                new RemoveFromMealDatabase(((MealInfo) cache).getMealObjectId()).execute();
                            }
                            else if (cache instanceof WaterInfo) {
                                new RemoveFromWaterDatabase(((WaterInfo) cache).getObjectId()).execute();
                            }
                            else if(cache instanceof ExerciseInfo) {
                                new RemoveFromExerciseDatabase(((ExerciseInfo) cache).getObjectId()).execute();
                            }
                            else if(cache instanceof CardioInfo) {
                                new RemoveFromExerciseDatabase(((CardioInfo) cache).getObjectId()).execute();
                            }
                            else if(cache instanceof CreatedWorkoutInfo) {
                                new RemoveFromExerciseDatabase(((CreatedWorkoutInfo) cache).getObjectId()).execute();
                            }
                        }
                    }
                })
                .show();
        items.remove(position);
        notifyItemRemoved(position);
    }

    private void removeMealFromDatabase(String mealObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.meal));
        query.whereEqualTo(Constants.mealObjectId, mealObjectId);

        ParseObject object = query.getFirst();
        object.delete();
    }

    private void removeWaterFromDatabase(String waterObjectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.WaterDiary);
        ParseObject object = query.get(waterObjectId);
        object.delete();
    }

    private void removeCreatedFoodFromDatabase(String createdFoodObjectId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) throws Exception {
        String mealType = null;
        switch(type) {
            case BREAKFAST: mealType = mContext.getString(R.string.breakfast);
                break;
            case LUNCH: mealType = mContext.getString(R.string.lunch);
                break;
            case DINNER: mealType = mContext.getString(R.string.dinner);
                break;
            case EXCERCISE: mealType = mContext.getString(R.string.excercise);
                break;
            case WATER: mealType = mContext.getString(R.string.water);
                break;
            case SNACKS: mealType = mContext.getString(R.string.snacks);
                break;
        }
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.created_food));
        query.whereEqualTo(Constants.numberServings, numberOfServings);
        query.whereEqualTo(Constants.createdFoodObjectId, createdFoodObjectId);
        query.whereEqualTo(Constants.mealType, mealType);
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        ParseObject object = query.getFirst();
        object.delete();
    }

    private void removeFoodFromDatabase(long foodId, long servingId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) throws Exception {
        String mealType = null;
        switch(type) {
            case BREAKFAST: mealType = mContext.getString(R.string.breakfast);
                            break;
            case LUNCH: mealType = mContext.getString(R.string.lunch);
                            break;
            case DINNER: mealType = mContext.getString(R.string.dinner);
                            break;
            case EXCERCISE: mealType = mContext.getString(R.string.excercise);
                            break;
            case WATER: mealType = mContext.getString(R.string.water);
                            break;
            case SNACKS: mealType = mContext.getString(R.string.snacks);
                            break;
        }
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.food));
        query.whereEqualTo(Constants.numberServings, numberOfServings);
        query.whereEqualTo(Constants.mealType, mealType);
        query.whereEqualTo(Constants.foodIdNumber, foodId);
        query.whereEqualTo(Constants.servingIdField, servingId);
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        ParseObject object = query.getFirst();

        long foodEntryId = object.getLong(Constants.foodEntryIdField);
        object.delete();

        // Must also delete from FatSecret
        FatSecretDiary diary = new FatSecretDiary();
        diary.deleteFoodEntry(foodEntryId);
    }

    private void removeExerciseFromDiary(String objectId) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.ExerciseDiary);
        ParseObject object = query.get(objectId);
        object.delete();
    }

    private void removeRecipeFromDatabase(long recipeId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) throws Exception {
        String mealType = null;
        switch(type) {
            case BREAKFAST: mealType = mContext.getString(R.string.breakfast);
                break;
            case LUNCH: mealType = mContext.getString(R.string.lunch);
                break;
            case DINNER: mealType = mContext.getString(R.string.dinner);
                break;
            case EXCERCISE: mealType = mContext.getString(R.string.excercise);
                break;
            case WATER: mealType = mContext.getString(R.string.water);
                break;
            case SNACKS: mealType = mContext.getString(R.string.snacks);
                break;
        }
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.recipe));
        query.whereEqualTo(Constants.numberServings, numberOfServings);
        query.whereEqualTo(Constants.mealType, mealType);
        query.whereEqualTo(Constants.recipeIdField, recipeId);
        query.whereEqualTo(Constants.daysSinceEpoch, DiaryPageFragment.getDateInDays(dayOffset));
        ParseObject object = query.getFirst();

        object.delete();
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(items, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    private void startEditCreatedFoodEntryActivity(DetailedFoodResultsInfo data, DiaryPageFragment.ENTRY_TYPE entryType) {
        Intent i = new Intent(mContext, EditFoodEntryActivity.class);
        i.putExtra(Constants.numberServings, data.getNumberOfServings());
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, entryType);
        i.putExtra(EditFoodEntryActivity.IS_CREATED_FOOD, true);
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, true);
        i.putExtra(EditFoodEntryActivity.IS_FROM_DIARY, true);
        i.putExtra(Constants.objectId, data.getObjectId());
        mContext.startActivity(i);
    }

    private void startEditFoodEntryActivity(DetailedFoodResultsInfo data, DiaryPageFragment.ENTRY_TYPE entryType) {
        Intent i = new Intent(mContext, EditFoodEntryActivity.class);
        i.putExtra(ExpandedSearchFoodActivity.FOOD_ID, data.getFoodId());
        i.putExtra(Constants.servingId, data.getServingId());
        i.putExtra(Constants.numberServings, data.getNumberOfServings());
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, entryType);
        mContext.startActivity(i);
    }

    private String getMealTypeString(DiaryPageFragment.ENTRY_TYPE type) {
        switch(type) {
            case BREAKFAST: return mContext.getString(R.string.breakfast);
            case LUNCH: return mContext.getString(R.string.lunch);
            case DINNER: return mContext.getString(R.string.dinner);
            case SNACKS: return mContext.getString(R.string.snacks);
            case EXCERCISE: return mContext.getString(R.string.excercise);
            case WATER: return mContext.getString(R.string.water);
            default: return null;
        }
    }

    private void startEditRecipeEntryActivity(DetailedRecipeResultsInfo data, DiaryPageFragment.ENTRY_TYPE entryType) {
        Intent i = new Intent(mContext, ExpandedRecipeActivity.class);

        i.putExtra(Constants.recipeId, data.getRecipeId());
        // Put the object in instead of the actual recipeId
//        i.putExtra(ExpandedRecipeActivity.RECIPE_OBJECT_STRING, data);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(Constants.mealType, getMealTypeString(entryType));
        i.putExtra(Constants.numberServings, data.getChosenNumberOfServings());
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, true);
        i.putExtra(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL, false);
        i.putExtra(Constants.type, entryType);
        mContext.startActivity(i);
    }

    private void startEditMealActivity(String objectId) {
        Intent i = new Intent(mContext, CreateOrEditMealActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(SearchFoodResultsFragment.ENTRY_TYPE_STRING, type);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_VIEW);
        i.putExtra(CreateOrEditMealActivity.IS_COMING_FROM_DIARY, true);
        i.putExtra(Constants.objectId, objectId);
        ((Activity) mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURNING_TO_MEAL_FRAGMENT_AFTER_EDIT);
    }

    private void startEditWaterActivity(String objectId) {
        Intent i = new Intent(mContext, AddWaterActivity.class);
        i.putExtra(Constants.objectId, objectId);
        mContext.startActivity(i);
    }

    private void startEditExerciseEntryActivity(ExerciseInfo info) {
        Intent i = new Intent(mContext, ExpandedExerciseActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedExerciseActivity.IS_EDITING, true);
        mContext.startActivity(i);
    }

    private void startCreatedEditExerciseActivity(ExerciseInfo info) {
        Intent i = new Intent(mContext, ExpandedExerciseActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.isCreated, true);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedExerciseActivity.IS_EDITING, true);
        mContext.startActivity(i);
    }

    private void startEditCreatedCardioEntryActivity(CardioInfo info) {
        Intent i = new Intent(mContext, EditCardioActivity.class);
        i.putExtra(Constants.isCreated, true);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        mContext.startActivity(i);
    }

    private void startEditCardioEntryActivity(CardioInfo info) {
        Intent i = new Intent(mContext, EditCardioActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        mContext.startActivity(i);
    }

    private void startEditWorkoutEntryActivity(CreatedWorkoutInfo info) {
        Intent i = new Intent(mContext, CreateOrEditWorkoutActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(Constants.workoutId, info.getWorkoutObjectId());
        i.putExtra(Constants.objectId, info.getObjectId());
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, MODE_VIEW);
        i.putExtra(CreateOrEditMealActivity.IS_COMING_FROM_DIARY, true);
        mContext.startActivity(i);
    }

    private String getMealComponenets(List<String> foodAndRecipeNames) {
        String mealComponents = "Includes ";
        int size = foodAndRecipeNames.size();
        for(int i = 0; i < size; i++) {
            if(i != size - 1)
                mealComponents+= foodAndRecipeNames.get(i) + ", ";
            else
                mealComponents+= " and " + foodAndRecipeNames.get(i) + ".";
        }
        return mealComponents;
    }

    private void initWaterView(final ItemViewHolder holder, final WaterInfo info) {
        holder.cookingIcon.setVisibility(View.GONE);
        holder.foodName.setText(mContext.getString(R.string.water));
        holder.foodBrand.setText(String.format(mContext.getString(R.string.x_cups), info.getNumberOfCups()));
        holder.servingSize.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditWaterActivity(info.getObjectId());
            }
        });
    }

    private void initCreatedFoodItemView(final ItemViewHolder holder, final DetailedFoodResultsInfo info) {
        holder.cookingIcon.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);

        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getFoodName(), TRUNCATE_LIMIT));
        if(info.getBrandName().isEmpty()) {
            holder.foodBrand.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.servingSize.getLayoutParams();
            params.setMarginStart(0);
        }
        else {
            holder.foodBrand.setVisibility(View.VISIBLE);
            holder.foodBrand.setText(String.format(mContext.getString(R.string.food_brand_name), info.getBrandName()));
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.servingSize.getLayoutParams();
            int dpValue = 5; // margin in dips
            float d = mContext.getResources().getDisplayMetrics().density;
            int margin = (int)(dpValue * d); // margin in pixels
            params.setMarginStart(margin);
        }

        holder.servingSize.setText(info.getServingDescription());
        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals),
                (int) ((Math.round(info.getCalories())) * info.getNumberOfServings())));

        setBonusFactIfApplicable(holder.bonusFact, info);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditCreatedFoodEntryActivity(info, type);
            }
        });
    }

    private void initFoodItemView(final ItemViewHolder holder, final DetailedFoodResultsInfo info) {
        holder.cookingIcon.setVisibility(View.GONE);

        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getFoodName(), TRUNCATE_LIMIT));
        holder.foodBrand.setText(String.format(mContext.getString(R.string.food_brand_name), info.getBrandName()));

        holder.servingSize.setText(getAdjustedServingSize(info));
        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), (int) ((Math.round(info.getCalories())) * info.getNumberOfServings())));
        setBonusFactIfApplicable(holder.bonusFact, info);


        if(info.getIsFavorite()) {
            holder.isFavorited = true;
            holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_filled);
        }
        else {
            holder.isFavorited = false;
            holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_unfilled);
        }

        holder.favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.isFavorited) {
                    new UpdateFavoriteFoods(false, info.getFoodId(), holder).execute();
                } else {
                    new UpdateFavoriteFoods(true, info.getFoodId(), holder).execute();
                }
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditFoodEntryActivity(info, type);
            }
        });
    }

    private void initWorkoutView(final ItemViewHolder holder, final CreatedWorkoutInfo info) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getWorkoutName(), TRUNCATE_LIMIT));
        holder.servingSize.setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.foodBrand.getLayoutParams();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                RECIPE_DESC_MARGIN_END,
                mContext.getResources().getDisplayMetrics()
        );
        params.setMarginEnd(px);
        holder.foodBrand.setLayoutParams(params);
        holder.foodBrand.setText(formatExerciseNames(info.getExerciseNames()));

        holder.cookingIcon.setImageResource(R.drawable.ic_workout_sm);

        if(info.getTotalNumberOfCalories() != 0) {
            holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), info.getTotalNumberOfCalories()));
        }

        holder.favoriteButton.setVisibility(View.GONE);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            startEditWorkoutEntryActivity(info);
            }
        });
    }

    private void initMealView(final ItemViewHolder holder, final MealInfo info) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getMealName(), TRUNCATE_LIMIT));
        holder.servingSize.setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.foodBrand.getLayoutParams();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                RECIPE_DESC_MARGIN_END,
                mContext.getResources().getDisplayMetrics()
        );
        params.setMarginEnd(px);
        holder.foodBrand.setLayoutParams(params);
        holder.foodBrand.setText(getMealComponenets(info.getFoodAndRecipeNames()));

        holder.cookingIcon.setImageResource(R.drawable.ic_meal);

        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), info.getTotalNumberOfCalories()));

        holder.favoriteButton.setVisibility(View.GONE);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditMealActivity(info.getMealObjectId());
            }
        });
    }

    private void initRecipeView(final ItemViewHolder holder, final DetailedRecipeResultsInfo info) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getRecipeName(), TRUNCATE_LIMIT));
        holder.foodBrand.setText(info.getRecipeDescription());

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.foodBrand.getLayoutParams();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                RECIPE_DESC_MARGIN_END,
                mContext.getResources().getDisplayMetrics()
        );
        params.setMarginEnd(px);
        holder.foodBrand.setLayoutParams(params);

        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals),
                (int) ((Math.round(info.getServingsList().get(info.getSelectedServingOption()).getCalories())) * info.getChosenNumberOfServings())));
        setBonusFactIfApplicable(holder.bonusFact, info);


        if(info.isFavorite()) {
            holder.isFavorited = true;
            holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_filled);
        }
        else {
            holder.isFavorited = false;
            holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_unfilled);
        }

        holder.favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.isFavorited) {
                    new UpdateFavoriteRecipes(false, info.getRecipeId(), holder).execute();
                } else {
                    new UpdateFavoriteRecipes(true, info.getRecipeId(), holder).execute();
                }
            }
        });

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startEditRecipeEntryActivity(info, type);
            }
        });
    }

    private void initCardioView(final ItemViewHolder holder, final CardioInfo info) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getName(), TRUNCATE_LIMIT));
        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), Double.valueOf(info.getCaloriesBurned()).intValue()));

        holder.cookingIcon.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);

        holder.servingSize.setText(String.format(mContext.getString(R.string.x_mins), (int) ((Math.round(info.getMinutesPerformed())))));
        holder.servingSize.setTextColor(ContextCompat.getColor(mContext, R.color.app_theme));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(info.isCreated()) {
                    startEditCreatedCardioEntryActivity(info);
                }
                else {
                    startEditCardioEntryActivity(info);
                }
            }
        });
    }

    private void initExerciseView(final ItemViewHolder holder, final ExerciseInfo info) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getName(), TRUNCATE_LIMIT));

        holder.cookingIcon.setVisibility(View.GONE);
        holder.calorieCount.setVisibility(View.GONE);
        holder.favoriteButton.setVisibility(View.GONE);

        holder.foodBrand.setText(String.format(mContext.getString(R.string.x_sets), info.getNumberOfSets()));

        holder.servingSize.setText(String.format(mContext.getString(R.string.x_reps_per_set), info.getRepsPerSet()));
        setWeightsOnExerciseIfApplicable(holder.bonusFact, info);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(info.isCreated()) {
                    startCreatedEditExerciseActivity(info);
                }
                else {
                    startEditExerciseEntryActivity(info);
                }
            }
        });
    }

    private String formatExerciseNames(List<String> names) {
        String formattedNames = "Includes ";
        int len = names.size();

        if(len == 1) {
            formattedNames += names.get(0) + ".";
        }
        else {
            for (int i = 0; i < len; i++) {
                if (i == len - 1) {
                    formattedNames += "and " + names.get(i) + ".";
                } else {
                    formattedNames += names.get(i) + ", ";
                }
            }
        }
        return formattedNames;
    }


    // TODO : Using measurement description instead of serving description extraction maybe?
    private String getAdjustedServingSize(DetailedFoodResultsInfo info) {
        double number = (info.getNumberOfServings() * info.getNumberOfUnits());
        String units = info.getServingDescription().split(" ")[1];
        return String.valueOf(number) + " " + units;
    }

    private void setWeightsOnExerciseIfApplicable(TextView bonusFact, ExerciseInfo info) {
        if(info.getWeightPerSet() != -1 && !info.getWeightUnit().equals(Constants.unknown)) {
            bonusFact.setText(String.format(mContext.getString(R.string.x_units), info.getWeightPerSet(), info.getWeightUnit()));
            bonusFact.setVisibility(View.VISIBLE);
        }
    }

    private void setBonusFactIfApplicable(TextView bonusFact, DetailedFoodResultsInfo info) {
        if(info.getBonusFact() != null) {
            bonusFact.setText(info.getBonusFact());
            DetailedFoodResultsInfo.FACT_TYPE type = info.getFactType();
            switch(type) {
                case FAT:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
                    break;
                case CARBS:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_light));
                    break;
                case PROTEIN:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
                    break;
            }
            bonusFact.setVisibility(View.VISIBLE);
        }
    }

    private void setBonusFactIfApplicable(TextView bonusFact, DetailedRecipeResultsInfo info) {
        if(info.getBonusFact() != null) {
            bonusFact.setText(info.getBonusFact());
            DetailedFoodResultsInfo.FACT_TYPE type = info.getFactType();
            switch(type) {
                case FAT:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
                    break;
                case CARBS:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_light));
                    break;
                case PROTEIN:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
                    break;
            }
            bonusFact.setVisibility(View.VISIBLE);
        }
    }

    private void addOrRemoveFoodToFavorites(boolean toAdd, long foodId) throws Exception {

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.food));
        query.whereEqualTo(Constants.foodIdNumber, foodId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.put(Constants.isFavorite, toAdd);
            object.save();
        }
    }

    private void addOrRemoveRecipeToFavorites(boolean toAdd, long recipeId) throws Exception {

        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.FoodDiary);
        query.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
        query.whereEqualTo(Constants.entryType, mContext.getString(R.string.recipe));
        query.whereEqualTo(Constants.recipeIdField, recipeId);

        List<ParseObject> result = query.find();
        for(ParseObject object : result) {
            object.put(Constants.isFavorite, toAdd);
            object.save();
        }
    }

    //------------------------
    // ASYNCTASK(S)
    //------------------------


    private class RemoveFromFoodDatabase extends AsyncTask<Void, Void, Boolean> {

        long foodId, servingId;
        double numberOfServings;
        DiaryPageFragment.ENTRY_TYPE type;

        public RemoveFromFoodDatabase(long foodId, long servingId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) {
            this.foodId = foodId;
            this.servingId = servingId;
            this.numberOfServings = numberOfServings;
            this.type = type;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeFoodFromDatabase(foodId, servingId, numberOfServings, type);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RemoveCreatedFoodFromDatabase extends AsyncTask<Void, Void, Boolean> {

        String createdFoodObjectId;
        double numberOfServings;
        DiaryPageFragment.ENTRY_TYPE type;

        public RemoveCreatedFoodFromDatabase(String createdFoodObjectId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) {
            this.createdFoodObjectId = createdFoodObjectId;
            this.numberOfServings = numberOfServings;
            this.type = type;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeCreatedFoodFromDatabase(createdFoodObjectId, numberOfServings, type);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RemoveFromRecipeDatabase extends AsyncTask<Void, Void, Boolean> {

        long recipeId;
        double numberOfServings;
        DiaryPageFragment.ENTRY_TYPE type;

        public RemoveFromRecipeDatabase(long foodId, double numberOfServings, DiaryPageFragment.ENTRY_TYPE type) {
            this.recipeId = foodId;
            this.numberOfServings = numberOfServings;
            this.type = type;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeRecipeFromDatabase(recipeId, numberOfServings, type);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RemoveFromMealDatabase extends AsyncTask<Void, Void, Boolean> {

        String mealObjectId;

        public RemoveFromMealDatabase(String mealObjectId) {
            this.mealObjectId = mealObjectId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeMealFromDatabase(mealObjectId);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RemoveFromExerciseDatabase extends AsyncTask<Void, Void, Boolean> {

        String objectId;

        public RemoveFromExerciseDatabase(String objectId) {
            this.objectId = objectId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeExerciseFromDiary(objectId);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class RemoveFromWaterDatabase extends AsyncTask<Void, Void, Boolean> {

        String waterObjectId;

        public RemoveFromWaterDatabase(String waterObjectId) {
            this.waterObjectId = waterObjectId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                removeWaterFromDatabase(waterObjectId);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                fragment.updateRecyclerHeaderCalorieCount(items, headerTextView);
                fragment.updateCalorieCountOnHeader();
                if(items.size() == 0) {
                    parentRecycler.setVisibility(View.GONE);
                }
            }
        }
    }

    private class UpdateFavoriteFoods extends AsyncTask<Void, Void, Boolean> {

        long foodId;
        boolean toAdd;
        ItemViewHolder holder;

        public UpdateFavoriteFoods(boolean toAdd, long foodId, ItemViewHolder holder) {
            this.toAdd = toAdd;
            this.foodId = foodId;
            this.holder = holder;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean result;
            if(toAdd) {
                result = foods.addFavoriteFood(foodId);
            }
            else {
                result = foods.deleteFavoriteFood(foodId);
            }

            if(result) {
                try {
                    addOrRemoveFoodToFavorites(toAdd, foodId);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean boolea) {
            super.onPostExecute(boolea);

            if(boolea) {
                holder.isFavorited = toAdd;
                if(toAdd) {
                    Snackbar.make(parentRecycler, mContext.getString(R.string.added_to_favorites), Snackbar.LENGTH_LONG)
                            .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new UpdateFavoriteFoods(!toAdd, foodId, holder).execute();
                                }
                            }).show();

                    holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_filled);
                }
                else {
                    Snackbar.make(parentRecycler, mContext.getString(R.string.removed_from_favorites), Snackbar.LENGTH_LONG)
                            .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new UpdateFavoriteFoods(!toAdd, foodId, holder).execute();
                                }
                            }).show();
                    holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_unfilled);
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }

        }
    }

    private class UpdateFavoriteRecipes extends AsyncTask<Void, Void, Boolean> {

        long recipeId;
        boolean toAdd;
        ItemViewHolder holder;

        public UpdateFavoriteRecipes(boolean toAdd, long recipeId, ItemViewHolder holder) {
            this.toAdd = toAdd;
            this.recipeId = recipeId;
            this.holder = holder;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            boolean result;
            if(toAdd) {
                result = recipes.addFavoriteRecipe(recipeId);
            }
            else {
                result = recipes.deleteFavoriteRecipe(recipeId);
            }

            if(result) {
                try {
                    addOrRemoveRecipeToFavorites(toAdd, recipeId);
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
            else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean boolea) {
            super.onPostExecute(boolea);

            if(boolea) {
                holder.isFavorited = toAdd;
                if(toAdd) {
                    Snackbar.make(parentRecycler, mContext.getString(R.string.added_to_favorites), Snackbar.LENGTH_LONG)
                            .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new UpdateFavoriteRecipes(!toAdd, recipeId, holder).execute();
                                }
                            }).show();

                    holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_filled);
                }
                else {
                    Snackbar.make(parentRecycler, mContext.getString(R.string.removed_from_favorites), Snackbar.LENGTH_LONG)
                            .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new UpdateFavoriteRecipes(!toAdd, recipeId, holder).execute();
                                }
                            }).show();
                    holder.favoriteButton.setBackgroundResource(R.drawable.ic_star_unfilled);
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }

        }
    }
}
