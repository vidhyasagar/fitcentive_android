package com.vidhyasagar.fitcentive.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.MainActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.activities.SinglePostActivity;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.NotificationInfo;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vharihar on 2016-10-19.
 */
public class NotificationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_FOLLOW_REQUEST = 1;
    public static final int VIEW_TYPE_LIKE = 2;
    public static final int VIEW_TYPE_COMMENT = 3;

    private int POSTS_COUNT;
    HashMap<String, Bitmap> pictureTable;

    private ArrayList<NotificationInfo> info;
    private Context mContext;

    public static class FollowRequestHolder extends RecyclerView.ViewHolder {

        protected FloatingActionButton acceptButton, declineButton;
        protected CircleImageView profilePicture;
        protected TextView notificationCaption;
        protected CardView cardView;
        protected TextView date;

        public FollowRequestHolder(View itemView) {
            super(itemView);
            profilePicture = (CircleImageView) itemView.findViewById(R.id.user_profile_picture);
            acceptButton = (FloatingActionButton) itemView.findViewById(R.id.follow_button);
            declineButton = (FloatingActionButton) itemView.findViewById(R.id.decline_button);
            notificationCaption = (TextView) itemView.findViewById(R.id.caption);
            cardView = (CardView) itemView.findViewById(R.id.notification_cardview);
            date = (TextView) itemView.findViewById(R.id.notification_date);
        }
    }

    public static class LikeCommentHolder extends RecyclerView.ViewHolder {

        protected TextView caption, date;
        protected CircleImageView profilePicture;
        protected CardView cardView;

        public LikeCommentHolder(View itemView) {
            super(itemView);
            caption = (TextView) itemView.findViewById(R.id.caption);
            profilePicture = (CircleImageView) itemView.findViewById(R.id.user_profile_picture);
            cardView = (CardView) itemView.findViewById(R.id.notification_cardview);
            date = (TextView) itemView.findViewById(R.id.notification_date);
        }
    }


    public NotificationsAdapter(ArrayList<NotificationInfo> info, Context context) {
        this.info = info;
        this.mContext = context;
        this.POSTS_COUNT  = mContext.getResources().getInteger(R.integer.notifications_limit);
    }

    public void setViewVisibility(final View v, final boolean show, final boolean isGone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = mContext.getResources().getInteger(android.R.integer.config_mediumAnimTime);
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
            v.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
                }
            });
        }
        else {
            v.setVisibility(show ? View.VISIBLE : (isGone ? View.GONE : View.INVISIBLE));
        }

    }

    public void addToFollowingList(String providerUsername, String receiverUsername) throws ParseException {
        // ReceiverUserName is the same as currentUserName
        // Receiver must modify their followerList
        // Provided must modify their followingList
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Follow);
        query.whereEqualTo(Constants.username, providerUsername);
        ParseQuery<ParseObject> query1 = new ParseQuery<>(Constants.Follow);
        query1.whereEqualTo(Constants.username, receiverUsername);
        List<ParseObject> result = query.find();
        List<ParseObject> result1 = query1.find();

        // Adding to following list
        if(result.isEmpty()) {
            // No such object exists, create it
            List<String> followingList = new ArrayList<>();
            followingList.add(receiverUsername);
            ParseObject object = new ParseObject(Constants.Follow);
            object.put(Constants.username, providerUsername);
            object.put(Constants.following, followingList);
            object.put(Constants.numFollowing, 1);
            object.put(Constants.numFollowers, 0);
            object.save();
        }
        else {
            // Object already exists, just add intentUserName to following
            // result size HAS to be 1, because usernames are UNIQUE
            List<String> followingList = result.get(0).getList(Constants.following);
            int numFollowing = result.get(0).getInt(Constants.numFollowing);
            numFollowing++;
            if(followingList == null) {
                followingList = new ArrayList<>();
            }
            followingList.add(receiverUsername);
            result.get(0).put(Constants.following, followingList);
            result.get(0).put(Constants.numFollowing, numFollowing);
            result.get(0).save();
        }

        // Addding to follower list
        if(result1.isEmpty()) {
            // No such object exists, create it
            List<String> followerList = new ArrayList<>();
            followerList.add(providerUsername);
            ParseObject object = new ParseObject(Constants.Follow);
            object.put(Constants.username, receiverUsername);
            object.put(Constants.followers, followerList);
            object.put(Constants.numFollowing, 0);
            object.put(Constants.numFollowers, 1);
            object.save();
        }
        else {
            // Object already exists, just add intentUserName to following
            // result size HAS to be 1, because usernames are UNIQUE
            List<String> followerList = result1.get(0).getList(Constants.followers);
            int numFollowers = result1.get(0).getInt(Constants.numFollowers);
            numFollowers++;
            if(followerList == null) {
                followerList = new ArrayList<>();
            }
            followerList.add(providerUsername);
            result1.get(0).put(Constants.followers, followerList);
            result1.get(0).put(Constants.numFollowers, numFollowers);
            result1.get(0).save();
        }
    }

    private void declineFollowRequest(String providerUsername, String receiverUsername,
                                      String postId, String providerName, boolean isDecline) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Notification);
        query.whereEqualTo(Constants.receiverUsername, receiverUsername);
        query.whereEqualTo(Constants.providerUsername, providerUsername);
        query.whereEqualTo(Constants.postId, postId);
        query.whereEqualTo(Constants.type, mContext.getString(R.string.follow_request));
        query.whereEqualTo(Constants.isActive, true);
        List<ParseObject> result = query.find();
        if (result.size() != 1) {
            throw new IllegalStateException();
        } else {
            result.get(0).put(Constants.isActive, false);
            if(isDecline) {
                result.get(0).put(Constants.changedCaption, String.format(mContext.getString(R.string.follow_request_decline), providerName));
            }
            else {
                result.get(0).put(Constants.changedCaption, String.format(mContext.getString(R.string.follow_request_accept), providerName));
            }
            result.get(0).save();
        }
    }

    private void acceptFollowRequest(String providerUsername, String receiverUsername, String postId, String providerName) throws Exception {
        declineFollowRequest(providerUsername, receiverUsername, postId, providerName, false);
        // Adding on to the disabling of the notification object
        addToFollowingList(providerUsername, receiverUsername);
    }

    private void startUserProfileActivity(String providerUsername) {
        Intent i = new Intent(mContext, ProfileActivity.class);
        i.putExtra("username", providerUsername);
        mContext.startActivity(i);
    }

    private void startSpecifiedPostActivity(String objectId) {
        Intent i = new Intent(mContext, SinglePostActivity.class);
        i.putExtra("postId", objectId);
        mContext.startActivity(i);
    }

    private void modifyLayoutParams(TextView caption) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) caption.getLayoutParams();
        params.width = RecyclerView.LayoutParams.WRAP_CONTENT;
        caption.setLayoutParams(params);
    }

    private void rectifyLayoutParams(TextView caption) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) caption.getLayoutParams();
        params.width = Utilities.dpToPx(175, mContext);
        caption.setLayoutParams(params);
    }

    private void deactivateNotification(String provider, String receiver, String postId, boolean isLikeNotification) throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<>(Constants.Notification);
        query.whereEqualTo(Constants.receiverUsername, receiver);
        query.whereEqualTo(Constants.providerUsername, provider);
        query.whereEqualTo(Constants.postId, postId);
        if(isLikeNotification) {
            query.whereEqualTo(Constants.type, mContext.getString(R.string.like_request));
        }
        else {
            query.whereEqualTo(Constants.type, mContext.getString(R.string.comment_request));
        }
        query.whereEqualTo(Constants.isActive, true);
        List<ParseObject> result =  query.find();
        for(ParseObject object : result) {
            object.put(Constants.isActive, false);
            object.save();
        }
    }

    private void setUpFollowRequestData(final FollowRequestHolder mHolder, final NotificationInfo notification) {
        mHolder.profilePicture.setImageBitmap(notification.getAssociatedImage());
        mHolder.date.setText(notification.getNotificationDate());

        mHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startUserProfileActivity(notification.getProviderUsername());
            }
        });


        if(!notification.getStatus()) {
            mHolder.declineButton.setVisibility(View.GONE);
            mHolder.acceptButton.setVisibility(View.GONE);

            // Set caption for notification here depending on whether or not user has been followed
//            modifyLayoutParams(mHolder.notificationCaption);
            mHolder.notificationCaption.setText(notification.getChangedCaption());
        }

        else {
            mHolder.declineButton.setVisibility(View.VISIBLE);
            mHolder.acceptButton.setVisibility(View.VISIBLE);
//            rectifyLayoutParams(mHolder.notificationCaption);

            mHolder.notificationCaption.setText(String.format(mContext.getString(R.string.follow_request_caption),
                    notification.getProviderName()));

            mHolder.declineButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mHolder.declineButton.hide();
                    mHolder.acceptButton.hide();
//                    modifyLayoutParams(mHolder.notificationCaption);
                    mHolder.notificationCaption.setText(String.format(mContext.getString(R.string.follow_request_decline),
                            notification.getProviderName()));

                    new FollowRequest(notification.getProviderUsername(), notification.getReceiverUsername(),
                            notification.getAssociatedPostId(), notification.getProviderName(), false).execute();
                }
            });

            mHolder.acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mHolder.declineButton.hide();
                    mHolder.acceptButton.hide();
//                    modifyLayoutParams(mHolder.notificationCaption);
                    mHolder.notificationCaption.setText(String.format(mContext.getString(R.string.follow_request_accept), notification.getProviderName()));

                    new FollowRequest(notification.getProviderUsername(), notification.getReceiverUsername(),
                            notification.getAssociatedPostId(), notification.getProviderName(), true).execute();
                }
            });
        }
    }

    private void setUpLikeData(LikeCommentHolder mHolder, final NotificationInfo notificationInfo) {
        mHolder.profilePicture.setImageBitmap(notificationInfo.getAssociatedImage());
        mHolder.date.setText(notificationInfo.getNotificationDate());
        mHolder.caption.setText(String.format(mContext.getString(R.string.x_likes_your_post), notificationInfo.getProviderName()));
        mHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSpecifiedPostActivity(notificationInfo.getAssociatedPostId());
            }
        });

        new DeactivateNotification(notificationInfo.getProviderUsername(), notificationInfo.getReceiverUsername(),
                notificationInfo.getAssociatedPostId(), true).execute();
    }

    private void setUpCommentData(LikeCommentHolder mHolder, final NotificationInfo notificationInfo) {
        mHolder.profilePicture.setImageBitmap(notificationInfo.getAssociatedImage());
        mHolder.date.setText(notificationInfo.getNotificationDate());
        mHolder.caption.setText(String.format(mContext.getString(R.string.x_commented_on_your_post), notificationInfo.getProviderName()));
        mHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startSpecifiedPostActivity(notificationInfo.getAssociatedPostId());
            }
        });

        new DeactivateNotification(notificationInfo.getProviderUsername(), notificationInfo.getReceiverUsername(),
                notificationInfo.getAssociatedPostId(), false).execute();
    }

    public void fetchNotifications() throws Exception {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>(Constants.Notification);
        query.whereEqualTo(Constants.receiverUsername, ParseUser.getCurrentUser().getUsername());
        query.orderByDescending(Constants.createdAt);
        query.setSkip(POSTS_COUNT);
        query.setLimit(mContext.getResources().getInteger(R.integer.notifications_limit));
        POSTS_COUNT += mContext.getResources().getInteger(R.integer.notifications_limit);
        List<ParseObject> result = query.find();
        for(ParseObject notification : result) {
            String receiverUsername = notification.getString(Constants.receiverUsername);
            String providerUsername = notification.getString(Constants.providerUsername);
            String type = notification.getString(Constants.type);
            String postId = notification.getString(Constants.postId);
            String changedCaption = notification.getString(Constants.changedCaption);
            Date notificationDate = notification.getCreatedAt();
            boolean isActive = notification.getBoolean(Constants.isActive);
            Bitmap image;
            ParseFile file = null;
            if(pictureTable.containsKey(providerUsername)) {
                image = pictureTable.get(providerUsername);
            }
            else {
                file = notification.getParseFile(Constants.image);
                if(file != null) {
                    byte[] bytes = file.getData();
                    image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    if(!pictureTable.containsKey(providerUsername)) {
                        pictureTable.put(providerUsername, image);
                    }

                }

                else {
                    image = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_logo);
                }
            }

            NotificationInfo.NOTIFICATION_TYPE notificationType = type.equals(mContext.getString(R.string.follow_request))
                    ? NotificationInfo.NOTIFICATION_TYPE.FOLLOW_REQUEST : (type.equals(mContext.getString(R.string.comment_request))
                    ? NotificationInfo.NOTIFICATION_TYPE.COMMENT : NotificationInfo.NOTIFICATION_TYPE.LIKE);

            ParseQuery<ParseUser> query2 = ParseUser.getQuery();
            query2.whereEqualTo(Constants.username, receiverUsername);
            ParseUser user1 = query2.getFirst();
            String receiverName = user1.getString(Constants.firstname) + " " + user1.getString(Constants.lastname);

            ParseQuery<ParseUser> query3 = ParseUser.getQuery();
            query3.whereEqualTo(Constants.username, providerUsername);
            ParseUser user2 = query3.getFirst();
            String providerName = user2.getString(Constants.firstname) + " " + user2.getString(Constants.lastname);

            NotificationInfo newInfo = new NotificationInfo(notificationType, receiverUsername, providerUsername,
                    postId, image, receiverName, providerName, isActive, changedCaption, notificationDate);
            info.add(newInfo);
        }
    }

    public void setPostsCount(int count) {
        this.POSTS_COUNT = count;
    }

    public void setPictureTable(HashMap<String, Bitmap> pictureTable) {
        this.pictureTable = pictureTable;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case VIEW_TYPE_FOLLOW_REQUEST:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification_request, parent, false);
                return new FollowRequestHolder(itemView);
            case VIEW_TYPE_LIKE:
            case VIEW_TYPE_COMMENT:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_notification_like_comment, parent, false);
                return new LikeCommentHolder(itemView);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final NotificationInfo notification = info.get(position);
        int typeOfView = getItemViewType(position);
        if(typeOfView == VIEW_TYPE_FOLLOW_REQUEST) {
            FollowRequestHolder mHolder = (FollowRequestHolder) holder;
            setUpFollowRequestData(mHolder, notification);
        }
        else if (typeOfView == VIEW_TYPE_LIKE) {
            LikeCommentHolder mHolder = (LikeCommentHolder) holder;
            setUpLikeData(mHolder, notification);
        }
        else if (typeOfView == VIEW_TYPE_COMMENT) {
            LikeCommentHolder mHolder = (LikeCommentHolder) holder;
            setUpCommentData(mHolder, notification);
        }

        if(POSTS_COUNT - position == 4) {
            // 3rd last position, fetch more posts as the user is nearing the end
            new FetchMoreInfo().execute();
        }
    }

    @Override
    public int getItemCount() {
        return info.size();
    }

    @Override
    public int getItemViewType(int position) {
        switch (info.get(position).getType()) {
            case COMMENT:
                return VIEW_TYPE_COMMENT;
            case LIKE:
                return VIEW_TYPE_LIKE;
            case FOLLOW_REQUEST:
                return VIEW_TYPE_FOLLOW_REQUEST;
            default:
                return 0;
        }
    }

    //=============================================
    // ASYNCTASK(S)
    //=============================================

    private class FollowRequest extends AsyncTask<Void, Void, Boolean> {

        String provider;
        String receiver;
        String postId;
        String providerName;
        boolean isAccept;

        public FollowRequest(String provider, String receiver, String postId, String providerName, boolean isAccept) {
            this.receiver = receiver;
            this.provider = provider;
            this.postId = postId;
            this.isAccept = isAccept;
            this.providerName = providerName;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                if(isAccept) {
                    acceptFollowRequest(provider, receiver, postId, providerName);
                }
                else {
                    declineFollowRequest(provider, receiver, postId, providerName, true);
                }
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                if(isAccept) {
                    Toast.makeText(mContext, mContext.getString(R.string.user_now_following_you), Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(mContext, mContext.getString(R.string.user_not_following_you), Toast.LENGTH_SHORT).show();
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class FetchMoreInfo extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchNotifications();
                return true;
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                notifyDataSetChanged();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class DeactivateNotification extends AsyncTask<Void, Void, Boolean> {

        String postId;
        String receiver;
        String provider;
        boolean isLikeNotification;

        public DeactivateNotification(String provider, String receiver, String postId, boolean isLikeNotification){
            this.postId = postId;
            this.receiver = receiver;
            this.provider = provider;
            this.isLikeNotification = isLikeNotification;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                deactivateNotification(provider, receiver, postId, isLikeNotification);
                return true;
            }
            catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(!aBoolean) {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected), Toast.LENGTH_SHORT).show();
            }
            else {
                // Update Navdrawer notification count
                ((MainActivity)mContext).updateNotificationsCount();
            }
        }
    }
}
