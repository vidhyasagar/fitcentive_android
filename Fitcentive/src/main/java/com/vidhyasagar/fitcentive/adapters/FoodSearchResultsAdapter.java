package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.api_requests.fatsecret.FatSecretFoods;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.search_food_fragments.SearchFoodResultsFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.FoodResultsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vharihar on 2016-11-11.
 */
public class FoodSearchResultsAdapter extends RecyclerView.Adapter<FoodSearchResultsAdapter.FoodSearchResultHolder> {

    public static int NAME_CHAR_LIMIT = 28;

    private ArrayList<FoodResultsInfo> foodList;
    private Context mContext;
    private SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType;
    private DiaryPageFragment.ENTRY_TYPE entryType;
    private int dayOffset;
    private FatSecretFoods food;
    private String searchText;

    private boolean isReturningToCreateMealActivity;

    private int CURRENT_PAGE_NUMBER = 1;
    private int RETRY_COUNT = 0;

    private boolean HAVE_RESULTS_EXHAUSTED = false;

    public static class FoodSearchResultHolder extends RecyclerView.ViewHolder {

        protected TextView foodName;
        protected TextView foodBrand;
        protected TextView foodServingSize;
        protected TextView foodCalories;
        protected CardView cardView;

        public FoodSearchResultHolder(View view) {
            super(view);

            foodBrand = (TextView) view.findViewById(R.id.food_brand);
            foodName = (TextView) view.findViewById(R.id.food_name);
            foodServingSize = (TextView) view.findViewById(R.id.food_serving_size);
            foodCalories = (TextView) view.findViewById(R.id.food_calories);
            cardView = (CardView) view.findViewById(R.id.user_results_card_view);
        }

    }

    public FoodSearchResultsAdapter(ArrayList<FoodResultsInfo> foodList, Context mContext,
                                    SearchFoodResultsFragment.RESULT_TYPE_ENUM resultType,
                                    DiaryPageFragment.ENTRY_TYPE entryType, int dayOffset,
                                    boolean isReturningToCreateMealActivity) {
        this.foodList = foodList;
        this.mContext = mContext;
        this.resultType = resultType;
        this.entryType = entryType;
        this.dayOffset = dayOffset;
        this.isReturningToCreateMealActivity = isReturningToCreateMealActivity;
        food = new FatSecretFoods();
    }

    public void setSearchText(String text) {
        this.searchText = text;
        // Setting this to false because with a new searchText, results may differ
        HAVE_RESULTS_EXHAUSTED = false;
    }

    public void setCurrentPage(int page) {
        this.CURRENT_PAGE_NUMBER = page;
    }

    @Override
    public FoodSearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_food_result, parent, false);
        return new FoodSearchResultHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(FoodSearchResultHolder holder, final int position) {
        FoodResultsInfo info = foodList.get(position);

        holder.foodName.setText(truncateString(info.getFoodName()));
        holder.foodBrand.setText(String.format(mContext.getString(R.string.food_brand_name), info.getBrandName()));
        holder.foodServingSize.setText(info.getServingSize());
        holder.foodCalories.setText(String.format(mContext.getString(R.string.x_kcals), info.getCalories()));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startExpandedSearchFoodActivity(position);
            }
        });

        if(resultType == SearchFoodResultsFragment.RESULT_TYPE_ENUM.TYPE_ALL) {
            // Fire away fetching more if you have reached the last 10 elements
            if(position == foodList.size() - 10) {
                if(!HAVE_RESULTS_EXHAUSTED) {
                    new SearchFoods(searchText).execute();
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    private void startExpandedSearchFoodActivity(int position) {
        Intent i = new Intent(mContext, ExpandedSearchFoodActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, entryType);
        i.putExtra(ExpandedSearchFoodActivity.RESULT_TYPE_STRING, resultType);
        i.putExtra(ExpandedSearchFoodActivity.CURRENT_ITEM_STRING, position);
        i.putParcelableArrayListExtra(ExpandedSearchFoodActivity.RESULT_LIST, foodList);
        i.putExtra(SearchFoodResultsFragment.IS_RETURNING_TO_CREATE_MEAL, isReturningToCreateMealActivity);
//        i.setFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURN_FROM_FOOD_SELECT);
//        ((Activity) mContext).finish();
    }

    public static String truncateString(String foodName) {
        if(foodName.length() <= NAME_CHAR_LIMIT) {
            return foodName;
        }
        else {
            return foodName.substring(0, NAME_CHAR_LIMIT) + "...";
        }
    }

    public static String truncateString(String foodName, int limit) {
        if(foodName.length() <= limit) {
            return foodName;
        }
        else {
            return foodName.substring(0, limit) + "...";
        }
    }

    private void fatSecretFoodSearchAll(String foodName) throws JSONException {
        JSONObject result = food.searchFoods(foodName, CURRENT_PAGE_NUMBER);
        if(result != null) {
            JSONArray resultArray = result.optJSONArray(Constants.food);
            if(resultArray != null) {
                CURRENT_PAGE_NUMBER++;
                int size = resultArray.length();
                for (int i = 0; i < size; i++) {
                    JSONObject foodItem = resultArray.getJSONObject(i);
                    String brandName = foodItem.optString(Constants.brandName, Constants.unknown);
                    String foodDescription = foodItem.optString(Constants.foodDescription, Constants.unknown);
                    String fooDName = foodItem.optString(Constants.foodName, Constants.unknown);
                    String foodType = foodItem.optString(Constants.foodType, Constants.unknown);
                    String foodUrl = foodItem.optString(Constants.foodUrl, Constants.noUrlFound);
                    long foodId = foodItem.optLong(Constants.foodId, -1);
                    FoodResultsInfo newFood = new FoodResultsInfo(brandName, foodDescription, fooDName, foodType, foodUrl, foodId);
                    foodList.add(newFood);
                }
            }
            else {
                // This means theres no value for "food", re-searching is useless because it's just gonna clog up the UI thread
                HAVE_RESULTS_EXHAUSTED = true;
            }
        }
    }


    public void replaceDataSet(ArrayList<FoodResultsInfo> newDataSet) {
        this.foodList = newDataSet;
        notifyDataSetChanged();
    }

    public boolean getIfDataSetEmpty() {
        return this.foodList.isEmpty();
    }

    //-------------------------------
    // ASYNCTASK(S)
    //-------------------------------

    private class SearchFoods extends AsyncTask<Void, Void, Boolean> {

        private String searchText;

        public SearchFoods(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                switch(resultType) {
                    case TYPE_ALL:
                        fatSecretFoodSearchAll(searchText);
                        break;
                    default: break;
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                notifyDataSetChanged();
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new SearchFoods(this.searchText).execute();
                }
            }
        }
    }

}
