package com.vidhyasagar.fitcentive.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.parse.ParseObject;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-14.
 */

public class CreatedExerciseAdapter extends ArrayAdapter<String> {

    ArrayList<String> items;
    Context context;

    boolean checkboxDisabled = false;

    ArrayList<Integer> selectedPositions;

    public CreatedExerciseAdapter(Context context, int resource, ArrayList<String> objects) {
        super(context, resource, objects);
        this.context = context;
        this.items = objects;
        selectedPositions = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.layout_exercise_meta_data, null);
        }

        TextView name = (TextView) convertView.findViewById(R.id.name);
        CheckBox box = (CheckBox) convertView.findViewById(R.id.checkBox1);

        name.setText(items.get(position));

        if (existsInSelectedPositions(position)) {
            box.setOnCheckedChangeListener(null);
            box.setChecked(true);
        } else {
            box.setOnCheckedChangeListener(null);
            box.setChecked(false);
        }

        if(checkboxDisabled) {
            box.setEnabled(false);
        }
        else {
            box.setEnabled(true);
        }

        box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    selectedPositions.add(position);
                }
                else {
                    selectedPositions.remove(Integer.valueOf(position));
                }
            }
        });

        return convertView;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    public ArrayList<Integer> getSelectedPositions() {
        return this.selectedPositions;
    }

    public void setPositionsAsChecked(ArrayList<Integer> list) {
        ArrayList<Integer> newList = new ArrayList<>();
        for(Integer i : list) {
            newList.add(i - CreateOrEditExerciseActivity.NORMAL_OFFSET);
        }

        selectedPositions = newList;
        notifyDataSetChanged();
    }

    public boolean existsInSelectedPositions(int position) {
        for(Integer i : selectedPositions) {
            if(i == position) {
                return true;
            }
        }
        return false;
    }

    public void disableCheckboxes(boolean disable) {
        checkboxDisabled = disable;
        notifyDataSetChanged();
    }
}
