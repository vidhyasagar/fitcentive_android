package com.vidhyasagar.fitcentive.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.vidhyasagar.fitcentive.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-12.
 */

public class ExerciseImageAdapter extends ArrayAdapter<Drawable> {

    Context mContext;
    ArrayList<Drawable> drawables;

    public ExerciseImageAdapter(Context context, int resource, ArrayList<Drawable> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.drawables = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.layout_exercise_image, null);
        }

        ImageView imageView = (ImageView) convertView.findViewById(R.id.exercise_image);

        imageView.setImageDrawable(drawables.get(position));
        imageView.setBackgroundColor(Color.TRANSPARENT);

        return convertView;
    }
}
