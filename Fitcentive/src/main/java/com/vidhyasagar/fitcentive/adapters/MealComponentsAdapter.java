package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.EditFoodEntryActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedSearchFoodActivity;
import com.vidhyasagar.fitcentive.fragments.DiaryPageFragment;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.wrappers.DetailedFoodResultsInfo;
import com.vidhyasagar.fitcentive.wrappers.DetailedRecipeResultsInfo;

import java.util.ArrayList;
import java.util.Collections;

import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;

/**
 * Created by vharihar on 2016-10-27.
 */
public class MealComponentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RVHAdapter {

    public static int RECIPE_DESC_MARGIN_END = 20;
    public static int TRUNCATE_LIMIT = 25;

    private ArrayList<Object> items;
    private Context mContext;
    private RecyclerView parentRecycler;

    private Object cache;
    private DiaryPageFragment.ENTRY_TYPE type;
    private DiaryPageFragment fragment;
    private TextView headerTextView;

    int dayOffset;
    int cachePosition;
    boolean isDisabled;


    public static class ItemViewHolder extends RecyclerView.ViewHolder implements RVHViewHolder{

        TextView foodName, servingSize, foodBrand, calorieCount, bonusFact;
        ImageView favoriteButton, cookingIcon;
        RelativeLayout parentLayout;

        public ItemViewHolder(View itemView) {
            super(itemView);
            foodName = (TextView) itemView.findViewById(R.id.food_name);
            servingSize = (TextView) itemView.findViewById(R.id.serving_size);
            foodBrand = (TextView) itemView.findViewById(R.id.food_brand);
            calorieCount = (TextView) itemView.findViewById(R.id.calorie_count);
            bonusFact = (TextView) itemView.findViewById(R.id.bonus_fact);
            favoriteButton = (ImageView) itemView.findViewById(R.id.favorite_button);
            cookingIcon = (ImageView) itemView.findViewById(R.id.cooking_icon);
            parentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_relative_layout);
        }

        @Override
        public void onItemSelected(int actionstate) {

        }

        @Override
        public void onItemClear() {

        }
    }



    public MealComponentsAdapter(ArrayList<Object> items, Context mContext, RecyclerView parentRecycler,
                        DiaryPageFragment.ENTRY_TYPE type, TextView headerTextView, int offset) {
        this.items = items;
        this.mContext = mContext;
        this.parentRecycler = parentRecycler;
        this.type = type;
        this.headerTextView = headerTextView;
        this.dayOffset = offset;
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if(!isDisabled) {
            swap(fromPosition, toPosition);
        }
        return false;
    }

    @Override
    public void onItemDismiss(int position, int direction) {
        if(!isDisabled) {
            remove(position);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_diary_entry_item, parent, false);
        return new ItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(items.get(position) instanceof DetailedFoodResultsInfo) {
            DetailedFoodResultsInfo info = (DetailedFoodResultsInfo) items.get(position);
            initItemView((ItemViewHolder) holder, info, position);
        }
        else {
            DetailedRecipeResultsInfo info = (DetailedRecipeResultsInfo) items.get(position);
            initRecipeView((ItemViewHolder) holder, info, position);
        }

    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public boolean isDisabled() {
        return isDisabled;
    }

    public void setDisabled(boolean disabled) {
        isDisabled = disabled;
    }

    public int getCachePosition() {
        return cachePosition;
    }

    public void updateTextViewHeader() {
        double totalCalories = 0;

        for(Object object : items) {
            if(object instanceof DetailedFoodResultsInfo) {
                DetailedFoodResultsInfo newInfo = (DetailedFoodResultsInfo) object;
                totalCalories += newInfo.getCalories() * newInfo.getNumberOfServings();
            }
            else {
                DetailedRecipeResultsInfo recipe = (DetailedRecipeResultsInfo) object;
                totalCalories += recipe.getServingsList().get(recipe.getSelectedServingOption()).getCalories()
                                     * recipe.getChosenNumberOfServings();
            }
        }

        if(totalCalories == 0) {
            headerTextView.setVisibility(View.GONE);
        }
        else {
            Double d = totalCalories;
            headerTextView.setVisibility(View.VISIBLE);
            headerTextView.setText(String.format(mContext.getString(R.string.x_kcals), d.intValue()));
        }
    }

    private void addOrRemoveItem(boolean toAdd, int position) {
        if(toAdd) {
            items.add(position, cache);
            notifyItemInserted(position);
        }
        else {
            items.remove(position);
            notifyItemRemoved(position);
        }

        if(items.isEmpty()) {
            parentRecycler.setVisibility(View.GONE);
        }
        else {
            parentRecycler.setVisibility(View.VISIBLE);
        }

        updateTextViewHeader();
    }

    private void remove(final int position) {
        cache = items.get(position);
        Snackbar.make(parentRecycler, mContext.getString(R.string.item_deleted), Snackbar.LENGTH_LONG)
                .setAction(mContext.getString(R.string.oops), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        addOrRemoveItem(true, position);
                    }
                })
                .setCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event) {
                        super.onDismissed(snackbar, event);
                        if(event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_SWIPE) {
                            ((CreateOrEditMealActivity)mContext).updatePieChartData();
                        }
                    }
                })
                .show();
        addOrRemoveItem(false, position);
    }

    private void swap(int firstPosition, int secondPosition) {
        Collections.swap(items, firstPosition, secondPosition);
        notifyItemMoved(firstPosition, secondPosition);
    }

    private void startEditFoodEntryActivity(DetailedFoodResultsInfo data, DiaryPageFragment.ENTRY_TYPE entryType, int position) {
        cachePosition = position;
        Intent i = new Intent(mContext, EditFoodEntryActivity.class);
        i.putExtra(ExpandedSearchFoodActivity.FOOD_ID, data.getFoodId());
        i.putExtra(Constants.servingId, data.getServingId());
        i.putExtra(Constants.numberServings, data.getNumberOfServings());
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedSearchFoodActivity.ENTRY_TYPE_STRING, entryType);
        i.putExtra(CreateOrEditMealActivity.IS_PART_OF_MEAL, true);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURN_FROM_EDIT_FOOD);
    }

    private String getMealTypeString(DiaryPageFragment.ENTRY_TYPE type) {
        switch(type) {
            case BREAKFAST: return mContext.getString(R.string.breakfast);
            case LUNCH: return mContext.getString(R.string.lunch);
            case DINNER: return mContext.getString(R.string.dinner);
            case SNACKS: return mContext.getString(R.string.snacks);
            case EXCERCISE: return mContext.getString(R.string.excercise);
            case WATER: return mContext.getString(R.string.water);
            default: return null;
        }
    }

    private void startEditRecipeEntryActivity(DetailedRecipeResultsInfo data, DiaryPageFragment.ENTRY_TYPE entryType, int position) {
        cachePosition = position;
        Intent i = new Intent(mContext, ExpandedRecipeActivity.class);
        i.putExtra(Constants.recipeId, data.getRecipeId());
        // Put the object in instead of the actual recipeId
//        i.putExtra(ExpandedRecipeActivity.RECIPE_OBJECT_STRING, data);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(Constants.mealType, getMealTypeString(entryType));
        i.putExtra(Constants.numberServings, data.getChosenNumberOfServings());
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, true);
        i.putExtra(CreateOrEditMealActivity.IS_PART_OF_MEAL, true);
        i.putExtra(Constants.type, entryType);
        ((Activity)mContext).startActivityForResult(i, CreateOrEditMealActivity.RETURN_FROM_EDIT_RECIPE);
    }

    private void initItemView(final ItemViewHolder holder, final DetailedFoodResultsInfo info, final int position) {
        holder.cookingIcon.setVisibility(View.GONE);

        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getFoodName(), TRUNCATE_LIMIT));
        holder.foodBrand.setText(String.format(mContext.getString(R.string.food_brand_name), info.getBrandName()));
        holder.servingSize.setText(getAdjustedServingSize(info));
        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals), (int) ((Math.round(info.getCalories())) * info.getNumberOfServings())));
        setBonusFactIfApplicable(holder.bonusFact, info);

        holder.favoriteButton.setVisibility(View.GONE);

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDisabled) {
                    startEditFoodEntryActivity(info, type, position);
                }
            }
        });
    }

    private void initRecipeView(final ItemViewHolder holder, final DetailedRecipeResultsInfo info, final int position) {
        holder.foodName.setText(FoodSearchResultsAdapter.truncateString(info.getRecipeName(), TRUNCATE_LIMIT));
        holder.foodBrand.setText(info.getRecipeDescription());

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.foodBrand.getLayoutParams();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                RECIPE_DESC_MARGIN_END,
                mContext.getResources().getDisplayMetrics()
        );
        params.setMarginEnd(px);
        holder.foodBrand.setLayoutParams(params);

        holder.calorieCount.setText(String.format(mContext.getString(R.string.xs_kcals),
                (int) ((Math.round(info.getServingsList().get(info.getSelectedServingOption()).getCalories())) * info.getChosenNumberOfServings())));
        setBonusFactIfApplicable(holder.bonusFact, info);

        holder.favoriteButton.setVisibility(View.GONE);


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isDisabled) {
                    startEditRecipeEntryActivity(info, type, position);
                }
            }
        });
    }


    // TODO : Using measurement description instead of serving description extraction maybe?
    private String getAdjustedServingSize(DetailedFoodResultsInfo info) {
        double number = (info.getNumberOfServings() * info.getNumberOfUnits());
        String units = info.getServingDescription().split(" ")[1];
        return String.valueOf(number) + " " + units;
    }

    private void setBonusFactIfApplicable(TextView bonusFact, DetailedFoodResultsInfo info) {
        if(info.getBonusFact() != null) {
            bonusFact.setText(info.getBonusFact());
            DetailedFoodResultsInfo.FACT_TYPE type = info.getFactType();
            switch(type) {
                case FAT:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
                    break;
                case CARBS:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_light));
                    break;
                case PROTEIN:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
                    break;
            }
            bonusFact.setVisibility(View.VISIBLE);
        }
    }

    private void setBonusFactIfApplicable(TextView bonusFact, DetailedRecipeResultsInfo info) {
        if(info.getBonusFact() != null) {
            bonusFact.setText(info.getBonusFact());
            DetailedFoodResultsInfo.FACT_TYPE type = info.getFactType();
            switch(type) {
                case FAT:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light));
                    break;
                case CARBS:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_blue_light));
                    break;
                case PROTEIN:
                    bonusFact.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_green_light));
                    break;
            }
            bonusFact.setVisibility(View.VISIBLE);
        }
    }

}
