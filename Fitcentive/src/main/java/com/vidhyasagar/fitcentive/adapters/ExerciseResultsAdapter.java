package com.vidhyasagar.fitcentive.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.vidhyasagar.fitcentive.R;
import com.vidhyasagar.fitcentive.activities.CreateOrEditExerciseActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditMealActivity;
import com.vidhyasagar.fitcentive.activities.CreateOrEditWorkoutActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedExerciseActivity;
import com.vidhyasagar.fitcentive.activities.ExpandedRecipeActivity;
import com.vidhyasagar.fitcentive.activities.ProfileActivity;
import com.vidhyasagar.fitcentive.api_requests.wger.ExerciseRequest;
import com.vidhyasagar.fitcentive.parse.Constants;
import com.vidhyasagar.fitcentive.search_exercise_fragments.CreatedExercisesFragment;
import com.vidhyasagar.fitcentive.search_exercise_fragments.ExerciseResultsFragment;
import com.vidhyasagar.fitcentive.utilities.Utilities;
import com.vidhyasagar.fitcentive.wrappers.ExerciseInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vharihar on 2016-12-09.
 */

public class ExerciseResultsAdapter extends  RecyclerView.Adapter<ExerciseResultsAdapter.ExerciseResultHolder>  {

    private ArrayList<ExerciseInfo> exerciseList;
    private Context mContext;
    private int dayOffset;
    private String searchText;


    private int CURRENT_PAGE_NUMBER = 1;
    private int RETRY_COUNT = 0;

    private boolean HAS_FETCH_BEEN_STARTED = false;
    private boolean isInteractable;
    private boolean isReturningToWorkoutFragment;
    private String nextUrlGlobal;

    private CreatedExercisesFragment fragment;


    public static class ExerciseResultHolder extends RecyclerView.ViewHolder {

        TextView exerciseName;
        CardView cardView;

        public ExerciseResultHolder(View itemView) {
            super(itemView);
            exerciseName = (TextView) itemView.findViewById(R.id.exercise_name);
            cardView = (CardView) itemView.findViewById(R.id.exercise_results_cardview);
        }
    }

    public ExerciseResultsAdapter(ArrayList<ExerciseInfo> exerciseList, Context mContext, int dayOffset,
                                  boolean isInteractable, boolean isReturningToWorkoutFragment) {
        this.exerciseList = exerciseList;
        this.mContext = mContext;
        this.dayOffset = dayOffset;
        this.isInteractable = isInteractable;
        this.isReturningToWorkoutFragment = isReturningToWorkoutFragment;
    }

    public ExerciseResultsAdapter(ArrayList<ExerciseInfo> exerciseList, Context mContext, int dayOffset,
                                  boolean isInteractable, CreatedExercisesFragment fragment, boolean isReturningToWorkoutFragment) {
        this.exerciseList = exerciseList;
        this.mContext = mContext;
        this.dayOffset = dayOffset;
        this.isInteractable = isInteractable;
        this.fragment = fragment;
        this.isReturningToWorkoutFragment = isReturningToWorkoutFragment;
    }

    @Override
    public ExerciseResultsAdapter.ExerciseResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_search_exercise_result, parent, false);
        return new ExerciseResultHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ExerciseResultsAdapter.ExerciseResultHolder holder, final int position) {
        final ExerciseInfo info = exerciseList.get(position);

        holder.exerciseName.setText(info.getName());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startViewExerciseActivity(info);
            }
        });

        if(isInteractable) {
            holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    bringUpLongClickOptionsMenu(info, position);
                    return true;
                }
            });
        }

        if(position == 0 && !HAS_FETCH_BEEN_STARTED) {
            if(this.nextUrlGlobal != null && !this.nextUrlGlobal.equals(Constants.nullString)) {
                new FetchMoreExercises(nextUrlGlobal).execute();
                HAS_FETCH_BEEN_STARTED = true;
            }
        }

    }

    @Override
    public int getItemCount() {
        return exerciseList.size();
    }

    private void bringUpLongClickOptionsMenu(final ExerciseInfo info, final int position) {
        final CharSequence [] choices;
        choices = mContext.getResources().getStringArray(R.array.created_exercise_list_choices);

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogCustom);

        builder.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0: copyToClipBoard(info.getName());
                        break;
                    case 1: startEditCreatedExerciseActivity(info.getCreatedExerciseObjectId());
                        break;
                    case 2: deleteCreatedExercise(info.getCreatedExerciseObjectId(), position);
                        break;
                    default: break;
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
    }

    // Deletion done in background here instead of using Asynctask
    private void deleteCreatedExercise(final String objectId, int position) {
        // Delete from database, delete from current data set, notifyDataSetChanged
        final ParseQuery<ParseObject> query = new ParseQuery<>(Constants.CreatedExercise);
        query.getInBackground(objectId, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                object.deleteInBackground(new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e != null) {
                            e.printStackTrace();
                        }
                        else {
                            Utilities.showSnackBar(mContext.getString(R.string.exercise_deleted_successfully), fragment.getLayoutForSnackbar());
                        }
                    }
                });

                // Now proceeding to delete all diary entries which have THIS createdExercise in it
                ParseQuery<ParseObject> deleteQuery = new ParseQuery<>(Constants.ExerciseDiary);
                deleteQuery.whereEqualTo(Constants.username, ParseUser.getCurrentUser().getUsername());
                deleteQuery.whereEqualTo(Constants.isCreated, true);
                deleteQuery.whereEqualTo(Constants.createdExerciseObjectId, objectId);

                deleteQuery.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> objects, ParseException e) {
                        if(e == null) {
                            for(ParseObject temp : objects) {
                                temp.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e != null) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });

        exerciseList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
        fragment.showStrengthGetStartedPageIfNeeded();
    }

    private void copyToClipBoard(String comment) {
        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(mContext.getString(R.string.food_name), comment);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(mContext, mContext.getString(R.string.exercise_name_copied), Toast.LENGTH_SHORT).show();
    }

    private void startEditCreatedExerciseActivity(String objectId) {
        Intent i = new Intent(mContext, CreateOrEditExerciseActivity.class);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.MODE_TYPE, CreateOrEditMealActivity.MODE.MODE_EDIT);
        i.putExtra(Constants.isCardio, false);
        i.putExtra(Constants.createdExerciseObjectId, objectId);
        ((Activity) mContext).startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
    }

    private void startViewExerciseActivity(ExerciseInfo info) {
        Intent i = new Intent(mContext, ExpandedExerciseActivity.class);
        i.putExtra(ExpandedExerciseActivity.INFO, info);
        i.putExtra(Constants.DAY_OFFSET, dayOffset);
        i.putExtra(ExpandedRecipeActivity.IS_EDITING, false);
        i.putExtra(Constants.isCreated, info.isCreated());
        i.putExtra(ExerciseResultsFragment.IS_RETURNING_TO_WORKOUT, isReturningToWorkoutFragment);
//        mContext.startActivity(i);
        if(isReturningToWorkoutFragment) {
            ((Activity) mContext).startActivityForResult(i, CreateOrEditWorkoutActivity.RETURN_TO_WORKOUT_COMPONENTS_FRAGMENT);
        }
        else {
            ((Activity) mContext).startActivityForResult(i, CreateOrEditExerciseActivity.RETURN_TO_CREATE_MEAL_FRAGMENT_AFTER_CREATING);
        }
    }

    public void setNextUrlGlobal(String url) {
        this.nextUrlGlobal = url;
    }

    public void setSearchText(String text) {
        this.searchText = text;
        // Setting this to false because with a new searchText, results may differ
        HAS_FETCH_BEEN_STARTED = false;
    }

    public void setCurrentPage(int page) {
        this.CURRENT_PAGE_NUMBER = page;
    }

    public void replaceDataSet(ArrayList<ExerciseInfo> newSet) {
        this.exerciseList = newSet;
        notifyDataSetChanged();
    }

    public void resetFetchFlag() {
        HAS_FETCH_BEEN_STARTED = false;
    }

    public boolean getIfDataSetEmpty() {
        return this.exerciseList.isEmpty();
    }

    private void fetchMoreExercises(String nextUrl) throws JSONException {
        JSONObject response;
        response = new ExerciseRequest().wgerExerciseSearchAll(nextUrl);
        if(response != null) {
            this.nextUrlGlobal = response.optString(Constants.next, Constants.unknown);
            JSONArray results = response.optJSONArray(Constants.results);
            if (results != null) {
                int size = results.length();
                for (int i = 0; i < size; i++) {
                    JSONObject object = results.getJSONObject(i);

                    String description = object.optString(Constants.description, Constants.unknown);
                    description = ExerciseResultsFragment.padDescription(description);
                    String name = object.optString(Constants.name, Constants.unknown);
                    long id = object.optLong(Constants.id, -1);
                    String originalName = object.optString(Constants.originalName, Constants.unknown);
                    long category = object.optLong(Constants.category, -1);

                    JSONArray musclesArray = object.optJSONArray(Constants.muscles);
                    JSONArray secondaryMusclesArray = object.optJSONArray(Constants.secondaryMuscles);
                    JSONArray equipmentArray = object.optJSONArray(Constants.equipment);

                    ArrayList<Long> muscles = new ArrayList<>();
                    ArrayList<Long> secondaryMuscles = new ArrayList<>();
                    ArrayList<Long> equipment = new ArrayList<>();
                    ExerciseResultsFragment.extractIntoList(musclesArray, muscles);
                    ExerciseResultsFragment.extractIntoList(secondaryMusclesArray, secondaryMuscles);
                    ExerciseResultsFragment.extractIntoList(equipmentArray, equipment);

                    ExerciseInfo exerciseInfo = new ExerciseInfo();
                    exerciseInfo.setDescription(description);
                    exerciseInfo.setName(name);
                    exerciseInfo.setOriginalName(originalName);
                    exerciseInfo.setId(id);
                    exerciseInfo.setCategory(category);
                    exerciseInfo.setPrimaryMuscles(muscles);
                    exerciseInfo.setSecondaryMuscles(secondaryMuscles);
                    exerciseInfo.setEquipment(equipment);

                    exerciseList.add(exerciseInfo);

                }
            }
        }
    }


    //--------------------------
    // ASYNCTASK(S)
    //--------------------------

    private class FetchMoreExercises extends AsyncTask<Void, Void, Boolean> {

        private String nextUrl;

        public FetchMoreExercises(String nextUrl) {
            this.nextUrl = nextUrl;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                fetchMoreExercises(nextUrl);
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                notifyDataSetChanged();
                if(!nextUrlGlobal.equals(Constants.nullString) && nextUrlGlobal != null) {
                    new FetchMoreExercises(nextUrlGlobal).execute();
                }
            }
            else {
                Toast.makeText(mContext, mContext.getString(R.string.error_unexpected_trying_again), Toast.LENGTH_SHORT).show();
                RETRY_COUNT++;
                if(RETRY_COUNT < ProfileActivity.RETRY_LIMIT) {
                    new FetchMoreExercises(this.nextUrl).execute();
                }
            }
        }
    }
}
